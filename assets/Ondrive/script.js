/**
 * Segmento Global. ============================================================================================
 *  ============================================================================================================
 * Contiene la declaración de las variables globales que serán utilizadas por los demás segmentos de código.
 */

var recordType = ""; //Almacena el tipo de registro consultado en Netsuite a través de la URL.
var recordId = ""; //Almacena el internalID del registro consultado en Netsuite a través de la URL.
var itemFolder = ""; //Contiene la carpeta "contenedora" para cada tipo de registro.
var recordFolder = ""; //Almacena el valor de la carpeta encontrada/creada al registro seleccionado.
var previewTab = ""; //Almacena el valor a mostrar en el Iframe del Preview.
var previewLink = ""; //Almacena la dirección del archivo a consultar en Preview

var repType = ""; //Clasifica el tipo de registro en transaccional o en expediente

var accountId = ""; //almacena el id de la cuenta que inició sesión en el navegador.
var accountRealm = ""; //almacena el realm de la cuenta que inició sesión en el navegador.
var accountEnvironment = ""; //almacena el env de la cuenta que inició sesión en el navegador.
var nombreArchivos = []; //Almacena los nombre de los archivos encontrados

var groupID = `46b0a57a-45ab-4534-8dde-cd4ba39e29b7`;

/**
 * Segmento Authorization =======================================================================================
 * ==============================================================================================================
 * Contiene las funciones necesarias para trabajar con la autenticación de Microsoft, y redirigir la autenticación al sitio deseado.
 */

//Constante: msalConfig: Contiene los datos necesarios para autenticarse en la aplicación.
//Id De Aplicación(cliente), Id. de directorio (inquilino), https://login.microsoftonline.com//<your directory ID here>
const msalConfig = {
    auth: {
        clientId: "5bdec38b-68b4-488a-a361-c9df7cd2fe3a",
        authority:
            "https://login.microsoftonline.com/52d206d1-13b5-4260-a65c-0b0cac860b1d",
        redirectUri:
            "",
        postLogoutRedirectUri:
            "",
    },
    system: {
        allowRedirectInIframe: true,
    },
};

//Constante: msalRequest, validar los permisos otorgados a la aplicación desde el usuario.
const msalRequest = { scopes: [] };

/**
 * Función ensureScope**
 * Encargada de validar los permisos solicitados por cada transacción del API.
 */
function ensureScope(scope) {
    if (
        !msalRequest.scopes.some((s) => s.toLowerCase() === scope.toLowerCase())
    ) {
        msalRequest.scopes.push(scope);
    }
}

//Constante: msalClient, crea una instancia de autenticación, usa como base los parámetros de autenticación de msalConfig
const msalClient = new msal.PublicClientApplication(msalConfig);

/**
 * Función asíncrona singIn().
 * Invoca la ventana de Autenticación. Almacena el token de autencicación en el navegador.
 */
async function signIn() {
    const authResult = await msalClient.loginPopup(msalRequest);
    //accountId = authResult.account.homeAccountId;
    //accountRealm = authResult.account.realm;
    //accountEnviroment = authResult.account.environment;
    sessionStorage.setItem("msalAccount", authResult.account.username);
}

/**
 * Función asíncrona getToken().
 * Consulta el Token de autenticación, y los permisos asignados al usuario.
 * En caso de que requiera un permiso nuevo, lo solicitará a través de una ventana emergente.
 * @return accessToken;
 */
async function getToken() {
    let account = sessionStorage.getItem("msalAccount");
    if (!account) {
        throw new Error(
            "User info cleared from session. Please sign out and sign in again."
        );
    }
    try {
        const silentRequest = {
            scopes: msalRequest.scopes,
            account: msalClient.getAccountByUsername(account),
        };
        const silentResult = await msalClient.acquireTokenSilent(silentRequest);
        return silentResult.accessToken;
    } catch (silentError) {
        if (silentError instanceof msal.InteractionRequiredAuthError) {
            const interactiveResult = await msalClient.acquireTokenPopup(
                msalRequest
            );
            return interactiveResult.accessToken;
        } else {
            console.log(silentError);
            throw silentError;
        }
    }
}

/**
 * Segmento drag&Drop ==========================================================================================
 * ==============================================================================================================
 * Contiene las funciones necesarias para interactuar con el HTML asociado a la carga de archivos.
 */

//Declaración de elementos asociados a la carga de archivos
const dropArea = document.querySelector(".files-upload"),
    dragText = dropArea.querySelector("header"),
    button = dropArea.querySelector("#buscar"),
    input = dropArea.querySelector("input");

//Ligar botón al input.
button.onclick = () => {
    input.click();
};

//Almacenar archivos del input en una variable.
let files;
input.addEventListener("change", function () {
    files = this.files;
    dropArea.classList.add("active");
});

//Animación del área de carga. |dropArea |
dropArea.addEventListener("dragover", (event) => {
    event.preventDefault();
    dropArea.classList.add("active");
    dragText.textContent = "Suelte sus archivos aquí";
});
dropArea.addEventListener("dragleave", () => {
    dropArea.classList.remove("active");
    dragText.textContent = "Arrastre sus archivos aquí";
});
dropArea.addEventListener("drop", (event) => {
    event.preventDefault();
    input.files = event.dataTransfer.files;
    console.log(input.files);
    fileSelected(input);
});

function inicializaDragArea() {
    dropArea.classList.remove("active");
    dragText.textContent = "Arrastre sus archivos aquí";
}

/**
 * Segmento GraphSDK ============================================================================================
 * ==============================================================================================================
 * Contiene las funciones necesarias para interactuar con el API de Microsoft Graph. (Carga, consulta y descarga.)
 */

//Constante: authProvider, valida que el token de autorización esté vigente.
const authProvider = {
    getAccessToken: async () => {
        return await getToken();
    },
};

//Constante: graphClient, crea una instancia del cliente autenticado.
const graphClient = MicrosoftGraph.Client.initWithMiddleware({ authProvider });

/**
 * Función asíncrona getUser().
 * Obtiene la información del cliente (usuario) autenticado.
 * @return graphClient;
 */
async function getUser() {
    ensureScope("user.read");
    try {
        return await graphClient.api("/me").select("id,displayName").get();
    } catch (error) {
        console.log(error);
        showError("getUser", error);
    }
}

/**
 * Función asíncrona getFiles().
 * Obtiene los archivos asociados al registro consultado.
 * @return files;
 */
async function getFiles(item) {
    ensureScope("files.read");
    try {
        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${item}/children`;
        } else {
            api_URL = `/groups/${groupID}/drive/items/${item}/children`;
        }
        const response = await graphClient
            .api(api_URL)
            .select("id,name,size,fileSystemInfo,folder,package")
            .get();
        return response.value;
    } catch (error) {
        console.log(error);
        showError("getFiles", error);
    }
}

/**
 * Función asíncrona downloadFile().
 * Genera un Url de descarga para el archivo seleccionado.
 */
async function downloadFile(file) {
    try {
        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${file.id}`;
        } else {
            api_URL = `/groups/${groupID}/drive/items/${file.id}`;
        }
        const response = await graphClient
            .api(api_URL)
            .select("@microsoft.graph.downloadUrl")
            .get();
        const downloadUrl = response["@microsoft.graph.downloadUrl"];
        window.open(downloadUrl, "_self");
    } catch (error) {
        console.log(error);
        showError("downloadFile", error);
    }
}

/**
 * Función asíncrona previewFile().
 * Genera un Url de vista previa para el archivo seleccionado.
 */
async function previewFile(file) {
    try {
        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${file.id}/preview`;
        } else {
            api_URL = `/groups/${groupID}/drive/items/${file.id}/preview`;
        }
        const response = await graphClient
            .api(api_URL)
            .select("@microsoft.graph.getUrl")
            .post();
        const contenedor = document.querySelector(".container");
        contenedor.style = "display: none";
        const previewContent = document.querySelector(".oneDrivePreview");
        previewContent.style = "display: block";
        previewLink = response["getUrl"];
        previewTab = window.open(previewLink, "filePreviewframe");
    } catch (error) {
        console.log(error);
        showError("previewFile", error);
    }
}
/**
 * Función expandPreview().
 * Genera un Url de vista previa para el archivo seleccionado en una ventana aparte.
 */
function expandPreview() {
    try {
        previewTab = window.open(previewLink, "_blank");
    } catch (error) {
        console.log(error);
        showError("expandPreview", error);
    }
}

/**
 * Función returnDisplay().
 * Vuelve a la pantalla donde se listan los archivos seleccionados.
 */
function returnDisplay() {
    try {
        const frame = document.querySelector(".filePreviewframe");
        frame.src = "about:blank";
        const previewContent = document.querySelector(".oneDrivePreview");
        previewContent.style = "display: none";
        previewLink = "";
        const contenedor = document.querySelector(".container");
        contenedor.style = "display: block";
    } catch (error) {
        console.log(error);
        showError("returnDisplay", error);
    }
}

/**
 * Función asíncrona newFolder().
 * Crea un nuevo folder en la carpeta asociada al tipo de registro.
 * @return response
 */
async function newFolder() {
    try {
        ensureScope("sites.readwrite.all");
        let options = {
            name: recordId,
            folder: {},
            "@microsoft.graph.conflictBehavior": "rename",
        };

        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${itemFolder}/children`;
        } else {
            api_URL = `/groups/${groupID}/drive/items/${itemFolder}/children`;
        }

        const response = await graphClient.api(api_URL).post(options);
        return response;
    } catch (error) {
        console.log(error);
        showError("newFolder", error);
    }
}

/**
 * Función asíncrona findFolder().
 * Realiza la búsqueda de una carpeta asociada al registro consultado.
 * @return response.value
 */
async function findFolder(item) {
    console.log(recordId);
    ensureScope("sites.read.all");
    try {
        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${item}/children`;
        } else {
            api_URL = `/groups/${groupID}/drive/items/${item}/children`;
        }
        const response = await graphClient
            .api(api_URL)
            .select("id,name,folder,package")
            .filter(`name eq '${recordId}'`)
            .get();
        console.log(response);
        return response.value;
    } catch (error) {
        if (error.message == "Access denied") {
            return false;
        } else {
            console.log(error);
            showError("findFolder", error);
        }
    }
}

/**
 * Función asíncrona uploadFile().
 * Realiza la carga de un archivo a la carpeta asociada a ese registro.
 * La carga se realiza con una sesión de carga y dividiendo el archivo en procesos pequeños.
 * @return uploadResult
 */
async function uploadFile(file, action) {
    try {
        var nombreArchivo = file.name.replace(/[\/\\:*?"<>|#%]/g, "");
        var api_URL = "";
        if (repType == "expediente") {
            api_URL = `https://graph.microsoft.com/v1.0/drives/b!IheXrZ1YqkaQnoxEtDTqVKci375XlX1AujLzBMwCMRW23DlOrDq0Tqo2xRgmkJ0b/items/${recordFolder}:/${nombreArchivo}://createuploadsession`;
        } else {
            api_URL = `https://graph.microsoft.com/v1.0/groups/${groupID}/drive/items/${recordFolder}:/${nombreArchivo}://createuploadsession`;
        }
        ensureScope("files.readwrite");
        const urlRequest = api_URL;
        const options = { rangeSize: 1024 * 1024 };
        const payload = {
            item: { "@microsoft.graph.conflictBehavior": action },
        };
        const fileObject = new MicrosoftGraph.FileUpload(
            file,
            nombreArchivo,
            file.size
        );
        const uploadSession =
            await new MicrosoftGraph.LargeFileUploadTask.createUploadSession(
                graphClient,
                urlRequest,
                payload
            );
        const task = new MicrosoftGraph.LargeFileUploadTask(
            graphClient,
            fileObject,
            uploadSession,
            options
        );
        const uploadResult = await task.upload();
        return uploadResult;
    } catch (error) {
        console.log(error);
        showError("uploadFile", error);
    }
}

/**
 * Segmento userInterface =======================================================================================
 * ==============================================================================================================
 * Contiene las funciones necesarias para interactuar con el HTML asociado al API. (Carga, consulta y descarga.)
 */

/**
 * Función asíncrona displayUI().
 * Maneja los cambios en el HTML asociados directamente al API después de autenticar el usuario.
 */
async function displayUI() {
    await signIn();
    const user = await getUser();

    var userName = document.getElementById("userName"); //Muestra el nombre de usuario.
    userName.innerText = user.displayName;
    var loginDiv = document.querySelector(".login"); //Oculta el login
    loginDiv.style = "display: none";
    var signInButton = document.getElementById("signin"); //Oculta el singin
    signInButton.style = "display: none";
    var content = document.getElementById("main-container"); //Muestra el contenedor principal
    content.style = "display: block";
    var newFolder = document.querySelector(".folder"); //Oculta el segmento del nuevo folder (Validar si es necesario.)
    newFolder.style = "display: none";
    var files = document.querySelector(".files"); //Oculta el segmento de archivos (Validar si es necesario.)
    files.style = "display: none";
    var empty = document.querySelector(".emptyFiles"); //Oculta el mensaje cuando no hay archivos.
    empty.style = "display: none";
    var accessD = document.querySelector(".accessdenied"); //Oculta el mensaje cuando no hay archivos.
    accessD.style = "display: none";
    var recordU = document.querySelector(".recordUnregistered"); //Oculta el mensaje cuando no hay archivos.
    recordU.style = "display: none";
    getUrl();
}

/**
 * Función asíncrona displayFiles().
 * Muestra los archivos encontrados en la carpeta asociada al registro. (Validar funcionamiento posterior al cambio)
 */
async function displayFiles() {
    try {
        inicializaDragArea();
        const ul = document.getElementById("downloadLinks");
        var empty = document.querySelector(".emptyFiles");
        const files = await getFiles(recordFolder);
        listaArchivos = files;
        if (files.length == 0) {
            empty.style = "display: block";
        } else {
            empty.style = "display: none";
            while (ul.firstChild) {
                ul.removeChild(ul.firstChild);
            }
            for (let file of files) {
                if (!file.folder && !file.package) {
                    nombreArchivos.push(file.name);
                    let div = document.createElement("div");
                    div.className = "fileInfo-details";
                    /*Crea el enlace de vista Previa. */
                    let a = document.createElement("a");
                    a.className = "fileInfo-link";
                    a.href = "javascript:void(0)";
                    a.onclick = (event) => {
                        event.preventDefault(); // Evita el comportamiento predeterminado del enlace
                        previewFile(file);
                    };
                    a.appendChild(document.createTextNode(file.name));
                    div.appendChild(a);
                    /*Crea label para fecha de carga */
                    let labelDate = document.createElement("label");
                    labelDate.className = "fileInfo-label";
                    labelDate.innerText =
                        file.fileSystemInfo.createdDateTime.substring(0, 10);
                    div.appendChild(labelDate);
                    /*Crea label para tamaño del archivo*/
                    let labelSize = document.createElement("label");
                    labelSize.className = "fileInfo-label";
                    let fsize = formatSize(file.size);
                    labelSize.innerText = fsize;
                    div.appendChild(labelSize);
                    let li = document.createElement("li");
                    li.appendChild(div);
                    ul.appendChild(li);
                    /*Crea el enlace para Descarga. */
                    let a2 = document.createElement("a");
                    a2.className = "fileInfo-linkDown";
                    a2.href = "#";
                    //a2.onclick = () => { previewFile(file); };
                    a2.onclick = () => {
                        downloadFile(file);
                    };
                    a2.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M144 480C64.5 480 0 415.5 0 336c0-62.8 40.2-116.2 96.2-135.9c-.1-2.7-.2-5.4-.2-8.1c0-88.4 71.6-160 160-160c59.3 0 111 32.2 138.7 80.2C409.9 102 428.3 96 448 96c53 0 96 43 96 96c0 12.2-2.3 23.8-6.4 34.6C596 238.4 640 290.1 640 352c0 70.7-57.3 128-128 128H144zm79-167l80 80c9.4 9.4 24.6 9.4 33.9 0l80-80c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-39 39V184c0-13.3-10.7-24-24-24s-24 10.7-24 24V318.1l-39-39c-9.4-9.4-24.6-9.4-33.9 0s-9.4 24.6 0 33.9z"/></svg>`;
                    //a.appendChild(document.createTextNode(file.name));
                    div.appendChild(a2);
                }
            }
        }
    } catch (error) {
        console.log(error);
        showError("displayFiles", error);
    }
}

/**
 * Función asíncrona fileSelected().
 * Muestra los archivos encontrados en la carpeta asociada al registro. (Validar funcionamiento posterior al cambio)
 */
async function fileSelected(e) {
    try {
        var reemplazar = "";
        var archivos = e.files;
        for (let i = 0; i < archivos.length; i++) {
            displayUploadMessage(`Cargando: ${archivos[i].name}.`, "begin");
            reemplazar = await validaRepetido(archivos[i].name);
            await uploadFile(e.files[i], reemplazar).then((response) => {
                displayUploadMessage(`Archivo cargado correctamente.`, "end");
                displayFiles();
            });
        }
    } catch (error) {
        console.log(error);
        showError("fileSelected", error);
    }
}

/**
 * Función asíncrona validaRepetido().
 * Valida los archivos repetidos en la carpeta seleccionada
 */

async function validaRepetido(nombre) {
    try {
        var accion = "";
        if (nombreArchivos.indexOf(nombre) != -1) {
            await Swal.fire({
                title: "Archivo Duplicado",
                text: "Este archivo ya se encuentra cargado. ¿Cómo desea proceder? ",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                confirmButtonText: "Reemplazarlo",
                cancelButtonColor: "#3085d6",
                cancelButtonText: "Conservar Ambos",
            }).then((result) => {
                if (result.isConfirmed) {
                    accion = "replace";
                } else {
                    accion = "rename";
                }
            });
        } else {
            accion = "rename";
        }
        return accion;
    } catch (error) {
        console.log(error);
        showError("validaRepetido", error);
    }
}

/**
 *  Función asíncrona createFolder();
 *  Se encarga de crear una carpeta asociada al registro y actualizar la vista.
 */
async function createFolder() {
    try {
        displayUploadMessage(`Creando carpeta: ${recordId}.`, "begin");
        const folder = document.querySelector(".folder");
        const files = document.querySelector(".files");
        await newFolder(itemFolder, recordId).then((response) => {
            displayUploadMessage(
                `Carpeta ${response.name} creada exitosamente.`,
                "end"
            );
            recordFolder = response.id;
            folder.style = "display: none";
            files.style = "display: grid";
            displayFiles();
        });
    } catch (error) {
        console.log(error);
        showError("createFolder", error);
    }
}

/**
 *  Función asíncrona searchFolder();
 *  Se encarga de buscar la carpeta asociada al registro y actualizar la vista.
 */
async function searchFolder() {
    try {
        displayUploadMessage(`Buscado carpeta: ${recordId}...`, "begin");
        const newFolder = document.querySelector(".folder");
        const files = document.querySelector(".files");
        const folders = await findFolder(itemFolder);
        if (!folders) {
            accesoDenegado();
        } else {
            if (folders == "") {
                displayUploadMessage(`Carpeta: ${recordId} no existe`, "end");
                newFolder.style = "display: flex";
            } else {
                for (let carp of folders) {
                    if (carp.folder) {
                        console.log(carp);
                        if (carp.name == recordId) {
                            displayUploadMessage(
                                `Gestión de Archivos: Registro: ${recordId}.`,
                                "end"
                            );
                            newFolder.style = "display: none";
                            files.style = "display: grid";
                            recordFolder = carp.id;
                            displayFiles();
                            {
                                break;
                            }
                        } else {
                            displayUploadMessage(
                                `Carpeta: ${recordId} no existe`,
                                "end"
                            );
                            newFolder.style = "display: flex";
                        }
                    }
                }
            }
        }
    } catch (error) {
        console.log(error);
        showError("searchFolder", error);
    }
}

/**
 *  Función displayUploadMessage();
 *  Se encarga de buscar mostrar un mensaje asociado a las interacciones del usuario.
 */
function displayUploadMessage(message, clase) {
    try {
        const messageElement = document.getElementById("uploadMessage");
       // const loadElement = document.querySelector(".load");
        if (clase == "begin") {
            //messageElement.classList.remove("end")
            messageElement.classList.add("begin");
            messageElement.innerText = message;
            //$("#uploadMessage").lettering();

            for (let i = 0; i < messageElement.childNodes.length; i++) {
                let texto = `-webkit-animation: foo 4s ease infinite, bar 5s linear infinite; -webkit-animation-delay: ${i / 10
                    }s;`;
                messageElement.childNodes[i].style = texto;
            }
        } else {
            messageElement.classList.remove("begin");
            messageElement.innerText = message;
        }
    } catch (error) {
        console.log(error);
        showError("displayUploadMessage", error);
    }
}

/**
 * Segmento urlIdentifier. =====================================================================================
 *  ============================================================================================================
 * Contiene la declaración de las variables globales que serán utilizadas por los demás segmentos de código.
 */

/**
 *  Función identifyItem();
 *  Se encarga de asociar el ID contenedor de cada tipo de registro.
 */
function identifyItem(tipo) {
    try {
        let itemId = "";
        switch (tipo) {
            case "inventoryadjustment":
                itemId = "01QPXOKIHLJCI3E2FUXNAKIKMBUJQCG5HJ";
                break;
            case "depositapplication":
                itemId = "01QPXOKIA732242URXYVAKA6XIUYEYXHMK";
                break;
            case "vendorreturnauthorization":
                itemId = "01QPXOKIH27IWELF333BAJYAND46BEWO5N";
                break;
            case "returnauthorization":
                itemId = "01QPXOKIEKVPWVHFRBCZAYZ5ENLGK4KPPW";
                break;
            case "expensecategory":
                itemId = "01QPXOKIE63DJHDXWVSJB2H3ZYMWSH7CCC";
                break;
            case "check":
                itemId = "01QPXOKIBO6X6OSUXN5NB2XXQ335XZQYF4";
                break;
            case "classification":
                itemId = "01QPXOKIH2I3KTPPDWINHI747XE7X7H5CI";
                break;
            case "customer":
                itemId = "01QPXOKICVOQPUSDN67VH3WHUUBQOWY7KQ";
                break;
            case "contact":
                itemId = "01QPXOKICA6IGM74IWD5ALCTNJ4WFHMWRC";
                break;
            case "purchasecontract":
                itemId = "01QPXOKIFRIU5HWFUK2RFK7RBCCB75CMCA";
                break;
            case "vendorcredit":
                itemId = "01QPXOKIHOPD6V2E6FRJGKJ6YGN3TTFZZD";
                break;
            case "account":
                itemId = "01QPXOKIGTS4LUGUJKZJDY2PMYYXECDLNE";
                break;
            case "department":
                itemId = "01QPXOKIC5GTUHMY34IVHYYVCMUK3ZTGAC";
                break;
            case "deposit":
                itemId = "01QPXOKIESVZXCI2T36VFIPLB5VWWYPRM7";
                break;
            case "customerdeposit":
                itemId = "01QPXOKIDLLQ6VP5BIEBHJNK5P33LRXSMN";
                break;
            case "journalentry":
                itemId = "01QPXOKIEOL3LODGCIOBBJTLLCM3VJYI7P";
                break;
            case "statisticaljournalentry":
                itemId = "01QPXOKIG4BZ6TR2MYEJHZS7CTQJ32FBXY";
                break;
            case "employee":
                itemId = "01QPXOKIF2N6CBHGRW2JDYKXNHDD4XSQZG";
                break;
            case "estimate":
                itemId = "01QPXOKIHH7IRQYQHQGVBZSKEW5QFQ3OQ6";
                break;
            case "calendarevent":
                itemId = "01QPXOKIAENRYX7N2P7RH2D3GALTNRX4WJ";
                break;
            case "vendorbill":
                itemId = "01QPXOKIBTMFMILUHOFRB32C7A5EDWQQBK";
                break;
            case "invoice":
                itemId = "01QPXOKIGCUVFROBIAQRDKZHSF4FKRVPXX";
                break;
            case "expensereport":
                itemId = "01QPXOKIBE65KE4BC6I5AZZFZNLBCPSYME";
                break;
            case "phonecall":
                itemId = "01QPXOKIBFSEY6AL6VSRFJK5QRWLZWWYJW";
                break;
            case "currency":
                itemId = "01QPXOKIFZBLAX2ZY4RZG2EU5IQJ3CQDKY";
                break;
            case "creditmemo":
                itemId = "01QPXOKICGZ5DZ5IDQEFBJME2JHH2US7LS";
                break;
            case "opportunity":
                itemId = "01QPXOKIFLAOVGHLVQNVBI5MM3RSEQKU4C";
                break;
            case "transferorder":
                itemId = "01QPXOKIFJYQ3VZVLVW5AYHIGQPVB4OWHG";
                break;
            case "salesorder":
                itemId = "01QPXOKIF43B3PZFOW4VFIPCME4QKXURLE";
                break;
            case "customerpayment":
                itemId = "01QPXOKIHKFU3JM3ZGXRDJQ2SWMGGUH3V5";
                break;
            case "custompurchase109":
                itemId = "01QPXOKIGMUGEWY5IWF5DIX4FUWXX6Q4CF";
                break;
            case "vendorpayment":
                itemId = "01QPXOKIAMMS32WWEAGJGYIAQD4NUSMMY6";
                break;
            case "purchaseorder":
                itemId = "01QPXOKIBZSTMKWLEAZFBZJIQRKWMGX6WL";
                break;
            case "blanketpurchaseorder":
                itemId = "01QPXOKIAPDDLFWZBS6ZBJTGXK6TEJXRGD";
                break;
            case "taxperiod":
                itemId = "01QPXOKIF6HDJYTXK2GJAI7YOSD5EKGCWJ";
                break;
            case "accountingperiod":
                itemId = "01QPXOKIHKKCGATAQ2WFGKBILM2PVMFJML";
                break;
            case "vendor":
                itemId = "01QPXOKIFJX5CLVUT7EZC2CH3TSYFGWU46";
                break;
            case "job":
                itemId = "01QPXOKIED3PKBGB6XHVAKZQLWYTN5XL6L";
                break;
            case "itemreceipt":
                itemId = "01QPXOKIAFGZW7DTRXLNFKLQUR4MDD2NME";
                break;
            case "inventorycount":
                itemId = "01QPXOKIAA57UWZ5VXNJCJQ5M3GHZ6HG5G";
                break;
            case "customerrefund":
                itemId = "01QPXOKIE7JT6PAX7GKJG2EER7KHJ2RYHQ";
                break;
            case "cashrefund":
                itemId = "01QPXOKIHETXIHEHEPSNBLNSLIYW53JPK4";
                break;
            case "inventorycostrevaluation":
                itemId = "01QPXOKIE6FT2PVTR5DBH35QS27A6H6SCR";
                break;
            case "partner":
                itemId = "01QPXOKIBNWSR3KXPJSVH2OO2EJGCJCCZ4";
                break;
            case "purchaserequisition":
                itemId = "01QPXOKID232NUU3JJGJCJ7KJFXORO63IN";
                break;
            case "requestforquote":
                itemId = "01QPXOKIBTACEIMO36JVHYKKVPN5XGRHA2";
                break;
            case "solution":
                itemId = "01QPXOKIBHD3OBWK5O35CKPPMKV2E74AO3";
                break;
            case "subsidiary":
                itemId = "01QPXOKIFBFZGOGB6XONFYTSSJCIHXWP3B";
                break;
            case "task":
                itemId = "01QPXOKIAJ6PE3GKAJ45D2QCCRXXRNEZCD";
                break;
            case "projecttask":
                itemId = "01QPXOKIDZXL6HOSCKCBC2DPQCB73JR5GN";
                break;
            case "creditcardcharge":
                itemId = "01QPXOKIEYDVB4ZIZ67VAZU3USZPBN4WFP";
                break;
            case "supportcase":
                itemId = "01QPXOKIF3ELMNMMQYSFGZIBAKWLXIMAAV";
                break;
            case "transfer":
                itemId = "01QPXOKIFFNS6PGXAWZND37O5Q6CMC3VIY";
                break;
            case "inventorytransfer":
                itemId = "01QPXOKIBLNH4QS4WYBRG34CZZHSHIKPZ4";
                break;
            case "location":
                itemId = "01QPXOKID74TSFMQ3BUJH2XQLBRLI3VTG7";
                break;
            case "cashsale":
                itemId = "01QPXOKIBOIBTZRRFNLVE26X6DSQMHUYOZ";
                break;
            //Faltan las unidades de expediente.
            //Se agregan las cuotas residenciales.
            case "customrecord_cuota_residencial":
                itemId = "01QPXOKIFRR5XWUUAUJNBI44HB4HMR5JXK";
                break;
            default:
                itemId = "Default";
                break;
        }
        return itemId;
    } catch (error) {
        console.log(error);
        showError("identifyItem", error);
    }
}

/**
 *  Función identifyRep();
 *  Se encarga de identificar si el registro es transaccional o de expediente
 */
function identifyRep(tipo) {
    try {
        let rtype = "";
        if (tipo == "uniExpediente" || tipo == "uniExpComercial") {
            rtype = "expediente";
        } else {
            rtype = "transaccion";
        }
        return rtype;
    } catch (error) {
        console.log(error);
        showError("identifyRep", error);
    }
}

/**
 *  Función getUrl();
 *  Se encarga de obtener el tipo de registro y el ID desde la URL.
 */
function getUrl() {
    try {
        // let url =
        //     window.location != window.parent.location
        //         ? document.referrer
        //         : document.location.href;

        const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');

        recordType = "salesorder";
        recordId = dataValue;
        itemFolder = identifyItem(recordType);
        repType = identifyRep(recordType);
        if (itemFolder == "Default") {
            recordUnreg();
        } else {
            searchFolder();
        }
    } catch (error) {
        console.log(error);
        showError("getUrl", error);
    }
}

/**
 *  Función getType();
 *  Obtiene el tipo de registro de la url y lo identifica.
 */
function getType(myurl) {
    try {
        let type = "";
        if (myurl.indexOf("invadjst") != -1) {
            type = "inventoryadjustment";
        }
        if (myurl.indexOf("depappl") != -1) {
            type = "depositapplication";
        }
        if (myurl.indexOf("vendauth") != -1) {
            type = "vendorreturnauthorization";
        }
        if (myurl.indexOf("rtnauth") != -1) {
            type = "returnauthorization";
        }
        if (myurl.indexOf("expcategory.nl") != -1) {
            type = "expensecategory";
        }
        if (myurl.indexOf("check") != -1) {
            type = "check";
        }
        if (myurl.indexOf("classtype.nl") != -1) {
            type = "classification";
        }
        if (myurl.indexOf("custjob.nl") != -1) {
            type = "customer";
        }
        if (myurl.indexOf("contact.nl") != -1) {
            type = "contact";
        }
        if (myurl.indexOf("purchcon") != -1) {
            type = "purchasecontract";
        }
        if (myurl.indexOf("vendcred") != -1) {
            type = "vendorcredit";
        }
        if (myurl.indexOf("account.nl") != -1) {
            type = "account";
        }
        if (myurl.indexOf("departmenttype.nl") != -1) {
            type = "department";
        }
        if (myurl.indexOf("deposit") != -1) {
            type = "deposit";
        }
        if (myurl.indexOf("custdep") != -1) {
            type = "customerdeposit";
        }
        if (myurl.indexOf("journal") != -1) {
            type = "journalentry";
        }
        if (myurl.indexOf("statisticaljournal") != -1) {
            type = "statisticaljournalentry";
        }
        if (myurl.indexOf("employee.nl") != -1) {
            type = "employee";
        }
        if (myurl.indexOf("estimate") != -1) {
            type = "estimate";
        }
        if (myurl.indexOf("event") != -1) {
            type = "calendarevent";
        }
        if (myurl.indexOf("vendbill") != -1) {
            type = "vendorbill";
        }
        if (myurl.indexOf("custinvc") != -1) {
            type = "invoice";
        }
        if (myurl.indexOf("exprept") != -1) {
            type = "expensereport";
        }
        if (myurl.indexOf("call") != -1) {
            type = "phonecall";
        }
        if (myurl.indexOf("currency.nl") != -1) {
            type = "currency";
        }
        if (myurl.indexOf("custcred") != -1) {
            type = "creditmemo";
        }
        if (myurl.indexOf("opprtnty") != -1) {
            type = "opportunity";
        }
        if (myurl.indexOf("trnfrord") != -1) {
            type = "transferorder";
        }
        if (myurl.indexOf("salesord") != -1) {
            type = "salesorder";
        }
        if (myurl.indexOf("custpymt") != -1) {
            type = "customerpayment";
        }
        if (myurl.indexOf("cutrprch") != -1) {
            type = "custompurchase109";
        }
        if (myurl.indexOf("vendpymt") != -1) {
            type = "vendorpayment";
        }
        if (myurl.indexOf("purchord") != -1) {
            type = "purchaseorder";
        }
        if (myurl.indexOf("blankord") != -1) {
            type = "blanketpurchaseorder";
        }
        if (myurl.indexOf("taxperiod.nl") != -1) {
            type = "taxperiod";
        }
        if (myurl.indexOf("fiscalperiod.nl") != -1) {
            type = "accountingperiod";
        }
        if (myurl.indexOf("vendor.nl") != -1) {
            type = "vendor";
        }
        if (myurl.indexOf("project.nl") != -1) {
            type = "job";
        }
        if (myurl.indexOf("itemrcpt") != -1) {
            type = "itemreceipt";
        }
        if (myurl.indexOf("invcount") != -1) {
            type = "inventorycount";
        }
        if (myurl.indexOf("custrfnd") != -1) {
            type = "customerrefund";
        }
        if (myurl.indexOf("cashrfnd") != -1) {
            type = "cashrefund";
        }
        if (myurl.indexOf("invreval") != -1) {
            type = "inventorycostrevaluation";
        }
        if (myurl.indexOf("partner.nl") != -1) {
            type = "partner";
        }
        if (myurl.indexOf("purchreq") != -1) {
            type = "purchaserequisition";
        }
        if (myurl.indexOf("rfq") != -1) {
            type = "requestforquote";
        }
        if (myurl.indexOf("solution.nl") != -1) {
            type = "solution";
        }
        if (myurl.indexOf("subsidiarytype.nl") != -1) {
            type = "subsidiary";
        }
        if (myurl.indexOf("task") != -1) {
            type = "task";
        }
        if (myurl.indexOf("projecttask") != -1) {
            type = "projecttask";
        }
        if (myurl.indexOf("cardchrg") != -1) {
            type = "creditcardcharge";
        }
        if (myurl.indexOf("supportcase.nl") != -1) {
            type = "supportcase";
        }
        if (myurl.indexOf("transfer") != -1) {
            type = "transfer";
        }
        if (myurl.indexOf("invadjst.nl") != -1) {
            type = "inventorytransfer";
        }
        if (myurl.indexOf("locationtype.nl") != -1) {
            type = "location";
        }
        if (myurl.indexOf("cashsale") != -1) {
            type = "cashsale";
        }
        //Se agregan las cuotas residenciales.
        if (myurl.indexOf("rectype=702") != -1) {
            type = "customrecord_cuota_residencial";
        }
        return type;
    } catch (error) {
        console.log(error);
        showError("getType", error);
    }
}

/**
 *  Función getId();
 *  Obtiene el id del registro de la url y lo retorna.
 */
function getId() {
    try {
        let params = window.parent.location.search;
        let urlSearch = new URLSearchParams(params);
        let id = urlSearch.get("id");
        return id;
    } catch (error) {
        console.log(error);
        showError("getId", error);
    }
}

/**
 *  Función formatSize();
 *  Formatea el número a mostrar con el tamaño del archivo.
 */

function formatSize(size) {
    try {
        let fsize = 0;
        if (size >= 1000000000) {
            fsize = `${Math.round((size / 1000000000) * 100) / 100} GB`;
        }
        if (size >= 1000000) {
            fsize = `${Math.round((size / 1000000) * 100) / 100} MB`;
        } else if (size >= 1000) {
            fsize = `${Math.round((size / 1000) * 100) / 100} KB`;
        } else {
            fsize = `${Math.round(size * 100) / 100} B`;
        }
        return fsize;
    } catch (error) {
        console.log(error);
        showError("formatSize", error);
    }
}

/**
 *  Función accesoDenegado();
 *  Muestra el banner de acceso denegado cuando no se cumplen los permisos.
 */

function accesoDenegado() {
    try {
        var accessD = document.querySelector(".accessdenied"); //Oculta el mensaje cuando no hay archivos.
        accessD.style = "display: block";
        displayUploadMessage("Acceso Denegado", "end");
    } catch (error) {
        console.log(error);
        showError("accesoDenegado", error);
    }
}

/**
 *  Función recordUnreg();
 *  Muestra el banner de registro no asociado.
 */

function recordUnreg() {
    try {
        var recordU = document.querySelector(".recordUnregistered"); //Oculta el mensaje cuando no hay archivos.
        recordU.style = "display: block";
        displayUploadMessage("Registro no asociado", "end");
    } catch (error) {
        console.log(error);
        showError("recordUnreg", error);
    }
}

/**
 *  Función reloadContent();
 *  Recarga el contenido del sitio al contar con un usuario sin permisos.
 */

function reloadContent() {
    try {
        var accessD = document.querySelector(".accessdenied"); //Oculta el mensaje cuando no hay archivos.
        accessD.style = "display: none";
        var recordU = document.querySelector(".recordUnregistered"); //Oculta el mensaje cuando no hay archivos.
        recordU.style = "display: none";
        var loginDiv = document.querySelector(".login"); //Muestra el login
        loginDiv.style = "display: flex";
        var signInButton = document.getElementById("signin"); //Oculta el singin
        signInButton.style = "display: inherit";
        displayUploadMessage("", "end");
        var messageElement = document.getElementById("uploadMessage");
        messageElement.style = "display: none";
    } catch (error) {
        console.log(error);
        showError("reloadContent", error);
    }
}

/**
 * Segmento gestionaErrores. =====================================================================================
 *  ============================================================================================================
 * Se encarga de interactuar con el usuario cuando se presentan errores y mostrar información útil.
 */

/**
 *  Función showError();
 *  Muestra la información del error que se produce.
 */
function showError(fNombre, fError) {
    Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
            "Se presentó un error en la función: " +
            fNombre +
            " | Detalle: " +
            fError +
            ", Favor reportar reportar este error a soporte.",
        footer: "OneDrive for Netsuite",
    });
}
