<?php
/*================================================================
1. Configurar el entorno de desarrollo dependiendo del dominio
=================================================================*/

// Obtener la URL del servidor actual
$url = $_SERVER["HTTP_HOST"];
$host_name_bd = 'localhost';
$usu_name_bd = 'root';

/*================================================================
Define las configuraciones para cada dominio en un array asociativo
Los elementos del array contienen las configuraciones específicas para cada dominio
=================================================================*/
$domain_configurations = [
    'crm.roccacr.com' => [
        'dominio' => 'https://crm.roccacr.com/',
        'titulo' => 'CRM ROCCA',
        'curlopt_url_php' => 'https://api-php.roccacr.com/',
        'curlopt_url_nodejs' => 'https://api-crm.roccacr.com/',
        'bd_name_bd' => 'crmdatabase-api',
        'clave_name_bd' => '!Roccacrm',
    ],'crm-local.com' => [
        'dominio' => 'http://crm-local.com/',
        'titulo' => 'LOCAL CRM',
        'curlopt_url_php' => 'https://api-php.roccacr.com/',
         'bd_name_bd' => 'crmdatabase-api',
        'clave_name_bd' => '',
    ],'crmtest.roccacr.com' => [
        'dominio' => 'https://crmtest.roccacr.com/',
        'titulo' => 'SAMBOX',
        'curlopt_url_php' => 'https://api-php.roccacr.com/',
        'bd_name_bd' => 'crmdatabase-api2',
        'clave_name_bd' => '!Roccacrm',
   ]
    // Puedes agregar más configuraciones para otros dominios si es necesario
];

$current_domain_config = $domain_configurations[$url] ?? [];

/*================================================================
Utilizar las configuraciones seleccionadas
=================================================================*/

/*================================================================
La URL principal del dominio
=================================================================*/
$dominio = $current_domain_config['dominio'] ?? '';

/*================================================================
El título asociado al dominio
=================================================================*/
$titulo = $current_domain_config['titulo'] ?? '';

/*================================================================
La URL del servicio PHP asociado al dominio
=================================================================*/
$curlopt_url_php = $current_domain_config['curlopt_url_php'] ?? '';

/*================================================================
La URL del servicio Node.js asociado al dominio
=================================================================*/
$curlopt_url_nodejs = $current_domain_config['curlopt_url_nodejs'] ?? '';

/*================================================================
El nombre de la base de datos asociado al dominio
=================================================================*/
$bd_name_bd = $current_domain_config['bd_name_bd'] ?? '';

/*================================================================
La clave de la base de datos asociada al dominio
=================================================================*/
$clave_name_bd = $current_domain_config['clave_name_bd'] ?? '';

/*================================================================
Fin del código
=================================================================*/