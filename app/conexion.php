<?php
/*===========================================================================
1. Configurar el entorno de desarrollo dependiendo del dominio
===========================================================================*/

/*===========================================================================
Requiere el archivo de configuración del sistema
===========================================================================*/
require_once "./app/systemConfig.php";

/*===========================================================================
Obtiene las variables de configuración del sistema y las asigna a nuevas variables
===========================================================================*/
$HOSTNAME_BD = "$host_name_bd";
$USERNAME_BD = "$usu_name_bd";
$PASSWORD_BD = "$clave_name_bd";
$NAME_BD     = "$bd_name_bd";


/*===========================================================================
Define constantes para la conexión a la base de datos
===========================================================================*/
define('DB_SERVER', $HOSTNAME_BD);
define('DB_USER', $USERNAME_BD);
define('DB_PASS', $PASSWORD_BD);
define('DB_NAME', $NAME_BD);

/*===========================================================================
Establece la conexión a la base de datos utilizando MySQLi
===========================================================================*/
$con = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

/*===========================================================================
Verifica si la conexión a MySQL fue exitosa
===========================================================================*/
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

/*===========================================================================
Define variables para la conexión PDO a la base de datos
===========================================================================*/
$db_host = $HOSTNAME_BD;
$db_user = $USERNAME_BD;
$db_password = $PASSWORD_BD;
$db_name = $NAME_BD;

/*===========================================================================
Intenta establecer una conexión PDO a la base de datos
===========================================================================*/
try {
    $db = new PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_password);
    /*===========================================================================
    Configura el modo de manejo de errores de PDO para lanzar excepciones en caso de errores
    ===========================================================================*/
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    /*===========================================================================
    En caso de un error de conexión, captura el mensaje de error
    ===========================================================================*/
    $e->getMessage();

}

/*===========================================================================
Fin del código
===========================================================================*/
