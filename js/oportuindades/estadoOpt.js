const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};
// Obtener referencia al checkbox
var checkbox = document.getElementById("square-switch3");

// Asignar evento change al checkbox
checkbox.addEventListener("change", function () {
    // Mostrar una alerta dependiendo del estado del checkbox
    if (this.checked) {
        estadoOpt(1);
    } else {
        estadoOpt(0);
    }
});
const estadoOpt = async (value) => {
    try {
        const data = `chek_oport="${value}", chek2_oport=1`;
        const table = "oportunidades"
        const query = `WHERE id_oportunidad_oport ="${dataParam2}";`;
        const result = await fetchRecords(data, table, query, "update", "", 2);
        if (result.statusCode == 200) {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Estado de oportuindad cambiado con exito',
                showConfirmButton: false,
                timer: 1500
            })
        } else {
            mostrarAlerta('No se pudo cambiar el Estado de oportuindad ', true);
        }
    } catch (error) {
        console.error(error);
    }
};

