/*ASIGNACIONES DE CAMPOS AL CARGAR LA VISTA*/
/*custbody60 -> PRIMA%*/
var custbody60 = document.getElementById('custbody60')
var custbody39 = document.getElementById('custbody39')
Number($("#custbody60").val(0.15));


/* ***************************************************************************************************** */
/* Obtener el valor de los botones de opción. */
var discounted = document.getElementById('isDiscounted');
var no_discounted = document.getElementById('isNotDiscounted')

/**=====================================================================================================
  Si la casilla de verificación está marcada, deshabilite el primer campo y habilite el segundo campo.
  Si la casilla de verificación no está marcada, habilite el primer campo y deshabilite el segundo campo.
 =====================================================================================================*/
function updateStatus() {
    if (discounted.checked) {
        custbody39.disabled = true;
        custbody60.disabled = false;

        //reseteamos el campo si seleciona monto oporcentaje
        Number($("#custbody60").val(0.15));
        // var lista = Number(Number(document.getElementById("custbody13").value)); /* variable global %*/
        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value)); /* variable global %*/
        var prima = Number(document.getElementById("custbody60").value);
        var res = (lista * prima).toFixed(5);
        Number(document.getElementById("custbody39").value = (res))
    } else {
        custbody39.disabled = false;
        custbody60.disabled = true;

        //reseteamos el campo si seleciona monto oporcentaje
        Number($("#custbody60").val(0.15));
        // var lista = Number(Number(document.getElementById("custbody13").value));
        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value));
        var prima = Number(document.getElementById("custbody60").value);
        var res = (lista * prima).toFixed(5);
        Number(document.getElementById("custbody39").value = (res))
    }
}
discounted.addEventListener('change', updateStatus)
no_discounted.addEventListener('change', updateStatus)














/**=====================================================================================================
  FUNCION QUE SE ENCARGA DE CALCULAR TODOS LOS MONTOS Y REASIGNAR MONTOS PARA SU CALCULO
 =====================================================================================================*/
function calcular(input) {
    var valor = input.id;

    function calcularValor(func, field) {
        func(valor, field);
        PRIMA_NETA(valor, field);
        MONTO_PRIMA_ASIGNABLE_NETA(valor, field);
        HITOS_MONTO_SIN_PRIMA_TOTAL(valor, field);
    }

    /*MONTO DESCUENTO DIRECTO*/
    if (valor == "custbody132") {
        var valor = input.value.trim();
        // Validar si hay letras
        if (/[a-zA-Z]/.test(valor)) {
            alert("Por favor, asegúrate de que el campo sea un número negativo  y no contenga letras.");
            input.value = "";
            return;
        }
        // Validar si hay más de un guion
        if (valor.indexOf("-") !== valor.lastIndexOf("-")) {
            alert("Solo se permite un guion al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        // Validar si el guion no está al inicio
        if (valor.indexOf("-") !== 0 && valor.length > 0) {
            alert("El guion solo está permitido al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody132");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody132");
        MONTO_TOTAL(valor, "custbody132");

        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*EXTRAS PAGADAS POR EL CLIENTE */
    if (valor == "custbody46") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody46");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody46");
        MONTO_TOTAL(valor, "custbody46");
        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*CASHBACK*/
    if (valor == "custbodyix_salesorder_cashback") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbodyix_salesorder_cashback");
        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*MONTO RESERVA*/
    if (valor == "custbody52") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody52");
        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*MONTO TOTAL DE CORTESÍAS*/
    if (valor == "custbody16") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody16");
        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*EXTRAS SOBRE EL PRECIO DE LISTA*/
    if (valor == "diferecia") {
        calcularValor(PRECIO_DE_VENTA_NETO, "diferecia");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "diferecia");
        MONTO_TOTAL(valor, "diferecia");
        var custbody60 = Number(Number(document.getElementById("custbody60").value))
        PORCENTAJE_VALUE(custbody60, "custbody60");
    }

    /*PRIMA TOTAL*/
    if (valor === "custbody39") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody39");
        PRIMA_TOTAL_VALUE(input.value, "custbody39");

    }

    if (valor == "custbody60") {
        PORCENTAJE_VALUE(input.value, "custbody60");
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody60");

    }



    if (valor == "custbody179" || valor == "custbody180" || valor == "custbody181" || valor == "custbody182" || valor == "custbody183" || valor == "custbody184" || valor == "o_2_uno_input" || valor == "custbody184_uno" || valor ==
        "o_2_dos_input" || valor == "custbody184_dos" || valor == "o_2_tres_input" || valor == "custbody184_tres") {
        MONTO_PRIMA_ASIGNABLE_NETA(input, valor);

        let valorCam = Number($("#avance_diferenciado_hito17").val().replace(',', '.'));

        let valorCam1 = Number($("#avance_diferenciado_hito11").val().replace(',', '.'));


        $("#avance_diferenciado_hito1").val((valorCam1 * valorCam).toFixed(0).replace('.', ','));
        let valorCam2 = Number($("#avance_diferenciado_hito12").val().replace(',', '.'));
        $("#avance_diferenciado_hito2").val((valorCam2 * valorCam).toFixed(0).replace('.', ','));

        let valorCam3 = Number($("#avance_diferenciado_hito13").val().replace(',', '.'));
        $("#avance_diferenciado_hito3").val((valorCam3 * valorCam).toFixed(0).replace('.', ','));

        let valorCam4 = Number($("#avance_diferenciado_hito14").val().replace(',', '.'));
        $("#avance_diferenciado_hito4").val((valorCam4 * valorCam).toFixed(0).replace('.', ','));

        let valorCam5 = Number($("#avance_diferenciado_hito15").val().replace(',', '.'));
        $("#avance_diferenciado_hito5").val((valorCam5 * valorCam).toFixed(0).replace('.', ','));

        let valorCam6 = Number($("#avance_diferenciado_hito16").val().replace(',', '.'));
        $("#avance_diferenciado_hito6").val((valorCam6 * valorCam).toFixed(0).replace('.', ','));
    }



    if (valor == "avance_diferenciado_hito11" || valor == "avance_diferenciado_hito12" || valor == "avance_diferenciado_hito13" || valor == "avance_diferenciado_hito14" || valor == "avance_diferenciado_hito15" || valor ==
        "avance_diferenciado_hito16") {
        
        
        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);



        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        // var suma_hitos = ( avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16 ).toFixed( 2 );
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);

        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());

            /* Multiplicando el valor de `mspt` por el valor de `avance_diferenciado_hito11` y luego
            redondeando el resultado a 2 decimales. */
            var monto_hito = (mspt * input.value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            /* Poniendo el valor de la entrada con id `avance_diferenciado_hito1` al valor de `monto_hito`. */

            if (valor == "avance_diferenciado_hito11") {
                Number(document.getElementById("avance_diferenciado_hito1").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito12") {
                Number(document.getElementById("avance_diferenciado_hito2").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito13") {
                Number(document.getElementById("avance_diferenciado_hito3").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito14") {
                Number(document.getElementById("avance_diferenciado_hito4").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito15") {
                Number(document.getElementById("avance_diferenciado_hito5").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito16") {
                Number(document.getElementById("avance_diferenciado_hito6").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'El Porcentaje total del monto sin prima total, debe estar entre 0.00 y 0.100 , o bien verificar si la suma de los hitos no supera el 100%',
                footer: ''
            })

        }






    }

    if (valor == "avance_diferenciado_hito1" || valor == "avance_diferenciado_hito2" || valor == "avance_diferenciado_hito3" || valor == "avance_diferenciado_hito4" || valor == "avance_diferenciado_hito5" || valor ==
        "avance_diferenciado_hito6") {
        


        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);




        let numero = parseFloat(input.value);
        let valorCam = Number($("#avance_diferenciado_hito17").val());
        if (valor == "avance_diferenciado_hito1") {
            $("#avance_diferenciado_hito11").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito2") {
            $("#avance_diferenciado_hito12").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito3") {
            $("#avance_diferenciado_hito13").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito4") {
            $("#avance_diferenciado_hito14").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito5") {
            $("#avance_diferenciado_hito15").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito6") {
            $("#avance_diferenciado_hito16").val((numero * 1000 / valorCam).toFixed(5));
        }

        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);



        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());
            var monto_hito = (mspt * avance_diferenciado_hito11).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'Asegúrate de que el valor del hito  esté correctamente formateado con comas para separar los miles. Además, verifica que la suma de todos los hitos no supere el 100% y que el valor del campo PORCENTAJE YA ASIGNADO sea menor a 1.00',
                footer: ''
            })

        }


    }
}



function PRIMA_TOTAL_VALUE(input, valor) {
    /*CALCULAMOS EL PORCENTAJE */
    var i = Number(Number(document.getElementById("custbody39").value));
    var a = Number(document.getElementById("custbody_ix_total_amount").value);
    rest = i / a;

    Number(document.getElementById("custbody60").value = (rest).toFixed(5));

}

function PORCENTAJE_VALUE(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_total_porcentaje = (monto_total * input).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_porcentaje));

}

/* CREAMOS LAS FUNCIONES QUE NOS VAN A CALCULAR LOS CAMPOS DEPENDIENDO DE CUAL EDITAMOS */
function PRECIO_DE_VENTA_NETO(input, valor) {


    /*CALCULAMOS EL PRECIO DE VENTA NETO */
    var precio_de_lista = Number(Number(document.getElementById("custbody13").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var extrasPagadasPorelcliente = Number(Number(document.getElementById("custbody46").value));
    var cashback = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var monto_de_cortecias = Number(Number(document.getElementById("custbody16").value));
    var monto_total_precio_venta_neto = (precio_de_lista - Math.abs(descuento_directo) + extrasPagadasPorelcliente - cashback - monto_de_cortecias).toFixed(3);
    Number(document.getElementById("pvneto").value = (monto_total_precio_venta_neto));

}


function EXTRAS_SOBRE_PRECIO_LISTA(input, valor) {


    /*EXTRAS SOBRE EL PRECIO DE LISTA */
    var extra_sobre_precio_lista = Number(Number(document.getElementById("custbody46").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var total_espl = (extra_sobre_precio_lista + descuento_directo).toFixed(3);
    Number(document.getElementById("diferecia").value = (total_espl));

}


function PRIMA_TOTAL(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_porcentaje = Number(Number(document.getElementById("custbody60").value));
    var prima_total_ex = (monto_total * prima_porcentaje).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_ex));

}

function MONTO_TOTAL(input, valor) {
    /* CALCULAMOS EL MONTO TOAL */
    var precio_de_lista_mo = Number(Number(document.getElementById("custbody13").value));
    var extra_sobre_precio_lista_mo = Number(Number(document.getElementById("diferecia").value));
    var calcula_monto_total = (precio_de_lista_mo + extra_sobre_precio_lista_mo).toFixed(2);
    Number(document.getElementById("custbody_ix_total_amount").value = (calcula_monto_total));

}



function PRIMA_NETA(input, valor) {
    /* Monto Prima Asignable Neta */
    var prima_total = Number(Number(document.getElementById("custbody39").value));
    var cash = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva = Number(Number(document.getElementById("custbody52").value));
    var total = (prima_total - cash - reserva).toFixed(2);
    Number($("#custbody_ix_salesorder_monto_prima").val(total));

}

function MONTO_PRIMA_ASIGNABLE_NETA(input, valor) {
    /* PRIMA NETA */
    var prima_total_neta = Number(Number(document.getElementById("custbody39").value));
    var cashback_neta = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva_neta = Number(Number(document.getElementById("custbody52").value));


    //MONTOS PRIMAS UNICA
    var monto_prima_unico = Number(Number(document.getElementById("custbody181").value));
    var monto_tracto_unico = Number(Number(document.getElementById("custbody182").value));

    //MONTOS PRIMAS
    var monto_prima_fraccionado = Number(Number(document.getElementById("custbody179").value));
    var monto_tracto_fraccionado = Number(Number(document.getElementById("custbody180").value));


    //MONTOS PRIMAS
    var monto_prima_extra = Number(Number(document.getElementById("custbody183").value));
    var monto_tracto_extra = Number(Number(document.getElementById("custbody184").value));


    //MONTOS PRIMAS +1
    var monto_prima_extra_uno = Number(Number(document.getElementById("o_2_uno_input").value));
    var monto_tracto_extra_uno = Number(Number(document.getElementById("custbody184_uno").value));


    //MONTOS PRIMAS +2
    var monto_prima_extra_dos = Number(Number(document.getElementById("o_2_dos_input").value));
    var monto_tracto_extra_dos = Number(Number(document.getElementById("custbody184_dos").value));

    //MONTOS PRIMAS +3
    var monto_prima_extra_tres = Number(Number(document.getElementById("o_2_tres_input").value));
    var monto_tracto_extra_tres = Number(Number(document.getElementById("custbody184_tres").value));




    var total_neta = (prima_total_neta - cashback_neta - reserva_neta - (monto_prima_unico * monto_tracto_unico) - (monto_prima_fraccionado * monto_tracto_fraccionado) - (monto_prima_extra * monto_tracto_extra) - (monto_prima_extra_uno *
        monto_tracto_extra_uno) - (monto_prima_extra_dos * monto_tracto_extra_dos) - (monto_prima_extra_tres * monto_tracto_extra_tres)).toFixed(2);
    Number($("#neta").val(total_neta));

}






function HITOS_MONTO_SIN_PRIMA_TOTAL(input, valor) {
    var monto_total_sin_prima = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var monto_prima_total_sin_prima = Number(Number(document.getElementById("custbody39").value));
    var monto_sin_prima_total = (monto_total_sin_prima - monto_prima_total_sin_prima).toFixed(2);


    /* MONTO SIN PRIMA TOTAL   Avance De Obra*/
    Number(document.getElementById("obra_enterega").value = (monto_sin_prima_total));
    /*MONTO SIN PRIMA TOTAL Contra Entrega*/
    Number(document.getElementById("mspt_contra_entrega").value = (monto_sin_prima_total));
    /* MONTO SIN PRIMA TOTAL DIFERENCIADO */
    Number(document.getElementById("avance_diferenciado_hito17").value = (monto_sin_prima_total));

    var obra_enterega = Number(Number(document.getElementById("obra_enterega").value));
    // calculo de avance obra hitos del 1 al hito6
    var ao_hito6_rest = (obra_enterega * 0.05).toFixed(2);
    Number($("#avnace_obra_hito6").val(ao_hito6_rest));
    var ao_hito5_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito5").val(ao_hito5_rest));
    var ao_hito4_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito4").val(ao_hito4_rest));
    var ao_hito3_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito3").val(ao_hito3_rest));
    var ao_hito2_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito2").val(ao_hito2_rest));
    var ao_hito1_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito1").val(ao_hito1_rest));

    var mspt_contra_entrega = Number(Number(document.getElementById("mspt_contra_entrega").value));
    var contra = (mspt_contra_entrega * 1).toFixed(2);
    Number($("#contra_enterega1").val(contra));
}























function ocultarDiv(divId) {
    var div = document.getElementById(divId);
    div.style.display = "none"; // Ocultar el div
}
ocultarDiv("f_0");
ocultarDiv("fraccionado");
ocultarDiv("u_0");
ocultarDiv("unico");
ocultarDiv("o_0");
ocultarDiv("extra");
ocultarDiv("o_Uno");
ocultarDiv("ex_uno");
ocultarDiv("o_dos");
ocultarDiv("ex_dos");
ocultarDiv("o_tres");
ocultarDiv("ex_tres");

function toggleDivF(value, checkboxId, divId, iId) {
    var checkbox = document.getElementById(checkboxId);
    var div = document.getElementById(divId);
    var i = document.getElementById(iId);
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
        i.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
        i.style.display = "none"; // Mostrar el div
    }
}



/*ASIGNAMOS LOS VALORES DE FECHA POR DEFECTO A LOS INPUTS*/
var fechaActual = new Date(); // Obtener la fecha actual
var dia = ("0" + fechaActual.getDate()).slice(-2); // Obtener el día en formato de dos dígitos
var mes = ("0" + (fechaActual.getMonth() + 1)).slice(-2); // Obtener el mes en formato de dos dígitos
var año = fechaActual.getFullYear(); // Obtener el año

var fechaPorDefecto = año + "-" + mes + "-" + dia; // Construir la fecha en formato YYYY-MM-DD

var camposFecha = ["custbody179_date", "custbody182_date", "custbody184_date", "custbody184_uno_date", "custbody184_dos_date", "custbody184_tres_date", "avance_diferenciado_hito1_date", "avance_diferenciado_hito2_date",
    "avance_diferenciado_hito3_date", "avance_diferenciado_hito4_date", "avance_diferenciado_hito5_date", "avance_diferenciado_hito6_date", "fech_reserva"
];

for (var i = 0; i < camposFecha.length; i++) {
    var campo = document.getElementById(camposFecha[i]);
    campo.value = fechaPorDefecto; // Establecer el valor por defecto del campo de fecha
}

/* OCULTAR Y MOSTAR CAMPOS DE PRE RESERVA*/
var div = document.getElementById("campos_prereserva");
div.style.display = "none"; // Ocultar el div
function toggleDiv() {
    var checkbox = document.getElementById("PRERESERVA");
    var div = document.getElementById("campos_prereserva");
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
    }
}



/* OCULTANOS O MOSTRAMOS LOS DIV DE LOS DIFERENTES METODOS DE PAGO*/
function pagoOnChange(selectElement) {
    const selectedValue = selectElement.value;

    const elementos = [{
        id: "avance_diferenciado",
        textId: "text_avance",
        value: "7"
    }, {
        id: "avance_obra",
        textId: "text_obra",
        value: "2"
    }, {
        id: "contra_entrega",
        textId: "text_contra",
        value: "1"
    }];

    elementos.forEach(elemento => {
        const div = document.getElementById(elemento.id);
        const textDiv = document.getElementById(elemento.textId);

        if (selectedValue === elemento.value) {
            div.style.display = "";
            textDiv.style.display = "";
        } else {
            div.style.display = "none";
            textDiv.style.display = "none";
        }
    });
}




/* HABILITAR LOS CAMPOS DE LOS INPUTS DE AVANCE DIFERENCIADO*/
function habilitarCampos(checkbox, inputId1, inputId2, inputId3) {
    var input1 = document.getElementById(inputId1);
    var input2 = document.getElementById(inputId2);
    var input3 = document.getElementById(inputId3);

    if (checkbox.checked) {
        input1.disabled = false;
        input2.disabled = false;
        input3.disabled = false;
    } else {
        input1.disabled = true;
        input2.disabled = true;
        input3.disabled = true;
        input1.value = "0.00";
        input2.value = "0.00";


    }

    function removeCommasAndConvertToNumber(value) {
        return Number(value.replace(/[,]/g, ''));
    }

    var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
    var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
    var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
    var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
    var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
    var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

    // Sum all values
    var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

    var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

    // Ensure valorCam2 is a number
    valorCam2 = Number(valorCam2);

    // Subtract valorCam2 from the total sum
    var valortotal = (totalSum - valorCam2).toFixed(2);

    $("#valortotal").val(valortotal);


    
}