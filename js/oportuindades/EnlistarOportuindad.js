
async function LLenarBanners() {

    //le damos formato a la fecha
    function formatDate(date, days) {
        const formattedDate = new Date(date);
        return formattedDate.toISOString().split('T')[0];
    }


    //segun el filtro de seleccion colocamos el tipo de fecha
    const selectedOption = $('#radio1').prop('checked') ? 1 : ($('#radio2').prop('checked') ? 2 : 0);
    const dateField = selectedOption === 1 ? 'fecha_creada_oport' : 'fecha_Condicion';

    //campos de fecha segun el rango
    let startDate = $("#startFilter").val();
    let endDate =$("#endtFilter").val();


    let dataSql = '';

    // Validación de que ambos valores estén llenos
    if (startDate && endDate) {
         startDate = formatDate($("#startFilter").val(), 1);
         endDate = formatDate($("#endtFilter").val(), -1);
        dataSql = `AND DATE(${dateField}) >= "${startDate}" AND DATE(${dateField}) < "${endDate}"`
    }


    //ejecutamos la funcion al api
    async function fetchData(data, table, where, filter) {
        const result = await fetchRecords(data, table, where, "select", "employee_oport", filter);
        return result.data;
    }

    // Obtén el elemento de anclaje por su ID
    var enlace1 = document.getElementById('oportInactivas');
    var enlace2 = document.getElementById('oportTotales');


    //si no tiene cliente enlaces normales
    enlace1.href = '/oportunidad/list?data=2&data2=0';
    enlace2.href = '/oportunidad/list?data=3&data2=0';


    //hacemos las consultas sql segun el banner de la oportunidad

    //mostramos todas las oportunidades con rango de fecha y el estatus_oport segun la url
    let whereCompleto = `and chek_oport=1 and estatus_oport=${dataParam1 === 2 ? 0 : 1} ${dataSql}`;
    //mostramos todas las oportunidades con rango de fecha y el estatus_oport segun la url y entitystatus_oport = condicional
    let whereCondicional = `and entitystatus_oport=22 and chek_oport=1 and estatus_oport=${dataParam1 === 2 ? 0 : 1} ${dataSql}`;
    //mostramos todas las oportunidades con rango de fecha y el estatus_oport segun la url y entitystatus_oport = firme
    let whereFirme = `and entitystatus_oport=11 and chek_oport=1 and estatus_oport=${dataParam1 === 2 ? 0 : 1} ${dataSql}`;

    //filter 1 significa que si el por vendedor y si es 2 es por cliente
    let filter = 1;

    //validamos que el data2 de la url no sea 0 si es diferente entonces es un cliente
    if (dataParam2 != 0) {
        whereCompleto = `where entity_oport=${dataParam2}`;
        whereCondicional = `where entity_oport=${dataParam2} and entitystatus_oport=22`
        whereFirme = `where entity_oport=${dataParam2} and entitystatus_oport=11`
        enlace1.href = `/oportunidad/list?data=2&data2=${dataParam2}`;
        enlace2.href = `/oportunidad/list?data=3&data2=${dataParam2}`;
        filter = 2;

        if (dataParam1 == 2) {
            whereCompleto = `where entity_oport=${dataParam2} and estatus_oport=0`;
            whereCondicional = `where entity_oport=${dataParam2} and entitystatus_oport=22 and estatus_oport=0`
            whereFirme = `where entity_oport=${dataParam2} and entitystatus_oport=11 and estatus_oport=0`
        }
        if (dataParam1 == 1) {
            whereCompleto = `where entity_oport=${dataParam2} and estatus_oport=1 and chek_oport=1`;
            whereCondicional = `where entity_oport=${dataParam2} and entitystatus_oport=22 and estatus_oport=1 and chek_oport=1`
            whereFirme = `where entity_oport=${dataParam2} and entitystatus_oport=11 and estatus_oport=1 and chek_oport=1`
        }
    }

    //pasamos a la funcion las condiciones para el api contrullendo la consulta
    const dataFilters = [
        { id: "catidads", where: ` ${whereCompleto}`, filter: ` ${filter}` },
        { id: "firme", where: `${whereCondicional}`, filter: ` ${filter}` },
        { id: "condiconal", where: ` ${whereFirme}`, filter: ` ${filter}` }
    ];

    //recorremos el filter para cada uno
    for (const filter of dataFilters) {
        const data = 'COUNT(id_oportunidad_oport) AS CN';
        const table = 'oportunidades';
        const result = await fetchData(data, table, filter.where, filter.filter);
        dataConsul(result, filter.id);
    }

    //una vez la consulta recorremos los datos en este caso solo seria el contadoe de CN
    function dataConsul(data, id) {
        const element = document.getElementById(id);
        element.textContent = data[0].CN;
    }
}










//darle formato de fecha  inicial
$('#startFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//darle formato de fecha  final
$('#endtFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//ejecutamos la consulta de los banners
LLenarBanners();
//funcion para llenar la tabla
ExtraDataOportuindad()



// Comentario explicando la función
async function ExtraDataOportuindad() {

    //Modal poara esperar a que cargen los datos
    mostrarSwalopacidadDiv();


    //funcion para darle formato a la fecha definida
    function formatDate(date, days) {
        const formattedDate = new Date(date);
        return formattedDate.toISOString().split('T')[0];
    }

    //segun el filtro de seleccion colocamos el tipo de fecha
    const selectedOption = $('#radio1').prop('checked') ? 1 : ($('#radio2').prop('checked') ? 2 : 0);
    const dateField = selectedOption === 1 ? 'fecha_creada_oport' : 'fecha_Condicion';


    //vamos a extraer solo los datos necesarios para acelerar la consulta
    const data = 'p.chek2_oport,p.chek_oport,p.entitystatus_oport,p.tranid_oport,p.entity_oport,p.id_oportunidad_oport ,p.exp_custbody38_oport,p.Motico_Condicion,p.fecha_Condicion,p.fecha_creada_oport,l.nombre_lead,    l.proyecto_lead,  l.campana_lead,exp.precioVentaUncio_exp, exp.precioDeVentaMinimo, exp.codigo_exp,admins.name_admin,compras.nombre_motivo_compra,pagos.nombre_motivo_pago';

    //definimos la tabla y las relaciones para unir tablas
    const table = 'oportunidades as p INNER JOIN leads as l ON l.idinterno_lead = p.entity_oport INNER JOIN expedientes as exp ON exp.ID_interno_expediente = p.exp_custbody38_oport INNER JOIN admins ON p.employee_oport = admins.idnetsuite_admin INNER JOIN compras ON p.custbody76_oport = compras.id_motivo_compra INNER JOIN pagos ON p.custbody75_oport = pagos.id_motivo_pago';

    //aun no sabemos cual es el where por eso esta vacio
    let where = '';


    //extraemos la fecha inicio y fecha final
    let startDate = $("#startFilter").val();
    let endDate = $("#endtFilter").val();


    let dataSql = '';

    // Validación de que ambos valores estén llenos
    if (startDate && endDate) {
        startDate = formatDate($("#startFilter").val(), 1);
        endDate = formatDate($("#endtFilter").val(), -1);
        dataSql = `AND DATE(${dateField}) >= "${startDate}" AND DATE(${dateField}) < "${endDate}"`
    }


    let filter = 1;


    if (dataParam1 == 1) {
        where = `and p.estatus_oport=1  and chek_oport=1 ${dataSql} `;
    }
    if (dataParam1 == 2) {
        where = `and p.estatus_oport=0 ${dataSql} ORDER BY ${dateField} DESC`;
    }

    if (dataParam1 == 3) {
        where = `${dataSql}`;
    }
    if (dataParam2 != 0) {
        filter = 2;
        if (dataParam1 == 1) {
            where = `where entity_oport=${dataParam2} and p.estatus_oport=1`;
        }
        if (dataParam1 == 2) {
            where = `where entity_oport=${dataParam2} and  p.estatus_oport=0`;
        }

        if (dataParam1 == 3) {
            where = `where entity_oport=${dataParam2}`;
        }

    }






    /*
    llamamos la funcion que se encarga de ir al api a extraer los datos
    *Data? se encarga de extraer solo los campos que nececitamos
    *table? tabla + relaciones
    *"select"? metodo o tipo de consulta ejemplo SELECT * FROM ?
    *"id_empleado_lead"? extraemos todos los datos que pertenescan al vendedor
    *1? es si requiero que el select extraiga datos del vendedor si es 2 entonces no.
    */
    const result = await fetchRecords(data, table, where, "select", "p.employee_oport", filter);


    //recorremos los datos y segun la data de la url y vista mostramos los datos
    async function procesarDatos() {
        return new Promise(async (resolve) => {
            // validamos que sea mayor a cero
            if (result.data && result.data.length > 0) {
                //recorremos data
                result.data.forEach(async (data, index) => {
                    createRowoP(data);
                    //esperamos a que se cumpla la promesa
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));
                    if (index === result.data.length - 1) {
                        resolve();
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }
    //ejecutamos esa funcion y luego cargamos la tabla con los datos llamados
    procesarDatos().then(() => {
        //al terminar el then ejecutamos datatable
        $(document).ready(function () {

            //columas a definir para el filtro y descargar en excel
            var columnsDatatable = [];
            var columsDatatableExel = [];


            columnsDatatable = [0, 1, 3, 4, 6, 7, 10,12,13];
            columsDatatableExel = [0,1,2,3,4,5,6,7,8,9,10,11,12,13];

            var table = $("#datatable-buttons").DataTable({
                initComplete: function () {
                    $("#preloader").hide();
                },
                processing: true,
                searchPanes: {
                    cascadePanes: true,
                    dtOpts: {
                        select: {
                            style: "multi",
                        },
                        count: {
                            show: false,
                        },
                    },
                    columns: columnsDatatable,
                },
                columnDefs: [
                    {
                        type: 'date-custom', targets: 11 ,
                        searchPanes: {
                            show: true,
                            initCollapsed: true,
                        },
                        targets: columnsDatatable,
                    },
                ],
                dom: "PBfrtip",
                stateSave: true,
                order: [[11, "ASC"]], // Asumiendo que la columna 11 tiene las fechas,
                pageLength: 59,
                lengthMenu: [
                    [10, 25, 50, 200, -1],
                    [10, 25, 50, 200, "All"],
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "No se encontraron resultados",
                    info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                    infoFiltered: "(filtrado de un total de _MAX_ registros)",
                    sSearch: "Buscar:",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior",
                    },
                    sProcessing: "Cargando...",
                },
                buttons: [
                    {
                        extend: "excel",
                        footer: true,
                        titleAttr: "Exportar a excel",
                        className: "btn btn-success",
                        exportOptions: {
                            columns: columsDatatableExel,
                        },
                    },
                ],
            });

            
        });

        // Cambiar estilos de banner
        const changeBannerStyles = () => {
            setTimeout(() => {
                const tableleads = document.getElementById(`tableleads`);
                tableleads.style.display = "none";
                if (esDispositivoMovil()) {
                    var elemento = document.getElementById("table-body");
                    elemento.style.opacity = "20";
                } else {
                    var elemento = document.getElementById("table-body");
                    if (elemento) {
                        elemento.style.display = "";
                    }
                }
                Swal.close();
            }, 100);


        };

        // Cambiar estilos de banner
        changeBannerStyles();
    });
}

// Esta función se encarga de llenar la tabla con los datos del resultado
function createRowoP(leadData) {


    function formatearFecha(fecha) {
        const fech = new Date(fecha);
        const anio = fech.getFullYear();
        const mes = fech.getMonth() + 1;
        const dia = fech.getDate();
        return `${anio}-${mes.toString().padStart(2, '0')}-${dia.toString().padStart(2, '0')}`;
    }

    const fechaCreadaOport = formatearFecha(leadData.fecha_creada_oport);
    const fechaCondicion = formatearFecha(leadData.fecha_Condicion);

    var probabilidad = leadData.entitystatus_oport == 11 ? "Condicional 50.0%" : "Firma 80.0%";
    var chek_oport = leadData.chek_oport == 1 ? "Probable" : "No probable";







    const tableBody = document.querySelector("#table-body");
    // Cantidad de td para llenar la tabla
    const row = document.createElement("tr");
    row.innerHTML = `
          <td style="text-align: right; font-weight: bold;" >${leadData.nombre_lead}</td>
          <td style="text-align: right; font-weight: bold;" ><a href="oportunidad/oprt_view?data=${leadData.entity_oport}&data2=${leadData.id_oportunidad_oport}">${leadData.tranid_oport}</a></td>
          <td style="text-align: right; font-weight: bold;" hidden><a href="/expedientes/view?data=${leadData.exp_custbody38_oport}">${leadData.codigo_exp}</a></td>
          <td style="text-align: right; font-weight: bold;" hidden>${leadData.Motico_Condicion}</td>
          <td style="text-align: right; font-weight: bold;" hidden>${probabilidad}</td>
          <td style = "text-align: right; font-weight: bold;" > ${fechaCreadaOport}</td>
          <td style="text-align: right; font-weight: bold;"><a href="/expedientes/view?data=${leadData.exp_custbody38_oport}">${leadData.codigo_exp}</a></td>
          <td style="text-align: right; font-weight: bold;" >${leadData.nombre_motivo_pago}</td>
          <td style="text-align: right; font-weight: bold;" >${leadData.precioVentaUncio_exp}</td>
          <td style="text-align: right; font-weight: bold;" >${leadData.precioDeVentaMinimo}</td>
          <td style="text-align: right; font-weight: bold;" >${chek_oport}</td>
          <td style="text-align: right; font-weight: bold;" >${fechaCondicion}</td>
          <td style="text-align: right; font-weight: bold;" >${leadData.campana_lead}</td>
          <td style="text-align: right; font-weight: bold;" >${leadData.proyecto_lead}</td>
          
`;

    // Agregamos los datos de 'row' al 'tableBody'
    tableBody.appendChild(row);


}


