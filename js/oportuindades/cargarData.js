
// Definición de un objeto 'opciones' que mapea valores de select a sus respectivas propiedades
var opciones = {
    "": ["", ""],       // Opción vacía con valores en blanco
    "11": ["Condicional", "50.0%"],  // Opción con valor "11" y sus propiedades
    "22": ["Firme", "80.0%"]        // Opción con valor "22" y sus propiedades
};

// Función que se llama cuando se cambia el valor en el select
function cambioOpciones() {
    // Obtiene el elemento select por su ID
    var combo = document.getElementById('opciones');
    // Obtiene el valor seleccionado en el select
    var opcion = combo.value;
    // Actualiza el campo 'nombre_valor' con la propiedad correspondiente del objeto 'opciones'
    document.getElementById('nombre_valor').value = opciones[opcion][0];
    // Actualiza el campo 'probability' con la propiedad correspondiente del objeto 'opciones'
    document.getElementById('probability').value = opciones[opcion][1];
}


$(document).ready(function () {
    $('#dateFirme').show();  // Mostrar 'dateFirme' por defecto

    $('#opciones').change(function () {
        var opcion = $(this).val();

        $('#dateCon, #lavel').toggle(opcion === "11");
        $(".calss").fadeOut(10500);
        $('#miinput').prop("disabled", opcion !== "11").val("");

        $('#dateFirme').toggle(opcion === "22");
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var fechaActual = new Date().toLocaleDateString('es-ES'); // Obtiene la fecha actual en el formato dd/mm/yyyy

    document.getElementById('fechaActual').textContent = fechaActual; // Actualiza la fecha actual en el elemento span

    var date_now = new Date(); // Fecha actual
    var date_past = new Date(date_now);
    date_past.setDate(date_now.getDate() + 7); // Suma 7 días
    var date_past_con = new Date(date_now);
    date_past_con.setDate(date_now.getDate() + 22); // Suma 22 días

    // Formatea las fechas a dd/mm/yyyy
    var date_past_formatted = date_past.toLocaleDateString('es-ES');
    var date_past_con_formatted = date_past_con.toLocaleDateString('es-ES');

    document.getElementById('dateFirme').value = date_past_formatted; // Actualiza el valor del input
    document.getElementById('dateCon').value = date_past_con_formatted; // Actualiza el valor del input
    document.getElementById('date').value = fechaActual; // Actualiza el valor del input oculto
});

const dataManager = new DataManager();
async function llenarSelectores() {

    dataManager.fillSelect("custbody76", "", "custbody76");
    dataManager.fillSelect("custbody75", "", "custbody75");
    dataManager.fillSelect("location", "", "location");
    dataManager.fillSelect("class", "", "class");


}

async function ExtraerDa() {
    const data = `*`;
    const table = "leads"
    const query = `WHERE idinterno_lead = ${dataParam1}`;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    result.data.forEach(async (datas) => {
        var nom = datas.idinterno_lead + " " + datas.nombre_lead;
        $("#entity").val(datas.idinterno_lead);
        $("#nombre").val(nom);
        $("#sub_name").val(datas.subsidiaria_lead);
        $("#sub_id").val(datas.idsubsidaria_lead);
        $("#pro").val(datas.proyecto_lead);
        $("#salesrep").val(datas.id_empleado_lead);
        $("#correo").val(datas.email_lead);
        $("#telefono").val(datas.telefono_lead);
        $("#estado").val(datas.segimineto_lead);
        $("#id_admin").val(datas.id_empleado_lead);
        dataManager.fillSelect("expediente", "", "expediente", $("#pro").val(),100);
    });
}

async function CargarExpediente() {
    var expediente = $("#expediente").val();
    if (expediente != 0) {
        const data = `ID_interno_expediente,codigo_exp,precioVentaUncio_exp,precioDeVentaMinimo,estado_exp`;
        const table = "expedientes"
        const query = `WHERE ID_interno_expediente = ${expediente}`;
        const result = await fetchRecords(data, table, query, "select", "", 2);
        result.data.forEach(async (datas) => {
            $("#ID_interno_expediente").val(datas.ID_interno_expediente);
            $("#Nombre_expediente").val(datas.codigo_exp);
            $("#Precio_de_Venta_Unico_expediente").val(datas.precioVentaUncio_exp);
            $("#Precio_de_Venta_Minimo_expediente").val(datas.precioDeVentaMinimo);
            $("#estado_expediente").val(datas.estado_exp);
        });
    } else {
        $("#ID_interno_expediente").val("");
        $("#Nombre_expediente").val("");
        $("#Precio_de_Venta_Unico_expediente").val("");
        $("#Precio_de_Venta_Minimo_expediente").val("");
        $("#estado_expediente").val("");
    }


}
async function ejecutarFunciones() {
    await llenarSelectores();
    await ExtraerDa();
    ExtraerLead("oportunidad");
}
ejecutarFunciones();



