
const dataManager = new DataManager();
async function ExtraerOportuindad(value) {
    try {




        const data = `*`;
        const table = "oportunidades p INNER JOIN leads l ON p.entity_oport = l.idinterno_lead INNER JOIN expedientes e ON p.exp_custbody38_oport = e.ID_interno_expediente INNER JOIN estados es ON p.entitystatus_oport = es.id_estado_oportuindad INNER JOIN pronosticos pr ON p.forecasttype_oport = pr.id_pronostico INNER JOIN compras c ON p.custbody76_oport = c.id_motivo_compra INNER JOIN pagos ON p.custbody75_oport = pagos.id_motivo_pago"
        const query = `WHERE p.id_oportunidad_oport  = ${dataParam2}`;
        const result = await fetchRecords(data, table, query, "select", "", 2);
        result.data.forEach(async (datas) => {
            function formatDate(date) {
                const year = date.getFullYear();
                const month = String(date.getMonth() + 1).padStart(2, '0');
                const day = String(date.getDate()).padStart(2, '0');
                return `${year}-${month}-${day}`;
            }

            const fecha = new Date(datas.fecha_Condicion);
            const fechaFormateada = formatDate(fecha);
            var checkbox = document.getElementById("square-switch3");
            if (datas.chek_oport == 1) {
                checkbox.checked = true;
            } else {
                checkbox.checked = false;
            }
            $("#id_oportunidad, #id_oportunidad_div").text("OPORTUNIDAD # : " + datas.tranid_oport);
            $("#id_oportunidad_oport").text("Detalles de la oportunidad: " + datas.id_oportunidad_oport);
            const options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: 'numeric', minute: 'numeric', hour12: true };
            const fechaCreacion = new Date(datas.fecha_creada_oport).toLocaleString('en-US', options).replace(/\//g, '-');
            const fechaModificacion = new Date(datas.update_fecha_oport).toLocaleString('en-US', options).replace(/\//g, '-');
            $("#fech_creacion").text("FECHA DE CREACIÓN: " + fechaCreacion);
            $("#fech_modificaion").text("FECHA DE MODIFICACIÓN: " + fechaModificacion);


            $("#cliente_div").text(datas.entityname_oport);
            $("#estado_div").text(datas.nombre_estado_oportuindad);
            $("#tipo_div").text(datas.nombre_pronostico);
            $("#probabilidad_div").text(datas.probability_oport);
            $("#cierre_div").text(fechaFormateada);
            $("#sub_div").text(datas.subsidiaria_oport);
            $("#motivo_div").text(datas.nombre_motivo_compra);
            $("#metodo_div_pagp").text(datas.nombre_motivo_pago);
            $("#desde_div").text((datas.rangelow_oport * 10).toLocaleString('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 3 }));

            $("#hasta_div").text((datas.rangehigh_oport * 10).toLocaleString('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 3 }));
            $("#total_previsto").text((datas.projectedtotal_oport * 10).toLocaleString('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 3 }));

            $("#detalle_div").text(datas.memo_oport);
            $("#id_ex").text(" " + datas.codigo_exp);
            const planos = datas.planosDeUnidad_exp;
            $('#planos').attr('href', planos);
            $("#precio_unico").text(" " + datas.precioVentaUncio_exp);
            $("#Precio_de_Venta_Minimo").text(" " + datas.precioDeVentaMinimo);
            $("#entrega").text(" " + datas.entregaEstimada);
            $("#custbody114").val(datas.entregaEstimada);

            $("#motivo_condicion").text(" " + datas.Motico_Condicion);
            const link = "/expedientes/view?data=" + datas.ID_interno_expediente;
            $('#link_port').attr('href', link);
            const memoValue = datas.memo_oport || "Sin detalle";
            $("#memo").text(" " + memoValue);
            const dateNow = formatDate(new Date(datas.fecha_Condicion));
            $("#date").val(dateNow);
            $("#p_date").text("ULTIMO DÍA DE CIERRE ASIGNADO: " + dateNow);
            const datePast = new Date();
            datePast.setDate(datePast.getDate() + 7);
            const datePastFormatted = formatDate(datePast);
            const datePastFormattedISO = datePast.toISOString().split('T')[0];

            $("#dateFirme").val(datePastFormattedISO);

            const datePastCon = new Date();
            datePastCon.setDate(datePastCon.getDate() + 22);
            const datePastConFormatted = formatDate(datePastCon);
            const datePastConFormattedISO = datePastCon.toISOString().split('T')[0];
            $("#dateCon").val(datePastConFormattedISO);

            $("#dateCon").toggle(datas.id_estado_oportuindad == 11);
            $("#dateFirme").toggle(datas.id_estado_oportuindad !== 11);

            //modal estimacion datos de campos
            $("#id_modal").text(" GENERAR ESTIMACION / OPORTUNIDAD# " + datas.tranid_oport);
            $("#entityname_oport").val(datas.entityname_oport);
            $("#entity").val(datas.entity_oport);
            $("#custbody38").val(datas.exp_custbody38_oport);
            $("#Nombre_de_Expediente_de_Unidad").val(datas.codigo_exp);
            $("#proyecto_lead_est").val(datas.proyecto_lead);
            $("#subsidiaria_oport").val(datas.subsidiaria_oport);
            $("#subsidiary").val(datas.idsubsidaria_lead);
            $("#entitystatus").val(datas.nombre_estado_oportuindad);
            $("#entitystatus_id").val(datas.id_estado_oportuindad);
            $("#tranid_oport").val(datas.tranid_oport);
            $("#opportunity").val(datas.id_oportunidad_oport);
            $("#expectedclosedate").val(datas.expectedclosedate_oport);
            var precioVentaUnico = datas.precioVentaUncio_exp;
            console.log("precioVentaUnico: ", precioVentaUnico);
            precioVentaUnico = precioVentaUnico.replace(/,/g, '');
            $("#custbody13").val(precioVentaUnico);
            var Precio_de_Venta_Minimo = datas.precioDeVentaMinimo;
            Precio_de_Venta_Minimo = Precio_de_Venta_Minimo.replace(/,/g, '');
            $("#custbody18").val(Precio_de_Venta_Minimo);

            var fechaActual_str = datas.entregaEstimada; // Obtener la fecha actual entregaEstimada como cadena
            var partesFecha = fechaActual_str.split('/'); // Dividir la cadena en partes separadas por "/"
            var dia_date = ("0" + partesFecha[0]).slice(-2); // Obtener el día en formato de dos dígitos
            var mes_date = ("0" + partesFecha[1]).slice(-2); // Obtener el mes en formato de dos dígitos
            var año_date = partesFecha[2]; // Obtener el año
            var fechaPorDefecto = año_date + "-" + mes_date + "-" + dia_date; // Construir la fecha en formato YYYY-MM-DD
            $("#contra_enterega11_date").val(fechaPorDefecto);

            dataManager.fillSelect("opciones", "", "opciones", datas.entitystatus_oport);
            dataManager.fillSelect("custbody76_opt", "", "custbody76_opt", datas.custbody76_oport);
            dataManager.fillSelect("custbody75_opt", "", "custbody75_opt", datas.custbody75_oport);
            dataManager.fillSelect("custbody75_estimacion", "", "custbody75_estimacion", datas.custbody75_oport);

            /* ***************************************************************************************************** */
            // sacamos la PRIMA TOTAL AL CARGAR EL FORMULARIO, ES DECIR POR DEFECTO
            var lista = Number(Number(document.getElementById("custbody13").value));
            var prima = Number(document.getElementById("custbody60").value);
            var res = (lista * prima).toFixed(5);
            Number(document.getElementById("custbody39").value = (res));//PRIMA TOTAL
            /* ***************************************************************************************************** */
            //ASIGNAMOS EL VALOR A MONTO TOTAL
            var total_amount = (lista).toFixed(2);
            Number($("#custbody_ix_total_amount").val(total_amount));
            /* ***************************************************************************************************** */
            // sacamos EL MONTO PRIMA NETA
            var monto_prima_total = Number(Number(document.getElementById("custbody39").value));
            var monto_cashbak = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
            var monto_reserva = Number(Number(document.getElementById("custbody52").value));
            var total_monto_prima_neta = (monto_prima_total - monto_cashbak - monto_reserva).toFixed(2);
            Number($("#custbody_ix_salesorder_monto_prima").val(total_monto_prima_neta));
            /* ***************************************************************************************************** */
            // MONTO ASIGNABLE PRIMA NETA:
            var monto_prima_total = Number(Number(document.getElementById("custbody39").value));
            var monto_cashbak = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
            var monto_reserva = Number(Number(document.getElementById("custbody52").value));
            //monto fraccionado
            var monto_fraccionado = Number(Number(document.getElementById("custbody179").value));
            var monto_tracto_fraccionado = Number(Number(document.getElementById("custbody180").value));
            //monto unico
            var monto_unico = Number(Number(document.getElementById("custbody181").value));
            var monto_tracto_unico = Number(Number(document.getElementById("custbody182").value));
            //monto unico
            var monto_extraor = Number(Number(document.getElementById("custbody183").value));
            var monto_tracto_extraor = Number(Number(document.getElementById("custbody184").value));

            var total_monto_asignalbe_prima_neta = (monto_prima_total - monto_cashbak - monto_reserva - (monto_unico * monto_tracto_unico) - (monto_fraccionado * monto_tracto_fraccionado) - (monto_extraor * monto_tracto_extraor)).toFixed(3);
            Number($("#neta").val(total_monto_asignalbe_prima_neta));

        });




        // Función para formatear la fecha en formato "YYYY-MM-DD"



    } catch (error) {
        console.log('error', error);
    }
}

async function ejecutarFunciones() {
    await ExtraerOportuindad();
}
ejecutarFunciones();

