const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};
const crear_oportunidad = async () => {
    function validar_campos(id, color) {
        var selectElement = document.getElementById(id);
        selectElement.style.borderColor = selectElement.value === "" ? color : color;
    }


    let custbody76 = $("#custbody76").val();//MOTIVO DE COMPRA 
    let custbody75 = $("#custbody75").val();//METODO DE PAGO
    let MotivoCondicion = $("#miinput").val();//motivo condicion
    let miinput = $("#miinput").val();
    let opciones = $("#opciones").val();//condiciones de estado
    let location = $("#location").val();//ubicacion
    let fechCondiconal = $("#dateCon").val();//fecha si es condicional
    let fechFirme = $("#dateFirme").val();//fecha si es firme
    let expediente = $("#expediente").val();//expediente

    let memo = $("#memo").val();
    //detalle
    if (memo.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Campo Detalle Introduzca información adicional sobre la oportunidad.", "warning");
    }

    //condiciones de estado
    if (opciones.length == 0) {
        validacion("opciones");
        return Swal.fire("Mensaje De Advertencia", "Campo Estado es obligatorio", "warning");
    }



    //condicion es condicional ==11
    if (opciones == 11) {
        if (MotivoCondicion.length == 0) {
            validar_campos("miinput", "red");
            return Swal.fire("Mensaje De Advertencia", "El campo Motivo de Condición está vacío. Si el estado es condicional, se debe llenar el motivo.", "warning");
        }
        validar_campos("miinput", "green");
        //Condicional
        MotivoCondicion_opt = MotivoCondicion;
        fech_estado = fechCondiconal;
    }
    //condicion es si es firme ==11
    if (opciones == 22) {
        //Firme
        MotivoCondicion_opt = "Firme 80.0%";
        fech_estado = fechFirme;
    }

 


    const fecha1 = fech_estado; // Assuming this is your fecha1 input

    // Split the fecha1 and fecha2 strings to extract day, month, and year components
    const [part1, part2, year] = fecha1.includes('/')
        ? fecha1.split('/').map(Number)
        : fecha1.split('/').reverse().map(Number);

    // Create date objects from the extracted components
    const date1 = new Date(year, part1 - 1, part2); // Month is zero-based (0-11)



    // Format the dates as 'D/M/YYYY'
    const options = { day: '2-digit', month: '2-digit', year: 'numeric' };
    const formattedDate1 = date1.toLocaleDateString('en-US', options);




    if (location.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Campo Ubicación obligatorio", "warning");
    }


    let classe = $("#class").val();

    if (classe.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Campo Clase obligatorio", "warning");
    }




    if (custbody76.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Campo Motivo de compra obligatorio", "warning");
    }
    if (custbody75.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Campo Motivo de Pago obligatorio", "warning");
    }


    if (expediente == 0) {
        return Swal.fire("Mensaje De Advertencia", "Escoger un expediente de unidad disponible para continuar", "warning");
    }


    Swal.fire({
        title: '¿Está seguro?',
        text: "¿De crear esta oportunidad?",
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, crear oportunidad'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            let entity = $("#entity").val();
            let subsidiary = $("#subsidiary").val();
            let currency = $("#currency").val();
            let entitystatus = opciones;
            let probability = $("#probability").val();
            let custbody38 = $("#ID_interno_expediente").val();
            let forecasttype = 2;
            let rangelow = $("#Precio_de_Venta_Unico_expediente").val();
            let numeros_rangelow = rangelow;
            numeros_rangelow = numeros_rangelow.replace(",", ""); // eliminar la coma
            numeros_rangelow = numeros_rangelow.replace(".", ""); // eliminar el punto
            rangelow = parseFloat(numeros_rangelow); // convertir a número
            let rangehigh = $("#Precio_de_Venta_Minimo_expediente").val();
            let projectedtotal = $("#Precio_de_Venta_Minimo_expediente").val();
            let numeros = projectedtotal;
            numeros = numeros.replace(",", ""); // eliminar la coma
            numeros = numeros.replace(".", ""); // eliminar el punto
            projectedtotal = parseFloat(numeros); // convertir a número
            let salesrep = $("#salesrep").val();
      

            // mostrarSwalopacidadDiv();
            const data = {
                entity: entity,
                custbody38: custbody38,
                entitystatus: entitystatus,
                forecasttype: forecasttype,
                probability: probability,
                expectedclosedate: formattedDate1,
                custbody76: custbody76,
                custbody75: custbody75,
                rangelow: rangelow,
                rangehigh: rangehigh,
                projectedtotal: projectedtotal,
                subsidiary: subsidiary,
                currency: currency,
                salesrep: salesrep,
                memo: memo,
                location: location,
                classe: classe
            };
            const response = await fetch("https://api-crm.roccacr.com/Api/V2/oportunidades/add", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            const result = await response.json();
            var ExTraerResultado = result['Detalle'];
            ExTraerResultado.id;
            if (ExTraerResultado.statusCode == 200) {

                var id_le = entity;
                descpDelEvento = "Se creo una oportuindad";
                const estado = '02-LEAD-OPORTUNIDAD';
                const valores = {
                    valorDeCaida: 53,
                    tipo: "Se creo una oportuindad",
                    estado_lead: 1,
                    accion_lead: 6,
                    seguimiento_calendar: 1,
                    valor_segimineto_lead: 3,
                };
                const StatusCodeBitacora = await BitacoraCliente(id_le, valores, descpDelEvento, estado);
                if (StatusCodeBitacora.statusCode == 200) {
                    const StatusCodeCliente = await ActualizarCliente(id_le, valores, estado);
                    if (StatusCodeCliente.statusCode == 200) {
                        Swal.close();
                        id_ex = ExTraerResultado.id;

                        const data = `Motico_Condicion="${MotivoCondicion_opt}"`;
                        const table = "oportunidades"
                        const query = `WHERE id_oportunidad_oport="${id_ex}";`;
                        await fetchRecords(data, table, query, "update", "", 2);






                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Se creo una nueva oportuindad',
                            showConfirmButton: false,
                            timer: 2500
                        }).then((result) => {
                            window.location.href = "oportunidad/oprt_view?data=" + entity + "&data2=" + id_ex;
                        })
                    }
                }
            }
            if (ExTraerResultado.statusCode == 500) {
                error = JSON.parse(result['Detalle'].Error);
                return Swal.fire({
                    title: "Detalle de error : " + error.details + ",  \nLo sentimos, favor revisar este error con su administrador.",
                    icon: 'question',
                    width: "40em",
                    padding: "0 0 1.25em",
                    iconHtml: '؟',
                    confirmButtonText: 'OK',
                    cancelButtonText: 'CORREGUIR',
                    showCancelButton: true,
                    showCloseButton: true
                })



            }

        }
    });
}