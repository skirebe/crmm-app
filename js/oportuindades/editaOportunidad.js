

const editaOportunidad = async () => {
    const id = dataParam2;
    const probability = $("#probability").val();
    const nombreValor = $("#nombre_valor").val();
    const memo = $("#memo").val();
    const opciones = $("#opciones").val();
    const miInput = $("#miinput").val();
    const dateCon = $("#dateCon").val();
    const dateFirme = $("#dateFirme").val();
    const custbody76 = $("#custbody76_opt").val();
    const custbody75_est = $("#custbody75_opt").val();
    let fechCierre, condicion;
    if (opciones == 11) {
        fechCierre = dateCon;
        condicion = miInput;
    }
    if (opciones == 22) {
        fechCierre = dateFirme;
        condicion = "Firme 80.0%";
    }
    const campos = [
        { id: 'probability', valor: probability },
        { id: 'nombre_valor', valor: nombreValor },
        { id: 'memo', valor: memo },
        { id: 'opciones', valor: opciones },
        { id: 'miinput', valor: condicion },
        { id: 'dateCon', valor: dateCon },
        { id: 'dateFirme', valor: dateFirme },
        { id: 'custbody76', valor: custbody76 },
        { id: 'custbody75_est', valor: custbody75_est },
    ];

    campos.forEach((campo) => {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
        }
    });


    // Verificar si los valores están vacíos antes de agregarlos a campos
    if (campos.some(campo => campo.valor === '')) {
        mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
        return;
    }

    Swal.fire({
        title: '¿Está seguro?',
        text: '¿Confirma que desea editar esta oportuindad?',
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, editar'
    }).then(async (result) => {
        if (result.isConfirmed) {

            //Modal poara esperar a que cargen los datos
            mostrarSwalopacidadDiv();
            const data = {
                id,
                probability,
                nombreValor,
                memo,
                opciones,
                condicion,
                fechCierre,
                custbody76,
                custbody75_est
            };

            const response = await fetch("https://api-crm.roccacr.com/api/v2/oportunidades/editNetsuite", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            const result = await response.json();
            var ExTraerResultado = result['Detalle'];
            if (ExTraerResultado.Status == 200) {
                const data = `probability_oport="${probability}",memo_oport='${memo}',entitystatus_oport='${opciones}',Motico_Condicion='${condicion}',fecha_Condicion='${fechCierre}',custbody76_oport='${custbody76}',custbody75_oport='${custbody75_est}'`;
                const table = "oportunidades";
                const query = `WHERE id_oportunidad_oport="${id}"`;
                const result = await fetchRecords(data, table, query, "update", "", 2);
                if (result.statusCode == 200) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se edito esta oportuinidad',
                        showConfirmButton: false,
                        timer: 2500
                    }).then((result) => {
                        window.location.reload();
                    })
                }
            } else {
                mostrarAlerta('No se pudo editar esta oportuindad ', true);
            }

            if (response.status === 200) {
            } else {
                mostrarAlerta('Error al realizar la solicitud al servidor', true);
            }







        }
    });
};






