ExtraDataEstimacions()
async function ExtraDataEstimacions() {
    const data = `*`;
    const table = "estimaciones"
    const query = `WHERE idOportunidad_est = ${dataParam2} ORDER BY  caduca DESC`;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    console.log("result: ", result);
    const tableBody = document.querySelector("#table-body");
    function createRow(data, contador) {
        const row = document.createElement("tr");
        row.innerHTML = `
        <td style="text-align: right; font-weight: bold;">${data.tranid_est}</td>
        <td style="text-align: right; font-weight: bold;">${data.caduca}</td>
        <td style="text-align: right; font-weight: bold;">${data.creado_est}</td>
        <td style="text-align: right; font-weight: bold;"><a href="/estimaciones/view?data=${data.idLead_est}&data2=${data.idEstimacion_est}" style="margin:10px;" class="btn btn-primary cardStyleLeads" data-bs-toggle='tooltip' data-bs-placement='top' title='Ver esta estimacion'>Ver</a></td>

        `;
        tableBody.appendChild(row);
    }
    let contador = 0;
    result.data.forEach((data) => {
        contador++;
        let Ocultar = "#Ocultar" + contador;
        let OcultarEsta = "#OcultarEsta" + contador;
        createRow(data, contador);
    });

    $(document).ready(function () {
        var columnsDatatable = [];
        var columsDatatableExel = [];
        columnsDatatable = [0];
        columsDatatableExel = [0, 1];
        var table = $("#datatable-buttons").DataTable({
            initComplete: function () {
                $("#preloader").hide();
            },
            processing: true,
            searchPanes: {
                cascadePanes: true,
                dtOpts: {
                    select: {
                        style: "multi",
                    },
                    count: {
                        show: false,
                    },
                },
                columns: columnsDatatable,
            },
            columnDefs: [
                {
                    searchPanes: {
                        show: true,
                        initCollapsed: true,
                    },
                    targets: columnsDatatable,
                },
            ],
            dom: "PBfrtip",
            stateSave: true,
            order: [[0, "asc"]],
            pageLength: 59,
            lengthMenu: [
                [10, 25, 50, 200, -1],
                [10, 25, 50, 200, "All"],
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Mostrar _MENU_ registros",
                zeroRecords: "No se encontraron resultados",
                info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                infoFiltered: "(filtrado de un total de _MAX_ registros)",
                sSearch: "Buscar:",
                oPaginate: {
                    sFirst: "Primero",
                    sLast: "Último",
                    sNext: "Siguiente",
                    sPrevious: "Anterior",
                },
                sProcessing: "Cargando...",
            },
            buttons: [
                {
                    extend: "excel",
                    footer: true,
                    titleAttr: "Exportar a excel",
                    className: "btn btn-success",
                    exportOptions: {
                        columns: columsDatatableExel,
                    },
                },
            ],
        });

    });
}
