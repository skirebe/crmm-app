
async function calendario(a) {
    const data = `name_admin,id_calendar, nombre_calendar, color_calendar, id_lead, fechaIni_calendar, fechaFin_calendar, horaInicio_calendar, horaFinal_calendar, decrip_calendar, tipo_calendar, cita_lead, 
    CASE WHEN cita_lead = 1 OR masDeUnaCita_calendar = 1 THEN 'categoria5'
    ELSE CASE WHEN tipo_calendar IN('LLamada', 'Correo', 'Whatsapp') THEN 'categoria1'
    WHEN tipo_calendar = 'Tarea' THEN 'categoria2'
    WHEN tipo_calendar = 'Reunion' THEN 'categoria3'
    WHEN tipo_calendar = 'Seguimientos' THEN 'categoria4'
    ELSE NULL END END AS categoria`;
    const table = 'calendars INNER JOIN admins ON admins.idnetsuite_admin = calendars.id_admin';
    let where = 'and calendars.estado_calendar = 1 AND calendars.accion_calendar = "Pendiente"';
    const eventos = [];

    const result = await fetchRecords(data, table, where, "select", "calendars.id_admin", 1);
    result.data.forEach(dataEvento => {
        const evento = {
            _id: dataEvento.id_calendar,
            title: dataEvento.nombre_calendar,
            start: dataEvento.fechaIni_calendar,
            end: dataEvento.fechaFin_calendar,
            timeUno: dataEvento.horaInicio_calendar,
            timeDos: dataEvento.horaFinal_calendar,
            color: dataEvento.color_calendar,
            descs: dataEvento.decrip_calendar,
            lead: dataEvento.id_lead,
            cita: dataEvento.cita_lead,
            category: dataEvento.categoria,
            name_admin: dataEvento.name_admin,
            className: 'evento-especial'
        };

        eventos.push(evento);
    });
    return eventos;
}

var element = "month";

if (esDispositivoMovil()) {
    element = "listWeek";
}
var events = [];


async function cargarCalendario() {
    var calendar = $('#calendar').fullCalendar({
        height: "700",
        locale: 'es',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,listWeek'
        },
        defaultView: element,
        navLinks: true,
        editable: true,
        eventLimit: true,
        selectable: true,
        selectHelper: false,
        default: 'true',
        events: await calendario(1), // Utiliza la variable events
        eventRender: function (event, element) {
            element.attr('title', event.name_admin + ' : ' + event.title + ' : ' + event.descs);
        },
        eventClick: function (calEvent, jsEvent, view) {
            Swal.fire({
                title: '¿Quieres editar este evento?',
                html: 'Título del evento: <br><strong>(' + calEvent.title + ')</strong><br>Descripción del evento: <br><strong>(' + calEvent.descs + ')</strong>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí editar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "evento/edit?data=" + calEvent.lead + "&data2=" + calEvent._id;
                }
            });
        },
        select: function (start, end) {
            var startDate = start.format("Y-MM-DD");
            var endDate = end.subtract(1, "day").format("Y-MM-DD");
            // Resto de tu código de selección
            window.location.href = "evento/crear?data1=" + startDate + "&data2=" + endDate;
        },
        eventDrop: async function (event, delta) {
            var idEvento = event._id;
            var timeUno = event.timeUno;
            var timeDos = event.timeDos;

            var start = event.start.format("Y-MM-DD");

            var end = event.end ? event.end.format("Y-MM-DD") : event.start.format("Y-MM-DD");

            // 10. Función para convertir la hora de formato de 12 horas a formato de 24 horas
            function convertirHoraAMPMa24(horaAMPM) {
                var partesHora = horaAMPM.split(":");
                var hora = parseInt(partesHora[0]);
                var minutos = parseInt(partesHora[1]);
                var esPM = horaAMPM.includes("PM");

                if (esPM && hora < 12) {
                    hora += 12;
                } else if (!esPM && hora === 12) {
                    hora = 0;
                }

                return (
                    hora.toString().padStart(2, "0") +
                    ":" +
                    minutos.toString().padStart(2, "0")
                );
            }
            var fecha_inicio_final = start + "T" + convertirHoraAMPMa24(timeUno);
            var fecha_final_final = end + "T" + convertirHoraAMPMa24(timeUno);

            const data = `fechaIni_calendar = "${fecha_inicio_final}", fechaFin_calendar = "${fecha_final_final}"`;
            const table = "calendars"
            const query = `WHERE id_calendar ="${idEvento}";`;
            const result = await fetchRecords(data, table, query, "update", "", 2);

            if (result.statusCode == 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal
                            .stopTimer)
                        toast.addEventListener('mouseleave', Swal
                            .resumeTimer)
                    }
                })
                Toast.fire({
                    icon: 'success',
                    title: 'El evento se movio con éxito'
                })
            }


        },
    });

    events = await calendario(1);

    $('.category-checkbox').on('change', function () {
        var checkedCategories = $('.category-checkbox:checked').map(function () {
            return $(this).val();
        }).get();

        calendar.fullCalendar('rerenderEvents');
        calendar.fullCalendar('removeEvents');
        calendar.fullCalendar('addEventSource', events.filter(function (event) {
            return checkedCategories.includes(event.category);
        }));
    });
}

setTimeout(function () {
    document.getElementById("miDiv").style.opacity = "1";
}, 800);

cargarCalendario(); // Llamar a la función cargarCalendario
