const exitSession = JSON.parse(sessionStorage.getItem('admin'));
if (!exitSession) {
    // Mostrar un mensaje de alerta
    //alert("Lo sentimos, pero se ha cerrado la sesión, favor ingresar de nuevo");
    $.ajax({
        type: "POST", // Puedes usar POST u otro método HTTP según tu configuración
        url: "/controllers/admin/destuirlaSession.php", // Ruta al controlador de cierre de sesión
        success: function (data) {
            // console.log("data: ", data);
            location.reload();
        },
        error: function () {
            // Manejar errores, si es necesario
            alert("Error al cerrar sesión");
        }
    });
}
