function extractPathAndQueryFromURL() {
    const url = new URL(window.location.href);
    const pathAndQuery = url.pathname + url.search;
    return pathAndQuery;
}

let pathAndQuery = extractPathAndQueryFromURL();

if (pathAndQuery === "/exit") {
    pathAndQuery = "/";
    const exitLoginElement = document.getElementById("exitLogin");
    if (exitLoginElement) {
        exitLoginElement.style.display = "block";
        exitLoginElement.textContent = "Se ha cerrado la sesión, vuelve a ingresar tus credenciales";
    }
}

sessionStorage.removeItem('admin');
const Login = async () => {
    try {
        const email_admin = $("#loginEmail").val();
        const password_admin = $("#password").val();
        const resultado = await fetchRecords(email_admin, password_admin, '', 'login');


        if (resultado.statusCode === 200 && resultado.result.length > 0) {
            const admin_acceso = resultado.result[0];
            // Crear un nuevo objeto con los campos deseados
            const adminData = {
                id_acceso: admin_acceso.id_admin,
                token_acceso: admin_acceso.token_admin,
                name_acceso: admin_acceso.name_admin,
                email_acceso: admin_acceso.email_admin,
                NetsuiteId_acceso: admin_acceso.idnetsuite_admin,
                pass_admin_va: admin_acceso.pass_admin,
                token_exp_va: admin_acceso.token_exp_admin,
                rol_va: admin_acceso.id_rol_admin
            };
            // Guardar el nuevo objeto en sessionStorage
            sessionStorage.setItem('admin', JSON.stringify(adminData));
            $.ajax({
                type: 'POST', // Puedes usar POST o GET según tus necesidades
                url: '/controllers/admin/creacionDeSession.php', // Reemplaza con la URL de tu script PHP
                data: {
                    adminData: JSON.stringify(adminData)
                },
                success: function (response) {
                    window.location = pathAndQuery;
                },
                error: function (error) {
                    // Maneja errores si es necesario
                    console.error(error);
                }
            });
        } else if (resultado.statusCode === 500) {
            const errorLoginElement = document.getElementById("errorLogin");
            if (errorLoginElement) {
                errorLoginElement.style.display = "block";
                errorLoginElement.textContent = resultado.error.results;
            }
        }

    } catch (error) {
        console.error(error);
    }
};

