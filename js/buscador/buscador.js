if (window.history.replaceState) { // Verificamos la disponibilidad de la función
    window.history.replaceState(null, null, window.location.href); // Reemplazamos la entrada actual en el historial sin agregar una nueva
}

async function buscadorGeneral() {
    // Get the selected search option and search term from the user input.
    const selectBuscador = $("#selectBuscador").val();
    const searchTerm = $("#searchTerm").val();

    // Check if the search term is empty and display an error message if it is.
    if (searchTerm === "") {
        Swal.fire({
            title: 'Error',
            text: 'Ingresa el dato a buscar y selecciona una opción para continuar',
            icon: 'error'
        });
        return;
    }

    // Show a loading modal and clear the existing DataTable if it exists.
    mostrarSwalopacidadDiv();
    var dataTable = $("#datatable-buttons").DataTable();
    if (dataTable) {
        dataTable.clear().destroy(); // Clear and destroy the existing DataTable
    }

    // Define variables for data, table, and where clauses based on the selected search option.
    var data = '';
    var table = '';
    var where = '';

    // Set data, table, and where based on the selected search option.
    if (selectBuscador === "leads") {
        data = 'l.nombre_lead, a.name_admin, l.idinterno_lead';
        table = 'leads as l INNER JOIN admins as a ON a.idnetsuite_admin = l.id_empleado_lead';
        where = `where nombre_lead LIKE '%${searchTerm}%'`;
    } else if (selectBuscador === "oportunidades") {
        data = 'p.tranid_oport, a.name_admin, p.id_oportunidad_oport, p.entity_oport';
        table = 'oportunidades as p INNER JOIN admins as a ON a.idnetsuite_admin = p.employee_oport';
        where = `where p.tranid_oport LIKE '%${searchTerm}%'`;
    } else if (selectBuscador === "estimaciones") {
        data = 'p.tranid_est, a.name_admin, p.idEstimacion_est, p.idLead_est';
        table = 'estimaciones as p INNER JOIN admins as a ON a.idnetsuite_admin = p.idAdmin_est';
        where = `where p.tranid_est LIKE '%${searchTerm}%'`;
    } else if (selectBuscador === "ordenventa") {
        data = 'p.id_ov_tranid, a.name_admin, p.id_ov_netsuite, p.id_ov_lead';
        table = 'ordenventa as p INNER JOIN admins as a ON a.idnetsuite_admin = p.id_ov_admin';
        where = `where p.id_ov_tranid LIKE '%${searchTerm}%'`;
    } else if (selectBuscador === "expedientes") {
        data = 'codigo_exp,ID_interno_expediente';
        table = 'expedientes';
        where = `where codigo_exp LIKE '%${searchTerm}%'`;
    }

    // Fetch records based on the selected search option and search term.
    const result = await fetchRecords(data, table, where, "select", "", 2);

    // Define a function to process the retrieved data and create rows in the DataTable.
    async function procesarDatos() {
        return new Promise(async (resolve) => {
            if (result.data && result.data.length > 0) {
                result.data.forEach(async (data, index) => {
                    createRow(data, selectBuscador);
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));
                    if (index === result.data.length - 1) {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }

    // Process and create data rows, then initialize the DataTable with the results.
    procesarDatos().then(() => {
        $(document).ready(function () {
            // Initialize DataTable with specific options.
            var columnsDatatable = [0, 2];
            var columsDatatableExel = [0, 2];


            var table = $("#datatable-buttons").DataTable({
                initComplete: function () {
                    $("#preloader").hide();
                },
                processing: true,
                searchPanes: {
                    cascadePanes: true,
                    dtOpts: {
                        select: {
                            style: "multi",
                        },
                        count: {
                            show: false,
                        },
                    },
                    columns: columnsDatatable,
                },
                columnDefs: [
                    {
                        searchPanes: {
                            show: true,
                            initCollapsed: true,
                        },
                        targets: columnsDatatable,
                    },
                ],
                dom: "PBfrtip",
                stateSave: true,
                order: [[2, "asc"]],
                pageLength: 59,
                lengthMenu: [
                    [10, 25, 50, 200, -1],
                    [10, 25, 50, 200, "All"],
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "No se encontraron resultados",
                    info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                    infoFiltered: "(filtrado de un total de _MAX_ registros)",
                    sSearch: "Buscar:",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior",
                    },
                    sProcessing: "Cargando...",
                },
                buttons: [
                    {
                        extend: "excel",
                        footer: true,
                        titleAttr: "Exportar a excel",
                        className: "btn btn-success",
                        exportOptions: {
                            columns: columsDatatableExel,
                        },
                    },
                ],
            });

            // Change banner styles once data is loaded.
            const changeBannerStyles = () => {
                setTimeout(() => {
                    const tableleads = document.getElementById(`tableleads`);
                    tableleads.style.display = "none";
                    if (esDispositivoMovil()) {
                        var elemento = document.getElementById("table-body");
                        elemento.style.opacity = "20";
                    } else {
                        var elemento = document.getElementById("table-body");
                        if (elemento) {
                            elemento.style.display = "";
                        }
                    }
                    Swal.close();
                }, 100);
            };

            // Change banner styles after DataTable initialization.
            changeBannerStyles();
        });
    });
}

// Esta función se encarga de llenar la tabla con los datos del resultado
function createRow(leadData, selectBuscador) {

    let vendedor = leadData.name_admin;
    let data = '';
    let url = '';
    let tipo = '';
    if (selectBuscador === "leads") {
        vendedor = leadData.name_admin;
        data = leadData.nombre_lead;
        url = "/leads/perfil?data=" + leadData.idinterno_lead;
        tipo = "Lead";
    }
    if (selectBuscador === "oportunidades") {
        vendedor = leadData.name_admin;
        data = leadData.tranid_oport;
        url = `/oportunidad/oprt_view?data=${leadData.entity_oport}&data2=${leadData.id_oportunidad_oport}`;
        tipo = "Oportunidad";
    }

    if (selectBuscador === "estimaciones") {
        vendedor = leadData.name_admin;
        data = leadData.tranid_est;
        url = `/estimaciones/view?data=${leadData.idLead_est}&data2=${leadData.idEstimacion_est}`;
        tipo = "Estimacion";

    }
    if (selectBuscador === "ordenventa") {
        vendedor = leadData.name_admin;
        data = leadData.id_ov_tranid;
        url = `/orden/view?data=${leadData.id_ov_lead}&data2=${leadData.id_ov_netsuite}`;
        tipo = "Orden de Venta";
    }


    if (selectBuscador === "expedientes") {
        vendedor = "No aplica";
        data = leadData.codigo_exp;
        url = `/expedientes/view?data=${leadData.ID_interno_expediente}`;
        tipo = "Expediente";
    }
    // Cantidad de td para llenar la tabla
    const tableBody = document.querySelector("#table-body");
    // Cantidad de td para llenar la tabla
    const row = document.createElement("tr");
    row.innerHTML = `
    <td style="text-align: left; font-weight: bold;"> ${vendedor}</td>
    <td style="text-align: left; font-weight: bold;"> ${tipo}</td>
    <td style="text-align: left; font-weight: bold;"><a href='${url}'>${data}</a></td>
`;

    // Agregamos los datos de 'row' al 'tableBody'
    tableBody.appendChild(row);
}


