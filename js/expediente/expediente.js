

async function llenarSelectores(dataForName, whereName, valueKey, textKey, selectId, proyectosString) {
    const dataFor = dataForName;
    const table = "expedientes";
    const where = whereName;
    const result = await fetchRecords(dataFor, table, where, "select", "", 2);

    const selectElement = document.getElementById(selectId);

    result.data.forEach((item) => {
        const optionElement = document.createElement("option");
        optionElement.value = item[valueKey];
        optionElement.textContent = item[textKey];
        selectElement.appendChild(optionElement);

        // Verificar si proyectosString no está vacío y es el select de "proyectos"
        if (selectId === "proyectos" && proyectosString && proyectosString.includes(item.proyectoPrincipal_exp)) {
            optionElement.selected = true;
        }
        if (selectId === "tipos" && proyectosString && proyectosString.includes(item.tipoDeVivienda_exp)) {
            optionElement.selected = true;
        }

        if (selectId === "estados" && proyectosString && proyectosString.includes(item.estado_exp)) {
            optionElement.selected = true;
        }
    });
}



async function proyectos() {

    const urlParams = new URLSearchParams(window.location.search);
    const proyectosValues = urlParams.getAll('proyectos');
    const proyectosString = proyectosValues.map(value => `"${value}"`).join(',');


    let dataForName = "proyectoPrincipal_exp";
    let whereName = "GROUP BY proyectoPrincipal_exp";
    let valueKey = "proyectoPrincipal_exp";
    let textKey = "proyectoPrincipal_exp";
    let selectId = "proyectos";
    await llenarSelectores(dataForName, whereName, valueKey, textKey, selectId, proyectosString)

}

async function modelo() {

    const urlParams = new URLSearchParams(window.location.search);
    const proyectosValues = urlParams.getAll('tipos');
    const proyectosString = proyectosValues.map(value => `"${value}"`).join(',');

    let dataForName = "tipoDeVivienda_exp";
    let whereName = "GROUP BY tipoDeVivienda_exp";
    let valueKey = "tipoDeVivienda_exp";
    let textKey = "tipoDeVivienda_exp";
    let selectId = "tipos";

    await llenarSelectores(dataForName, whereName, valueKey, textKey, selectId, proyectosString)

}

async function estados() {

    const urlParams = new URLSearchParams(window.location.search);
    const proyectosValues = urlParams.getAll('estados');
    const proyectosString = proyectosValues.map(value => `"${value}"`).join(',');
    let dataForName = "estado_exp";
    let whereName = "GROUP BY estado_exp;";
    let valueKey = "estado_exp";
    let textKey = "estado_exp";
    let selectId = "estados";
    await llenarSelectores(dataForName, whereName, valueKey, textKey, selectId, proyectosString)
}





async function cargartabla() {

    const urlParamsProyectos = new URLSearchParams(window.location.search);
    const proyectosValues = urlParamsProyectos.getAll('proyectos');
    const proyectosString = proyectosValues.length > 0 ? proyectosValues.map(value => `"${value}"`).join(',') : '';
    const consultaProyectos = proyectosString.length > 0 ? `AND proyectoPrincipal_exp IN (${proyectosString})` : '';


    // Bloque para 'estados'
    const urlParamsEstados = new URLSearchParams(window.location.search);
    const estadosValues = urlParamsEstados.getAll('estados');
    const estadosString = estadosValues.length > 0 ? estadosValues.map(value => `"${value}"`).join(',') : '';
    const consultaEstados = estadosString.length > 0 ? `AND estado_exp IN (${estadosString})` : '';

    // Bloque para 'tipos'
    const urlParamsTipos = new URLSearchParams(window.location.search);
    const tiposValues = urlParamsTipos.getAll('tipos');
    const tiposString = tiposValues.length > 0 ? tiposValues.map(value => `"${value}"`).join(',') : '';
    const consultaTipos = tiposString.length > 0 ? `AND tipoDeVivienda_exp IN (${tiposString})` : '';




    const dataFor = "*";
    const tableS = "expedientes";
    const where = `WHERE ID_interno_expediente !=0 ${consultaProyectos} ${consultaEstados} ${consultaTipos}`;


    if (consultaProyectos === '' && consultaEstados === '' && consultaTipos === '') {

    } else {


        

        const result = await fetchRecords(dataFor, tableS, where, "select", "", 2);
        result.data.forEach((item) => {
            const tableBody = document.querySelector("#table-body");

            const row = document.createElement("tr");
            row.innerHTML = `
                <td hidden style="text-align: left; font-weight: bold;" > ${item.ID_interno_expediente}</td>
                <td style="text-align: left; font-weight: bold;">${item.codigo_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.proyectoPrincipal_exp}</td>
                <td style="text-align: left; font-weight: bold;">${item.tipoDeVivienda_exp}</td>
                <td style="text-align: left; font-weight: bold;">${item.precioVentaUncio_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.estado_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.entregaEstimada}</td>
                <td style="text-align: left; font-weight: bold;" >${item.areaTotalM2_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.loteM2_exp}</td>
                <td style="text-align: left; font-weight: bold;">${item.areaDeParqueoAprox}</td>
                <td style="text-align: left; font-weight: bold;" >${item.areaDeBodegaM2_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.areaDeMezzanieM2_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.areacomunLibe_exp}</td>
                <td style="text-align: left; font-weight: bold;" >${item.precioDeVentaMinimo}</td>
            `;

            // Agregamos los datos de 'row' al 'tableBody'
            tableBody.appendChild(row);

        });
        var columnsDatatable = [1,2,3,5,8];
        var columsDatatableExel = [1,2,3,4,5,6,7,8,9,10,11,12,13]
        var table = $("#datatable-buttons").DataTable({
            initComplete: function () {
                $("#preloader").hide();
            },
            processing: true,
            searchPanes: {
                cascadePanes: true,
                dtOpts: {
                    select: {
                        style: "multi",
                    },
                    count: {
                        show: false,
                    },
                },
                columns: columnsDatatable,
            },
            columnDefs: [
                {
                    searchPanes: {
                        show: true,
                        initCollapsed: true,
                    },
                    targets: columnsDatatable,
                },
            ],
            dom: "PBfrtip",
            stateSave: true,
            order: [[8, "asc"]],
            pageLength: 59,
            lengthMenu: [
                [10, 25, 50, 200, -1],
                [10, 25, 50, 200, "All"],
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Mostrar _MENU_ registros",
                zeroRecords: "No se encontraron resultados",
                info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                infoFiltered: "(filtrado de un total de _MAX_ registros)",
                sSearch: "Buscar:",
                oPaginate: {
                    sFirst: "Primero",
                    sLast: "Último",
                    sNext: "Siguiente",
                    sPrevious: "Anterior",
                },
                sProcessing: "Cargando...",
            },
            buttons: [
                {
                    extend: "excel",
                    footer: true,
                    titleAttr: "Exportar a excel",
                    className: "btn btn-success",
                    exportOptions: {
                        columns: columsDatatableExel,
                    },
                },
            ],
        });

        // $('#datatable-buttons tbody').on('click', 'tr', function () {
        //     var data = table.row(this).data();
        //     var id = data[0]; // Suponiendo que el ID está en la primera columna, puedes ajustar esto según tu estructura de datos

        //     window.location.href = 'https://proveedores-sambox.roccacr.com/login?id=' + id;
        // });
  
    }
}


async function cargar() {
    //Modal poara esperar a que cargen los datos
    mostrarSwalopacidadDiv();
    await proyectos();
    await modelo();
    await estados();
    await cargartabla();
    Swal.close();
}
cargar();




