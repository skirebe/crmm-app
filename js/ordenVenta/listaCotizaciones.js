

//darle formato de fecha  inicial
$('#startFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//darle formato de fecha  inicial
$('#endtFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//didentificamos si esta iniciando sesion en un movil o navegador
var element = document.getElementById("collapseExample");
if (esDispositivoMovil()) {
    element.classList.remove("show");
}







// Comentario explicando la función
async function ExtraDataCotizaciones() {

    //Modal poara esperar a que cargen los datos
    mostrarSwalopacidadDiv();


    //funcion para darle formato a la fecha definida
    function formatDate(date, days) {
        const formattedDate = new Date(date);
        return formattedDate.toISOString().split('T')[0];
    }


    //validamos cual es el tipo de filtro a definir
    const selectedOption = $('#radio1').prop('checked') ? 1 : ($('#radio2').prop('checked') ? 2 : 0);
    //definmos el filtro segun la base de datos
    // const dateField = selectedOption === 1 ? 'l.creado_lead' : 'l.actualizadaaccion_lead';


    //vamos a extraer solo los datos necesarios para acelerar la consulta
    const data = 'o.status_ov,o.id_ov_tranid, o.id_ov_netsuite, l.nombre_lead,l.idinterno_lead, p.tranid_oport,p.id_oportunidad_oport,e.idEstimacion_est,e.tranid_est,ex.ID_interno_expediente,ex.codigo_exp,o.creado_ov';

    //definimos la tabla y las relaciones para unir tablas
    const table = 'ordenventa as o INNER JOIN leads as l ON l.idinterno_lead  = o.id_ov_lead  INNER JOIN oportunidades as p ON p.id_oportunidad_oport = o.id_ov_opt INNER JOIN estimaciones as e ON e.idEstimacion_est = o.id_ov_est  INNER JOIN expedientes as ex ON ex.ID_interno_expediente   = o.idExpediente_ov ';

    //aun no sabemos cual es el where por eso esta vacio
    let where = '';


    //extraemos la fecha inicio y fecha final
    const startDate = formatDate($("#startFilter").val(), 1);
    const endDate = formatDate($("#endtFilter").val(), -1);

    //ahora si creamos el where de la consulta segun el parametro de la url
    const filterConditions = {
        //todos los leads
        1: `and o.caida_ov = 0 AND o.comision_cancelada_ov = 0 AND o.status_ov = 1 and o.contrado_frima_ov = 0`,
        2: `and o.caida_ov = 0 AND o.comision_cancelada_ov = 0 AND o.contrado_frima_ov = 1 AND o.cierre_firmado_ov = 1 AND o.aprobacion_forma_ov = 1 AND o.aprobacion__rdr_ov = 1 AND o.calculo_comision_asesor_ov = 1 and o.status_ov=1`,
    };


    //recorremos la lista y asignamos el nnuevo valor a where
    where = filterConditions[dataParam1] || filterConditions.default;
    /*
    llamamos la funcion que se encarga de ir al api a extraer los datos
    *Data? se encarga de extraer solo los campos que nececitamos
    *table? tabla + relaciones
    *"select"? metodo o tipo de consulta ejemplo SELECT * FROM ?
    *"id_empleado_lead"? extraemos todos los datos que pertenescan al vendedor
    *1? es si requiero que el select extraiga datos del vendedor si es 2 entonces no.
    */
    const result = await fetchRecords(data, table, where, "select", "id_ov_admin ", 1);
    console.log("where: ", result);



    //definimos un contadoe para saber cuantas lineas son
    let contador = 0;


    //recorremos los datos y segun la data de la url y vista mostramos los datos
    async function procesarDatos() {
        return new Promise(async (resolve) => {
            // validamos que sea mayor a cero
            if (result.data && result.data.length > 0) {
                //recorremos data
                result.data.forEach(async (data, index) => {
                    //si es 1 entonces ejecutamos el where 1 para mostrar esa consulta
                    createRow(data);

                    //esperamos a que se cumpla la promesa
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));
                    if (index === result.data.length - 1) {
                        resolve();
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }
    //ejecutamos esa funcion y luego cargamos la tabla con los datos llamados
    procesarDatos().then(() => {
        //al terminar el then ejecutamos datatable
        $(document).ready(function () {

            //columas a definir para el filtro y descargar en excel
            var columnsDatatable = [0, 1,2, 3, 4];
            var columsDatatableExel = [0, 1, 2, 3, 4, 5];


            var table = $("#datatable-buttons").DataTable({
                initComplete: function () {
                    $("#preloader").hide();
                },
                processing: true,
                searchPanes: {
                    cascadePanes: true,
                    dtOpts: {
                        select: {
                            style: "multi",
                        },
                        count: {
                            show: false,
                        },
                    },
                    columns: columnsDatatable,
                },
                columnDefs: [
                    {
                        searchPanes: {
                            show: true,
                            initCollapsed: true,
                        },
                        targets: columnsDatatable,
                    },
                ],
                dom: "PBfrtip",
                stateSave: true,
                order: [[0, "asc"]],
                pageLength: 59,
                lengthMenu: [
                    [10, 25, 50, 200, -1],
                    [10, 25, 50, 200, "All"],
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "No se encontraron resultados",
                    info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                    infoFiltered: "(filtrado de un total de _MAX_ registros)",
                    sSearch: "Buscar:",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior",
                    },
                    sProcessing: "Cargando...",
                },
                buttons: [
                    {
                        extend: "excel",
                        footer: true,
                        titleAttr: "Exportar a excel",
                        className: "btn btn-success",
                        exportOptions: {
                            columns: columsDatatableExel,
                        },
                    },
                ],
            });

        });

        // Cambiar estilos de banner
        const changeBannerStyles = () => {
            setTimeout(() => {
                const tableleads = document.getElementById(`tableCotizaciones`);
                tableleads.style.display = "none";
                if (esDispositivoMovil()) {
                    var elemento = document.getElementById("table-body");
                    elemento.style.opacity = "20";
                } else {
                    var elemento = document.getElementById("table-body");
                    if (elemento) {
                        elemento.style.display = "";
                    }
                }
                Swal.close();
            }, 100);


        };

        // Cambiar estilos de banner
        changeBannerStyles();
    });
}

// Esta función se encarga de llenar la tabla con los datos del resultado
function createRow(leadData) {
    // Construimos un arreglo de las variables
    const { status_ov, id_ov_tranid, id_ov_netsuite, nombre_lead, idinterno_lead, tranid_oport, id_oportunidad_oport, idEstimacion_est, tranid_est, ID_interno_expediente, codigo_exp, creado_ov } = leadData;

    let fech = creado_ov ? new Date(creado_ov) : new Date();

    // Formateamos la fecha
    const fechaActualizada = new Date(fech);
    const anio = fechaActualizada.getFullYear();
    const mes = fechaActualizada.getMonth() + 1;
    const dia = fechaActualizada.getDate();
    const fechaFormateada = `${anio} -${mes.toString().padStart(2, '0')} -${dia.toString().padStart(2, '0')} `;

    const tableBody = document.querySelector("#table-body");
    // Cantidad de td para llenar la tabla
    const row = document.createElement("tr");
    row.innerHTML = `
    <td style="text-align: left; font-weight: bold;"><a href='orden/view?data=${idinterno_lead}&data2=${id_ov_netsuite}'>${id_ov_tranid}</a> </td>
    <td style="text-align: left; font-weight: bold;"><a href='leads/perfil?data=${idinterno_lead}'>${nombre_lead}</a></td>
    <td style="text-align: left; font-weight: bold;"><a href='oportunidad/oprt_view?data=${idinterno_lead}&data2=${id_oportunidad_oport}'>${tranid_oport}</a></td>
    <td style="text-align: left; font-weight: bold;"><a href='estimaciones/view?data=${idinterno_lead}&data2=${idEstimacion_est}'>${tranid_est}</a></td>
    <td style="text-align: left; font-weight: bold;">${codigo_exp}</td>
    <td style="text-align: left; font-weight: bold;">${fechaFormateada}</td>
`;

    // Agregamos los datos de 'row' al 'tableBody'
    tableBody.appendChild(row);


}


