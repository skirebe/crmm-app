async function cierrecaida() {
    var exp_correo = $("#exp_correo").val();
    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    try {
        const result = await Swal.fire({
            title: '¿Está seguro?',
            text: "¿Desea enviar el correo de RESERVA CAIDA?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, enviar'
        });

        if (result.isConfirmed) {
            // Mostrar el cuadro de diálogo Swal con un textarea
            const { value: formValues } = await Swal.fire({
                width: '900px',
                title: 'Reserva Caida: ' + exp_correo,
                html:
                    '<label for="swal-textarea">Comentario de caida</label>' +
                    '<textarea id="swal-textarea" class="swal2-textarea" style="width: 100%; padding: 10px; box-sizing: border-box;"></textarea>',
                focusConfirm: false,
                preConfirm: () => {
                    const textareaValue = document.getElementById('swal-textarea').value;
                    return textareaValue;
                }
            });

            if (formValues) {
                mostrarSwal();
                Swal.close();

                try {
                    async function mostrarMensaje() {
                        // Verificar si el usuario está en un dispositivo móvil (celular)
                        const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

                        if (isMobile) {
                            // Si está en un dispositivo móvil, mostrar un cuadro de diálogo de confirmación personalizado
                            const respuesta = confirm(
                                "No se puede enviar la reserva caída desde un dispositivo móvil. Solo se puede enviar desde una computadora.\nPresiona 'Aceptar' para recordarlo más tarde."
                            );

                            if (!respuesta) {
                                alert("Recuerda enviar el correo más tarde.");
                            }
                        } else {
                            // Si no está en un dispositivo móvil, abrir el cliente de correo predeterminado
                            const destinatario = 'formalizacion@roccacr.com';
                            const asunto = 'Reserva Caida: ' + exp_correo;
                            const cuerpo = formValues;
                            const mensajeCorreo = `Buen día compañeras,\n\nEspero que se encuentren bien. Les comento que la siguiente venta, ${exp_correo} ha sido cancelada debido a ${cuerpo}.\n\nSaludos cordiales,`;
                            const mailtoLink = `mailto:${destinatario}?subject=${encodeURIComponent(asunto)}&body=${encodeURIComponent(mensajeCorreo)}`;
                            // Abrir el cliente de correo predeterminado del usuario
                            window.location.href = mailtoLink;
                            Swal.close();
                            // Preguntar si el correo fue enviado después de redirigir al cliente de correo
                            const confirmacion = confirm("¿Has enviado el correo?\n\nHaz clic en 'Aceptar' si lo enviaste o en 'Cancelar' si aún no lo has enviado.");

                            if (confirmacion) {
                                alert("Correo enviado y reserva caída con éxito. " + exp_correo);
                                const dataValueA = new URLSearchParams(new URL(window.location.href).search).get('data2');

                                const today = new Date();
                                const formattedDate = today.toISOString().split('T')[0];
                                const data = `caida_ov=1,envioReservaCaida="${formattedDate}"`;
                                const table = "ordenventa";
                                const query = `WHERE id_ov_netsuite ="${dataValue}";`;
                                const result = await fetchRecords(data, table, query, "update", "", 2);

                                // Forzar un recargado completo de la página
                                window.location.reload(true);

                            } else {
                                alert("Recuerda enviar el correo más tarde. " + exp_correo);
                            }
                        }

                        // Forzar un recargado completo de la página
                        window.location.reload(true);
                    }


                    // Llamar a la función cuando sea apropiado, por ejemplo, al hacer clic en un botón.
                    mostrarMensaje();


                } catch (error) {
                    Swal.fire({
                        title: 'Error',
                        text: 'No se pudo procesar la consulta, inténtelo de nuevo o bien comuníquese con soporte',
                        icon: 'error',
                        confirmButtonColor: '#d33',
                        confirmButtonText: 'Aceptar'
                    });
                    console.error(error);
                }
            }

        } else {
            const selectElement = document.getElementById('swal-select');
            selectElement.addEventListener('invalid', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: 'Error',
                    text: 'Debes seleccionar un motivo de caída',
                    icon: 'error'
                });
            });
        }
    } catch (error) {
        console.error(error);
    }
}
