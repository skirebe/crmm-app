async function generarOrdenDeVenta() {
    const est = new URLSearchParams(new URL(window.location.href).search).get('data2');
    const lead = new URLSearchParams(new URL(window.location.href).search).get('data');
    mostrarSwal();
    try {
        const response = await fetch("https://api-crm.roccacr.com/api/v2/orden/recordTraform", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id: est })
        });

        const result = await response.json();
        const ExTraerResultado = result['Detalle'];
        const datas_train = JSON.parse(result['Detalle'].msg);

        if (ExTraerResultado.status === 200) {
            const id_orden = ExTraerResultado.id;

            const tranid = datas_train.tranid || 0;
            const opportunityInternalId = datas_train.opportunity ? datas_train.opportunity.internalid || 0 : 0;
            const employeeInternalId = datas_train.salesteam && datas_train.salesteam[0] ? datas_train.salesteam[0].employee.internalid || 0 : 0;
            const entityInternalId = datas_train.entity ? datas_train.entity.internalid || 0 : 0;
            const custbody38Value = datas_train.custbody38 ? datas_train.custbody38.internalid || 0 : 0;
            const custbody17Value = datas_train.custbody17 ? datas_train.custbody17.internalid || 0 : 0;
            const subsidiaryInternalId = datas_train.subsidiary ? datas_train.subsidiary.internalid || 0 : 0;


            guardaEstimacion(tranid, id_orden, est, opportunityInternalId, employeeInternalId, entityInternalId, custbody38Value, subsidiaryInternalId, lead, custbody17Value);
        } else {
            Swal.fire('Alo no esta bien.', 'No se pudo crear la orden de venta, verifica con el administrador de sistemas', 'question');
        }
    } catch (error) {
        alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comunícate con soporte');
        console.error(error);
    }
}

async function guardaEstimacion(tranid, id_orden, id_est, opportunityInternalId, employeeInternalId, entityInternalId, custbody38Value, subsidiaryInternalId, lead, custbody17Value) {

    try {
        const data = {
            tranid,
            id_orden,
            id_est,
            opportunityInternalId,
            employeeInternalId,
            entityInternalId,
            custbody38Value,
            subsidiaryInternalId,
            custbody17Value
        };
        const response = await fetch("https://api-crm.roccacr.com/api/v2/orden/GuardarEstimacion", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });



        const results = await response.json();
        if (results.status == 200) {
            window.location.href = '/orden/view?data=' + lead + '&data2=' + id_orden;
        }
        if (results.status == 500) {
            Swal.fire('Alo no esta bien.', 'No se pudo crear la orden de venta, verifica con el administrador de sistemas', 'question');
        }
    } catch (error) {
        alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comunícate con soporte');
        console.error(error);
    }
}
