async function reserva() {
    const NetsuiteId_acceso = $("#empleado_pre").val();
    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    const dataValue1 = new URLSearchParams(new URL(window.location.href).search).get('data');
    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Desea enviar la reserva?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, enviar'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwal();
            try {
                const data = {
                    id: dataValue
                };
                const response = await fetch("https://api-crm.roccacr.com/api/v2/orden/reserva", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });

                const result = await response.json();
                var ExTraerResultado = result['Detalle'];
                console.log(ExTraerResultado);
                if (ExTraerResultado.status === 200) {

                    // var segimineto_lead = '04-LEAD-RESERVA';
                    // var valor_segimineto_lead = 3;
                    // var descpDelEvento = "Se genero una RESERVA";
                    // var estado_lead = '1';
                    // var accion_lead = 6;
                    // var seguimiento_calendar = 1;
                    // var caida = "-1";
                    var id_admin = NetsuiteId_acceso;



                    const estado = "04-LEAD-RESERVA";
                    const valores = {
                        valorDeCaida: "55",
                        tipo: "Se envio la RESERVA",
                        estado_lead: 1,
                        accion_lead: 6,
                        seguimiento_calendar: 0,
                        valor_segimineto_lead: 3,
                    };
                    const descpDelEvento = "Se envio la RESERVA";

                    const StatusCodeBitacora = await BitacoraCliente(dataValue1, valores, descpDelEvento, estado);
                    if (StatusCodeBitacora.statusCode == 200) {
                        const StatusCodeCliente = await ActualizarCliente(dataValue1, valores, estado);
                        if (StatusCodeCliente.statusCode == 200) {


                            var fecha_prereserva = $("#pre_5").val() || "";
                            const today = new Date();
                            const formattedDate = today.toISOString().split('T')[0];
                            const data = `reserva_ov = 1, envioReserva="${formattedDate}",fechaClienteComprobante_ov="${fecha_prereserva}"`;
                            const table = "ordenventa"
                            const query = `WHERE id_ov_netsuite ="${dataParam2}";`;

                            const result = await fetchRecords(data, table, query, "update", "", 2);

                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se envió la Reserva',
                                showConfirmButton: false,
                                timer: 2500
                            });
                            location.reload();
                        }
                    }

                } else {
                    Swal.fire('Algo no está bien.', 'No se pudo hacer la reserva.', 'question');
                }
            } catch (error) {
                alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comuníquese con soporte');
                console.error(error);
            }
        }
    });
}
