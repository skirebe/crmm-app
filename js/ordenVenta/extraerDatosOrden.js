async function verOrdenDeVenta() {
    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    const idLeads = new URLSearchParams(new URL(window.location.href).search).get('data');



    const datasCorredor = `*`;
    const tableCorredor = "info_extra_lead i INNER JOIN corredores ON corredores.id_netsuiteCorredor = i.Corredor_lead"
    const queryCorredor = `WHERE id_lead_fk  = ${idLeads}`;
    const resultsCorredor = await fetchRecords(datasCorredor, tableCorredor, queryCorredor, "select", "", 2);


    if (resultsCorredor.statusCode == 200) {
        resultsCorredor.data.forEach(async (datasEx) => {
            if (datasEx.id_netsuiteCorredor > 0) {
                $("#corredorNombre").val(datasEx.nombre_corredor);
                $("#corredorID").val(datasEx.id_netsuiteCorredor);
                $("#corredor_extra").val(1);
       
            }
        });
    }







    mostrarSwal();
    try {
        const data = {
            id: dataValue
        };
        const response = await fetch("https://api-crm.roccacr.com/api/v2/orden/consult", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const result = await response.json();
        var ExTraerResultado = result['Detalle'];

        if (ExTraerResultado.status === 200) {
            var dataEs = ExTraerResultado.data;
            var fields = dataEs.fields;

            var pre_lista_ov = fields.custbody13;
            var prec_ventaneto_ov = fields.custbody17;
            var prec_venta_ov = fields.custbody_ix_total_amount;
            var comison_asesor_ov = fields.custbody21;
            var ubicacion = ExTraerResultado.Ubi;
            var cliente = ExTraerResultado.cli;


            var trandate = fields.trandate; // Supongamos que trandate es "31/08/2023"
            var parts = trandate.split('/'); // Divide la fecha en partes: ["31", "08", "2023"]
            var formattedDate = parts[2] + '-' + parts[1] + '-' + parts[0]; // Formatea la fecha como "2023-08-31"

            $.ajax({
                url: "/controllers/ordenes/data/extraercampos.php",
                type: 'POST',
                data: {
                    dataValue: dataValue,
                    trandate: formattedDate,
                    pre_lista_ov: pre_lista_ov,
                    prec_ventaneto_ov: prec_ventaneto_ov,
                    prec_venta_ov: prec_venta_ov,
                    comison_asesor_ov: comison_asesor_ov,
                    ubicacion: ubicacion,
                    cliente: cliente
                }
            }).done(function (resp) {
                // alert(resp)
                // // You can handle the response here
                // // For example, you might want to log the response or perform some action
            });

















            $("#div_custbody75").text(" " + (ExTraerResultado.METODO_PAGO || "--").replace(/"/g, ""));
            $("#div_leadsource").text(" " + (ExTraerResultado.CAMPANA || "--").replace(/"/g, ""));
            $("#div_custbody37").text(" " + (ExTraerResultado.FONDOS || "--").replace(/"/g, ""));
            $("#div_partner").text(" " + (ExTraerResultado.socio || "--").replace(/"/g, ""));
            $("#div_custbody115").text(" " + (ExTraerResultado.MOTIVO_CANCE || "--").replace(/"/g, ""));
            $("#div_expediente").text(" " + (ExTraerResultado.Expediente || "--").replace(/"/g, ""));
            $("#div_salesrep").text(" " + (ExTraerResultado.vendedor || "--").replace(/"/g, ""));
            $("#div_saleseffectivedate").text(" " + (fields.saleseffectivedate || "--").replace(/"/g, ""));
            $("#div_entityname").text(" " + (fields.entityname || "--"));
            $("#div_custbody68").text(" " + (fields.custbody68 || "--"));
            $("#div_trandate").text(" " + (fields.trandate || "--"));
            $("#div_entityname2").text(" " + (fields.entityname || "--"));
            $("#div_custbody114").text(" " + (fields.custbody114 || "--"));
            $("#div_custbody_ix_total_amount").text(" " + (fields.custbody_ix_total_amount || "--"));
            $("#div_custbody116").text(" " + (fields.custbody116 || "--"));
            $("#div_custbody13").text(" " + (fields.custbody13 || "--"));
            $("#div_custbody46").text(" " + (fields.custbody46 || "--"));
            $("#div_custbody20").text(" " + (fields.custbody20 || "--"));
            $("#div_custbody22").text(" " + (fields.custbody22 || "--"));
            $("#div_custbody47").text(" " + (fields.custbody47 || "--"));
            $("#div_custbody21").text(" " + (fields.custbody21 || "--"));
            $("#div_custbody14").text(" " + (fields.custbody14 || "--"));
            $("#div_custbodyix").text(" " + (fields.custbodyix_salesorder_cashback || "--"));
            $("#div_custbody71").text(" " + (fields.custbody71 || "--"));
            $("#div_custbody15").text(" " + (fields.custbody15 || "--"));
            $("#div_custbody17").text(" " + (fields.custbody17 || "--"));
            $("#div_custbody18").text(" " + (fields.custbody18 || "--"));
            $("#div_custbody188").text(" " + (ExTraerResultado.METODO_PAGO || "--").replace(/"/g, ""));
            $("#div_custbody189").text(" " + (fields.custbody189 || "--"));
            $("#div_custbody207").text(" " + (fields.custbody207 || "--"));
            $("#div_custbody208").text(" " + (fields.custbody208 || "--"));
            $("#div_custbody191").text(" " + (fields.custbody191 || "--"));
            $("#div_custbody206").text(" " + (fields.custbody206 || "--"));
            $("#div_custbody190").text(" " + (fields.custbody190 || "--"));
            $("#div_custbody16").text(" " + (fields.custbody16 || "--"));
            $("#div_custbody19").text(" " + (fields.custbody19 || "--"));
            $("#div_custbody35").text(" " + (fields.custbody35 || "--"));
            $("#div_exchangerate").text(" " + (fields.exchangerate || "--"));
            $("#div_custbody132").text(" " + (fields.custbody132 || "--"));
            $("#div_custbody39").text(" " + (fields.custbody39 || "--"));
            $("#div_custbody52").text(" " + (fields.custbody52 || "--"));
            $("#div_memo").text(" " + (fields.memo || "--"));



            var precio_de_lista = Number(fields.custbody13) || 0;
            var descuento_directo = Number(fields.custbody132) || 0;
            var extrasPagadasPorelcliente = Number(fields.custbody46) || 0;
            var cashback = Number(fields.custbodyix_salesorder_cashback) || 0;
            var monto_de_cortecias = Number(fields.custbody16) || 0;

            var monto_total_precio_venta_neto = (precio_de_lista - Math.abs(descuento_directo) + extrasPagadasPorelcliente - cashback - monto_de_cortecias).toFixed(3);

            if (isNaN(monto_total_precio_venta_neto)) {
                monto_total_precio_venta_neto = "0";
            }

            $("#pvneto").val(monto_total_precio_venta_neto);
            $("#exp_correo").val(ExTraerResultado.Expediente);





            $("#custbodyix_salesorder_cashback").val((fields.custbodyix_salesorder_cashback || "0"));
            $("#custbody_ix_total_amount").val((fields.custbody_ix_total_amount || "0"));


            /*EXTRAS SOBRE EL PRECIO DE LISTA */
            var extra_sobre_precio_lista = Number(fields.custbody46) || 0;
            var descuento_directo = Number(fields.custbody132) || 0;

            var total_espl = (extra_sobre_precio_lista + descuento_directo).toFixed(3);

            var diferenciaInput = document.getElementById("diferecia");
            diferenciaInput.value = isNaN(total_espl) ? "0" : total_espl;


            //medio de pago
            var pagos = fields.custbody188 || "0";
            const Pago = pagos;
            const selectElement = document.getElementById("custbody188");
            const optionValues = Array.from(selectElement.options).map(option => option.value);

            const index = optionValues.findIndex(optionValue => optionValue.includes(Pago));
            if (index !== -1) {
                selectElement.selectedIndex = index;
            }

            $("#pre_3").val((fields.custbody189 || "0"));
            $("#custbody191").val((fields.custbody191 || "0"));


            $("#pre_8").val((fields.custbody190 || "0"));


            $("#custbody13").val(((fields.custbody13 || "0")));
            $("#custbody132").val(((fields.custbody132 || "0")));
            $("#custbody46").val(((fields.custbody46 || "0")));
            $("#custbody47").val(((fields.custbody47 || "--")));
            $("#memo").val(((fields.memo || "--")));


            console.log("fields.saleseffectivedate: ", fields.saleseffectivedate);
            //ENTREGA ESTIMADA
            $("#custbody114").val(fields.custbody114);
            //FECHA DE VIGENCIA DE LA VENTA:
            $("#saleseffectivedate").val(moment(fields.saleseffectivedate, "DD/MM/YYYY").format("YYYY-MM-DD"));



            $("#pre_5").val(moment(fields.custbody208, "DD/MM/YYYY").format("YYYY-MM-DD"));

            $("#custbody20").val((fields.custbody20 || "").replace('%', ''));
            $("#custbody14").val((fields.custbody14 || "").replace('%', ''));





            $("#custbody207").val((fields.custbody207) || 0);




            $("#custbody52").val((fields.custbody52 || "0"));
            $("#custbody16").val((fields.custbody16 || "0"));
            $("#custbody35").val((fields.custbody35 || "0"));
            $("#custbody18").val((fields.custbody18 || "0"));
            $("#custbody116").val((fields.custbody116 || "-"));



            $("#div_custbody50").text(" " + (fields.custbody50 || "--"));
            const fieldsToCheck = [
                "custbody90",
                "custbody51",
                "custbody91",
                "custbody105",
                "custbody43",
                "custbody103",
                "custbody_ix_comision_pagada",
                "custbodyid_firma_rc",
                "custbodyid_firma_rocca",
                "custbody74",
                "custbody73",
                "custbody141",
                "custbody157"
            ];

            fieldsToCheck.forEach(field => {
                if (fields[field] === "T") {
                    var checkbox = document.getElementById(field);
                    checkbox.checked = true;
                }
            });


            var checkboxes = [
                { field: "custbody90", checkboxId: "val_custbody90" },
                { field: "custbody51", checkboxId: "val_custbody51" },
                { field: "custbody91", checkboxId: "val_custbody91" }
            ];

            checkboxes.forEach(function (item) {
                if (fields[item.field] === "T") {
                    var checkbox = document.getElementById(item.checkboxId);
                    if (checkbox) {
                        checkbox.checked = true;
                    }
                }
            });



            var fondos = fields.custbody37 || 0;
            if (fondos > 0) {
                const fondossele = fondos;
                const selectElementfondos = document.getElementById("custbody37");
                const optionValues = Array.from(selectElementfondos.options).map(option => option.value);

                const index = optionValues.findIndex(optionValue => optionValue.includes(fondossele));
                if (index !== -1) {
                    selectElementfondos.selectedIndex = index;
                }
            }


            var cancelacion = fields.custbody115 || 0;
            if (cancelacion > 0) {
                const asas = cancelacion;
                const selectElementfondos = document.getElementById("custbody115");
                const optionValues = Array.from(selectElementfondos.options).map(option => option.value);

                const index = optionValues.findIndex(optionValue => optionValue.includes(asas));
                if (index !== -1) {
                    selectElementfondos.selectedIndex = index;
                }
            }


            //RESREVA



            if (fields.custbody207 > 0 && fields.custbody207 !== '') {
                var div = document.getElementById("campos_prereserva");
                div.style.display = ""; // Mostrar el div

                var checkbox = document.getElementById("RESERVA");
                checkbox.checked = true;

                const valorReserva = document.getElementById("valor_reserva").value;


                if (valorReserva == 1) {
                    var campo = document.getElementById("boton_reserav");
                    campo.style.display = "none"; // Mostrar el div
                } else {
                    var campo = document.getElementById("boton_reserav");
                    campo.style.display = ""; // Mostrar el div
                }




                var boton_reserav_valido_campos = document.getElementById("boton_reserav_valido_campos");
                boton_reserav_valido_campos.style.display = "none"; // Mostrar el div


            } else {
                var div = document.getElementById("campos_prereserva");
                div.style.display = "none"; // Ocultar el div

                var checkbox = document.getElementById("RESERVA");
                checkbox.checked = false;

                const valorReserva = document.getElementById("valor_reserva").value;


                if (valorReserva == 1) {
                    var campo = document.getElementById("boton_reserav");
                    campo.style.display = "none"; // Mostrar el div
                } else {
                    var campo = document.getElementById("boton_reserav");
                    campo.style.display = "none"; // Mostrar el div
                }


                var boton_reserav_valido_campos = document.getElementById("boton_reserav_valido_campos");
                boton_reserav_valido_campos.style.display = ""; // Mostrar el div

            }




            if (fields.custbody51 == "T") {
                var campo = document.getElementById("boton_cierre_firmado_enviado");
                campo.style.display = ""; // Mostrar el div
            } else {
                var campo = document.getElementById("boton_cierre_firmado");
                campo.style.display = ""; // Mostrar el div
            }



            //prima total
            $("#custbody39").val((fields.custbody39 || "0"));
            //prima%
            $("#custbody60").val((fields.custbody60 || "0"));
            //MONTO PRIMA NETA
            $("#custbody_ix_salesorder_monto_prima").val((fields.custbody_ix_salesorder_monto_prima || "0"));
            //TRACTO
            $("#custbody40").val((fields.custbody40 || "0"));
            //MONTO ASIGNABLE PRIMA NETA:
            $("#neta").val((fields.custbody211 || "0"));

            if (fields.custbody75) {
                var metodos = fields.custbody75 || "0";
                const metodo = metodos;
                const selectElement = document.getElementById("custbody75");
                const optionValues = Array.from(selectElement.options).map(option => option.value);

                const index = optionValues.findIndex(optionValue => optionValue.includes(metodo));
                if (index !== -1) {
                    selectElement.selectedIndex = index;
                }
            }


            var sublists = dataEs.sublists;
            const tableBody = document.getElementById("table-body");
            let contador = 0;

            for (const key in sublists.item) {
                if (key.startsWith("line")) {
                    const line = sublists.item[key];
                    const { rate, custcol_porcentajediferenciadocrm, custcol_indentificadorprima, amount, custcolfecha_pago_proyectado, item_display, quantity, description, item } = line;
                    contador++;


                    var numeroConComas = custcol_indentificadorprima;
                    var numeroSinComas;

                    if (numeroConComas !== null) {
                        numeroSinComas = parseInt(numeroConComas.replace(/,/g, ''), 10);
                    } else {
                        // Valor predeterminado si custcol_indentificadorprima es null
                        numeroSinComas = 0;
                    }

                    if (numeroSinComas === 551) {
                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("fech_reserva");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;
                    }





                    if (numeroSinComas === 5551) {
                        var checkbox = document.getElementById("chec_fra");
                        checkbox.checked = true;

                        document.getElementById("f_0").style.display = "";
                        document.getElementById("fraccionado").style.display = "";
                        $("#custbody179").val(rate || "0");
                        $("#custbody180").val(quantity || "0");


                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody179_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody193").val(description || "0");
                    }

                    if (numeroSinComas === 5552) {
                        var checkbox = document.getElementById("chec_uica");
                        checkbox.checked = true;

                        document.getElementById("u_0").style.display = "";
                        document.getElementById("unico").style.display = "";
                        $("#custbody181").val(amount || "0");
                        $("#custbody182").val(quantity || "0");


                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody182_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody194").val(description || "0");
                    }

                    if (numeroSinComas === 5553) {
                        var checkbox = document.getElementById("chec_extra");
                        checkbox.checked = true;

                        document.getElementById("o_0").style.display = "";
                        document.getElementById("extra").style.display = "";
                        $("#custbody183").val(amount || "0");
                        $("#custbody184").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195").val(description || "0");
                    }


                    if (numeroSinComas === 5554) {
                        var checkbox = document.getElementById("chec_extra_uno");
                        checkbox.checked = true;

                        document.getElementById("o_Uno").style.display = "";
                        document.getElementById("ex_uno").style.display = "";
                        $("#o_2_uno_input").val(amount || "0");
                        $("#custbody184_uno").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_uno_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_uno").val(description || "0");
                    }

                    if (numeroSinComas === 5555) {
                        var checkbox = document.getElementById("chec_extra_dos");
                        checkbox.checked = true;

                        document.getElementById("o_dos").style.display = "";
                        document.getElementById("ex_dos").style.display = "";
                        $("#o_2_dos_input").val(amount || "0");
                        $("#custbody184").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_dos_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_dos").val(description || "0");
                    }
                    if (numeroSinComas === 5556) {
                        var checkbox = document.getElementById("chec_extra_tres");
                        checkbox.checked = true;

                        document.getElementById("o_tres").style.display = "";
                        document.getElementById("ex_tres").style.display = "";
                        $("#o_2_tres_input").val(amount || "0");
                        $("#custbody184_tres").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_tres_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_tres").val(description || "0");
                    }

                    if (numeroSinComas === 5557) {
                        var div = document.getElementById("text_contra");
                        div.style.display = ""; // Ocultar el div por defecto

                        var contra_entrega = document.getElementById("contra_entrega");
                        contra_entrega.style.display = ""; // Ocultar el div por defecto
                        $("#contra_enterega1").val(amount || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("contra_enterega11_date");
                        if (fechaPagoProyectado == null || fechaPagoProyectado === "") {
                            // Obtener la fecha actual
                            var fechaActual = new Date();
                            var dia = fechaActual.getDate().toString().padStart(2, '0');
                            var mes = (fechaActual.getMonth() + 1).toString().padStart(2, '0');
                            var anio = fechaActual.getFullYear();

                            fechaPagoProyectado = dia + '/' + mes + '/' + anio;
                        }
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;
                        $("#mspt_contra_entrega").val(fields.custbody163 || "0");
                    }


                    if (numeroSinComas === 11115 || numeroSinComas === 11116 || numeroSinComas === 111167 || numeroSinComas === 111168 ||
                        numeroSinComas === 111169 || numeroSinComas === 111170) {


                        var div = document.getElementById("text_obra");
                        div.style.display = ""; // Ocultar el div por defecto

                        var avance_obra = document.getElementById("avance_obra");
                        avance_obra.style.display = ""; // Ocultar el div por defecto

                        if (numeroSinComas === 11115) {
                            $("#avnace_obra_hito1").val(amount || "0");
                        }
                        if (numeroSinComas === 11116) {
                            $("#avnace_obra_hito2").val(amount || "0");
                        }
                        if (numeroSinComas === 111167) {
                            $("#avnace_obra_hito3").val(amount || "0");
                        }
                        if (numeroSinComas === 111168) {
                            $("#avnace_obra_hito4").val(amount || "0");
                        }
                        if (numeroSinComas === 111169) {
                            $("#avnace_obra_hito5").val(amount || "0");
                        }
                        if (numeroSinComas === 111170) {
                            $("#avnace_obra_hito6").val(amount || "0");
                        }

                        $("#obra_enterega").val(fields.custbody163 || "0");
                    }


                    if (numeroSinComas === 55565 || numeroSinComas === 55566 || numeroSinComas === 55567 || numeroSinComas === 55568 ||
                        numeroSinComas === 55569 || numeroSinComas === 55570) {

                        var text_avance = document.getElementById("text_avance");
                        text_avance.style.display = ""; // Ocultar el div por defecto

                        var avance_diferenciado = document.getElementById("avance_diferenciado");
                        avance_diferenciado.style.display = ""; // Ocultar el div por defecto
                        var amountFormateado = parseFloat(amount).toLocaleString('es-ES');
                        if (numeroSinComas === 55565) {
                            $("#avance_diferenciado_hito11").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito1").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito1_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;
                            var checkbox = document.getElementById("hito_1");
                            checkbox.checked = true;
                            var input1 = document.getElementById("avance_diferenciado_hito11");
                            var input2 = document.getElementById("avance_diferenciado_hito1");
                            var input3 = document.getElementById("avance_diferenciado_hito1_date");

                            input1.disabled = false;
                            input2.disabled = false;
                            input3.disabled = false;
                        }
                        if (numeroSinComas === 55566) {
                            $("#avance_diferenciado_hito12").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito2").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito2_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_2");
                            checkbox.checked = true;


                            var input11 = document.getElementById("avance_diferenciado_hito12");
                            var input21 = document.getElementById("avance_diferenciado_hito2");
                            var input31 = document.getElementById("avance_diferenciado_hito2_date");

                            input11.disabled = false;
                            input21.disabled = false;
                            input31.disabled = false;



                        }
                        if (numeroSinComas === 55567) {
                            $("#avance_diferenciado_hito13").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito3").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito3_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;



                            var checkbox = document.getElementById("hito_3");
                            checkbox.checked = true;

                            var input12 = document.getElementById("avance_diferenciado_hito13");
                            var input22 = document.getElementById("avance_diferenciado_hito3");
                            var input32 = document.getElementById("avance_diferenciado_hito3_date");

                            input12.disabled = false;
                            input22.disabled = false;
                            input32.disabled = false;


                        }
                        if (numeroSinComas === 55568) {
                            $("#avance_diferenciado_hito14").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito4").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito4_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_4");
                            checkbox.checked = true;
                            var input13 = document.getElementById("avance_diferenciado_hito14");
                            var input23 = document.getElementById("avance_diferenciado_hito4");
                            var input33 = document.getElementById("avance_diferenciado_hito4_date");

                            input13.disabled = false;
                            input23.disabled = false;
                            input33.disabled = false;
                            ;
                        }
                        if (numeroSinComas === 55569) {
                            $("#avance_diferenciado_hito15").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito5").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito5_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;


                            var checkbox = document.getElementById("hito_5");
                            checkbox.checked = true;

                            var input14 = document.getElementById("avance_diferenciado_hito15");
                            var input24 = document.getElementById("avance_diferenciado_hito5");
                            var input34 = document.getElementById("avance_diferenciado_hito5_date");
                            input14.disabled = false;
                            input24.disabled = false;
                            input34.disabled = false;
                        }
                        if (numeroSinComas === 55570) {
                            $("#avance_diferenciado_hito16").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito6").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito6_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_6");
                            checkbox.checked = true;
                            var input15 = document.getElementById("avance_diferenciado_hito16");
                            var input25 = document.getElementById("avance_diferenciado_hito6");
                            var input35 = document.getElementById("avance_diferenciado_hito6_date");


                            input15.disabled = false;
                            input25.disabled = false;
                            input35.disabled = false;
                        }

                        $("#avance_diferenciado_hito17").val(fields.custbody163 || "0");
                    }









                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td style="text-align: right;">${contador}</td>
                        <td style="text-align: right;">${item_display}</td>
                        <td style="text-align: right;">${rate}</td>
                        <td style="text-align: right;">${custcolfecha_pago_proyectado}</td>
                        <td style="text-align: right;">${quantity}</td>
                        <td style="text-align: right;">${description}</td>
                        `;

                    tableBody.appendChild(row);
                }
            }




            var partners = dataEs.fields.partnerid || "";
            if (partners) {
                $("#corredorIDNetsuite").val(partners);
            }






            $(document).ready(function () {
                var columnsDatatable = [];
                var columsDatatableExel = [];

                columnsDatatable = [1, 2, 3, 5];
                columsDatatableExel = [1, 2, 3, 5];



                var table = $("#datatable-buttons").DataTable({
                    initComplete: function () {
                        $("#preloader").hide();
                    },
                    searching: false, // Quita la funcionalidad de búsqueda
                    lengthChange: false, // Quita la opción de cambiar la cantidad de registros por página
                    processing: true,
                    stateSave: true,
                    order: [[1, "asc"]],
                    language: {
                        decimal: ",",
                        thousands: ".",
                        lengthMenu: "Mostrar _MENU_ registros",
                        zeroRecords: "No se encontraron resultados",
                        info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                        infoFiltered: "(filtrado de un total de _MAX_ registros)",
                        sSearch: "Buscar:",
                        oPaginate: {
                            sFirst: "Primero",
                            sLast: "Último",
                            sNext: "Siguiente",
                            sPrevious: "Anterior",
                        },
                        sProcessing: "Cargando...",
                    },
                    buttons: [
                        {
                            extend: "excel",
                            footer: true,
                            titleAttr: "Exportar a excel",
                            className: "btn btn-success",
                            exportOptions: {
                                columns: columsDatatableExel,
                            },
                        },
                    ],
                });

            });

            var exped = fields.custbody38;
    

            const datas = `precioDeVentaMinimo`;
            const tables = "expedientes"
            const querys = `WHERE ID_interno_expediente= ${exped}`;
            const result = await fetchRecords(datas, tables, querys, "select", "", 2);
            result.data.forEach(async (datas) => {
                $("#custbody18").val(datas.precioDeVentaMinimo);
            });



            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Datos de la Ov',
                showConfirmButton: false,
                timer: 1500
            })
        } else {
            Swal.fire('Alo no esta bien.', 'Ese registro no existe.', 'question');
        }
    } catch (error) {
        alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comunícate con soporte');
        console.error(error);
    }
}



/* OCULTAR Y MOSTAR CAMPOS DE PRE RESERVA*/
var div = document.getElementById("campos_prereserva");
div.style.display = "none"; // Ocultar el div
function toggleDiv() {
    var checkbox = document.getElementById("RESERVA");
    var div = document.getElementById("campos_prereserva");
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
    }
}


/*ASIGNACIONES DE CAMPOS AL CARGAR LA VISTA*/
/*custbody60 -> PRIMA%*/
var custbody60 = document.getElementById('custbody60')
var custbody39 = document.getElementById('custbody39')
document.getElementById("custbody60").value;




/* ***************************************************************************************************** */
/* Obtener el valor de los botones de opción. */
var discounted = document.getElementById('isDiscounted');
var no_discounted = document.getElementById('isNotDiscounted')

/**=====================================================================================================
  Si la casilla de verificación está marcada, deshabilite el primer campo y habilite el segundo campo.
  Si la casilla de verificación no está marcada, habilite el primer campo y deshabilite el segundo campo.
 =====================================================================================================*/
function updateStatus() {
    if (discounted.checked) {
        custbody39.disabled = true;
        custbody60.disabled = false;

        //reseteamos el campo si seleciona monto oporcentaje
        document.getElementById("custbody60").value;

        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value));
        var porcentaje = parseFloat(document.getElementById("custbody60").value);
        var res = (lista * (porcentaje / 100)).toFixed(3);
        document.getElementById("custbody39").value = res;

    } else {
        custbody39.disabled = false;
        custbody60.disabled = true;

        //reseteamos el campo si seleciona monto oporcentaje
        document.getElementById("custbody60").value;

        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value));
        var porcentaje = parseFloat(document.getElementById("custbody60").value);
        var res = (lista * (porcentaje / 100)).toFixed(3);
        document.getElementById("custbody39").value = res;

    }
}
discounted.addEventListener('change', updateStatus)
no_discounted.addEventListener('change', updateStatus)
















/**=====================================================================================================
  FUNCION QUE SE ENCARGA DE CALCULAR TODOS LOS MONTOS Y REASIGNAR MONTOS PARA SU CALCULO
 =====================================================================================================*/
function calcular(input) {
    var valor = input.id;

    function calcularValor(func, field) {
        func(valor, field);
        PRIMA_NETA(valor, field);
        MONTO_PRIMA_ASIGNABLE_NETA(valor, field);
        HITOS_MONTO_SIN_PRIMA_TOTAL(valor, field);
    }

    /*MONTO DESCUENTO DIRECTO*/
    if (valor == "custbody132") {
        var valor = input.value.trim();
        // Validar si hay letras
        if (/[a-zA-Z]/.test(valor)) {
            alert("Por favor, asegúrate de que el campo sea un número negativo  y no contenga letras.");
            input.value = "";
            return;
        }
        // Validar si hay más de un guion
        if (valor.indexOf("-") !== valor.lastIndexOf("-")) {
            alert("Solo se permite un guion al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        // Validar si el guion no está al inicio
        if (valor.indexOf("-") !== 0 && valor.length > 0) {
            alert("El guion solo está permitido al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody132");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody132");
        MONTO_TOTAL(valor, "custbody132");
    }

    /*EXTRAS PAGADAS POR EL CLIENTE */
    if (valor == "custbody46") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody46");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody46");
        MONTO_TOTAL(valor, "custbody46");
    }

    /*CASHBACK*/
    if (valor == "custbodyix_salesorder_cashback") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbodyix_salesorder_cashback");
    }

    /*MONTO RESERVA*/
    if (valor == "custbody52") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody52");
        MONTO_RESERVA("custbody52");
    }

    /*MONTO TOTAL DE CORTESÍAS*/
    if (valor == "custbody16") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody16");
    }

    /*EXTRAS SOBRE EL PRECIO DE LISTA*/
    if (valor == "diferecia") {
        calcularValor(PRECIO_DE_VENTA_NETO, "diferecia");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "diferecia");
        MONTO_TOTAL(valor, "diferecia");
    }

    /*PRIMA TOTAL*/
    if (valor == "custbody39") {
        PRIMA_TOTAL_VALUE(input.value, "custbody39");
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody39");
    }

    if (valor == "custbody60") {
        PORCENTAJE_VALUE(input.value, "custbody60");
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody39");
    }


    if (valor == "custbody179" || valor == "custbody180" || valor == "custbody181" || valor == "custbody182" || valor == "custbody183" || valor == "custbody184" || valor == "o_2_uno_input" || valor == "custbody184_uno" || valor ==
        "o_2_dos_input" || valor == "custbody184_dos" || valor == "o_2_tres_input" || valor == "custbody184_tres") {
        MONTO_PRIMA_ASIGNABLE_NETA(input, valor);

        let valorCam = Number($("#avance_diferenciado_hito17").val().replace(',', '.'));

        let valorCam1 = Number($("#avance_diferenciado_hito11").val().replace(',', '.'));


        $("#avance_diferenciado_hito1").val((valorCam1 * valorCam).toFixed(0).replace('.', ','));
        let valorCam2 = Number($("#avance_diferenciado_hito12").val().replace(',', '.'));
        $("#avance_diferenciado_hito2").val((valorCam2 * valorCam).toFixed(0).replace('.', ','));

        let valorCam3 = Number($("#avance_diferenciado_hito13").val().replace(',', '.'));
        $("#avance_diferenciado_hito3").val((valorCam3 * valorCam).toFixed(0).replace('.', ','));

        let valorCam4 = Number($("#avance_diferenciado_hito14").val().replace(',', '.'));
        $("#avance_diferenciado_hito4").val((valorCam4 * valorCam).toFixed(0).replace('.', ','));

        let valorCam5 = Number($("#avance_diferenciado_hito15").val().replace(',', '.'));
        $("#avance_diferenciado_hito5").val((valorCam5 * valorCam).toFixed(0).replace('.', ','));

        let valorCam6 = Number($("#avance_diferenciado_hito16").val().replace(',', '.'));
        $("#avance_diferenciado_hito6").val((valorCam6 * valorCam).toFixed(0).replace('.', ','));

    }



    if (valor == "avance_diferenciado_hito11" || valor == "avance_diferenciado_hito12" || valor == "avance_diferenciado_hito13" || valor == "avance_diferenciado_hito14" || valor == "avance_diferenciado_hito15" || valor ==
        "avance_diferenciado_hito16") {

        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);

        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        // var suma_hitos = ( avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16 ).toFixed( 2 );
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);

        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());

            /* Multiplicando el valor de `mspt` por el valor de `avance_diferenciado_hito11` y luego
            redondeando el resultado a 2 decimales. */
            var monto_hito = (mspt * input.value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            /* Poniendo el valor de la entrada con id `avance_diferenciado_hito1` al valor de `monto_hito`. */

            if (valor == "avance_diferenciado_hito11") {
                Number(document.getElementById("avance_diferenciado_hito1").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito12") {
                Number(document.getElementById("avance_diferenciado_hito2").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito13") {
                Number(document.getElementById("avance_diferenciado_hito3").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito14") {
                Number(document.getElementById("avance_diferenciado_hito4").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito15") {
                Number(document.getElementById("avance_diferenciado_hito5").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito16") {
                Number(document.getElementById("avance_diferenciado_hito6").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'El Porcentaje total del monto sin prima total, debe estar entre 0.00 y 0.100 , o bien verificar si la suma de los hitos no supera el 100%',
                footer: ''
            })

        }






    }

    if (valor == "avance_diferenciado_hito1" || valor == "avance_diferenciado_hito2" || valor == "avance_diferenciado_hito3" || valor == "avance_diferenciado_hito4" || valor == "avance_diferenciado_hito5" || valor ==
        "avance_diferenciado_hito6") {


        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);


        let numero = parseFloat(input.value);
        let valorCam = Number($("#avance_diferenciado_hito17").val());
        if (valor == "avance_diferenciado_hito1") {
            $("#avance_diferenciado_hito11").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito2") {
            $("#avance_diferenciado_hito12").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito3") {
            $("#avance_diferenciado_hito13").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito4") {
            $("#avance_diferenciado_hito14").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito5") {
            $("#avance_diferenciado_hito15").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito6") {
            $("#avance_diferenciado_hito16").val((numero * 1000 / valorCam).toFixed(5));
        }

        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);



        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());
            var monto_hito = (mspt * avance_diferenciado_hito11).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'Asegúrate de que el valor del hito  esté correctamente formateado con comas para separar los miles. Además, verifica que la suma de todos los hitos no supere el 100% y que el valor del campo PORCENTAJE YA ASIGNADO sea menor a 1.00',
                footer: ''
            })

        }


    }
}



function PRIMA_TOTAL_VALUE(input, valor) {
    /*CALCULAMOS EL PORCENTAJE */
    var i = input;
    var a = Number(document.getElementById("custbody_ix_total_amount").value);
    rest = i / a;

    Number(document.getElementById("custbody60").value = (rest).toFixed(5));

}

function PORCENTAJE_VALUE(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_total_porcentaje = (monto_total * input).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_porcentaje));

}

/* CREAMOS LAS FUNCIONES QUE NOS VAN A CALCULAR LOS CAMPOS DEPENDIENDO DE CUAL EDITAMOS */
function PRECIO_DE_VENTA_NETO(input, valor) {


    /*CALCULAMOS EL PRECIO DE VENTA NETO */
    var precio_de_lista = Number(Number(document.getElementById("custbody13").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var extrasPagadasPorelcliente = Number(Number(document.getElementById("custbody46").value));
    var cashback = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var monto_de_cortecias = Number(Number(document.getElementById("custbody16").value));
    var monto_total_precio_venta_neto = (precio_de_lista - Math.abs(descuento_directo) + extrasPagadasPorelcliente - cashback - monto_de_cortecias).toFixed(3);
    Number(document.getElementById("pvneto").value = (monto_total_precio_venta_neto));

}


function EXTRAS_SOBRE_PRECIO_LISTA(input, valor) {


    /*EXTRAS SOBRE EL PRECIO DE LISTA */
    var extra_sobre_precio_lista = Number(Number(document.getElementById("custbody46").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var total_espl = (extra_sobre_precio_lista + descuento_directo).toFixed(3);
    Number(document.getElementById("diferecia").value = (total_espl));

}


function PRIMA_TOTAL(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_porcentaje = Number(Number(document.getElementById("custbody60").value));
    var prima_total_ex = (monto_total * prima_porcentaje).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_ex));

}

function MONTO_TOTAL(input, valor) {
    /* CALCULAMOS EL MONTO TOAL */
    var precio_de_lista_mo = Number(Number(document.getElementById("custbody13").value));
    var extra_sobre_precio_lista_mo = Number(Number(document.getElementById("diferecia").value));
    var calcula_monto_total = (precio_de_lista_mo + extra_sobre_precio_lista_mo).toFixed(2);
    Number(document.getElementById("custbody_ix_total_amount").value = (calcula_monto_total));

}



function PRIMA_NETA(input, valor) {
    /* Monto Prima Asignable Neta */
    var prima_total = Number(Number(document.getElementById("custbody39").value));
    var cash = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva = Number(Number(document.getElementById("custbody52").value));
    var total = (prima_total - cash - reserva).toFixed(2);
    Number($("#custbody_ix_salesorder_monto_prima").val(total));

}

function MONTO_RESERVA(valor) {
    var custbody52 = Number(document.getElementById("custbody52").value);
    var custbody191 = Number(document.getElementById("custbody191").value);
    var total = (custbody52 - custbody191).toFixed(2);
    $("#custbody207").val(total);
}

function MONTO_PRIMA_ASIGNABLE_NETA(input, valor) {
    /* PRIMA NETA */
    var prima_total_neta = Number(Number(document.getElementById("custbody39").value));
    var cashback_neta = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva_neta = Number(Number(document.getElementById("custbody52").value));


    //MONTOS PRIMAS UNICA
    var monto_prima_unico = Number(Number(document.getElementById("custbody181").value));
    var monto_tracto_unico = Number(Number(document.getElementById("custbody182").value));

    //MONTOS PRIMAS
    var monto_prima_fraccionado = Number(Number(document.getElementById("custbody179").value));
    var monto_tracto_fraccionado = Number(Number(document.getElementById("custbody180").value));


    //MONTOS PRIMAS
    var monto_prima_extra = Number(Number(document.getElementById("custbody183").value));
    var monto_tracto_extra = Number(Number(document.getElementById("custbody184").value));


    //MONTOS PRIMAS +1
    var monto_prima_extra_uno = Number(Number(document.getElementById("o_2_uno_input").value));
    var monto_tracto_extra_uno = Number(Number(document.getElementById("custbody184_uno").value));


    //MONTOS PRIMAS +2
    var monto_prima_extra_dos = Number(Number(document.getElementById("o_2_dos_input").value));
    var monto_tracto_extra_dos = Number(Number(document.getElementById("custbody184_dos").value));

    //MONTOS PRIMAS +3
    var monto_prima_extra_tres = Number(Number(document.getElementById("o_2_tres_input").value));
    var monto_tracto_extra_tres = Number(Number(document.getElementById("custbody184_tres").value));




    var total_neta = (prima_total_neta - cashback_neta - reserva_neta - (monto_prima_unico * monto_tracto_unico) - (monto_prima_fraccionado * monto_tracto_fraccionado) - (monto_prima_extra * monto_tracto_extra) - (monto_prima_extra_uno *
        monto_tracto_extra_uno) - (monto_prima_extra_dos * monto_tracto_extra_dos) - (monto_prima_extra_tres * monto_tracto_extra_tres)).toFixed(2);
    Number($("#neta").val(total_neta));


}






function HITOS_MONTO_SIN_PRIMA_TOTAL(input, valor) {
    var monto_total_sin_prima = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var monto_prima_total_sin_prima = Number(Number(document.getElementById("custbody39").value));
    var monto_sin_prima_total = (monto_total_sin_prima - monto_prima_total_sin_prima).toFixed(2);


    /* MONTO SIN PRIMA TOTAL   Avance De Obra*/
    Number(document.getElementById("obra_enterega").value = (monto_sin_prima_total));
    /*MONTO SIN PRIMA TOTAL Contra Entrega*/
    Number(document.getElementById("mspt_contra_entrega").value = (monto_sin_prima_total));
    /* MONTO SIN PRIMA TOTAL DIFERENCIADO */
    Number(document.getElementById("avance_diferenciado_hito17").value = (monto_sin_prima_total));

    var obra_enterega = Number(Number(document.getElementById("obra_enterega").value));
    // calculo de avance obra hitos del 1 al hito6
    var ao_hito6_rest = (obra_enterega * 0.05).toFixed(2);
    Number($("#avnace_obra_hito6").val(ao_hito6_rest));
    var ao_hito5_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito5").val(ao_hito5_rest));
    var ao_hito4_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito4").val(ao_hito4_rest));
    var ao_hito3_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito3").val(ao_hito3_rest));
    var ao_hito2_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito2").val(ao_hito2_rest));
    var ao_hito1_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito1").val(ao_hito1_rest));

    var mspt_contra_entrega = Number(Number(document.getElementById("mspt_contra_entrega").value));
    var contra = (mspt_contra_entrega * 1).toFixed(2);
    Number($("#contra_enterega1").val(contra));
}

function ocultarDiv(divId) {
    var div = document.getElementById(divId);
    div.style.display = "none"; // Ocultar el div
}
ocultarDiv("f_0");
ocultarDiv("fraccionado");
ocultarDiv("u_0");
ocultarDiv("unico");
ocultarDiv("o_0");
ocultarDiv("extra");
ocultarDiv("o_Uno");
ocultarDiv("ex_uno");
ocultarDiv("o_dos");
ocultarDiv("ex_dos");
ocultarDiv("o_tres");
ocultarDiv("ex_tres");




function toggleDivF(value, checkboxId, divId, iId) {
    var checkbox = document.getElementById(checkboxId);
    var div = document.getElementById(divId);
    var i = document.getElementById(iId);
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
        i.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
        i.style.display = "none"; // Mostrar el div
    }
}





/* OCULTANOS O MOSTRAMOS LOS DIV DE LOS DIFERENTES METODOS DE PAGO*/
function pagoOnChange(selectElement) {
    const selectedValue = selectElement.value;

    const elementos = [{
        id: "avance_diferenciado",
        textId: "text_avance",
        value: "7"
    }, {
        id: "avance_obra",
        textId: "text_obra",
        value: "2"
    }, {
        id: "contra_entrega",
        textId: "text_contra",
        value: "1"
    }];

    elementos.forEach(elemento => {
        const div = document.getElementById(elemento.id);
        const textDiv = document.getElementById(elemento.textId);

        if (selectedValue === elemento.value) {
            div.style.display = "";
            textDiv.style.display = "";
        } else {
            div.style.display = "none";
            textDiv.style.display = "none";
        }
    });
}

/* HABILITAR LOS CAMPOS DE LOS INPUTS DE AVANCE DIFERENCIADO*/
function habilitarCampos(checkbox, inputId1, inputId2, inputId3) {
    var input1 = document.getElementById(inputId1);
    var input2 = document.getElementById(inputId2);
    var input3 = document.getElementById(inputId3);

    if (checkbox.checked) {
        input1.disabled = false;
        input2.disabled = false;
        input3.disabled = false;
    } else {
        input1.disabled = true;
        input2.disabled = true;
        input3.disabled = true;
        // input1.value = "0.00";
        // input2.value = "0.00";


    }


    function removeCommasAndConvertToNumber(value) {
        return Number(value.replace(/[,]/g, ''));
    }

    var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
    var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
    var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
    var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
    var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
    var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

    // Sum all values
    var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

    var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

    // Ensure valorCam2 is a number
    valorCam2 = Number(valorCam2);

    // Subtract valorCam2 from the total sum
    var valortotal = (totalSum - valorCam2).toFixed(2);

    $("#valortotal").val(valortotal);
}
