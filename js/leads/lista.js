

//darle formato de fecha  inicial
$('#startFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//darle formato de fecha  inicial
$('#endtFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//didentificamos si esta iniciando sesion en un movil o navegador
var element = document.getElementById("collapseExample");
if (esDispositivoMovil()) {
    element.classList.remove("show");
}







// Comentario explicando la función
async function ExtraDataLead() {

    //Modal poara esperar a que cargen los datos
    mostrarSwalopacidadDiv();


    //funcion para darle formato a la fecha definida
    function formatDate(date, days) {
        const formattedDate = new Date(date);
        return formattedDate.toISOString().split('T')[0];
    }


    //validamos cual es el tipo de filtro a definir
    const selectedOption = $('#radio1').prop('checked') ? 1 : ($('#radio2').prop('checked') ? 2 : 0);
    //definmos el filtro segun la base de datos
    const dateField = selectedOption === 1 ? 'l.creado_lead' : 'l.actualizadaaccion_lead';


    //vamos a extraer solo los datos necesarios para acelerar la consulta
    const data = 'l.seguimiento_calendar, l.accion_lead, l.estado_lead, a.name_admin, l.estado_lead, l.nombre_lead, l.idinterno_lead, l.email_lead, l.telefono_lead, l.proyecto_lead, l.campana_lead, l.segimineto_lead, l.creado_lead, l.actualizadaaccion_lead, l.subsidiaria_lead, s.nombre_caida';

    //definimos la tabla y las relaciones para unir tablas
    const table = 'leads as l INNER JOIN caidas as s ON s.id_caida = l.id_Caida INNER JOIN admins as a ON a.idnetsuite_admin = l.id_empleado_lead';

    //aun no sabemos cual es el where por eso esta vacio
    let where = '';


    //extraemos la fecha inicio y fecha final
    const startDate = formatDate($("#startFilter").val(), 1);
    const endDate = formatDate($("#endtFilter").val(), -1);

    //ahora si creamos el where de la consulta segun el parametro de la url
    const filterConditions = {
        //si los leads estan repetidos
        10: 'AND email_lead IN (SELECT email_lead FROM leads WHERE estado_lead = 1 GROUP BY email_lead HAVING COUNT(*) > 1) AND l.estado_lead =1',
        //mostramos todos lo que requieren atencion y tengan 4 dias de seguimientos pero que no esten es esos estados
        3: `and accion_lead = 3 and estado_lead=1 and seguimiento_calendar=0 and actualizadaaccion_lead <= DATE_SUB(NOW(), INTERVAL 4 DAY) and segimineto_lead NOT IN ('02-LEAD-OPORTUNIDAD', '03-LEAD-PRE-RESERVA', '04-LEAD-RESERVA', '05-LEAD-CONTRATO', '06-LEAD-ENTREGADO')`,
        //todos los leads
        1: `AND estado_lead = 1 AND DATE(${dateField}) >= "${startDate}" AND DATE(${dateField}) <= "${endDate}"`,
        //todos los inactivos
        4: `AND estado_lead = 0 AND DATE(${dateField}) >= "${startDate}" AND DATE(${dateField}) <= "${endDate}"`,
        //todos los nuevos
        2: `AND accion_lead in (0,2) and estado_lead=1 and  segimineto_lead in ('01-LEAD-INTERESADO')`,
        //contacto masivo
        9: `AND accion_lead in (0,2) and estado_lead=1 and  segimineto_lead in ('01-LEAD-INTERESADO')`,
        default: `AND DATE(${dateField}) >= "${startDate}" AND DATE(${dateField})<= "${endDate}"`
    };


    //recorremos la lista y asignamos el nnuevo valor a where
    where = filterConditions[dataParam1] || filterConditions.default;
    /*
    llamamos la funcion que se encarga de ir al api a extraer los datos
    *Data? se encarga de extraer solo los campos que nececitamos
    *table? tabla + relaciones
    *"select"? metodo o tipo de consulta ejemplo SELECT * FROM ?
    *"id_empleado_lead"? extraemos todos los datos que pertenescan al vendedor
    *1? es si requiero que el select extraiga datos del vendedor si es 2 entonces no.
    */
    const result = await fetchRecords(data, table, where, "select", "id_empleado_lead", 1);


    //definimos un contadoe para saber cuantas lineas son
    let contador = 0;


    //recorremos los datos y segun la data de la url y vista mostramos los datos
    async function procesarDatos() {
        return new Promise(async (resolve) => {
            // validamos que sea mayor a cero
            if (result.data && result.data.length > 0) {
                //recorremos data
                result.data.forEach(async (data, index) => {
                    contador++;
                    let Ocultar = "#Ocultar" + contador;
                    let OcultarEsta = "#OcultarEsta" + contador;
                    const estado = data.estado_lead === 1 ? "ACTIVO" : "INACTIVO";

                    //si es 1 entonces ejecutamos el where 1 para mostrar esa consulta
                    if (dataParam1 == 1) {
                        //estado igual a un lead activo
                        if (data.estado_lead == 1) {
                            createRow(data, "lead_inactivos", contador, estado);
                            document.getElementById("Motivo").hidden = true;
                        }
                    }
                    //si es 4 entonces ejecutamos el where 4 para mostrar esa consulta
                    if (dataParam1 == 4) {
                        //estado igual a un lead inactivo
                        if (data.estado_lead == 0) {
                            createRow(data, "lead_activos", contador, estado);
                            console.log("data: ", data);
                            $("#Motivo").removeAttr("hidden").text("MOTIVO DE CAIDA");
                            $("#estado").removeAttr("hidden").text("ESTADO LEAD");
                            $(Ocultar).removeAttr("hidden");
                            $(OcultarEsta).removeAttr("hidden");
                        }
                    }

                    //si es 2 o 9  entonces ejecutamos el where por defecto para mostrar esa consulta
                    if (dataParam1 == 2 || dataParam1 == 9) {
                        //validmos todos los estados y que sean nuevos solamente
                        if (data.estado_lead == 1 && data.segimineto_lead === "01-LEAD-INTERESADO") {
                            createRow(data, "lead_activos", contador, estado);
                            document.getElementById("Motivo").hidden = true;
                        }
                    }

                    //si es 3 entonces ejecutamos el where 3 para mostrar todos los que requieren atencion
                    if (dataParam1 == 3) {
                        if (data.estado_lead === 1 && data.accion_lead === 3 && data.seguimiento_calendar === 0) {
                            createRow(data, "lead_activos", contador, estado);
                            $("#Motivo").removeAttr("hidden").text("SEGUIMIENTO");
                            $(Ocultar).removeAttr("hidden");
                        }
                    }

                    //Mostramos todos los lead activos inactivos
                    if (dataParam1 == 5) {
                        if (data.estado_lead == 0 || data.estado_lead == 1) {
                            createRow(data, "lead_activos", contador, estado);
                            $("#Motivo").removeAttr("hidden").text("MOTIVO");
                            $("#estado").removeAttr("hidden").text("ESTADO LEAD");
                            $(Ocultar).removeAttr("hidden");
                            $(OcultarEsta).removeAttr("hidden");
                        }
                    }
                    //Todos los que estan repetidos
                    if (dataParam1 == 10) {
                        createRow(data, "lead_activos", contador, estado);
                        $("#Motivo").removeAttr("hidden").text("MOTIVO");
                        $("#estado").removeAttr("hidden").text("ESTADO LEAD");
                        $(Ocultar).removeAttr("hidden");
                        $(OcultarEsta).removeAttr("hidden");
                    }


                    //esperamos a que se cumpla la promesa
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));
                    if (index === result.data.length - 1) {
                        resolve();
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }
    //ejecutamos esa funcion y luego cargamos la tabla con los datos llamados
    procesarDatos().then(() => {
        //al terminar el then ejecutamos datatable
        $(document).ready(function () {

            //columas a definir para el filtro y descargar en excel
            var columnsDatatable = [];
            var columsDatatableExel = [];


            //si esta desde el telefono
            if (esDispositivoMovil()) {
                columnsDatatable = [1];
            }
            if (dataParam1 == 2 || dataParam1 == 1) {
                columnsDatatable = [0, 1, 3, 4, 5, 6, 7];
            }
            if (dataParam1 == 3 || dataParam1 == 4 || dataParam1 == 10 || dataParam1 == 5) {
                columnsDatatable = [0, 1, 3, 4, 5, 6, 7, 11];
            }

            columsDatatableExel = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

            var table = $("#datatable-buttons").DataTable({
                initComplete: function () {
                    $("#preloader").hide();
                },
                processing: true,
                searchPanes: {
                    cascadePanes: true,
                    dtOpts: {
                        select: {
                            style: "multi",
                        },
                        count: {
                            show: false,
                        },
                    },
                    columns: columnsDatatable,
                },
                columnDefs: [
                    {
                        searchPanes: {
                            show: true,
                            initCollapsed: true,
                        },
                        targets: columnsDatatable,
                    },
                ],
                dom: "PBfrtip",
                stateSave: true,
                order: [[8, "asc"]],
                pageLength: 59,
                lengthMenu: [
                    [10, 25, 50, 200, -1],
                    [10, 25, 50, 200, "All"],
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "No se encontraron resultados",
                    info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                    infoFiltered: "(filtrado de un total de _MAX_ registros)",
                    sSearch: "Buscar:",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior",
                    },
                    sProcessing: "Cargando...",
                },
                buttons: [
                    {
                        extend: "excel",
                        footer: true,
                        titleAttr: "Exportar a excel",
                        className: "btn btn-success",
                        exportOptions: {
                            columns: columsDatatableExel,
                        },
                    },
                ],
            });

            //opcion para seleccionar un valor de la tabla y asi ejecutar una funcion que nos muestra los datos del cliente
            if (dataParam1 == 1 || dataParam1 == 2 || dataParam1 == 3 || dataParam1 == 4 || dataParam1 == 5) {
                $("#datatable-buttons tbody").on("click", "tr", function () {
                    //datos y posicion
                    var data = table.row(this).data();
                    //extaremos el id del lead
                    let id_cliente = data[2];
                    //no puede ser cero
                    if (id_cliente != 0) {
                        //ocultamos este boton si esta en requiere atencion
                        if (dataParam1 == 3) {
                            $("#whatsappLink").hide();
                        }
                        //abrimos el modal antes de ejecutar la funcion
                        var modal = document.querySelector(".bs-example-modal-center");
                        var bsModal = new bootstrap.Modal(modal);
                        bsModal.show();

                        //ejecutamos esta funcion que esta en js
                        modalAcciones(id_cliente);
                    }
                });
            }

            if (dataParam1 == 9) {
                // Obtiene una referencia al elemento con el id 'extract-button'.
                var boton = document.getElementById("extract-button");
                // Quita la propiedad 'display: none;' para mostrar el botón.
                boton.style.display = ""; // Puedes usar "inline" o "inline-block" en lugar de "block" si es más apropiado para tu diseño.
                // Crea un arreglo para almacenar las filas seleccionadas.
                var selectedRows = [];
                $('#datatable-buttons tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    var rowData = table.row(this).data();
                    if ($(this).hasClass('selected')) {
                        selectedRows.push(rowData[2]);
                    } else {
                        var index = selectedRows.indexOf(rowData[1]);
                        if (index > -1) {
                            selectedRows.splice(index, 1);
                        }
                    }
                });
                // Configura un evento cuando se hace clic en el botón con id 'extract-button'.
                $('#extract-button').click(async function () {
                    if (selectedRows.length > 0) {
                        //Modal poara esperar a que cargen los datos
                        mostrarSwalopacidadDiv();
                        const detalle = "Contacto generadon desde la funcion masivo";
                        const valores = {
                            valorDeCaida: 50,
                            tipo: "Contactado Masivo",
                            estado_lead: 1,
                            accion_lead: 6,
                            seguimiento_calendar: 0,
                            valor_segimineto_lead: 3,
                        };

                        // Crea un conjunto para llevar un registro de los id_lead procesados
                        const processedLeads = new Set();
                        selectedRows.forEach(async (id_lead, index) => {
                            // Verifica si ya se procesó este id_lead
                            if (processedLeads.has(id_lead)) {
                                return;
                            }
                            // Agrega el id_lead al conjunto de id_lead procesados
                            processedLeads.add(id_lead);
                            const StatusCodeBitacora = await BitacoraCliente(id_lead, valores, detalle, "08-LEAD-SEGUIMIENTO");
                            if (StatusCodeBitacora.statusCode == 200) {
                                const StatusCodeCliente = await ActualizarCliente(id_lead, valores, "08-LEAD-SEGUIMIENTO");
                                if (StatusCodeCliente.statusCode == 200) {
                                    console.log("masivo completado");
                                }
                            }
                            // Verificar si es el último elemento en el bucle
                            if (index === selectedRows.length - 1) {
                                Swal.fire('Contacto Masivo generado con éxito', '', 'success').then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload(); // Refresh the page
                                    }
                                });
                            }
                        });

                    }
                });
            }
        });

        // Cambiar estilos de banner
        const changeBannerStyles = () => {
            setTimeout(() => {
                const tableleads = document.getElementById(`tableleads`);
                tableleads.style.display = "none";
                if (esDispositivoMovil()) {
                    var elemento = document.getElementById("table-body");
                    elemento.style.opacity = "20";
                } else {
                    var elemento = document.getElementById("table-body");
                    if (elemento) {
                        elemento.style.display = "";
                    }
                }
                Swal.close();
            }, 100);


        };

        // Cambiar estilos de banner
        changeBannerStyles();
    });
}

// Esta función se encarga de llenar la tabla con los datos del resultado
function createRow(leadData, displayElementId, contador, estado) {
    // Construimos un arreglo de las variables
    const { name_admin, nombre_lead, idinterno_lead, email_lead, telefono_lead, proyecto_lead, campana_lead, segimineto_lead, creado_lead, actualizadaaccion_lead, subsidiaria_lead, nombre_caida } = leadData;

    let fech = actualizadaaccion_lead ? new Date(actualizadaaccion_lead) : new Date();

    // Formateamos la fecha
    const fechaActualizada = new Date(fech);
    const anio = fechaActualizada.getFullYear();
    const mes = fechaActualizada.getMonth() + 1;
    const dia = fechaActualizada.getDate();
    const fechaFormateada = `${anio} -${mes.toString().padStart(2, '0')} -${dia.toString().padStart(2, '0')} `;

    const tableBody = document.querySelector("#table-body");
    // Cantidad de td para llenar la tabla
    const row = document.createElement("tr");
    row.innerHTML = `
    <td style="text-align: left; font-weight: bold;" hidden> ${name_admin}</td>
    <td style="text-align: left; font-weight: bold;">${nombre_lead}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${idinterno_lead}</td>
    <td style="text-align: left; font-weight: bold;">${email_lead}</td>
    <td style="text-align: left; font-weight: bold;">${telefono_lead}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${proyecto_lead}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${campana_lead}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${segimineto_lead.substring(3)}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${creado_lead}</td>
    <td style="text-align: left; font-weight: bold;">${fechaFormateada}</td>
    <td style="text-align: left; font-weight: bold;" hidden>${subsidiaria_lead}</td>
    <td style="text-align: left; font-weight: bold;" hidden id="Ocultar${contador}">${nombre_caida}</td>
    <td style="text-align: left; font-weight: bold;" hidden id="OcultarEsta${contador}">${estado}</td>
`;

    // Agregamos los datos de 'row' al 'tableBody'
    tableBody.appendChild(row);

    // Esto decide si queremos habilitar o no el campo
    document.getElementById(displayElementId).style.display = "";
}

