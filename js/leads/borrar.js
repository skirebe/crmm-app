if (dataIn.tipos == "update_add_lead_id") {

    var registro = record.load({ type: 'lead', id: dataIn.id })

    //Paso # 2 - Actualizar el registro indicando los campos a modificar, con su respectivo valor.
    try {
        registro.setValue({ fieldId: 'customform', value: "28" });
        //Modificamos los datos principales
        registro.setValue({ fieldId: 'comments', value: dataIn.comentario_cliente });
        registro.setValue({ fieldId: 'custentity_csegix_proyectos', value: dataIn.proyecto_new });
        registro.setValue({ fieldId: 'subsidiary', value: dataIn.subsidiary_new });
        registro.setValue({ fieldId: 'custentity_ix_campanademarketing', value: dataIn.campana_new });
        registro.setValue({ fieldId: 'email', value: dataIn.email });
        registro.setValue({ fieldId: 'phone', value: dataIn.phone });
        //Nombre y apellidos en blanco para poder Colcoar en el form
        registro.setValue({ fieldId: 'firstname', value: "" });
        registro.setValue({ fieldId: 'lastname', value: "" });
        registro.setValue({ fieldId: 'middlename', value: "" });
        //ahora los llenamos
        registro.setValue({ fieldId: 'firstname', value: dataIn.nombre });
        registro.setValue({ fieldId: 'lastname', value: dataIn.segundoApellido });
        registro.setValue({ fieldId: 'middlename', value: dataIn.primerApellido });
        //agregamos el vendedor
        var sub = registro.getSublist({
            sublistId: 'salesteam'
        });
        var contador_sublist = registro.getLineCount({
            sublistId: 'salesteam'
        });
        for (var i = 0; i < contador_sublist; i++) {
            var isprimary = registro.getSublistValue({
                sublistId: 'salesteam',
                fieldId: 'isprimary',
                line: i
            });

            if (isprimary == true || isprimary == false) {
                var id_vendedor = registro.setSublistValue({
                    sublistId: 'salesteam',
                    fieldId: 'employee',
                    line: i,
                    value: dataIn.employee
                });
                var rol_vendedor = registro.setSublistValue({
                    sublistId: 'salesteam',
                    fieldId: 'salesrole',
                    line: i,
                    value: "-2"

                });

                log.debug({ title: 'id vendedor', details: id_vendedor });
            }
        }

        if (dataIn.informacion_Extra == 1) {

            //cambiamos la cedula del usuario
            registro.setValue({ fieldId: 'vatregnumber', value: dataIn.vatregnumber });
            //cambiamos la nacionalidad
            registro.setValue({ fieldId: 'custentity1', value: dataIn.custentity1 });
            //cambiamos el estado civil
            registro.setValue({ fieldId: 'custentityestado_civil', value: dataIn.custentityestado_civil });
            //telefono alternativo
            registro.setValue({ fieldId: 'altphone', value: dataIn.altphone });
            //edad del cliente
            registro.setValue({ fieldId: 'custentity11', value: dataIn.custentity11 });
            //hijos del cliente
            registro.setValue({ fieldId: 'custentityhijos_cliente', value: dataIn.custentityhijos_cliente });
            //hijos profesion del cliente
            registro.setValue({ fieldId: 'custentity_ix_customer_profession', value: dataIn.custentity_ix_customer_profession });
            //hijos cambiamos el corredor del cliente
        }

        //validamos que el corredor exista
        if (dataIn.corredor_extra == 1) {
            var contador_sublist_partners = registro.getLineCount({ sublistId: 'partners' });
            for (var i = 0; i < contador_sublist_partners; i++) {
                var isprimary = registro.getSublistValue({
                    sublistId: 'partners',
                    fieldId: 'isprimary',
                    line: i
                });

                if (isprimary === true || isprimary === false) {
                    var id_vendedor = registro.setSublistValue({
                        sublistId: 'partners',
                        fieldId: 'partner',
                        line: i,
                        value: dataIn.corredor_new
                    });
                    var rol_vendedor = registro.setSublistValue({
                        sublistId: 'partners',
                        fieldId: 'partnerrole',
                        line: i,
                        value: "3"

                    });
                    var isprimary2 = registro.setSublistValue({
                        sublistId: 'partners',
                        fieldId: 'isprimary',
                        line: i,
                        value: true
                    });
                    var contribution = registro.setSublistValue({
                        sublistId: 'partners',
                        fieldId: 'contribution',
                        line: i,
                        value: "100.0"

                    });

                }
            }

        }

        // Obtener el subregistro de dirección
        var addressSubrecord = registro.getSublistValue({
            fieldId: 'addressbook'
        });

        // Establecer los valores de los campos del subregistro de dirección
        addressSubrecord.setValue({
            fieldId: 'country',
            value: 'US'
        });
        addressSubrecord.setValue({
            fieldId: 'addr1',
            value: '123 Main St'
        });
        addressSubrecord.setValue({
            fieldId: 'city',
            value: 'New York'
        });
        addressSubrecord.setValue({
            fieldId: 'state',
            value: 'NY'
        });
        addressSubrecord.setValue({
            fieldId: 'zip',
            value: '10001'
        });





        if (dataIn.infromacion_extra_dos == 1) {
            registro.setValue({ fieldId: 'custentity77', value: dataIn.custentity77 });
            registro.setValue({ fieldId: 'custentity78', value: dataIn.custentity78 });
            registro.setValue({ fieldId: 'custentity79', value: dataIn.custentity79 });
            registro.setValue({ fieldId: 'custentity80', value: dataIn.custentity80 });
            registro.setValue({ fieldId: 'custentity82', value: dataIn.custentity82 });
            registro.setValue({ fieldId: 'custentity81', value: dataIn.custentity81 });
            registro.setValue({ fieldId: 'custentity84', value: dataIn.custentity84 });
        }
        registro.save();



        return { 'status': 200, 'Data': registro };
    } catch (error) {
        log.error("Se presentó un error: ", error);
        return { 'status': 500, 'Data': error }
    }



}