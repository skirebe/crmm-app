async function ExtraerLead(value) {
    try {
        const id_data = dataParam1;
        await getLeadData(id_data, 1, value);
    } catch (error) {
        console.log('error', error);
    }
}

async function ExtraerLeadModal(value) {
    try {
        const id_data = dataParam1;
        await getLeadData(id_data, 2, value);
        const modal = document.querySelector(".bs-example-modal-center");
        const bsModal = new bootstrap.Modal(modal);
        bsModal.show();
        switch (value) {
            case "primerContacto":
                $("#PrimerContacto").hide();
                $("#whatsappLink").hide();
                break;
            case "CrearEvento":
                $("#CrearEvento").hide();
                $("#whatsappLink").hide();
                break;
            case "perdido":
                $("#ColocarPerdido").hide();
                $("#whatsappLinkAuto").hide();
                $("#whatsappLink").hide();
                break;
            case "seguimiento":
                $("#ColocarSeguimiento").hide();
                $("#whatsappLinkAuto").hide();
                $("#whatsappLink").hide();
                break;
            case "Perfil":
                $("#perfil").hide();
                break;
            case "oportunidad":
                $("#Oportuindad").hide();
                break;

        }

    } catch (error) {
        // Captura y maneja cualquier error lanzado durante la ejecución.
        console.error("Modal error al cargarlo", error);
    }
}

async function getLeadData(id_data, valor, escenario) {

    const data = `*`;
    const table = "leads"
    const query = `WHERE idinterno_lead = ${id_data}`;
    const result = await fetchRecords(data, table, query, "select", "", 2);


    result.data.forEach(async (data) => {

        if (id_data != 0 || id_data == "") {
            if (escenario === "CrearEvento") {
                const nombreElement = document.getElementById("nombre");
                const correoElement = document.getElementById("correo");
                const telefonoElement = document.getElementById("telefono");
                const idAdminElement = document.getElementById("id_admin");
                const idLeElement = document.getElementById("id_le");
                const validarCitaElement = document.getElementById("validarCita");
                const citaValorElement = $("#citavalor");
                const switch3Element = document.getElementById("square-switch3");
                const cita2Element = document.getElementById("cita2");
                const leadA1Element = document.getElementById("leadA1");
                const contendorLeadElement = document.getElementById("contendorlead");
                document.getElementById("estado_lead").value = data.segimineto_lead;

                nombreElement.value = data.nombre_lead;
                correoElement.value = data.email_lead;
                telefonoElement.value = data.telefono_lead;
                idAdminElement.value = data.id_empleado_lead;
                idLeElement.value = data.idinterno_lead;

                // Validar citas
                const datas = "id_calendar,cita_lead,citas_chek,masDeUnaCita_calendar,accion_calendar";
                const table = "calendars";
                const query = `WHERE id_lead = ${id_data} AND (cita_lead = 1 OR citas_chek = 1) and accion_calendar="Pendiente"`;
                const result = await fetchRecords(datas, table, query, "select", "", 2);

                if (result.statusCode == 200) {
                    validarCitaElement.textContent = "Este cliente ya cuenta con una primera cita. Generar una nueva cita.";
                    citaValorElement.val(10);
                    switch3Element.disabled = true;
                    switch3Element.checked = true;
                    cita2Element.style.display = "block";
                } else {
                    validarCitaElement.textContent = "Agregar Primera Cita?";
                    citaValorElement.val(0);
                    switch3Element.disabled = false;
                    switch3Element.checked = false;

                    cita2Element.style.display = "none";
                }

                leadA1Element.style.display = "none";
                contendorLeadElement.style.display = "block";
            }
        } else {
            const nombreElement = document.getElementById("nombre");
            const correoElement = document.getElementById("correo");
            const telefonoElement = document.getElementById("telefono");
            const idAdminElement = document.getElementById("id_admin");
            const idLeElement = document.getElementById("id_le");
            const switch3Element = document.getElementById("square-switch3");
            document.getElementById("estado_lead").value = "";
            nombreElement.value = "";
            correoElement.value = "";
            telefonoElement.value = ""
            idAdminElement.value = "";
            idLeElement.value = "";
            switch3Element.disabled = false;
            switch3Element.checked = false;
        }
        if (escenario == "Perfil") {

            const bitacorLi = document.getElementById('preload');
            const tabla = document.getElementById('table');

            // Mostrar la tabla después de 4 segundos
            setTimeout(function () {
                // Ocultar el preload y mostrar la tabla
                bitacorLi.querySelector('.loader').style.display = 'none';
                tabla.style.display = 'table';
            }, 2000);


            $("#active").toggle(data.estado_lead == 0);
            $("#inactive").toggle(data.estado_lead != 0);
            $("#perfil_estado_segui").text(data.segimineto_lead.substring(3, 53));
            $("#perfil_name").text("Lead: " + data.nombre_lead);
            $("#perfil_telefono").text(data.telefono_lead);
            $("#perfil_correo").text(data.email_lead);
            $("#perfil_proyecto").text(data.proyecto_lead);
            $("#perfil_sub").text(data.subsidiaria_lead);
            $("#perfil_cam").text(data.campana_lead);

            const datasCorredor = `corredores.nombre_corredor`;
            const tableCorredor = "info_extra_lead i INNER JOIN corredores ON corredores.id_netsuiteCorredor = i.Corredor_lead"
            const queryCorredor = `WHERE id_lead_fk  = ${id_data}`;
            const resultsCorredor = await fetchRecords(datasCorredor, tableCorredor, queryCorredor, "select", "", 2);
            console.log("results: ", resultsCorredor);
            if (resultsCorredor.statusCode == 200) {
                resultsCorredor.data.forEach(async (datasEx) => {
                    $("#perfil_Corredor").text(datasEx.nombre_corredor || "--");
                });
            }



            const datas = `i.*, e1.Nombre_civil AS Estado_ciLead_nombre, e2.Nombre_civil AS estado_civil_extra_lead_nombre`;
            const table = "info_extra_lead i INNER JOIN etados_civil e1 ON i.Estado_ciLead = e1.id_civil INNER JOIN etados_civil e2 ON i.estado_civil_extra_lead = e2.id_civil"
            const query = `WHERE id_lead_fk  = ${id_data}`;
            const results = await fetchRecords(datas, table, query, "select", "", 2);

            if (results.statusCode == 200) {
                results.data.forEach(async (datasEx) => {
                    $("#perfil_cedula").text(datasEx.cedula_lead || "--");
                    $("#perfil_nacionalidad").text(datasEx.Nacionalidad_lead || "--");
                    $("#perfil_estado_civil").text(datasEx.Estado_ciLead_nombre || "--");
                    $("#perfil_telefono_alternativo").text(datasEx.TelefonoAlternatovo_lead || "--");
                    $("#perfil_edad_cliente").text(datasEx.Edad_lead || "--");
                    $("#perfil_profecion").text(datasEx.Profesion_lead || "--");
                    $("#perfil_hijos").text(datasEx.Hijos_lead || "--");
                    $("#perfil_direccion").text(datasEx.Direccion || "--");
                    $("#perfil_nombreExtra").text(datasEx.nombre_extra_lead || "--");
                    $("#perfil_cedula2").text(datasEx.cedula_extra_lead || "--");
                    $("#perfil_nacionalidad2").text(datasEx.nacionalidad_extra_lead || "--");
                    $("#perfil_email2").text(datasEx.email_extra_lead || "--");
                    $("#perfil_estado2").text(datasEx.estado_civil_extra_lead_nombre || "--");
                    $("#perfil_progesion2").text(datasEx.profesion_extra_lead || "--");
                    $("#perfil_telefono2").text(datasEx.telefono_extra_lead || "--");
                    document.getElementById("alertInformation").style.display = "none";
                });

            }




            var editar_perfil = 'leads/edit?data=' + data.idinterno_lead;
            $('#editar_perfil').attr('href', editar_perfil);
        }
        if (valor === 1) {
            document.getElementById("estado_lead").value = data.segimineto_lead;
        }
        if (valor === 2) {
            modalAcciones(data.idinterno_lead);
        }

    });

}
