const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};

async function Button_Seguimiento() {

    const nota = $("#metadescription").val();

    if (nota == '') {
        alert("Es obligatorio Generar Nota de Seguimiento para continuar ");
        // Obtén el elemento <textarea> por su ID
        var textarea = document.getElementById("metadescription");

        // Aplica el estilo directamente en JavaScript
        textarea.style.border = "1px solid red";
        return;
    }
    const motivo = $("#motivo").val();
    const datepicker = $("#datepicker").val();
    const estado_seg = $("#estado").val();



    const campos = [
        { id: 'metadescription', valor: nota },
        { id: 'motivo', valor: motivo },
        { id: 'datepicker', valor: datepicker },

    ];

    campos.forEach((campo) => {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
        }
    });
    if (campos.some(campo => campo.valor === '')) {
        mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
        return;
    }
    var fecha_inicio = datepicker;
    var fecha_final = datepicker;

    const fecha_fi = new Date(fecha_final); // Convierte el valor de #end a un objeto Date
    fecha_fi.setDate(fecha_fi.getDate() + 2); // Añade dos días a la fecha
    // Formatear la fecha en el formato deseado
    const fecha_formateada = `${fecha_fi.getFullYear()}-${(fecha_fi.getMonth() + 1).toString().padStart(2, '0')}-${fecha_fi.getDate().toString().padStart(2, '0')}`;


    fech_calendario_ini = fecha_inicio + ":00: 00";
    fech_calendario_fin = fecha_formateada + ":00: 00";
    const hora_inicio = "8:00 AM";
    const hora_final = "11:00 AM";


    const valores = {
        valorDeCaida: motivo,
        tipo: "Se generó un seguimiento programado para el dia " + datepicker,
        estado_lead: estado_seg,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };
    const fech2 = dateFilterResult.today + ":00: 00";

    const estado = $("#estado_lead").val();
    const estadosPermitidos = ["LEAD-OPORTUNIDAD", "LEAD-PRE-RESERVA", "LEAD-RESERVA", "LEAD-CONTRATO", "LEAD-ENTREGADO"];
    const estadoActual = estadosPermitidos.includes(estado.substring(3, 53)) ? estado : "08-LEAD-SEGUIMIENTO";



    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Confirma que desea colcoar este lead en seguimiento?",
        icon: 'warning',
        iconHtml: '؟',
        width: "55em",
        padding: "0 0 1.30em",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si colocar en seguimiento'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
            const data = `nombre_calendar, color_calendar, id_lead, id_admin, fechaIni_calendar, fechaFin_calendar, horaInicio_calendar, horaFinal_calendar, decrip_calendar, tipo_calendar, cita_lead,citas_chek,masDeUnaCita_calendar`;
            const table = "calendars"
            const query = `"Seguimiento","#A41D00","${dataParam1}","${storedAdminData.NetsuiteId_acceso}","${fech_calendario_ini}","${fech_calendario_fin}","${hora_inicio}","${hora_final}","${nota}","Seguimientos","0","0","0"`;
            const result = await fetchRecords(data, table, query, "insert", "", 2);
            try {
                if (result.statusCode == 200) {
                    const StatusCodeBitacora = await BitacoraCliente(dataParam1, valores, nota, estado);
                    if (StatusCodeBitacora.statusCode == 200) {
                        const StatusCodeCliente = await ActualizarCliente(dataParam1, valores, estado);
                        if (StatusCodeCliente.statusCode == 200) {
                            Swal.close();
                            // Muestra un mensaje de éxito al usuario y ofrece opciones adicionales
                            Swal.fire({
                                title: '¡Evento creado con éxito! ',
                                text: "¿Qué desea hacer a continuación?",
                                icon: 'question',
                                iconHtml: '✔️',
                                width: "40em",
                                padding: "0 0 1.20em",
                                showDenyButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Volver a la vista anterior',
                                denyButtonText: `Ir al perfil del cliente`,
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    // Vuelve a la vista anterior en la navegación.
                                    javascript: history.go(-1)
                                }
                                else if (result.isDenied) {
                                    // Redirige a la página de perfil del cliente.
                                    window.location.href = "leads/perfil?data=" + dataParam1;
                                } else {
                                    // Recarga la página actual.
                                    location.reload();
                                }
                            })
                        }
                    }
                } else {
                    // La función no se ejecutó correctamente, muestra un mensaje de error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se pudo crear el evento, inténtalo de nuevo en unos minutos',
                    })
                }
            } catch (err) {
                console.log("err: ", err);

            }
        }
    });


}
