
const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};
async function Perdido() {

    const idConsulta = $("#motivoss").val();
    if (idConsulta === "") {
        $("#valor").css("border-color", "red").on("change", () => {
            if ($("#valor").val() !== "") {
                $("#valor").css("border-color", "#ccc");
            }
        });
        Swal.fire("Mensaje De Advertencia", "Se debe seleccionar al menos un motivo de pérdida para el reporte", "warning");
        return;
    }

    const nota = $("#metadescriptionMotivo").val();
    const campos = ['motivoss', 'metadescriptionMotivo'];

    for (const campo of campos) {
        if ($("#" + campo).val() === "") {
            $("#" + campo).css("border-color", "red").on("change", () => {
                if ($("#" + campo).val() !== "") {
                    $("#" + campo).css("border-color", "#ccc");
                }
            });
            mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
            return;
        }
    }


    const valores = {
        valorDeCaida: idConsulta,
        tipo: "Se Coloco este cliente como perdido",
        estado_lead: 0,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };
    const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Confirma que desea colocar este lead como perdido?",
        icon: 'warning',
        iconHtml: '؟',
        width: "55em",
        padding: "0 0 1.30em",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si colocar como perdido'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const result = await ActualizarClientePerdido(dataParam1, idConsulta);
            if (result.statusCode == 200) {
                const StatusCodeBitacora = await BitacoraClientePerdido(dataParam1, valores, idConsulta, nota);
                if (StatusCodeBitacora.statusCode == 200) {
                    Swal.close();
                    // Muestra un mensaje de éxito al usuario y ofrece opciones adicionales
                    Swal.fire({
                        title: '¡Lead Colocado como perdido! ',
                        text: "¿Qué desea hacer a continuación?",
                        icon: 'question',
                        iconHtml: '✔️',
                        width: "40em",
                        padding: "0 0 1.20em",
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Volver a la vista anterior',
                        denyButtonText: `Ir al perfil del cliente`,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Vuelve a la vista anterior en la navegación.
                            javascript: history.go(-1)
                        }
                        else if (result.isDenied) {
                            // Redirige a la página de perfil del cliente.
                            window.location.href = "leads/perfil?data=" + dataParam1;
                        } else {
                            // Recarga la página actual.
                            location.reload();
                        }
                    })


                }

            } else {
                // La función no se ejecutó correctamente, muestra un mensaje de error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se pudo crear el evento perdido, inténtalo de nuevo en unos minutos',
                })
            }
        }
    });
}
