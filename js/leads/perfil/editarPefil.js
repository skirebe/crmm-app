const dataManager = new DataManager();
async function informacionLead() {

    mostrarSwalopacidadDiv();
    const datasCorredor = `*`;
    const tableCorredor = "leads"
    const queryCorredor = `WHERE idinterno_lead = ${dataParam1}`;
    const resultsCorredor = await fetchRecords(datasCorredor, tableCorredor, queryCorredor, "select", "", 2);
    if (resultsCorredor.statusCode == 200) {

        resultsCorredor.data.forEach(async (datas) => {
            $("#firstnames").val(datas.nombre_lead);
            $("#employee").val(datas.id_empleado_lead);
            $("#entitynumber").val(datas.idinterno_lead);
            $("#emails").val(datas.email_lead);
            $("#id").val(datas.idinterno_lead);
            $("#phones").val(datas.telefono_lead);
            $("#comentario_clientes").val(datas.comentario_lead);
            $("#id_cam").val(datas.idcampana_lead);
            $("#moneda_clientes").val(datas.moneda_lead);
            $("#id_pro").val(datas.idproyecto_lead);
            $("#id_su").val(datas.idsubsidaria_lead);
            $("#id_estadointeresado_cliente").val(datas.estadointeresado_lead);
            $("#empresa_cliente").val(datas.empresa_lead);
            $("#referencia_cliente").val(datas.referencia_lead);
            $("#id_referencia_cliente").val(datas.idreferencia_lead);
            dataManager.fillSelect("campana_new_edit", "", "campana_new_edit", datas.idcampana_lead);
            dataManager.fillSelect("proyecto_new_edit", "", "proyecto_new_edit", datas.idproyecto_lead);
            dataManager.fillSelect("subsidiary_new_edit", "", "subsidiary_new_edit", datas.idsubsidaria_lead);
            dataManager.fillSelect("corredor_lead_edit", "", "corredor_lead_edit", 77777777777777777777777777);



            const datasCorredor = `*`;
            const tableCorredor = "info_extra_lead i INNER JOIN corredores ON corredores.id_netsuiteCorredor = i.Corredor_lead"
            const queryCorredor = `WHERE id_lead_fk  = ${datas.idinterno_lead}`;
            const resultsCorredor = await fetchRecords(datasCorredor, tableCorredor, queryCorredor, "select", "", 2);
            if (resultsCorredor.statusCode == 200) {
                resultsCorredor.data.forEach(async (datasEx) => {
                    if (datasEx.id_netsuiteCorredor > 0) {
                        dataManager.fillSelect("corredor_lead_edit", "", "corredor_lead_edit", datasEx.id_netsuiteCorredor);
                        $("#corredor_extra").val(1);
                    }
                });
            }

            const datasInfo = `i.*, e1.Nombre_civil AS Estado_ciLead_nombre, e2.Nombre_civil AS estado_civil_extra_lead_nombre`;
            const tableInfo = "info_extra_lead i INNER JOIN etados_civil e1 ON i.Estado_ciLead = e1.id_civil INNER JOIN etados_civil e2 ON i.estado_civil_extra_lead = e2.id_civil"
            const queryInfo = `WHERE id_lead_fk  = ${datas.idinterno_lead}`;
            const resultsInfo = await fetchRecords(datasInfo, tableInfo, queryInfo, "select", "", 2);

            if (resultsInfo.statusCode == 200) {
                resultsInfo.data.forEach(async (datasExEdit) => {
                    $("#informacion_extra").val(1);
                    $("#vatregnumber").val(datasExEdit.cedula_lead || "--");
                    $("#custentity1").val(datasExEdit.Nacionalidad_lead || "--");
                    $("#altphone").val(datasExEdit.TelefonoAlternatovo_lead || "--");
                    $("#custentity11").val(datasExEdit.Edad_lead || "--");
                    $("#custentity_ix_customer_profession").val(datasExEdit.Profesion_lead || "--");
                    $("#custentityhijos_cliente").val(datasExEdit.Hijos_lead || "--");
                    $("#partner").val(datasExEdit.nombre_corredor || "--");
                    $("#defaultaddress").val(datasExEdit.Direccion || "--");
                    dataManager.fillSelect("custentityestado_civil", "", "custentityestado_civil", datasExEdit.Estado_ciLead);

                    if (datasExEdit.nombre_extra_lead === "--" && datasExEdit.cedula_extra_lead === "--") {
                        console.log("sin información extra");
                    } else {
                        $("#infromacion_extra_dos").val(1);
                        $("#custentity77").val(datasExEdit.nombre_extra_lead || "--");
                        $("#custentity78").val(datasExEdit.cedula_extra_lead || "--");
                        $("#custentity79").val(datasExEdit.profesion_extra_lead || "--");
                        $("#custentity80").val(datasExEdit.estado_civil_extra_lead || "--");
                        $("#custentity82").val(datasExEdit.telefono_extra_lead || "--");
                        $("#custentity81").val(datasExEdit.nacionalidad_extra_lead || "--");
                        $("#custentity84").val(datasExEdit.email_extra_lead || "--");
                        dataManager.fillSelect("custentityestado_civil_extra", "", "custentityestado_civil_extra", datasExEdit.estado_civil_extra_lead);
                    }
                });
            }
        });
        Swal.close();
    }
}
informacionLead();


const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};
const Editar_lead_Net = async () => {
    var id = $('#id').val() || '';
    var firstname = $('#firstnames').val() || '';

    const componentes = firstname.split(/\s+/);
    // Eliminamos elementos vacíos (si los hubiera) que se generan por espacios consecutivos
    const componentesLimpios = componentes.filter((componente) => componente.trim() !== '');
    // Asignamos cada componente a una variable
    const nombre = componentesLimpios[0];
    const primerApellido = componentesLimpios[1];
    const segundoApellido = componentesLimpios.slice(2).join(' ') || '.';


    var email = $('#emails').val() || '';
    var phone = $('#phones').val() || '';
    var comentario_cliente = $('#comentario_clientes').val() || '';
    var campana_new = $('#campana_new_edit').val() || '';
    var proyecto_new = $('#proyecto_new_edit').val() || '';
    var subsidiary_new = $('#subsidiary_new_edit').val() || '';
    var employee = $('#employee').val() || '';

    const campos = [
        { id: "firstnames", valor: firstname },
        { id: "emails", valor: email },
        { id: "phones", valor: phone },
        { id: "comentario_clientes", valor: comentario_cliente },
        { id: "campana_new", valor: campana_new },
        { id: "proyecto_new", valor: proyecto_new },
        { id: "subsidiary_new", valor: subsidiary_new },
    ];

    campos.forEach((campo) => {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
        }
    });

    if (firstname === '' || email === '' || phone === '' || comentario_cliente === '' || campana_new === '' || proyecto_new === '' || subsidiary_new === '') {
        mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
    }

    let informacion_Extra = 1;




    var vatregnumber = $('#vatregnumber').val() || '';
    var custentity1 = $('#custentity1').val() || '';
    var custentityestado_civil = $('#custentityestado_civil').val() || '';
    var custentity11 = $('#custentity11').val() || '';
    if (vatregnumber !== '' || custentity1 !== '' || custentityestado_civil !== '' || custentity11 !== '') {
        var custentity1 = $('#custentity1').val() || '';
        var altphone = $('#altphone').val() || '';
        var custentityhijos_cliente = $('#custentityhijos_cliente').val() || '';
        var custentity_ix_customer_profession = $('#custentity_ix_customer_profession').val() || '';
        var defaultaddress = $('#defaultaddress').val() || '';
        const campos = [
            { id: "vatregnumber", valor: vatregnumber },
            { id: "custentity1", valor: custentity1 },
            { id: "custentityestado_civil", valor: custentityestado_civil },
            { id: "altphone", valor: altphone },
            { id: "custentity11", valor: custentity11 },
            { id: "custentityhijos_cliente", valor: custentityhijos_cliente },
            { id: "custentity_ix_customer_profession", valor: custentity_ix_customer_profession },
            { id: "defaultaddress", valor: defaultaddress },
        ];
        campos.forEach((campo) => {
            const elemento = $(`#${campo.id}`);
            if (campo.valor === "") {
                elemento.css("border-color", "red");
                elemento.on("change", () => {
                    if (elemento.val() !== "") {
                        elemento.css("border-color", "#ccc");
                    }
                });
            }
        });

        if (vatregnumber === '' || custentity1 === '' || custentityestado_civil === '' || altphone === '' ||
            custentity11 === '' || custentityhijos_cliente === '' || custentity_ix_customer_profession === '' ||
            defaultaddress === '') {
            mostrarAlerta('¡Se deben llenar los campos obligatorios al llenar el campo cedula!', true);
            return;
        }
    } else {
        informacion_Extra = 0;
        vatregnumber = '';
        custentity1 = '';
        custentityestado_civil = '';
        altphone = '';
        custentity11 = '';
        custentityhijos_cliente = '';
        custentity_ix_customer_profession = '';
        defaultaddress = 'null';
    }



    var tipos = "update_add_lead_id";



    var corredor_new = $('#corredor_lead_edit').val() || '';
    let corredor_extra = 1;
    if (corredor_new === "") {
        corredor_extra = 0;
    }

    let infromacion_extra_dos = 1;

    var custentity77 = $('#custentity77').val() || '';
    var custentity78 = $('#custentity78').val() || '';
    var custentity79 = $('#custentity79').val() || '';
    var custentity80 = $('#custentityestado_civil_extra').val() || '';
    var custentity82 = $('#custentity82').val() || '';
    var custentity81 = $('#custentity81').val() || '';
    var custentity84 = $('#custentity84').val() || '';


    // Valida que ninguno de los campos esté vacío
    if (custentity77 === '' || custentity78 === '' || custentity79 === '' || custentity80 === '') {
        infromacion_extra_dos = 0;
    }

    Swal.fire({
        title: '¿Está seguro?',
        text: '¿Confirma que desea editar este lead?',
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, editar'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const data = {
                infromacion_extra_dos,
                corredor_extra,
                informacion_Extra,
                id,
                tipos,
                employee,
                firstname,
                email,
                phone,
                comentario_cliente,
                campana_new,
                proyecto_new,
                subsidiary_new,
                vatregnumber,
                custentity1,
                custentityestado_civil,
                altphone,
                custentity11,
                custentityhijos_cliente,
                custentity_ix_customer_profession,
                defaultaddress,
                corredor_new,
                nombre,
                primerApellido,
                segundoApellido,
                custentity77,
                custentity78,
                custentity79,
                custentity80,
                custentity82,
                custentity81,
                custentity84,
            };

            const response = await fetch("https://api-crm.roccacr.com/api/v2/client/editNetsuite", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            const result = await response.json();
            var ExTraerResultado = result['Detalle'];
            if (ExTraerResultado.status == 200) {
                const data = {
                    idConsulta: dataParam1
                };
                const response = await fetch("https://api-crm.roccacr.com/api/v2/client/consult", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const resultData = await response.json();
                if (resultData.Detalle.status === 200) {
                    const detalle = resultData['Detalle'];
                    const info = detalle.data;
                    const fields = info.fields;
                    const data = `
                        nombre_lead ="${firstname}",
                        email_lead="${email}",
                        telefono_lead="${phone}",
                        comentario_lead="${comentario_cliente}",
                        idproyecto_lead="${fields.custentity_csegix_proyectos}",
                        proyecto_lead="${detalle.Proyecto}",
                        idsubsidaria_lead="${fields.subsidiary}",
                        subsidiaria_lead="${detalle.Subsidaria}",
                        idcampana_lead="${fields.custentity_ix_campanademarketing}",
                        campana_lead="${detalle.Marketing}"
                    `;
                    const table = "leads"
                    const query = `WHERE idinterno_lead ="${dataParam1}";`;
                    const result = await fetchRecords(data, table, query, "update", "", 2);
                    if (result.statusCode == 200) {
                        const data = `
                            cedula_lead ="${vatregnumber}",
                            Nacionalidad_lead="${custentity1}",
                            Estado_ciLead="${custentityestado_civil}",
                            Edad_lead="${custentity11}",
                            Profesion_lead="${custentity_ix_customer_profession}",
                            Hijos_lead="${custentityhijos_cliente}",
                            TelefonoAlternatovo_lead="${altphone}",
                            Direccion="${defaultaddress}",
                            Corredor_lead="${corredor_new}",
                            nombre_extra_lead="${custentity77}",
                            cedula_extra_lead="${custentity78}",
                            profesion_extra_lead="${custentity79}",
                            estado_civil_extra_lead="${custentity80}",
                            telefono_extra_lead="${custentity82}",
                            nacionalidad_extra_lead="${custentity81}",
                            email_extra_lead="${custentity84}"
                        `;
                        const table = "info_extra_lead"
                        const query = `WHERE id_lead_fk  ="${dataParam1}";`;
                        const result = await fetchRecords(data, table, query, "update", "", 2);
                        if (result.statusCode == 200) {
                            console.log("editado completo")
                        } else { console.log("no se edito info extra") }

                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Se modifico el lead con exito',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            var targetUrl = 'leads/perfil?data=' + dataParam1;
                            // Redireccionar a la URL especificada
                            window.location.href = targetUrl;
                        })

                    } else {
                        console.log("no se edito el lead")

                    }
                }
            }
            if (ExTraerResultado.status == 500) {
                error = result['Detalle']['Data']['message'];
                return Swal.fire({
                    title: "Detalle de error : " + error + ",  \nLo sentimos, favor revisar este error con su administrador.",
                    icon: 'question',
                    iconHtml: '؟',
                    width: "40em",
                    padding: "0 0 1.25em",
                    confirmButtonText: 'OK',
                    cancelButtonText: 'CORREGUIR',
                    showCancelButton: true,
                    showCloseButton: true
                })
            }


        }
    });
};