function esDispositivoMovil() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

async function bitacoraPerfil() {
    const id = new URLSearchParams(new URL(window.location.href).search).get('data');
    const colors = {
        'LEAD-INTERESADO': 'success',
        'LEAD-OPORTUNIDAD': 'primary',
        'LEAD-PRE-RESERVA': 'warning',
        'LEAD-RESERVA': 'warning',
        'LEAD-CONTRATO': 'info',
        'LEAD-ENTREGADO': 'dark',
        'LEAD-PERDIDO': 'danger',
        'LEAD-SEGUIMIENTO': 'success',
        default: 'success',
    };

    const data = "*";
    const table = "bitacoras";
    const where = `where id_lead_bit="${dataParam1}" ORDER BY fecha_creado_bit ASC;`;



    const response = await fetchRecords(data, table, where, "select", "", 2);


    try {
        // Mostrar el indicador de carga
        const bitacorLi = document.getElementById('bitacorLi');
        bitacorLi.innerHTML = '<div class="loader"><center><img src="https://www.seguroslagunaro.com/corporativa/uploads/html/memoria-2017/es/images/preload.gif"></center></div>';


        // Crear elementos HTML de tarjeta
        const cardElements = response.data.map(Nt => {
            const fecha = new Date(Nt.fecha_creado_bit);
            const formattedDate = `${fecha.getFullYear()}-${(fecha.getMonth() + 1).toString().padStart(2, '0')}-${fecha.getDate().toString().padStart(2, '0')} ${fecha.toLocaleTimeString([], { hour: 'numeric', minute: '2-digit', hour12: true })}`;
            let estado_bit2 = "";
            const estado_bit = Nt.estado_bit ? Nt.estado_bit.substr(3, 50) : '';
          

            const cardDiv = document.createElement('div');
            cardDiv.className = 'col-xl-3 col-sm-6';
            cardDiv.innerHTML = `
                <div class="card text-center">
                    <div class="card-body cardStyleLeads">
                        <div class="mb-4">
                            <i class="bx bx-comment"></i>
                        </div>
                        <h5 class="font-size-15 mb-1"><a href="javascript: void(0);" class="text-dark">${formattedDate}</a></h5>
                        <p class="text-muted">${Nt.detalle_bit}</p>
                        <div>
                            <a style="font-size: 15px;"  class="badge bg-${colors[Nt.estado_bit.substr(3, 50)] || colors.default}" font-size-25 m-5">${estado_bit} ${estado_bit2}</a>
                        </div>
                    </div>
                </div>
            `;

            return cardDiv;
        });


        setTimeout(function () {
            bitacorLi.innerHTML = ''; // Elimina el indicador de carga
            cardElements.forEach(cardElement => {
                bitacorLi.appendChild(cardElement);
            });
        }, 2000); // Retraso de 4 segundos
        // Limpiar el indicador de carga y agregar tarjetas

    } catch (error) {
        // Manejo de errores aquí
        console.error('Error:', error);
    }
}

// Ejecuta la función esDispositivoMovil y oculta un elemento si es un dispositivo móvil
var element = document.getElementById("collapseOne");
if (esDispositivoMovil()) {
    element.classList.remove("show");
}
