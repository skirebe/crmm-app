alert
async function estado_le(value, valor2) {




    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Confirma que desea cambiar el estado del lead?",
        icon: 'warning',
        iconHtml: '؟',
        width: "55em",
        padding: "0 0 1.30em",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si Modificar estado'
    }).then(async (result) => {
        if (result.isConfirmed) {



            const estado = $("#estado_lead").val();
            const descpDelEvento = "Se cambio de estado a este lead :" + value;
            const valores = {
                valorDeCaida: 59,
                tipo: "Se cambio de estado al lead",
                estado_lead: valor2,
                accion_lead: 6,
                seguimiento_calendar: 0,
                valor_segimineto_lead: 3,
            };

            const StatusCodeBitacora = await BitacoraCliente(dataParam1, valores, descpDelEvento, estado);
            if (StatusCodeBitacora.statusCode == 200) {
                const StatusCodeCliente = await ActualizarCliente(dataParam1, valores, estado);
                if (StatusCodeCliente.statusCode == 200) {

                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se cambió de estado a este lead: ' + value,
                        showConfirmButton: false,
                        timer: 2500
                    }).then((result) => {
                        window.location.href = "/leads/perfil?data=" + dataParam1;
                    })

                }
            }




            $.ajax({
                "url": "/controllers/leads/estado_modificar/estado_modificar.php",
                type: 'POST',
                data: {
                    id: id,
                    valor: valor2


                }
            }).done(function (resp) {

                if (resp == 0) {
                    return Swal.fire({
                        title: "Error: Lo sentimos, no se ha podido cambiar el estado del lead. Por favor, inténtelo de nuevo. \nSi el problema persiste, comuníquese con su administrador de sistemas para obtener ayuda. ¡Gracias!",
                        icon: 'question',
                        iconHtml: '؟',
                        width: "55em",
                        padding: "0 0 1.30em",
                        confirmButtonText: 'OK',
                        cancelButtonText: 'CORREGUIR',
                        showCancelButton: true,
                        showCloseButton: true
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se modifico el estado del lead con exito',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        location.reload();
                    })
                }
            })
        }
    })
}