async function ActualizarCliente(id_le, valores, estado) {
    const estadosPermitidos = ["LEAD-OPORTUNIDAD", "LEAD-PRE-RESERVA", "LEAD-RESERVA", "LEAD-CONTRATO", "LEAD-ENTREGADO"];
    const estadoActual = estadosPermitidos.includes(estado.substring(3, 53)) ? estado : "08-LEAD-SEGUIMIENTO";
    const fech2 = dateFilterResult.today;

    const data = `segimineto_lead = "${estadoActual}", 
    valor_segimineto_lead = "${valores.valor_segimineto_lead}", 
    estado_lead = "${valores.estado_lead}",
    accion_lead="${valores.accion_lead}", 
    seguimiento_calendar="${valores.seguimiento_calendar}" ,
    id_Caida="${valores.valorDeCaida}",
    actualizadaaccion_lead="${fech2}"`;
    const table = "leads"
    const query = `WHERE idinterno_lead ="${id_le}";`;
    const result = await fetchRecords(data, table, query, "update", "", 2);
    return result;

}

async function ActualizarClientePerdido(dataParam1, motivoss) {
    const data = `id_Caida=${motivoss},  estado_lead = 0, segimineto_lead="07-LEAD-PERDIDO"`;
    const table = "leads"
    const query = `WHERE idinterno_lead ="${dataParam1}";`;
    const result = await fetchRecords(data, table, query, "update", "", 2);
    return result;
}