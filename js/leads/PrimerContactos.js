async function Button_contactNota() {

    const nota = document.getElementById("metadescription").value;

    // Validar campos y aplicar estilos de borde
    const campos = [
        { id: 'metadescription', valor: nota }
    ];

    campos.forEach(campo => {
        const elemento = document.getElementById(campo.id);
        if (campo.valor === "") {
            elemento.style.borderColor = "red";
            elemento.addEventListener("change", () => {
                if (elemento.value !== "") {
                    elemento.style.borderColor = "#ccc";
                }
            });
        }
    });

    if (campos.some(campo => campo.valor === '')) {
        alert("Es obligatorio generar una Nota de Contacto para continuar.");
        return;
    }
    // Validar y actualizar estado
    const estado = $("#estado_lead").val();
    const valores = {
        valorDeCaida: 50,
        tipo: "Se generó una nota",
        estado_lead: 1,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };


    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Confirma que desea generar una nota de contacto?",
        icon: 'warning',
        iconHtml: '؟',
        width: "55em",
        padding: "0 0 1.30em",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, generar Nota'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();

            const StatusCodeBitacora = await BitacoraCliente(dataParam1, valores, nota, estado);
            try {
                if (StatusCodeBitacora.statusCode == 200) {
                    const StatusCodeCliente = await ActualizarCliente(dataParam1, valores, estado);
                    if (StatusCodeCliente.statusCode == 200) {
                        Swal.fire({
                            title: 'Nota creada con éxito! ',
                            text: "¿Qué desea hacer a continuación?",
                            icon: 'question',
                            iconHtml: '✔️',
                            width: "40em",
                            padding: "0 0 1.20em",
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Volver a la vista anterior',
                            denyButtonText: `Ir al perfil del cliente`,
                        }).then((result) => {
                            if (result.isConfirmed) {
                                // Vuelve a la vista anterior en la navegación.
                                javascript: history.go(-1)
                            }
                            else if (result.isDenied) {
                                // Redirige a la página de perfil del cliente.
                                window.location.href = "leads/perfil?data=" + dataParam1;
                            } else {
                                // Recarga la página actual.
                                location.reload();
                            }
                        })

                    }
                } else {
                    // La función no se ejecutó correctamente, muestra un mensaje de error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se pudo crear la nota, inténtalo de nuevo en unos minutos',
                    })
                }
            } catch (err) {
                console.log("err: ", err);

            }
        }
    });
}
