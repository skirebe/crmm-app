
const dataManager = new DataManager();
async function select() {
    // Obtén una referencia al elemento div
    var divElement = document.getElementById('admins');
    // Oculta el div estableciendo el estilo "display" en "none"
    divElement.style.display = 'none';
    dataManager.fillSelect("campana_new", "", "campana_new", 0);
    dataManager.fillSelect("corredor_lead", "", "corredor_lead", 0);
    dataManager.fillSelect("proyecto_new", "", "proyecto_new", 0);
    dataManager.fillSelect("subsidiary_new", "", "subsidiary_new", 0);
    const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
    if (storedAdminData.rol_va != 2) {
        dataManager.fillSelect("vendedor_new", "", "vendedor_new", 0);
        divElement.style.display = 'block';
    }
}
async function cargarSelect() {
    await select();
}

cargarSelect()


async function CrearLead() {

    const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
    var employee = $("#vendedor_new").val() ? $("#vendedor_new").val() : storedAdminData.NetsuiteId_acceso;

    var firstname = $("#firstname_new").val();
    var lastname = $("#lastname_new").val();
    var middlename = $("#middlename_new").val();






    var email = $("#email_new").val();
    var isValidEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

    if (isValidEmail) {
        console.log("Valid email:", email);
    } else {
        alert("El formato de correo es invalido");
    }




    var phone = $("#phone_new").val();
    var comentario_cliente = $("#comentario_cliente_new").val();
    var campana = $("#campana_new").val();
    var currency = $("#currency_new").val();
    var proyecto = $("#proyecto_new").val();
    var subsidiary = $("#subsidiary_new").val();
    var corredor_lead = $("#corredor_lead").val() || 0;

    const campos = [
        { id: 'firstname_new', valor: firstname },
        { id: 'lastname_new', valor: lastname },
        { id: 'middlename_new', valor: middlename },
        { id: 'email_new', valor: email },
        { id: 'phone_new', valor: phone },
        { id: 'comentario_cliente_new', valor: comentario_cliente },
        { id: 'campana_new', valor: campana },
        { id: 'currency_new', valor: currency },
        { id: 'proyecto_new', valor: proyecto },
        { id: 'subsidiary_new', valor: subsidiary },
    ];
    campos.forEach((campo) => {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
        }
    });

    // Verificar si los valores están vacíos antes de agregarlos a campos
    const variables = [
        firstname,
        lastname,
        middlename,
        email,
        phone,
        comentario_cliente,
        campana,
        currency,
        proyecto,
        subsidiary,
    ];
    if (variables.some(variable => variable === '')) {
        Swal.fire({
            icon: "error",
            title: "Algo no está bien...",
            text: "¡Se deben llenar los campos obligatorios!",
        });
        return;
    }

    async function CreateLead() {
        const data = {
            firstname: firstname,
            lastname: lastname,
            middlename: middlename,
            email: email,
            phone: phone,
            comentario_cliente: comentario_cliente,
            campana: campana,
            currency: currency,
            proyecto: proyecto,
            subsidiary: subsidiary,
            employee: employee,
            corredor_lead: corredor_lead

        };

        mostrarSwalopacidadDiv();;
        try {
            const response = await fetch("https://api-crm.roccacr.com/api/v2/client/add/netsuite", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            Swal.close();
            const result = await response.json();
            var ExTraerResultado = result['Detalle'];
            console.log("ExTraerResultado: ", ExTraerResultado);
            if (ExTraerResultado.status == 200) {
                id_ex = ExTraerResultado.id;

                if (corredor_lead !== null && corredor_lead !== "") {
                    const datas = "`id_Info_lead`, `cedula_lead`, `Nacionalidad_lead`, `Estado_ciLead`, `Edad_lead`, `Profesion_lead`, `Hijos_lead`, `TelefonoAlternatovo_lead`, `Direccion`, `Corredor_lead`, `id_lead_fk`, `nombre_extra_lead`, `cedula_extra_lead`, `profesion_extra_lead`, `estado_civil_extra_lead`, `telefono_extra_lead`, `nacionalidad_extra_lead`, `email_extra_lead`";
                    const tables = "info_extra_lead";
                    const querys = `NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '${corredor_lead}', '${id_ex}', '--', '--', '--', '--', '--', '--', '--'`;
                    await fetchRecords(datas, tables, querys, "insert", "", 2);
                }


                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Se creo un nuevo leads con exito ',
                    showConfirmButton: false,
                    timer: 3500
                }).then((result) => {
                    window.location.href = "/leads/perfil?data=" + id_ex;
                })
            }
            if (ExTraerResultado.status == 500) {
                error = JSON.parse(result['Detalle'].Error);
                return Swal.fire({
                    title: "Detalle de error : " + error.details + ",  \nLo sentimos, favor revisar este error con su administrador.",
                    icon: 'question',
                    width: "40em",
                    padding: "0 0 1.25em",
                    iconHtml: '؟',
                    confirmButtonText: 'OK',
                    cancelButtonText: 'CORREGUIR',
                    showCancelButton: true,
                    showCloseButton: true
                })



            }
        } catch (error) {
            alert('No se pundo crear el lead Intentalo de nuevo', error);
        }
    }

    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Listo para agregar un nuevo lead a tu lista?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
    }).then((result) => {
        if (result.isConfirmed) {
            CreateLead();
        }
    })

}