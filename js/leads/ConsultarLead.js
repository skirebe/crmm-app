async function consultarLeadNetsuite() {
    const idConsulta = $("#valor").val();
    if (idConsulta === "") {
        $("#valor").css("border-color", "red");
        $("#valor").on("change", () => {
            if ($("#valor").val() !== "") {
                $("#valor").css("border-color", "#ccc");
            }
        });
        return Swal.fire("Mensaje De Advertencia", "El campo id está vacío, antes de consultar el lead debe proporcionar un valor", "warning");
    }

    mostrarSwalopacidadDiv();
    try {
        const data = {
            idConsulta: idConsulta
        };
        const response = await fetch("https://api-crm.roccacr.com/api/v2/client/consult", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        const result = await response.json();

        if (result.Detalle.status === 200) {
            const detalle = result.Detalle;
            const info = detalle.data;
            const fields = info.fields;
            console.log("fields: ", fields);

            $("#id").val(info.id);
            $("#entitynumber").val(result["Detalle"]["data"]["fields"].entitynumber);
            const {
                firstname: firstnameL,
                lastname: lastname,
                lastname: apell,
                email: emails,
                phone: pho,
                comments: cm,
                currency: mony,
                custentity_ix_campanademarketing: idCam,
                custentity_csegix_proyectos: idPro,
                subsidiary: idSub,
                custentityix_dondeescuchodenostros: idRef
            } = fields;

            var nombreCompleto = firstnameL + " " + lastname

            $("#firstnames").val(nombreCompleto);
            $("#emails").val(emails);
            $("#phones").val(pho);
            $("#comentario_clientes").val(cm);
            $("#moneda_clientes").val(mony);
            $("#campana_clientes").val(detalle.Marketing);
            $("#id_campana_clientes").val(idCam);
            $("#proyecto_cliente").val(detalle.Proyecto);
            $("#id_proyecto_cliente").val(idPro);
            $("#subsidiaria_cliente").val(detalle.Subsidaria);
            $("#id_subsidaria_cliente").val(idSub);
            $("#referencia_cliente").val(detalle.Referido);
            $("#id_referencia_cliente").val(idRef);

            const salesteamLine1 = info.sublists.salesteam["line 1"];
            const nombreVendedor = salesteamLine1.employee_display;
            const customers = salesteamLine1.customer;
            const id_empleados = salesteamLine1.employee;
            const contributions = salesteamLine1.contribution;

            $("#employees").val(id_empleados);
            $("#employee_display").val(nombreVendedor);
            $("#customer").val(customers);
            $("#contribucion_cliente").val(contributions);

            document.getElementById('miDivConsulta').style.display = "block";
            Swal.close();
        } else {
            Swal.fire('Algo no esta bien.', 'Ese registro no existe.', 'question');
        }
    } catch (e) {
        console.log(e)
    }

}


async function Guardar_Lead() {
    const id = $("#id").val();
    const entitynumber = $("#entitynumber").val();
    const firstnames2 = $("#firstnames").val();
    const emails = $("#emails").val();
    const phones = $("#phones").val();
    const comentario_clientes = $("#comentario_clientes").val();
    const campana_clientes = $("#campana_clientes").val();
    const id_campana_clientes = $("#id_campana_clientes").val();
    const proyecto_cliente = $("#proyecto_cliente").val();
    const id_proyecto_cliente = $("#id_proyecto_cliente").val();
    const subsidiaria_cliente = $("#subsidiaria_cliente").val();
    const id_subsidaria_cliente = $("#id_subsidaria_cliente").val();
    const employees = $("#employees").val();
    const referencia_cliente = $("#referencia_cliente").val();
    const id_referencia_cliente = $("#id_referencia_cliente").val();

    const campos = [
        { id: 'entitynumber', valor: entitynumber },
        { id: 'firstnames', valor: firstnames2 },
        { id: 'emails', valor: emails },
        { id: 'phones', valor: phones },
        { id: 'campana_clientes', valor: campana_clientes },
        { id: 'id_campana_clientes', valor: id_campana_clientes },
        { id: 'proyecto_cliente', valor: proyecto_cliente },
        { id: 'id_proyecto_cliente', valor: id_proyecto_cliente },
        { id: 'subsidiaria_cliente', valor: subsidiaria_cliente },
        { id: 'id_subsidaria_cliente', valor: id_subsidaria_cliente },
    ];

    for (const campo of campos) {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
            Swal.fire({
                icon: "error",
                title: "Algo no está bien...",
                text: "¡Se deben llenar los campos obligatorios!",
            });
            return;
        }
    }

    var textoOriginal = $("#firstnames").val();
    var firstnames = textoOriginal.replace(/[^a-zA-Z\s]/g, '');
    const estados = "01-LEAD-INTERESADO";
    async function CreateLeadC() {
        mostrarSwalopacidadDiv();
        const descpDelEvento = "Se ha consultado y creado el lead en el sistema";
        const valores = {
            valorDeCaida: 0,
            tipo: "Se ha consultado y creado el lead en el sistema",
            estado_lead: 1,
            accion_lead: 0,
            seguimiento_calendar: 0,
            valor_segimineto_lead: 3,
        };

        try {
            const data = `id_empleado_lead, idnetsuite_lead, idinterno_lead, nombre_lead, email_lead, telefono_lead, proyecto_lead, idproyecto_lead, subsidiaria_lead, idsubsidaria_lead, idestadointeresado_lead, estadointeresado_lead, comentario_lead, campana_lead, idcampana_lead, empresa_lead, referencia_lead, idreferencia_lead, moneda_lead, contribucion_lead`;
            const table = "leads"
            const query = `"${employees}", "${entitynumber}", "${id}", "${firstnames}", "${emails}", "${phones}", "${proyecto_cliente}", "${id_proyecto_cliente}", "${subsidiaria_cliente}", "${id_subsidaria_cliente}", "1", "01-LEAD-INTERESADO", "${comentario_clientes}", "${campana_clientes}", "${id_campana_clientes}", "--", "${referencia_cliente}", "${id_referencia_cliente}", "1", "100%"`;
            const result = await fetchRecords(data, table, query, "insert", "", 2);
            if (result.statusCode == 200) {
                const StatusCodeBitacora = await BitacoraClienteConsultar(id, valores, descpDelEvento, estados, employees);
                if (StatusCodeBitacora.statusCode == 200) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se creó un nuevo lead con éxito',
                        showConfirmButton: false,
                        timer: 4500
                    }).then((result) => {
                        const encodedStringBtoA = id;
                        window.location.href = "/leads/perfil?data=" + encodedStringBtoA;
                    });
                }
            }
            if (result.statusCode == 500) {
                Swal.fire("Mensaje De Error", "Lo sentimos, no se puede crear el lead.\nFavor inténtalo de nuevo, si el problema persiste comunícate con tu administrador", "error");
                return;
            }

        } catch (error) {
            alert('No se pudo crear el lead. Inténtalo de nuevo', error);
        }
    }

    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Listo para agregar un nuevo lead en la lista?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
    }).then(async (result) => {
        if (result.isConfirmed) {

            const datas = 'idinterno_lead,nombre_lead';
            const tables = 'leads';
            const wheres = `where idinterno_lead =${id}`

            const result = await fetchRecords(datas, tables, wheres, "select", "", 2);
            if (result.statusCode == 200) {
                Swal.fire("Mensaje De Error", "Lo sentimos, no se puede crear el lead, ya que se encuentra registrado en el sistema.\nFavor inténtalo de nuevo, si el problema persiste comunícate con tu administrador", "error");
                return;
            }
            CreateLeadC();
        }
    });
}