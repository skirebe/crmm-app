async function BitacoraCliente(id_login, valores, descpDelEvento, estado) {

    const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
    const estadosPermitidos = ["LEAD-OPORTUNIDAD", "LEAD-PRE-RESERVA", "LEAD-RESERVA", "LEAD-CONTRATO", "LEAD-ENTREGADO"];
    const estadoActual = estadosPermitidos.includes(estado.substring(3, 53)) ? estado : "08-LEAD-SEGUIMIENTO";
    const data = `id_lead_bit, id_admin_bit, id_caida_bit, detalle_bit, tipo_documento_bit, estado_bit`;
    const table = "bitacoras"
    const query = `"${id_login}","${storedAdminData.NetsuiteId_acceso}",${valores.valorDeCaida},"${descpDelEvento}","${valores.tipo}","${estadoActual}"`;
    const result = await fetchRecords(data, table, query, "insert", "", 2);
    return result;
}

async function BitacoraClienteConsultar(id_login, valores, descpDelEvento, estado, employees) {
    const estadoActual = "01-LEAD-INTERESADO";
    const data = `id_lead_bit, id_admin_bit, id_caida_bit, detalle_bit, tipo_documento_bit, estado_bit`;
    const table = "bitacoras"
    const query = `"${id_login}","${employees}",${valores.valorDeCaida},"${descpDelEvento}","${valores.tipo}","${estadoActual}"`;
    const result = await fetchRecords(data, table, query, "insert", "", 2);
    return result;
}

async function BitacoraClientePerdido(dataParam1, valores, idConsulta, nota) {
    const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
    const data = `id_lead_bit, id_admin_bit, id_caida_bit, detalle_bit, tipo_documento_bit, estado_bit`;
    const table = "bitacoras"
    const query = `"${dataParam1}","${storedAdminData.NetsuiteId_acceso}",${idConsulta},"${nota}","${valores.tipo}","07-LEAD-PERDIDO"`;
    const result = await fetchRecords(data, table, query, "insert", "", 2);

    const dataOrsales = `status_ov = 0`;
    const tableOrsales = "ordenventa"
    const queryOrsales = `WHERE id_ov_lead ="${dataParam1}";`;
    await fetchRecords(dataOrsales, tableOrsales, queryOrsales, "update", "", 2);

    const dataestimaciones = `status = 0`;
    const tableestimaciones = "estimaciones"
    const queryestimaciones = `WHERE idLead_est ="${dataParam1}";`;
    await fetchRecords(dataestimaciones, tableestimaciones, queryestimaciones, "update", "", 2);
    const datacalendars = `calendars.accion_calendar = CASE
                        WHEN calendars.accion_calendar = 'Pendiente' THEN 'Cancelado'
                        WHEN calendars.accion_calendar = 'Cancelado' THEN 'Cancelado'
                        WHEN calendars.accion_calendar = 'Completado' THEN 'Completado'
                        ELSE calendars.accion_calendar
                    END,
                    calendars.estado_calendar = 0,
                    calendars.noticia_calendar = 5,
                    calendars.cancelado = "${nota}"`;
    const tablecalendars = "calendars"
    const querycalendars = `WHERE id_lead ="${dataParam1}";`;
    await fetchRecords(datacalendars, tablecalendars, querycalendars, "update", "", 2);

    const dataoportunidades = `estatus_oport = 0, chek_oport = 0`;
    const tableoportunidades = "ordenventa"
    const queryoportunidades = `WHERE id_ov_lead ="${dataParam1}";`;
    await fetchRecords(dataoportunidades, tableoportunidades, queryoportunidades, "update", "", 2);
    return result;


}