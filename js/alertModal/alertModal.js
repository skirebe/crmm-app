/*=================================== MODAL MINESTRAS CARGA LA VISTA GENERAL DE LEADS===================================*/
function mostrarSwal() {
    let timerInterval;
    Swal.fire({
        width: '700px', // Establece el ancho del modal,
        title: 'Cargando los datos...',
        html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
        timer: 48000,
        timerProgressBar: true,
        allowOutsideClick: false, // Evita cerrar al hacer clic fuera del modal
        didOpen: () => {
            Swal.showLoading();
            const b = Swal.getHtmlContainer().querySelector('b');
            timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft();
            }, 100);
        },
        willClose: () => {
            clearInterval(timerInterval);
        }
    }).then((result) => {

    });
}

function mostrarSwalopacidadDiv() {
    let timerInterval;
    Swal.fire({
        title: 'Cargando los datos...',
        html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
        timer: 48000,
        timerProgressBar: true,
        allowOutsideClick: false, // Evita cerrar al hacer clic fuera del modal
        didOpen: () => {
            Swal.showLoading();
            const b = Swal.getHtmlContainer().querySelector('b');
            timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft();
            }, 100);
        },
        willClose: () => {
            clearInterval(timerInterval);
        }
    }).then((result) => {
        setTimeout(function () {
            document.getElementById("opacidadDiv").style.opacity = "1";
        }, 600);

    });
}
/*=================================== MODAL MINESTRAS CARGA LA VISTA GENERAL DE LEADS===================================*/

