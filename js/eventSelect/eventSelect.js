class SelectDataLoader {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    async loadSelectData(queryData) {

        const result = await fetchRecords(queryData.dataFor, queryData.table, queryData.where, "select", queryData.vendorId, queryData.filtro);

        if (result.statusCode === 500) {
            console.log("Could not fill the select " + queryData.selectId);
            return;
        }
        const selectElement = document.getElementById(queryData.selectId);
        result.data.forEach((item) => {
            const optionElement = document.createElement("option");
            optionElement.value = item[queryData.valueKey];
            optionElement.textContent = item[queryData.textKey];
            selectElement.appendChild(optionElement);
            if (queryData.selectId === "tipo_evento_edit" && queryData.idData === item.nombre_Tevento) {
                optionElement.selected = true;
            }

            //seleccionamos al lead
            if (item.idinterno_lead != 0) {
                if (queryData.selectId === "listaLeads" && queryData.idData === item.idinterno_lead) {
                    optionElement.selected = true;
                }
            }

            if (queryData.selectId === "opciones" && queryData.idData === item.id_data) {
                optionElement.selected = true;
            }

            if (queryData.selectId === "custbody76_opt" && queryData.idData == item.id_motivo_compra) {
                optionElement.selected = true;
                return;
            }

            if (queryData.selectId === "custbody75_opt" && queryData.idData == item.id_motivo_pago) {
                optionElement.selected = true;
            }


            if (queryData.selectId === "custbody75_estimacion" && queryData.idData == item.id_motivo_pago) {
                optionElement.selected = true;
            }
            if (queryData.selectId === "campana_new_edit" && queryData.idData == item.id_NetsauiteCampana) {
                optionElement.selected = true;
            }

            if (queryData.selectId === "proyecto_new_edit" && queryData.idData == item.id_ProNetsuite) {
                optionElement.selected = true;
            }
            if (queryData.selectId === "subsidiary_new_edit" && queryData.idData == item.id_NetsuiteSub) {
                optionElement.selected = true;
            }
            if (queryData.selectId === "corredor_lead_edit" && queryData.idData == item.id_netsuiteCorredor) {
                optionElement.selected = true;
            }
            if (queryData.selectId === "custentityestado_civil" && queryData.idData == item.id_civil) {
                optionElement.selected = true;
            }
            if (queryData.selectId === "custentityestado_civil_extra" && queryData.idData == item.id_civil) {
                optionElement.selected = true;
            }
        });
    }

}
/*====================================NEW WORK AREA===================================================*/
class DataManager {
    constructor() {
        this.selectDataLoader = new SelectDataLoader(
            ""
        );
    }

    async fillSelect(Value, wheres, selectId, id_data,expediente) {


        function construirConsulta(id_data) {
            // Dividir la cadena en palabras
            const palabras = id_data.split(' ');

            // Inicializar un array para almacenar las partes de la cláusula WHERE
            const partesWhere = [];

            // Construir la cláusula WHERE para cada palabra
            palabras.forEach(palabra => {
                const condicion = `proyectoPrincipal_exp LIKE '%${palabra}%'`;
                partesWhere.push(condicion);
            });

            // Unir las partes de la cláusula WHERE con el operador OR
            const consultaWhere = partesWhere.join(' OR ');

            // Construir la consulta completa
            const consultaCompleta = `WHERE (${consultaWhere}) AND estado_exp='1. Disponible para Venta'`;

            return consultaCompleta;
        }

        let consultaEx ="";
        if (expediente == 100) {
            const expedienteName = id_data;
             consultaEx = construirConsulta(expedienteName);
        }







        try {
            // Define SQL queries and configurations for different "Value" values.
            const selectData = [
                {
                    value: "ProyectReport",
                    queries: [
                        {
                            table: "leads",
                            where: `${wheres}`,
                            selectId: `${selectId}`,
                            valueKey: "proyecto",
                            textKey: "proyecto",
                            dataFor: "DISTINCT proyecto_lead AS proyecto",
                            vendorId: "id_empleado_lead",
                            filtro: 1,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "CampanaReport",
                    queries: [
                        {
                            table: "leads",
                            where: `${wheres}`,
                            selectId: `${selectId}`,
                            valueKey: "campanas",
                            textKey: "campanas",
                            dataFor: "DISTINCT campana_lead AS campanas",
                            vendorId: "id_empleado_lead",
                            filtro: 1,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "tipo_evento",
                    queries: [
                        {
                            table: "tipoevento",//nombre de la tabla
                            where: `where estado_Tevento=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "nombre_Tevento",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_Tevento",//nombre que aparece en el selcct
                            dataFor: "nombre_Tevento",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,//si aplica o no el filtro del vendedor
                            idData: 0
                        },
                    ],
                },
                {
                    value: "tipo_evento_edit",
                    queries: [
                        {
                            table: "tipoevento",//nombre de la tabla
                            where: `where estado_Tevento=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "nombre_Tevento",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_Tevento",//nombre que aparece en el selcct
                            dataFor: "nombre_Tevento",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "motivoss",
                    queries: [
                        {
                            table: "caidas",//nombre de la tabla
                            where: `WHERE estado_caida=3`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_caida",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_caida",//nombre que aparece en el selcct
                            dataFor: "id_caida,nombre_caida",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "motivo",
                    queries: [
                        {
                            table: "caidas",//nombre de la tabla
                            where: `WHERE segui=2`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_caida",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_caida",//nombre que aparece en el selcct
                            dataFor: "id_caida,nombre_caida",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "listaLeads",
                    queries: [
                        {
                            table: "leads",//nombre de la tabla
                            where: `and estado_lead=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "idinterno_lead",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_lead",//nombre que aparece en el selcct
                            dataFor: "idinterno_lead,nombre_lead",//campos a llamar
                            vendorId: "id_empleado_lead",//vendedor
                            filtro: 1,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "custbody76",
                    queries: [
                        {
                            table: "compras",//nombre de la tabla
                            where: `where estado_motivo_compra=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_motivo_compra",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_motivo_compra",//nombre que aparece en el selcct
                            dataFor: "id_motivo_compra,nombre_motivo_compra",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "custbody75",
                    queries: [
                        {
                            table: "pagos",//nombre de la tabla
                            where: `WHERE id_motivo_pago IN (1,7,2) ORDER BY  nombre_motivo_pago`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_motivo_pago",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_motivo_pago",//nombre que aparece en el selcct
                            dataFor: "id_motivo_pago,nombre_motivo_pago",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "custbody75_estimacion",
                    queries: [
                        {
                            table: "pagos",//nombre de la tabla
                            where: `WHERE id_motivo_pago IN (1,7,2) ORDER BY  nombre_motivo_pago`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_motivo_pago",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_motivo_pago",//nombre que aparece en el selcct
                            dataFor: "id_motivo_pago,nombre_motivo_pago",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "location",
                    queries: [
                        {
                            table: "ubicaciones",//nombre de la tabla
                            where: `WHERE estado_ubicaciones=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "idNetsuite_ubicaciones",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_ubicaciones",//nombre que aparece en el selcct
                            dataFor: "idNetsuite_ubicaciones,nombre_ubicaciones",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "class",
                    queries: [
                        {
                            table: "clases",//nombre de la tabla
                            where: `WHERE estado_calase=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "idNetsuite_clase",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_clase",//nombre que aparece en el selcct
                            dataFor: "idNetsuite_clase,nombre_clase",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "expediente",
                    queries: [
                        {
                            table: "expedientes",//nombre de la tabla
                            where: `${consultaEx}`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "ID_interno_expediente",//nombre del valor que se envia a la bd value="?"
                            textKey: "codigo_exp",//nombre que aparece en el selcct
                            dataFor: "ID_interno_expediente,codigo_exp",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: 0
                        },
                    ],
                },
                {
                    value: "opciones",
                    queries: [
                        {
                            table: "estados",//nombre de la tabla
                            where: `WHERE id_estado_oportuindad  IN (11,22) ORDER BY nombre_estado_oportuindad`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_estado_oportuindad",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_estado_oportuindad",//nombre que aparece en el selcct
                            dataFor: "id_estado_oportuindad,nombre_estado_oportuindad",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },

                {
                    value: "custbody76_opt",
                    queries: [
                        {
                            table: "compras",//nombre de la tabla
                            where: ``, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_motivo_compra",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_motivo_compra",//nombre que aparece en el selcct
                            dataFor: "id_motivo_compra,nombre_motivo_compra",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "custbody75_opt",
                    queries: [
                        {
                            table: "pagos",//nombre de la tabla
                            where: `WHERE id_motivo_pago IN (1,7,2) ORDER BY  nombre_motivo_pago`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_motivo_pago",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_motivo_pago",//nombre que aparece en el selcct
                            dataFor: "id_motivo_pago,nombre_motivo_pago",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "campana_new",
                    queries: [
                        {
                            table: "campanas",//nombre de la tabla
                            where: `WHERE Estado_Campana=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_NetsauiteCampana",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_Campana",//nombre que aparece en el selcct
                            dataFor: "id_NetsauiteCampana,Nombre_Campana",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "corredor_lead",
                    queries: [
                        {
                            table: "corredores",//nombre de la tabla
                            where: `WHERE estado_corredor=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_netsuiteCorredor",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_corredor",//nombre que aparece en el selcct
                            dataFor: "id_netsuiteCorredor,nombre_corredor",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "corredor_lead_edit",
                    queries: [
                        {
                            table: "corredores",//nombre de la tabla
                            where: `WHERE estado_corredor=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_netsuiteCorredor",//nombre del valor que se envia a la bd value="?"
                            textKey: "nombre_corredor",//nombre que aparece en el selcct
                            dataFor: "id_netsuiteCorredor,nombre_corredor",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },



                {
                    value: "proyecto_new",
                    queries: [
                        {
                            table: "proyectos",//nombre de la tabla
                            where: `WHERE estado_proyecto=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_ProNetsuite",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_proyecto",//nombre que aparece en el selcct
                            dataFor: "id_ProNetsuite,Nombre_proyecto",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "subsidiary_new",
                    queries: [
                        {
                            table: "subsidiarias",//nombre de la tabla
                            where: `WHERE Estado_Subsidiaria=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_NetsuiteSub",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_Subsidiaria",//nombre que aparece en el selcct
                            dataFor: "id_NetsuiteSub,Nombre_Subsidiaria",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "vendedor_new",
                    queries: [
                        {
                            table: "admins",//nombre de la tabla
                            where: `WHERE status_admin=1 and id_rol_admin=2`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "idnetsuite_admin",//nombre del valor que se envia a la bd value="?"
                            textKey: "name_admin",//nombre que aparece en el selcct
                            dataFor: "idnetsuite_admin ,name_admin",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "campana_new_edit",
                    queries: [
                        {
                            table: "campanas",//nombre de la tabla
                            where: `WHERE Estado_Campana=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_NetsauiteCampana",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_Campana",//nombre que aparece en el selcct
                            dataFor: "id_NetsauiteCampana,Nombre_Campana",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "proyecto_new_edit",
                    queries: [
                        {
                            table: "proyectos",//nombre de la tabla
                            where: `WHERE estado_proyecto=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_ProNetsuite",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_proyecto",//nombre que aparece en el selcct
                            dataFor: "id_ProNetsuite,Nombre_proyecto",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "subsidiary_new_edit",
                    queries: [
                        {
                            table: "subsidiarias",//nombre de la tabla
                            where: `WHERE Estado_Subsidiaria=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_NetsuiteSub",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_Subsidiaria",//nombre que aparece en el selcct
                            dataFor: "id_NetsuiteSub,Nombre_Subsidiaria",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "custentityestado_civil",
                    queries: [
                        {
                            table: "etados_civil",//nombre de la tabla
                            where: `WHERE estadoCivil=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_civil",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_civil",//nombre que aparece en el selcct
                            dataFor: "id_civil,Nombre_civil",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },
                {
                    value: "custentityestado_civil_extra",
                    queries: [
                        {
                            table: "etados_civil",//nombre de la tabla
                            where: `WHERE estadoCivil=1`, //condicion de filtro
                            selectId: `${selectId}`, //nombre del select
                            valueKey: "id_civil",//nombre del valor que se envia a la bd value="?"
                            textKey: "Nombre_civil",//nombre que aparece en el selcct
                            dataFor: "id_civil,Nombre_civil",//campos a llamar
                            vendorId: "",//vendedor
                            filtro: 2,
                            idData: `${id_data}`
                        },
                    ],
                },





            ];

            // Find the configuration corresponding to the provided value.
            const selectedData = selectData.find((item) => item.value === Value);

            if (!selectedData) {
                throw new Error("Invalid value");
            }

            // Get a reference to the <select> element to be populated.
            const selectElement = document.getElementById(
                selectedData.queries[0].selectId
            );

            // Remove all existing options from the <select> except for the "Choose Client" option.
            while (selectElement.options.length > 1) {
                selectElement.remove(1);
            }

            // Load the new options into the <select>.
            await Promise.all(
                selectedData.queries.map((query) =>
                    this.selectDataLoader.loadSelectData(query, Value)
                )
            );
        } catch (error) {
            // Capture and handle any errors thrown during execution.
            throw error;
        }
    }
}

/*====================================NEW WORK AREA===================================================*/
