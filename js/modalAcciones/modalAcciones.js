async function modalAcciones(id_interno) {

    const data = `name_admin,nombre_lead,idinterno_lead ,email_lead,telefono_lead,proyecto_lead,segimineto_lead,campana_lead`;
    const table = "leads INNER JOIN admins ON leads.id_empleado_lead = admins.idnetsuite_admin";
    const query = `WHERE idinterno_lead =${id_interno}`;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    const { name_admin,idinterno_lead, campana_lead, nombre_lead, email_lead, proyecto_lead, segimineto_lead, telefono_lead } = result.data[0];
    $("#DataName").text(nombre_lead);
    $("#Dataid").text(idinterno_lead);
    $("#estado_cliente").text("Campaña: " + campana_lead);
    $("#proyecto_lead").text("Proyecto: " + proyecto_lead);
    $("#dataidcLIENT").val(idinterno_lead)
    $("#admin_name").text("Asesor: " + name_admin);

    await getLeadDataBitacora(idinterno_lead);
    const changeBannerStyles = () => {
        setTimeout(() => {
            document.getElementById('bitacoraA1').style.display = "none";
            document.getElementById('bitacoraA2').style.display = "block";

        }, 600);
    };
    changeBannerStyles();


    //generamos el enlace para ir a whatsapp

    const cleanedPhoneNumbers = telefono_lead.replace(/[\s-]/g, "");
    const prefijos = "+506"; // Esto puede variar según el prefijo de tu país.
    const whatsappUrl = cleanedPhoneNumbers.includes(prefijos)
        ? `https://wa.me/${cleanedPhoneNumbers}`
        : `https://wa.me/${prefijos}${cleanedPhoneNumbers}`;

    $('#whatsappLinkir1').attr('href', whatsappUrl);


    var PrimerContacto = 'leads/contacto?data=' + idinterno_lead;
    $('#PrimerContacto').attr('href', PrimerContacto);

    //boton de crear evento al cliente
    var LinkCrearEvento = 'evento/createEventLead?data=' + idinterno_lead;
    $('#CrearEvento').attr('href', LinkCrearEvento);

    //boton de colocar como perdido el cliente
    var ColocarPerdido = 'leads/lead_perdido?data=' + idinterno_lead;
    $('#ColocarPerdido').attr('href', ColocarPerdido);

    //boton para colcoar en seguimiento
    var ColocarSeguimiento = 'leads/seguimiento?data=' + idinterno_lead;
    $('#ColocarSeguimiento').attr('href', ColocarSeguimiento);

    var Oportuindad_list = '/oportunidad/list?data=1&data2=' + idinterno_lead;
    // Actualizar el atributo href del enlace de WhatsApp
    $('#Oportuindad_list').attr('href', Oportuindad_list);

    var Oportuindad = 'oportunidad/crear?data=' + idinterno_lead;
    // Actualizar el atributo href del enlace de WhatsApp
    $('#Oportuindad').attr('href', Oportuindad);

    var perfil = 'leads/perfil?data=' + idinterno_lead;
    // Actualizar el atributo href del enlace de WhatsApp
    $('#perfil').attr('href', perfil);

    var llamar = 'tel:' + whatsappUrl;
    // Actualizar el atributo href del enlace de WhatsApp
    $('#llamar').attr('href', llamar);


}
async function getLeadDataBitacora(idinterno_lead) {
    clearHTMLElements();
    const data = `*`;
    const table = `bitacoras INNER JOIN caidas ON bitacoras.id_caida_bit = caidas.id_caida `;
    const query = `WHERE id_lead_bit="${idinterno_lead}" ORDER BY id_bitacora_bit DESC `;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    const bitcora = await divBitacora(result);


}

function clearHTMLElements() {
    $("#bitA1").text("");
    $("#bitA2").text("");
    $("#bitA3").text("");
    $("#bitA4").text("");
}

async function divBitacora(result) {
    const ulElement = document.getElementById('bitacoraA2');
    ulElement.innerHTML = ''; // Limpiar la lista
    let contador = 1;
    result.data.forEach((item) => {
        const fechaCreadoBit = new Date(item.fecha_creado_bit);
        const fechaFormateada = `${fechaCreadoBit.getFullYear()}-${padZero(fechaCreadoBit.getMonth() + 1)}-${padZero(fechaCreadoBit.getDate())} ${padZero(fechaCreadoBit.getHours())}:${padZero(fechaCreadoBit.getMinutes())}:${padZero(fechaCreadoBit.getSeconds())}`;
        let estado2 = "";
        if (item.estado_bit_seg) {
            let estado1 = item.estado_bit.substring(3, 53);
            if (estado1 == "LEAD-OPORTUNIDAD" || estado1 == "LEAD-PRE-RESERVA" || estado1 == "LEAD-RESERVA" || estado1 == "LEAD-CONTRATO" || estado1 == "LEAD-ENTREGADO") {
                estado2 = "/ " + item.estado_bit_seg.substring(3, 53);
            }
        }
        const newLi = document.createElement('li');
        newLi.className = 'event-list';
        newLi.innerHTML = `
            <div class="event-timeline-dot">
                <i class="bx bx-right-arrow-circle"></i>
            </div>
            <div class="d-flex">
                <div class="flex-grow-1">
                    <div>
                        <h6 class="font-size-14 mb-1" id="bitA1">${contador++}- Estado: ${item.estado_bit.substring(3, 53)} ${estado2}</h6>
                        <p class="text-muted" id="bitA2">Accion:  <strong>${item.detalle_bit} </strong></p>
                        <p class="text-muted mb-0"id="bitA3">Motivo:  <strong>${item.nombre_caida}</strong></p>
                        <p class="text-muted mb-0" id="bitA4">Fecha de acción : ${fechaFormateada} </p>
                    </div>
                </div>
            </div>
        `;

        // Agregar el nuevo elemento li al elemento ul con el ID "bitacoraA2"

        ulElement.appendChild(newLi);
    });
    function padZero(number) {
        return number.toString().padStart(2, '0');
    }
}

async function contact_whatsapp() {
    var id_interno = $("#dataidcLIENT").val()

    const data = `telefono_lead`;
    const table = "leads";
    const query = `WHERE idinterno_lead =${id_interno}`;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    const { telefono_lead, segimineto_lead } = result.data[0];


    let estado = segimineto_lead; // Obtiene el valor del elemento con ID 'estadoH6'



    if (estado != "LEAD-OPORTUNIDAD" && estado != "LEAD-PRE-RESERVA" && estado != "LEAD-RESERVA" && estado != "LEAD-CONTRATO" && estado != "LEAD-ENTREGADO") {
        estado = "08-LEAD-SEGUIMIENTO";
    }



    const cleanedPhoneNumbers = telefono_lead.replace(/[\s-]/g, "");
    const prefijos = "+506"; // Esto puede variar según el prefijo de tu país.
    const whatsappUrl = cleanedPhoneNumbers.includes(prefijos)
        ? `https://wa.me/${cleanedPhoneNumbers}`
        : `https://wa.me/${prefijos}${cleanedPhoneNumbers}`;



    const fech2 = dateFilterResult.today + ":00: 00";




    const detalle= "Contacto generadon desde el boton de whatsapp";

    const valores = {
        valorDeCaida: 51,
        tipo: "Se generó un evento para el cliente",
        estado_lead: 1,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3, 
    };


    Swal.fire({
        title: '¿Está seguro?',
        text: "Generar un contacto automático y abrir WhatsApp",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, generar'
    }).then(async (result) => {
        if (result.isConfirmed) {

            const StatusCodeBitacora = await BitacoraCliente(id_interno, valores, detalle, estado);
            if (StatusCodeBitacora.statusCode == 200) {
                const StatusCodeCliente = await ActualizarCliente(id_interno, valores, estado);
                if (StatusCodeCliente.statusCode == 200) {
                    console.log("StatusCodeCliente: ", StatusCodeCliente);
                    Swal.fire({
                        title: 'El reporte se ha generado correctamente',
                        text: "¿Deseas abrir WhatsApp y contactar al lead?",
                        icon: 'success',
                        iconHtml: '✔',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, ir a whatsapp'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            /**
                             * Abre una nueva ventana o pestaña del navegador para iniciar WhatsApp
                             * y contactar al lead.
                             ===============================================================================*/
                            window.open(whatsappUrl, "_blank");
                            /**
                             * Recarga la página actual después de realizar la acción.
                             ===============================================================================*/
                            window.location.reload();
                        } else {
                            /**
                             * Recarga la página actual sin abrir WhatsApp.
                             ===============================================================================*/
                            window.location.reload();
                        }
                    });
                }
            }
        }
    });
}




function cerrar() {
    $('.modal').modal('hide');
}

function ejecutar(idelemento) {
    var aux = document.createElement("div");
    aux.setAttribute("contentEditable", true);
    aux.innerHTML = document.getElementById(idelemento).innerHTML;
    aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)");
    document.body.appendChild(aux);
    aux.focus();
    document.execCommand("copy");
    document.body.removeChild(aux);
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: 'Nombre Copiado: '
    })
}
