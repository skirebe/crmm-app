

//darle formato de fecha  inicial
$('#startFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//darle formato de fecha  inicial
$('#endtFilter').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd',
});

//didentificamos si esta iniciando sesion en un movil o navegador
var element = document.getElementById("collapseExample");
if (esDispositivoMovil()) {
    element.classList.remove("show");
}






ExtraDataEventos()
// Comentario explicando la función
async function ExtraDataEventos() {

    //Modal poara esperar a que cargen los datos
    mostrarSwalopacidadDiv();


    //funcion para darle formato a la fecha definida
    function formatDate(date, days) {
        const formattedDate = new Date(date);
        return formattedDate.toISOString().split('T')[0];
    }



    //vamos a extraer solo los datos necesarios para acelerar la consulta
    const data = 'c.id_calendar ,c.id_lead,le.campana_lead,le.proyecto_lead, le.idinterno_lead ,le.nombre_lead, c.nombre_calendar, c.fechaIni_calendar, c.horaInicio_calendar,c.estado_calendar,c.accion_calendar,c.tipo_calendar,c.decrip_calendar,c.cita_lead,c.citas_chek,c.masDeUnaCita_calendar';

    //definimos la tabla y las relaciones para unir tablas
    const table = 'calendars as c INNER JOIN leads as le ON le.idinterno_lead = c.id_lead';

    //aun no sabemos cual es el where por eso esta vacio
    let where = '';


    //extraemos la fecha inicio y fecha final
    const startDate = formatDate($("#startFilter").val(), -1);
    const endDate = formatDate($("#endtFilter").val(), +1);




    // Obtener la fecha de hoy
    var fechaHoy = new Date();

    // Calcular las fechas de un día antes y un día después
    var fechaUnDiaAntes = new Date(fechaHoy);


    var fechaUnDiaDespues = new Date(fechaHoy);
    fechaUnDiaDespues.setDate(fechaHoy.getDate() + 1);

    // Formatear las fechas en el formato deseado "YYYY-MM-DD"
    function formatearFecha(fecha) {
        var year = fecha.getFullYear();
        var month = (fecha.getMonth() + 1).toString().padStart(2, '0');
        var day = fecha.getDate().toString().padStart(2, '0');
        return year + '-' + month + '-' + day;
    }


    var start = formatearFecha(fechaUnDiaAntes);
    var end = formatearFecha(fechaUnDiaDespues);

    //ahora si creamos el where de la consulta segun el parametro de la url
    const filterConditions = {
        1: `and estado_calendar=1 and accion_calendar='Pendiente' and fechaIni_calendar >=  '${start}' and fechaIni_calendar < '${end}'`,
        2: `and accion_calendar='Completado' and DATE(fechaIni_calendar) >= "${startDate}" AND DATE(fechaIni_calendar) < "${endDate}" ORDER BY estado_calendar DESC`,
        3: `and (citas_chek = 1 OR cita_lead = 1)  and DATE(fechaIni_calendar) >= "${startDate}" AND DATE(fechaIni_calendar) < "${endDate}" ORDER BY estado_calendar DESC`,
        4: `and accion_calendar='Cancelado' and DATE(fechaIni_calendar) >= "${startDate}" AND DATE(fechaIni_calendar) < "${endDate}" ORDER BY estado_calendar DESC`,
        5: `and DATE(fechaIni_calendar) >= "${startDate}" AND DATE(fechaIni_calendar) <= "${endDate}" ORDER BY estado_calendar DESC`,
        default: `and accion_calendar='Pendiente' and DATE(fechaIni_calendar) >= "${startDate}" AND DATE(fechaIni_calendar) < "${endDate}" ORDER BY estado_calendar DESC`
    };

    //recorremos la lista y asignamos el nnuevo valor a where
    where = filterConditions[dataParam1] || filterConditions.default;
    /*
    llamamos la funcion que se encarga de ir al api a extraer los datos
    *Data? se encarga de extraer solo los campos que nececitamos
    *table? tabla + relaciones
    *"select"? metodo o tipo de consulta ejemplo SELECT * FROM ?
    *"id_empleado_lead"? extraemos todos los datos que pertenescan al vendedor
    *1? es si requiero que el select extraiga datos del vendedor si es 2 entonces no.
    */
    const result = await fetchRecords(data, table, where, "select", "c.id_admin", 1);




    //recorremos los datos y segun la data de la url y vista mostramos los datos
    async function procesarDatos() {
        return new Promise(async (resolve) => {
            // validamos que sea mayor a cero
            if (result.data && result.data.length > 0) {
                //recorremos data
                result.data.forEach(async (data, index) => {
                    console.log("data: ", data);
                    const cita = data.cita_lead === 1 ? "Cita" : "No aplica";

                    const lead = data.id_lead === 0 ? "--" : data.nombre_lead;
                    createRow(data, cita, lead);


                    //esperamos a que se cumpla la promesa
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));
                    if (index === result.data.length - 1) {
                        resolve();
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    }
    //ejecutamos esa funcion y luego cargamos la tabla con los datos llamados
    procesarDatos().then(() => {
        //al terminar el then ejecutamos datatable
        $(document).ready(function () {

            //columas a definir para el filtro y descargar en excel
            var columnsDatatable = [];
            var columsDatatableExel = [];

            columnsDatatable = [0, 1, 5, 6, 7,9,10];

            columsDatatableExel = [0, 1, 2, 3, 4, 5, 6, 7,9,10];

            var table = $("#datatable-buttons").DataTable({
                initComplete: function () {
                    $("#preloader").hide();
                },
                processing: true,
                searchPanes: {
                    cascadePanes: true,
                    dtOpts: {
                        select: {
                            style: "multi",
                        },
                        count: {
                            show: false,
                        },
                    },
                    columns: columnsDatatable,
                },
                columnDefs: [
                    {
                        searchPanes: {
                            show: true,
                            initCollapsed: true,
                        },
                        targets: columnsDatatable,
                    },
                ],
                dom: "PBfrtip",
                stateSave: true,
                order: [[3, "asc"]],
                pageLength: 59,
                lengthMenu: [
                    [10, 25, 50, 200, -1],
                    [10, 25, 50, 200, "All"],
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "No se encontraron resultados",
                    info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                    infoFiltered: "(filtrado de un total de _MAX_ registros)",
                    sSearch: "Buscar:",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior",
                    },
                    sProcessing: "Cargando...",
                },
                buttons: [
                    {
                        extend: "excel",
                        footer: true,
                        titleAttr: "Exportar a excel",
                        className: "btn btn-success",
                        exportOptions: {
                            columns: columsDatatableExel,
                        },
                    },
                ],
            });

            $("#datatable-buttons tbody").on("click", "tr", function () {
                // Obtiene los datos de la fila en la que se hizo clic.
                var data = table.row(this).data();
                let nombre_evento = data[0];
                let id_evento = data[8];
                let id_lead = data[2];

                // Muestra un modal de confirmación para editar el evento.
                Swal.fire({
                    title: "¿Quieres editar este evento?",
                    html: "Título del evento: <br><strong>(" + nombre_evento + ")",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Sí, editar",
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Redirige a la página de edición del evento con los datos del evento.
                        window.location.href =
                            "/evento/edit?data=" + id_lead + "&data2=" + id_evento;
                    }
                });
            });


        });

        // Cambiar estilos de banner
        const changeBannerStyles = () => {
            setTimeout(() => {
                const tableleads = document.getElementById(`tableleads`);
                tableleads.style.display = "none";
                if (esDispositivoMovil()) {
                    var elemento = document.getElementById("table-body");
                    elemento.style.opacity = "20";
                } else {
                    var elemento = document.getElementById("table-body");
                    if (elemento) {
                        elemento.style.display = "";
                    }
                }
                Swal.close();
            }, 100);


        };

        // Cambiar estilos de banner
        changeBannerStyles();
    });
}

// Esta función se encarga de llenar la tabla con los datos del resultado
function createRow(leadData, cita, lead) {
    // Construimos un arreglo de las variables
    const { id_calendar, idinterno_lead, proyecto_lead, campana_lead, nombre_lead, nombre_calendar, fechaIni_calendar, horaInicio_calendar, estado_calendar, accion_calendar, tipo_calendar, decrip_calendar, cita_lead, citas_chek, masDeUnaCita_calendar } = leadData;
    let fech = new Date(fechaIni_calendar);

    // Formateamos la fecha
    const fechaActualizada = new Date(fech);
    const anio = fechaActualizada.getFullYear();
    const mes = fechaActualizada.getMonth() + 1;
    const dia = fechaActualizada.getDate();
    const fechaFormateada = `${anio} -${mes.toString().padStart(2, '0')} -${dia.toString().padStart(2, '0')} `;

    const tableBody = document.querySelector("#table-body");
    // Cantidad de td para llenar la tabla
    const row = document.createElement("tr");
    row.innerHTML = `
    <td style="text-align: right; font-weight: bold;" > ${nombre_calendar}</td>
    <td style="text-align: right; font-weight: bold;">${lead}</td>
    <td style="text-align: right; font-weight: bold;" hidden>${idinterno_lead}</td>
    <td style="text-align: right; font-weight: bold;">${fechaFormateada}</td>
    <td style="text-align: right; font-weight: bold;">${horaInicio_calendar}</td>
    <td style="text-align: right; font-weight: bold;" >${accion_calendar}</td>
    <td style="text-align: right; font-weight: bold;" >${tipo_calendar}</td>
    <td style="text-align: right; font-weight: bold;" >${cita}</td>
    <td style="text-align: right; font-weight: bold;" hidden>${id_calendar}</td>
    <td style="text-align: right; font-weight: bold;" >${proyecto_lead}</td>
    <td style="text-align: right; font-weight: bold;" >${campana_lead}</td>

`;

    // Agregamos los datos de 'row' al 'tableBody'
    tableBody.appendChild(row);
}

