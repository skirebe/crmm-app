
const dataManager = new DataManager();
async function ExtraerEvento() {
    const data = `estado_calendar,accion_calendar,id_lead,nombre_calendar,fechaIni_calendar,fechaFin_calendar,horaInicio_calendar,horaFinal_calendar,decrip_calendar,tipo_calendar,cita_lead,masDeUnaCita_calendar`;
    const table = "calendars"
    const query = `WHERE id_calendar = ${dataParam2}`;
    const result = await fetchRecords(data, table, query, "select", "", 2);
    result.data.forEach(async (data) => {

        $("#nombre_evento").val(data.nombre_calendar);
        dataManager.fillSelect("tipo_evento_edit", "", "tipo_evento_edit", data.tipo_calendar);
        dataManager.fillSelect("listaLeads", "", "listaLeads", data.id_lead);
        $("#textarea").val(data.decrip_calendar);

        // Supongamos que data.fechaIni_calendar y data.fechaFin_calendar son strings en el formato "2023-11-03T16:45"
        // Primero, encuentre la posición del carácter "T" en las cadenas
        var tPositionIni = data.fechaIni_calendar.indexOf("T");
        var tPositionFin = data.fechaFin_calendar.indexOf("T");

        // Luego, toma solo la parte de la cadena antes de "T16:45"
        var fechaIniSinHora = data.fechaIni_calendar.substring(0, tPositionIni);
        var fechaFinSinHora = data.fechaFin_calendar.substring(0, tPositionFin);

        $("#start").val(fechaIniSinHora);
        $("#end").val(fechaFinSinHora);

        $("#timepicker3").val(data.horaInicio_calendar);
        $("#timepicker").val(data.horaFinal_calendar);

        if (data.estado_calendar > 0 && data.accion_calendar === "Pendiente") {

            document.getElementById("completar").style.display = '';
            document.getElementById("cancelar").style.display = '';
        } else {
            document.getElementById("reactivar").style.display = '';
        }

        $("#estado_lead").val("NO APLICA");
        if (data.id_lead != 0) {
            await getLeadData(data.id_lead, "", "CrearEvento");
            document.getElementById("activar").style.display = '';
        }

    });
}
async function editarEvento() {
    // Obtener valores de los campos del formulario
    const tipoDeEvento = $("#tipo_evento_edit").val();
    const hora_inicio = $("#timepicker3").val();
    const hora_final = $("#timepicker").val();
    const descpDelEvento = $("#textarea").val();
    const nombre_evento = $("#nombre_evento").val();

    if (hora_inicio === hora_final) {
        mostrarAlerta('Las horas de inicio y finalización son iguales, edita la hora de inicio para calcular la nueva hora', true);
        return;
    }

    // Definir colores para diferentes tipos de eventos
    const colores = {
        "LLamada": "#556ee6",
        "Whatsapp": "#556ee6",
        "Correo": "#556ee6",
        "Tarea": "#343a40",
        "Reunion": "#34c38f",
        "Seguimientos": "#f46a6a"
    };

    // Obtener el color del evento basado en el tipo de evento seleccionado
    let colorDelEvento = colores[tipoDeEvento] || "";

    // Definir campos obligatorios del formulario
    const campos = [
        'nombre_evento', 'tipo_evento', 'start', 'end', 'timepicker3', 'timepicker', 'textarea'
    ];

    // Filtrar los campos que están vacíos
    const camposVacios = campos.filter(campo => $("#" + campo).val() === '');

    // Validar si hay campos obligatorios vacíos y resaltarlos
    if (camposVacios.length > 0) {
        camposVacios.forEach(campo => $("#" + campo).css("border-color", "red").on("change", () => {
            if ($("#" + campo).val() !== "") {
                $("#" + campo).css("border-color", "#ccc");
            }
        }));
        mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
        return;

    }
    // Obtener fechas de inicio y finalización
    const fecha_inicio = $("#start").val();
    const fecha_final = $("#end").val();
    // Validar si la fecha final es anterior a la fecha inicial
    if (fecha_final < fecha_inicio) {
        mostrarAlerta(`Error: La fecha final del evento no puede ser anterior a la fecha inicial.\n
            Fecha inicial establecida: ${fecha_inicio}\n
            Fecha final ingresada: ${fecha_final}`);
        return;
    }

    // Obtener valores de las casillas de verificación
    let cita_chek = $('#square-switch3').is(":checked") ? 1 : 0;
    let cita_chek2 = $('#square-switch4').is(":checked") ? 1 : 0;

    // Obtener valor de la casilla "citavalor"
    let citavalor = $("#citavalor").val();
    let id_login_acces = $("#id_login").val();
    let valorde_cehk = 1;

    // Validar si la opción de primera cita está seleccionada y si se ha asignado un lead
    let id_le = $("#id_le").val() || 0;
    if (citavalor > 0) {
        cita_chek = 1;
        valorde_cehk = 0;
    }

    if (cita_chek == 1 && id_le == 0) {
        mostrarAlerta('Para poder generar el evento, es necesario asignar un lead si la opción de primera cita está seleccionada. Por favor, asegúrate de incluir un lead antes de continuar.');
        return;
    }

    // Cambiar el color del evento si la opción de primera cita está seleccionada
    if (cita_chek === 1) {
        colorDelEvento = "#f1b44c";
    }






    function convertirHoraAMPMa24(horaAMPM) {
        var partesHora = horaAMPM.split(':');
        var hora = parseInt(partesHora[0]);
        var minutos = parseInt(partesHora[1]);
        var esPM = horaAMPM.includes('PM');

        if (esPM && hora < 12) {
            hora += 12;
        } else if (!esPM && hora === 12) {
            hora = 0;
        }

        return hora.toString().padStart(2, '0') + ':' + minutos.toString().padStart(2, '0');
    }

    // Obtener fechas y horas en formato adecuado para el evento
    var fecha24_inicio = fecha_inicio;
    var hora24_inicio = convertirHoraAMPMa24(hora_inicio);
    var fecha_inicio_final = fecha24_inicio + 'T' + hora24_inicio;


    // Validar y actualizar estado
    const estado = $("#estado_lead").val();

    const valores = {
        valorDeCaida: 51,
        tipo: "Se generó un evento para el cliente",
        estado_lead: 1,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };

    const fecha_final2 = new Date($("#end").val()); // Convierte el valor de #end a un objeto Date
    fecha_final2.setDate(fecha_final2.getDate() + 1); // Añade dos días a la fecha
    // Formatear la fecha en el formato deseado
    const fecha_formateada = `${fecha_final2.getFullYear()}-${(fecha_final2.getMonth() + 1).toString().padStart(2, '0')}-${fecha_final2.getDate().toString().padStart(2, '0')}`;
    var hora24_final = convertirHoraAMPMa24(hora_final);
    var fecha_final_final = fecha_formateada + 'T' + hora24_final;
    Swal.fire({
        title: '¿Está seguro?',
        text: '¿Confirma que desea editar este evento?',
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, editar'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
            const data = `nombre_calendar="${nombre_evento}"
            , color_calendar="${colorDelEvento}"
            , id_lead="${id_le}"
            , id_admin="${storedAdminData.NetsuiteId_acceso}"
            , fechaIni_calendar="${fecha_inicio_final}"
            , fechaFin_calendar="${fecha_final_final}"
            , horaInicio_calendar="${hora_inicio}"
            , horaFinal_calendar="${hora_final}"
            , decrip_calendar="${descpDelEvento}"
            , tipo_calendar="${tipoDeEvento}"
            , cita_lead="${cita_chek}"
            , citas_chek="${valorde_cehk}"
            ,masDeUnaCita_calendar="${cita_chek2}"
            `;

            const table = "calendars"
            const query = `WHERE id_calendar ="${dataParam2}";`;
            const result = await fetchRecords(data, table, query, "update", "", 2);
            try {
                if (result.statusCode == 200) {
                    Swal.close();
                    // Muestra un mensaje de éxito al usuario y ofrece opciones adicionales
                    Swal.fire({
                        title: '¡Evento editado con éxito! ',
                        text: "¿Qué desea hacer a continuación?",
                        icon: 'question',
                        iconHtml: '✔️',
                        width: "40em",
                        padding: "0 0 1.20em",
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Volver a la vista anterior',
                        denyButtonText: `Calendario`,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Vuelve a la vista anterior en la navegación.
                            javascript: history.go(-1)
                        }
                        else if (result.isDenied) {
                            // Redirige a la página de perfil del cliente.
                            window.location.href = "/calendario";
                        } else {
                            // Recarga la página actual.
                            location.reload();
                        }
                    })
                } else {
                    // La función no se ejecutó correctamente, muestra un mensaje de error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se pudo crear el evento, inténtalo de nuevo en unos minutos',
                    })
                }
            } catch (err) {
                console.log("err: ", err);
            }
        }
    });
}

ExtraerLead("CreEvento");
validacionCmapo();
ExtraerEvento();



async function Accion(ids) {
    if (ids == 1) {
        mensaje = "Esta seguro de completar este evento?";
        valor_estado = 0;
        estado = "Completado";
        nota = "Evento Completado";
        accion = 1;
    }
    if (ids == 0) {
        mensaje = "Esta seguro de Cancelar este evento?";
        valor_estado = 0;
        estado = "Cancelado";
        nota = "Evento Cancelado";
        accion = 0;
    }
    if (ids == 2) {
        mensaje = "Esta seguro de Reactivar este evento?";
        valor_estado = 1;
        estado = "Pendiente";
        nota = "Evento Reactivado";
        accion = 2;

    }


    const estados = $("#estado_lead").val();
    const nombre_evento = $("#nombre_evento").val();
    const descpDelEvento = nota + " : " + nombre_evento;
    const valores = {
        valorDeCaida: 20,
        tipo: nota,
        estado_lead: 1,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };

    Swal.fire({
        title: '¿Está seguro?',
        text: "Detalle : " + mensaje,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const dataValue1 = new URLSearchParams(new URL(window.location.href).search).get('data2');
            const data = `estado_calendar="${valor_estado}" ,accion_calendar="${estado}"`;
            const table = "calendars"
            const query = `WHERE id_calendar ="${dataValue1}";`;
            const result = await fetchRecords(data, table, query, "update", "", 2);

            if (result.statusCode == 200) {
                if (dataParam1 != 0) {
                    const StatusCodeBitacora = await BitacoraCliente(dataParam1, valores, descpDelEvento, estados);
                    if (StatusCodeBitacora.statusCode == 200) {
                        const StatusCodeCliente = await ActualizarCliente(dataParam1, valores, estados);
                        if (StatusCodeCliente.statusCode == 200) {
                            Swal.close();
                            Swal.fire({
                                title: '¡Evento estado editado con éxito! ',
                                text: "¿Qué desea hacer a continuación?",
                                icon: 'question',
                                iconHtml: '✔️',
                                width: "40em",
                                padding: "0 0 1.20em",
                                showDenyButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Volver a la vista anterior',
                                denyButtonText: `Ir al perfil del cliente`,
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    // Vuelve a la vista anterior en la navegación.
                                    javascript: history.go(-1)
                                }
                                else if (result.isDenied) {
                                    // Redirige a la página de perfil del cliente.
                                    window.location.href = "leads/perfil?data=" + dataParam1;
                                } else {
                                    // Recarga la página actual.
                                    location.reload();
                                }
                            })
                        }
                    }
                } else {
                    Swal.close();
                    // Muestra un mensaje de éxito al usuario y ofrece opciones adicionales
                    Swal.fire({
                        title: '¡Evento estado editado con éxito!',
                        text: "¿Qué desea hacer a continuación?",
                        icon: 'question',
                        iconHtml: '✔️',
                        width: "40em",
                        padding: "0 0 1.20em",
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Volver a la vista anterior',
                        denyButtonText: `Calendario`,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Vuelve a la vista anterior en la navegación.
                            javascript: history.go(-1)
                        }
                        else if (result.isDenied) {
                            // Redirige a la página de perfil del cliente.
                            window.location.href = "/calendario";
                        } else {
                            // Recarga la página actual.
                            location.reload();
                        }
                    })
                }

            } else {
                // La función no se ejecutó correctamente, muestra un mensaje de error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se pudo crear el evento, inténtalo de nuevo en unos minutos',
                })
            }






        }
    })
}


