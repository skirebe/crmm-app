const mostrarAlerta = (mensaje, error = false) => {
    Swal.fire({
        icon: error ? 'error' : 'info',
        title: error ? 'Error' : 'Información',
        text: mensaje,
    });
};

const crearEvento = async () => {
    // Obtener valores de los campos del formulario
    const tipoDeEvento = $("#tipo_evento").val();
    const hora_inicio = $("#timepicker3").val();
    const hora_final = $("#timepicker").val();
    const descpDelEvento = $("#textarea").val();
    const nombre_evento = $("#nombre_evento").val();

    if (hora_inicio === hora_final) {
        mostrarAlerta('Las horas de inicio y finalización son iguales, edita la hora de inicio para calcular la nueva hora', true);
        return;
    }

    // Definir colores para diferentes tipos de eventos
    const colores = {
        "LLamada": "#556ee6",
        "Whatsapp": "#556ee6",
        "Correo": "#556ee6",
        "Tarea": "#343a40",
        "Reunion": "#34c38f",
        "Seguimientos": "#f46a6a"
    };

    // Obtener el color del evento basado en el tipo de evento seleccionado
    let colorDelEvento = colores[tipoDeEvento] || "";

    // Definir campos obligatorios del formulario
    const campos = [
        'nombre_evento', 'tipo_evento', 'start', 'end', 'timepicker3', 'timepicker', 'textarea'
    ];

    // Filtrar los campos que están vacíos
    const camposVacios = campos.filter(campo => $("#" + campo).val() === '');

    // Validar si hay campos obligatorios vacíos y resaltarlos
    if (camposVacios.length > 0) {
        camposVacios.forEach(campo => $("#" + campo).css("border-color", "red").on("change", () => {
            if ($("#" + campo).val() !== "") {
                $("#" + campo).css("border-color", "#ccc");
            }
        }));
        mostrarAlerta('¡Se deben llenar los campos obligatorios!', true);
        return;

    }
    // Obtener fechas de inicio y finalización
    const fecha_inicio = $("#start").val();
    const fecha_final = $("#end").val();
    // Validar si la fecha final es anterior a la fecha inicial
    if (fecha_final < fecha_inicio) {
        mostrarAlerta(`Error: La fecha final del evento no puede ser anterior a la fecha inicial.\n
            Fecha inicial establecida: ${fecha_inicio}\n
            Fecha final ingresada: ${fecha_final}`);
        return;
    }

    // Obtener valores de las casillas de verificación
    let cita_chek = $('#square-switch3').is(":checked") ? 1 : 0;
    let cita_chek2 = $('#square-switch4').is(":checked") ? 1 : 0;

    // Obtener valor de la casilla "citavalor"
    let citavalor = $("#citavalor").val();
    let id_login_acces = $("#id_login").val();
    console.log("id_login_acces: ", id_login_acces);
    let valorde_cehk = 1;

    // Validar si la opción de primera cita está seleccionada y si se ha asignado un lead
    let id_le = $("#id_le").val() || 0;



    if (citavalor > 0) {
        cita_chek = 0;
        valorde_cehk = 0;
    }

    if (cita_chek == 1 && id_le == 0) {
        mostrarAlerta('Para poder generar el evento, es necesario asignar un lead si la opción de primera cita está seleccionada. Por favor, asegúrate de incluir un lead antes de continuar.');
        return;
    }

    // Cambiar el color del evento si la opción de primera cita está seleccionada
    if (cita_chek === 1) {
        colorDelEvento = "#f1b44c";
    }




    function convertirHoraAMPMa24(horaAMPM) {
        var partesHora = horaAMPM.split(':');
        var hora = parseInt(partesHora[0]);
        var minutos = parseInt(partesHora[1]);
        var esPM = horaAMPM.includes('PM');

        if (esPM && hora < 12) {
            hora += 12;
        } else if (!esPM && hora === 12) {
            hora = 0;
        }

        return hora.toString().padStart(2, '0') + ':' + minutos.toString().padStart(2, '0');
    }

    // Obtener fechas y horas en formato adecuado para el evento
    var fecha24_inicio = fecha_inicio;
    var hora24_inicio = convertirHoraAMPMa24(hora_inicio);
    var fecha_inicio_final = fecha24_inicio + 'T' + hora24_inicio;


    // Validar y actualizar estado
    const estado = $("#estado_lead").val();

    const valores = {
        valorDeCaida: 51,
        tipo: "Se generó un evento para el cliente",
        estado_lead: 1,
        accion_lead: 6,
        seguimiento_calendar: 0,
        valor_segimineto_lead: 3,
    };

    const fecha_final2 = new Date($("#end").val()); // Convierte el valor de #end a un objeto Date
    fecha_final2.setDate(fecha_final2.getDate() + 1); // Añade dos días a la fecha
    // Formatear la fecha en el formato deseado
    const fecha_formateada = `${fecha_final2.getFullYear()}-${(fecha_final2.getMonth() + 1).toString().padStart(2, '0')}-${fecha_final2.getDate().toString().padStart(2, '0')}`;
    var hora24_final = convertirHoraAMPMa24(hora_final);
    var fecha_final_final = fecha_formateada + 'T' + hora24_final;
    Swal.fire({
        title: '¿Está seguro?',
        text: '¿Confirma que desea generar este evento?',
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, crear'
    }).then(async (result) => {
        if (result.isConfirmed) {
            mostrarSwalopacidadDiv();
            const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));
            const data = `nombre_calendar, color_calendar, id_lead, id_admin, fechaIni_calendar, fechaFin_calendar, horaInicio_calendar, horaFinal_calendar, decrip_calendar, tipo_calendar, cita_lead,citas_chek,masDeUnaCita_calendar`;
            const table = "calendars"
            const query = `"${nombre_evento}","${colorDelEvento}","${id_le}","${storedAdminData.NetsuiteId_acceso}","${fecha_inicio_final}","${fecha_final_final}","${hora_inicio}","${hora_final}","${descpDelEvento}","${tipoDeEvento}","${cita_chek}","${valorde_cehk}","${cita_chek2}"`;
            const result = await fetchRecords(data, table, query, "insert", "", 2);
            try {
                if (result.statusCode == 200) {
                    if (id_le != 0) {
                        const StatusCodeBitacora = await BitacoraCliente(id_le, valores, descpDelEvento, estado);
                        if (StatusCodeBitacora.statusCode == 200) {
                            const StatusCodeCliente = await ActualizarCliente(id_le, valores, estado);
                            if (StatusCodeCliente.statusCode == 200) {
                                Swal.close();
                                Swal.fire({
                                    title: '¡Evento creado con éxito! ',
                                    text: "¿Qué desea hacer a continuación?",
                                    icon: 'question',
                                    iconHtml: '✔️',
                                    width: "40em",
                                    padding: "0 0 1.20em",
                                    showDenyButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Volver a la vista anterior',
                                    denyButtonText: `Ir al perfil del cliente`,
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        // Vuelve a la vista anterior en la navegación.
                                        javascript: history.go(-1)
                                    }
                                    else if (result.isDenied) {
                                        // Redirige a la página de perfil del cliente.
                                        window.location.href = "leads/perfil?data=" + id_le;
                                    } else {
                                        // Recarga la página actual.
                                        location.reload();
                                    }
                                })
                            }
                        }
                    } else {
                        Swal.close();
                        // Muestra un mensaje de éxito al usuario y ofrece opciones adicionales
                        Swal.fire({
                            title: '¡Evento creado con éxito! ',
                            text: "¿Qué desea hacer a continuación?",
                            icon: 'question',
                            iconHtml: '✔️',
                            width: "40em",
                            padding: "0 0 1.20em",
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Volver a la vista anterior',
                            denyButtonText: `Calendario`,
                        }).then((result) => {
                            if (result.isConfirmed) {
                                // Vuelve a la vista anterior en la navegación.
                                javascript: history.go(-1)
                            }
                            else if (result.isDenied) {
                                // Redirige a la página de perfil del cliente.
                                window.location.href = "/calendario";
                            } else {
                                // Recarga la página actual.
                                location.reload();
                            }
                        })
                    }
                } else {
                    // La función no se ejecutó correctamente, muestra un mensaje de error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se pudo crear el evento, inténtalo de nuevo en unos minutos',
                    })
                }
            } catch (err) {
                console.log("err: ", err);
            }
        }
    });
}