/**
 * Esta función realiza diversas tareas relacionadas con la validación de campos y la manipulación de valores en formularios.
 */
function validacionCmapo() {
    // Obtén los elementos de input por su ID
    var startInput = document.getElementById("start");
    var endInput = document.getElementById("end");

    function sumarUnaHoraYMostrarHoraFormato12H() {
        // Obtén la hora actual
        const horaActual = new Date();

        // Suma una hora
        horaActual.setMinutes(horaActual.getMinutes() + 60);

        // Obtiene la hora y los minutos
        const hora = horaActual.getHours();
        const minutos = horaActual.getMinutes();

        // Determina si es AM o PM
        const amPm = hora < 12 ? 'AM' : 'PM';

        // Convierte la hora a formato 12 horas
        let horaFormato12H = hora % 12;
        horaFormato12H = horaFormato12H === 0 ? 12 : horaFormato12H; // Si es 0, establece 12

        // Formatea la hora y los minutos con AM o PM
        const horaFormateada = `${horaFormato12H}:${minutos < 10 ? '0' : ''}${minutos} ${amPm}`;
        $("#timepicker").val(horaFormateada);
    }

    sumarUnaHoraYMostrarHoraFormato12H();

    /**
     * Esta función se utiliza para asignar el valor del campo "start" al campo "end" cuando cambia el contenido de "start".
     */
    function asignarValor() {
        endInput.value = startInput.value;
    }

    // Agrega un evento de escucha para el evento 'input' en startInput
    startInput.addEventListener("input", asignarValor);

    // JavaScript
    $(document).ready(function () {
        // Agregar eventos 'change' e 'input' al elemento con ID 'timepicker3'
        $("#timepicker3").on("change input", function (event) {
            // Obtener el nuevo valor del campo de entrada con ID 'timepicker3'
            var nuevoValor = $(this).val();
            // Convertir el valor del campo de entrada a un objeto Date
            var fecha = new Date("01/01/1970 " + nuevoValor);

            // Sumar 60 minutos al objeto Date
            fecha.setMinutes(fecha.getMinutes() + 60);

            // Obtener la hora y minutos en formato de hora de 12 horas (AM/PM) del objeto Date sumado
            var nuevoValorSumado = fecha.toLocaleTimeString("en-US", {
                hour12: true,
                hour: "numeric",
                minute: "numeric",
            });

            // Asignar el nuevo valor al campo de entrada con ID 'timepicker'
            $("#timepicker").val(nuevoValorSumado);
        });
    });
}
