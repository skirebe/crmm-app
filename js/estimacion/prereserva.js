async function prereserva() {

    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    const dataValue1 = new URLSearchParams(new URL(window.location.href).search).get('data');
    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Desea enviar la pre-reserva?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, enviar'
    }).then(async (result) => {
        if (result.isConfirmed) {
            let timerInterval;
            Swal.fire({
                width: '700px', // Establece el ancho del modal,
                title: 'Cargando los datos...',
                html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
                timer: 48000,
                timerProgressBar: true,
                allowOutsideClick: false, // Evita cerrar al hacer clic fuera del modal
                didOpen: () => {
                    Swal.showLoading();
                    const b = Swal.getHtmlContainer().querySelector('b');
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft();
                    }, 100);
                },
                willClose: () => {
                    clearInterval(timerInterval);
                }
            }).then((result) => {

            });
            try {
                const data = {
                    id: dataValue
                };
                const response = await fetch("https://api-crm.roccacr.com/api/v2/estimacion/prereserva", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });

                const result = await response.json();
                var ExTraerResultado = result['Detalle'];
                if (ExTraerResultado.status === 200) {



                    const estado = "03-LEAD-PRE-RESERVA";
                    const valores = {
                        valorDeCaida: 51,
                        tipo: "Se envio la PRE-RESERVA",
                        estado_lead: 1,
                        accion_lead: 6,
                        seguimiento_calendar: 0,
                        valor_segimineto_lead: 3,
                    };
                    const descpDelEvento = "Se envio la PRE-RESERVA";

                    const StatusCodeBitacora = await BitacoraCliente(dataValue1, valores, descpDelEvento, estado);
                    if (StatusCodeBitacora.statusCode == 200) {
                        const StatusCodeCliente = await ActualizarCliente(dataValue1, valores, estado);
                        if (StatusCodeCliente.statusCode == 200) {

                            var fecha_prereserva = $("#pre_5").val() || "";
                            const today = new Date();
                            const formattedDate = today.toISOString().split('T')[0];

                            const data = `pre_reserva=1,envioPreReserva="${formattedDate}",fechaClienteComprobante_est="${fecha_prereserva}"`;
                            const table = "estimaciones"
                            const query = `WHERE idEstimacion_est ="${dataValue}";`;

                            const result = await fetchRecords(data, table, query, "update", "", 2);


                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se envió la Pre-Reserva',
                                showConfirmButton: false,
                                timer: 2500
                            });
                            location.reload();
                        }
                    }

                } else {
                    Swal.fire('Algo no está bien.', 'No se pudo hacer la pre reserva.', 'question');
                }
            } catch (error) {
                alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comuníquese con soporte');
                console.error(error);
            }
        }
    });
}
