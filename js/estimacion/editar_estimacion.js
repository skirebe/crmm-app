

const editar_estimacion = async () => {

    const mostrarAlerta = (mensaje, error = false) => {
        Swal.fire({
            icon: error ? 'error' : 'info',
            title: error ? 'Error' : 'Información',
            text: mensaje,
        });
    };


    const idEst = new URLSearchParams(new URL(window.location.href).search).get('data2');


    /*MONTO TOTAL*/
    let custbody_ix_total_amount = $("#custbody_ix_total_amount").val();
    /*OPORTUNIDAD*/
    let opportunity = $("#opportunity").val();
    /*PRECIO DE LISTA:*/
    let custbody13 = $("#custbody13").val();
    /*SUBSIDIARIA*/
    let subsidiary = $("#subsidiary").val();
    /*PRIMA TOTAL*/
    let custbody39 = $("#custbody39").val();
    /*PRIMA%*/
    let custbody60 = $("#custbody60").val();
    /*MONTO PRIMA NETA%*/
    let custbody_ix_salesorder_monto_prima = $("#custbody_ix_salesorder_monto_prima").val();
    /*MONTO DESCUENTO DIRECTO%*/
    let custbody132 = $("#custbody132").val();
    /*CASHBACK*/
    let custbodyix_salesorder_cashback = $("#custbodyix_salesorder_cashback").val();

    /*EXTRAS SOBRE EL PRECIO DE LISTA /diferencia*/
    let custbody185 = $("#diferecia").val();
    //MONTO EXTRAS SOBRE EL PRECIO DE LISTA / EXTRAS PAGADAS POR EL CLIENTE 
    let custbody46 = $("#custbody46").val();
    //MONTO TOTAL DE CORTESÍAS
    let custbody16 = $("#custbody16").val();

    //DESCRIPCIÓN EXTRAS
    let custbody47 = $("#custbody47").val();
    //DESCRIPCIÓN DE LAS CORTESIAS
    let custbody35 = $("#custbody35").val();
    //MONTO RESERVA
    let rateReserva = $("#custbody52").val();
    //MONTO ASIGNABLE PRIMA NETA:
    let neta = $("#neta").val();
    let fech_reserva = $("#fech_reserva").val();



    /******************************************HITOS METODO DE PAGO **********************************************************************************/
    //METODO DE PAGO
    let custbody75 = $("#custbody75").val();

    //SI EL METODO DE PAGO ES 1 ES CONTRA ENTREGA */
    var custbody62 = "";
    var custbodyix_salesorder_hito1 = "";
    var custbody63 = "";
    var custbody_ix_salesorder_hito2 = "";
    var custbody64 = "";
    var custbody_ix_salesorder_hito3 = "";
    var custbody65 = "";
    var custbody_ix_salesorder_hito4 = "";
    var custbody66 = "";
    var custbody_ix_salesorder_hito5 = "";
    var custbody67 = "";
    var custbody_ix_salesorder_hito6 = "";
    var custbody163 = "";
    var hito_chek_uno = 0;
    var hito_chek_dos = 0;
    var hito_chek_tres = 0;
    var hito_chek_cuatro = 0;
    var hito_chek_cinco = 0;
    var hito_chek_seis = 0;
    var date_hito_1 = "";
    var date_hito_2 = "";
    var date_hito_3 = "";
    var date_hito_4 = "";
    var date_hito_5 = "";
    var date_hito_6 = "";

    if (custbody75 == 1) {
        custbody67 = $("#contra_enterega11").val();
        custbody_ix_salesorder_hito6 = $("#contra_enterega1").val();
        custbody163 = $("#mspt_contra_entrega").val();
        date_hito_6 = $("#contra_enterega11_date").val();
    }

    if (custbody75 == 2) {
        custbody62 = $("#avnace_obra_hito11").val();
        custbodyix_salesorder_hito1 = $("#avnace_obra_hito1").val();
        custbody63 = $("#avnace_obra_hito12").val();
        custbody_ix_salesorder_hito2 = $("#avnace_obra_hito2").val();
        custbody64 = $("#avnace_obra_hito13").val();
        custbody_ix_salesorder_hito3 = $("#avnace_obra_hito3").val();
        custbody65 = $("#avnace_obra_hito14").val();
        custbody_ix_salesorder_hito4 = $("#avnace_obra_hito4").val();
        custbody66 = $("#avnace_obra_hito15").val();
        custbody_ix_salesorder_hito5 = $("#avnace_obra_hito5").val();
        custbody67 = $("#avnace_obra_hito16").val();
        custbody_ix_salesorder_hito6 = $("#avnace_obra_hito6").val();
        custbody163 = $("#obra_enterega").val();
    }

    if (custbody75 == 7) {
        hito_chek_uno = $('#hito_1').prop('checked') ? 1 : 0;
        hito_chek_dos = $('#hito_2').prop('checked') ? 1 : 0;
        hito_chek_tres = $('#hito_3').prop('checked') ? 1 : 0;
        hito_chek_cuatro = $('#hito_4').prop('checked') ? 1 : 0;
        hito_chek_cinco = $('#hito_5').prop('checked') ? 1 : 0;
        hito_chek_seis = $('#hito_6').prop('checked') ? 1 : 0;
        custbody62 = $("#avance_diferenciado_hito11").val();
        custbodyix_salesorder_hito1 = $("#avance_diferenciado_hito1").val().replace(/,/g, "");
        date_hito_1 = $("#avance_diferenciado_hito1_date").val();
        custbody63 = $("#avance_diferenciado_hito12").val();
        custbody_ix_salesorder_hito2 = $("#avance_diferenciado_hito2").val().replace(/,/g, "");
        date_hito_2 = $("#avance_diferenciado_hito2_date").val();
        custbody64 = $("#avance_diferenciado_hito13").val();
        custbody_ix_salesorder_hito3 = $("#avance_diferenciado_hito3").val().replace(/,/g, "");
        date_hito_3 = $("#avance_diferenciado_hito3_date").val();
        custbody65 = $("#avance_diferenciado_hito14").val();
        custbody_ix_salesorder_hito4 = $("#avance_diferenciado_hito4").val().replace(/,/g, "");
        date_hito_4 = $("#avance_diferenciado_hito4_date").val();
        custbody66 = $("#avance_diferenciado_hito15").val();
        custbody_ix_salesorder_hito5 = $("#avance_diferenciado_hito5").val().replace(/,/g, "");
        date_hito_5 = $("#avance_diferenciado_hito5_date").val();
        custbody67 = $("#avance_diferenciado_hito16").val();
        custbody_ix_salesorder_hito6 = $("#avance_diferenciado_hito6").val().replace(/,/g, "");
        date_hito_6 = $("#avance_diferenciado_hito6_date").val();
        custbody163 = $("#avance_diferenciado_hito17").val();
    }
    /******************************************HITOS METODO DE PAGO **********************************************************************************/

    /******************************************PRIMAS CAMPOS CHEKS **********************************************************************************/

    var custbody176 = ($('#chec_fra').prop('checked')) ? "T" : "F";
    var custbody179 = ($('#chec_fra').prop('checked')) ? $("#custbody179").val() : 0;
    var custbody180 = ($('#chec_fra').prop('checked')) ? $("#custbody180").val() : 0;
    var custbody193 = ($('#chec_fra').prop('checked')) ? $("#custbody193").val() : 0;
    var custbody179_date = ($('#chec_fra').prop('checked')) ? $("#custbody179_date").val() : "12/12/2023";

    var custbody177 = ($('#chec_uica').prop('checked')) ? "T" : "F";
    var custbody181 = ($('#chec_uica').prop('checked')) ? $("#custbody181").val() : 0;
    var custbody182 = ($('#chec_uica').prop('checked')) ? $("#custbody182").val() : 0;
    var custbody194 = ($('#chec_uica').prop('checked')) ? $("#custbody194").val() : 0;
    var custbody182_date = ($('#chec_uica').prop('checked')) ? $("#custbody182_date").val() : "12/12/2023";

    var custbody178 = ($('#chec_extra').prop('checked')) ? "T" : "F";
    var custbody183 = ($('#chec_extra').prop('checked')) ? $("#custbody183").val() : 0;
    var custbody184 = ($('#chec_extra').prop('checked')) ? $("#custbody184").val() : 0;
    var custbody195 = ($('#chec_extra').prop('checked')) ? $("#custbody195").val() : 0;
    var custbody184_date = ($('#chec_extra').prop('checked')) ? $("#custbody184_date").val() : "12/12/2023";

    var prima_extra_uno = ($("#chec_extra_uno").prop("checked")) ? "T" : "F";
    var monto_extra_uno = ($("#chec_extra_uno").prop("checked")) ? $("#o_2_uno_input").val() : 0;
    var monto_tracto_uno = ($("#chec_extra_uno").prop("checked")) ? $("#custbody184_uno").val() : 0;
    var desc_extra_uno = ($("#chec_extra_uno").prop("checked")) ? $("#custbody195_uno").val() : "";
    var custbody184_uno_date = ($('#chec_extra_uno').prop('checked')) ? $("#custbody184_uno_date").val() : "12/12/2023";

    var prima_extra_dos = ($("#chec_extra_dos").prop("checked")) ? "T" : "F";
    var monto_extra_dos = ($("#chec_extra_dos").prop("checked")) ? $("#o_2_dos_input").val() : 0;
    var monto_tracto_dos = ($("#chec_extra_dos").prop("checked")) ? $("#custbody184_dos").val() : 0;
    var desc_extra_dos = ($("#chec_extra_dos").prop("checked")) ? $("#custbody195_dos").val() : "";
    var custbody184_dos_date = ($('#chec_extra_dos').prop('checked')) ? $("#custbody184_dos_date").val() : "12/12/2023";

    var prima_extra_tres = ($("#chec_extra_tres").prop("checked")) ? "T" : "F";
    var monto_extra_tres = ($("#chec_extra_tres").prop("checked")) ? $("#o_2_tres_input").val() : 0;
    var monto_tracto_tres = ($("#chec_extra_tres").prop("checked")) ? $("#custbody184_tres").val() : 0;
    var desc_extra_tres = ($("#chec_extra_tres").prop("checked")) ? $("#custbody195_tres").val() : "";
    var custbody184_tres_date = ($('#chec_extra_tres').prop('checked')) ? $("#custbody184_tres_date").val() : "12/12/2023";
    /******************************************PRIMAS CAMPOS CHEKS **********************************************************************************/


    /****************************************** PRE RESERVA **********************************************************************************/

    var custbody113 = ($('#PRERESERVA').prop('checked')) ? "T" : "F";
    var custbody191 = ($('#PRERESERVA').prop('checked')) ? $("#custbody191").val() : 0;
    var custbody188 = ($('#PRERESERVA').prop('checked')) ? $("#custbody188").val() : 0;
    var custbody189 = ($('#PRERESERVA').prop('checked')) ? $("#pre_3").val() : 0;
    var custbody206 = ($('#PRERESERVA').prop('checked')) ? $("#pre_5").val() : "12/12/2023";
    var custbody190 = ($('#PRERESERVA').prop('checked')) ? $("#pre_8").val() : 0;


    /****************************************** PRE RESERVA **********************************************************************************/

    //prima
    // let custbody18 = $("#custbody18").val();
    let location = $("#location").val();


    const campos = [
        { id: 'custbody_ix_total_amount', valor: custbody_ix_total_amount },
        { id: 'opportunity', valor: opportunity },
        { id: 'custbody13', valor: custbody13 },
        { id: 'subsidiary', valor: subsidiary },
        { id: 'custbody39', valor: custbody39 },
        { id: 'custbody60', valor: custbody60 },
        { id: 'custbody_ix_salesorder_monto_prima', valor: custbody_ix_salesorder_monto_prima },
        { id: 'custbody132', valor: custbody132 },
        { id: 'custbodyix_salesorder_cashback', valor: custbodyix_salesorder_cashback },
        { id: 'diferecia', valor: custbody185 },
        { id: 'custbody46', valor: custbody46 },
        { id: 'custbody16', valor: custbody16 },
        { id: 'custbody47', valor: custbody47 },
        { id: 'custbody35', valor: custbody35 },
        { id: 'custbody52', valor: rateReserva },
        { id: 'neta', valor: neta },
        { id: 'custbody75', valor: custbody75 },
        { id: 'location', valor: location },
    ];

    campos.forEach((campo) => {
        const elemento = $(`#${campo.id}`);
        if (campo.valor === "") {
            elemento.css("border-color", "red");
            elemento.on("change", () => {
                if (elemento.val() !== "") {
                    elemento.css("border-color", "#ccc");
                }
            });
        }
    });



    if (location === "") {
        mostrarAlerta('¡Se deben selecionar una ubicacion antes de continuar!', true);
        return;
    }

    Swal.fire({
        title: '¿Está seguro?',
        text: "¿De que desea editar esta estimación tome en cuenta que no se puede revertir este proceso?",
        icon: 'warning',
        iconHtml: '؟',
        width: '55em',
        padding: '0 0 1.30em',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, editar'
    }).then(async (result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: 'Cargando los datos...',
                html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
                timer: 58000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading();
                    const b = Swal.getHtmlContainer().querySelector('b');
                    const timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft();
                    }, 100);

                }
            });



            try {
                const data = {
                    "rType": "estimacion",
                    "idEst": idEst,
                    "location": location,
                    /*MONTO TOTAL*/
                    "custbody_ix_total_amount": custbody_ix_total_amount,
                    /*OPORTUNIDAD*/
                    "opportunity": opportunity,
                    /*PRECIO DE LISTA:*/
                    "custbody13": custbody13,
                    /*SUBSIDIARIA*/
                    "subsidiary": subsidiary,
                    /*PRIMA TOTAL*/
                    "custbody39": custbody39,
                    /*PRIMA%*/
                    "custbody60": custbody60,
                    /*MONTO PRIMA NETA%*/
                    "custbody_ix_salesorder_monto_prima": custbody_ix_salesorder_monto_prima,
                    /*MONTO DESCUENTO DIRECTO%*/
                    "custbody132": custbody132,
                    /*CASHBACK*/
                    "custbodyix_salesorder_cashback": custbodyix_salesorder_cashback,

                    /*EXTRAS SOBRE EL PRECIO DE LISTA /diferencia*/
                    "custbody185": custbody185,
                    //MONTO EXTRAS SOBRE EL PRECIO DE LISTA / EXTRAS PAGADAS POR EL CLIENTE 
                    "custbody46": custbody46,
                    //MONTO TOTAL DE CORTESÍAS
                    "custbody16": custbody16,

                    //DESCRIPCIÓN EXTRAS
                    "custbody47": custbody47,
                    //DESCRIPCIÓN DE LAS CORTESIAS
                    "custbody35": custbody35,
                    //MONTO RESERVA
                    "rateReserva": rateReserva,
                    "fech_reserva": fech_reserva,
                    //MONTO ASIGNABLE PRIMA NETA:
                    "neta": neta,
                    /*-----------Nuevo*/
                    //METODO DE PAGO
                    "custbody75": custbody75,
                    "custbody67": custbody67,
                    "custbody_ix_salesorder_hito6": custbody_ix_salesorder_hito6,
                    "custbody163": custbody163,
                    "custbody62": custbody62,
                    "custbodyix_salesorder_hito1": custbodyix_salesorder_hito1,
                    "custbody63": custbody63,
                    "custbody_ix_salesorder_hito2": custbody_ix_salesorder_hito2,
                    "custbody64": custbody64,
                    "custbody_ix_salesorder_hito3": custbody_ix_salesorder_hito3,
                    "custbody65": custbody65,
                    "custbody_ix_salesorder_hito4": custbody_ix_salesorder_hito4,
                    "custbody66": custbody66,
                    "custbody_ix_salesorder_hito5": custbody_ix_salesorder_hito5,
                    "hito_chek_uno": hito_chek_uno,
                    "hito_chek_dos": hito_chek_dos,
                    "hito_chek_tres": hito_chek_tres,
                    "hito_chek_cuatro": hito_chek_cuatro,
                    "hito_chek_cinco": hito_chek_cinco,
                    "hito_chek_seis": hito_chek_seis,
                    "date_hito_1": date_hito_1,
                    "date_hito_2": date_hito_2,
                    "date_hito_3": date_hito_3,
                    "date_hito_4": date_hito_4,
                    "date_hito_5": date_hito_5,
                    "date_hito_6": date_hito_6,
                    "custbody113": custbody113,
                    "custbody191": custbody191,
                    "custbody188": custbody188,
                    "custbody189": custbody189,
                    "custbody206": custbody206,
                    "custbody190": custbody190,



                    "custbody176": custbody176,
                    "custbody179": custbody179,
                    "custbody180": custbody180,
                    "custbody193": custbody193,
                    "custbody179_date": custbody179_date,


                    "custbody177": custbody177,
                    "custbody181": custbody181,
                    "custbody182": custbody182,
                    "custbody194": custbody194,
                    "custbody182_date": custbody182_date,


                    "custbody178": custbody178,
                    "custbody183": custbody183,
                    "custbody184": custbody184,
                    "custbody195": custbody195,
                    "custbody184_date": custbody184_date,


                    "prima_extra_uno": prima_extra_uno,
                    "monto_extra_uno": monto_extra_uno,
                    "monto_tracto_uno": monto_tracto_uno,
                    "desc_extra_uno": desc_extra_uno,
                    "custbody184_uno_date": custbody184_uno_date,

                    "prima_extra_dos": prima_extra_dos,
                    "monto_extra_dos": monto_extra_dos,
                    "monto_tracto_dos": monto_tracto_dos,
                    "desc_extra_dos": desc_extra_dos,
                    "custbody184_dos_date": custbody184_dos_date,

                    "prima_extra_tres": prima_extra_tres,
                    "monto_extra_tres": monto_extra_tres,
                    "monto_tracto_tres": monto_tracto_tres,
                    "desc_extra_tres": desc_extra_tres,
                    "custbody184_tres_date": custbody184_tres_date

                };

                const response = await fetch("https://api-crm.roccacr.com/api/v2/estimacion/edit/Netsuite", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const result = await response.json();
                console.log("result: ", result);
                var ExTraerResultado = result['Detalle'];
                if (ExTraerResultado.Status == 200) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se editó la estimación',
                        showConfirmButton: false,
                        timer: 2500
                    }).then((result) => {
                        // Redireccionar a la página actual
                        window.location.href = window.location.href;
                    });
                }
                if (ExTraerResultado.Status == 500) {
                    console.log(result['Detalle'].Error.message)

                    if (result['Detalle'].Error.message == 'El campo custcolfecha_pago_proyectado contenía más del número máximo ( 10 ) de caracteres permitidos.') {
                        return Swal.fire({
                            title: "Lo sentimos, ha ocurrido un error.  : El campo de fecha debe estar seleccionado o tener el formato correcto.",
                            icon: 'question',
                            width: "40em",
                            padding: "0 0 1.25em",
                            iconHtml: '؟',
                            confirmButtonText: 'OK',
                            cancelButtonText: 'CORREGUIR',
                            showCancelButton: true,
                            showCloseButton: true
                        })
                    } else {
                        return Swal.fire({
                            title: "Detalle de error : " + result['Detalle'].Error.message + ",  \nLo sentimos, favor revisar este error con su administrador.",
                            icon: 'question',
                            iconHtml: '؟',
                            width: "40em",
                            padding: "0 0 1.25em",
                            confirmButtonText: 'OK',
                            cancelButtonText: 'CORREGUIR',
                            showCancelButton: true,
                            showCloseButton: true
                        })

                    }
                }


            } catch (error) {
                mostrarAlerta('No se pudo crear el evento', true);
                console.error(error);
            }
        }
    });
};

