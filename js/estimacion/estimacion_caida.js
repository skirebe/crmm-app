async function estimacion_caida() {
    const NetsuiteId_acceso = $("#empleado_pre").val();
    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    const id_le = new URLSearchParams(new URL(window.location.href).search).get('data');

    try {
        const result = await Swal.fire({
            title: '¿Está seguro?',
            text: "¿Desea enviar la pre-reserva caída?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, enviar'
        });

        if (result.isConfirmed) {
            const { value: formValues } = await Swal.fire({
                width: '900px',
                title: 'Estimacion Caida',
                html:
                    '<label for="swal-input1">Comentario de caida</label>' +
                    '<input id="swal-input1" class="swal2-input" style="width: 100%; padding: 10px; box-sizing: border-box;">' +
                    '<label for="swal-select">Seleccióne el motivo</label>' +
                    '<select id="swal-select" class="swal2-select" required style="width: 100%; padding: 10px; box-sizing: border-box; max-height: 200px; overflow-y: auto;">' +
                    '<option value="">Escoger</option>' +
                    '<option value="2">Inconformidad - Cambios en proyecto</option>' +
                    '<option value="3">Inconformidad - Distribución</option>' +
                    '<option value="1">Inconformidad - Fecha de entrega</option>' +
                    '<option value="12">Incumplimiento contractual</option>' +
                    '<option value="10">Mejor Oferta</option>' +
                    '<option value="13">Motivo de empresa - Proyecto pospuesto</option>' +
                    '<option value="9">Motivo Financiero - Condiciones bancarias</option>' +
                    '<option value="8">Motivo Financiero - venta de propiedad</option>' +
                    '<option value="14">Motivo Laboral</option>' +
                    '<option value="6">Motivo Personal - Económico</option>' +
                    '<option value="5">Motivo Personal - Familiar</option>' +
                    '<option value="4">Motivo Personal - Salud</option>' +
                    '<option value="7">Motivo Personal - Sin Especificar</option>' +
                    '<option value="11">No sujeto a crédito</option>' +
                    '<option value="15">Traslado de FF/proyecto</option>' +
                    '</select>',
                focusConfirm: false,
                preConfirm: () => {
                    const input1Value = document.getElementById('swal-input1').value;
                    const selectValue = document.getElementById('swal-select').value;
                    return [input1Value, selectValue];
                }
            });

            if (formValues) {
                const [input1Value, selectValue] = formValues;
                let timerInterval;
                Swal.fire({
                    width: '700px', // Establece el ancho del modal,
                    title: 'Cargando los datos...',
                    html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
                    timer: 48000,
                    timerProgressBar: true,
                    allowOutsideClick: false, // Evita cerrar al hacer clic fuera del modal
                    didOpen: () => {
                        Swal.showLoading();
                        const b = Swal.getHtmlContainer().querySelector('b');
                        timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft();
                        }, 100);
                    },
                    willClose: () => {
                        clearInterval(timerInterval);
                    }
                }).then((result) => {

                });
                try {
                    const data = {
                        id: dataValue,
                        motivo: formValues[1],
                        comentario: formValues[0]
                    };
                    const response = await fetch("https://api-crm.roccacr.com/api/v2/estimacion/prereservaCaida", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data)
                    });

                    const result = await response.json();
                    var ExTraerResultado = result['Detalle'];
                    console.log(ExTraerResultado);
                    if (ExTraerResultado.status === 200) {

                        Swal.fire({
                            title: 'Quires enviar como perdido este cliente?',
                            text: "Al perder el cliente se perderan todas sus transacciones!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, perder'
                        }).then(async (result) => {
                            if (result.isConfirmed) {
                                valor = 1;
                            } else {
                                valor = 0;
                            }

                            const valores = {
                                valorDeCaida: 56,
                                tipo: "Se envio la PRE-RESERVA CAIDA",
                                estado_lead: 0,
                                accion_lead: 6,
                                seguimiento_calendar: 0,
                                valor_segimineto_lead: 3,
                            };

                            const descpDelEvento = "Se envio la PRE-RESERVA CAIDA";

                            const estado = "03-LEAD-PRE-RESERVA";


                            const StatusCodeBitacora = await BitacoraCliente(id_le, valores, descpDelEvento, estado);
                            if (StatusCodeBitacora.statusCode == 200) {
                                const StatusCodeCliente = await ActualizarCliente(id_le, valores, estado);
                                if (StatusCodeCliente.statusCode == 200) {
                                    const today = new Date();
                                    const formattedDate = today.toISOString().split('T')[0];


                                    const data = `status=0,pre_caida=1,envioPreReservaCaida="${formattedDate}"`;
                                    const table = "estimaciones"
                                    const query = `WHERE idEstimacion_est ="${dataValue}";`;
                                    const result = await fetchRecords(data, table, query, "update", "", 2);

                                    const datas= `status=${valor}`;
                                    const tables = "leads"
                                    const querys = `WHERE idinterno_lead  ="${id_le}";`;
                                    const results = await fetchRecords(datas, tables, querys, "update", "", 2);

                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Se envió la Pre-Reserva caída',
                                        showConfirmButton: false,
                                        timer: 2500
                                    });
                                    location.reload();
                                }
                            }
                        })
                    } else {
                        Swal.fire('Algo no está bien.', 'No se pudo hacer la pre reserva caída.', 'question');
                    }
                } catch (error) {
                    alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comuníquese con soporte');
                    console.error(error);
                }
            }
        } else {
            const selectElement = document.getElementById('swal-select');
            selectElement.addEventListener('invalid', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: 'Error',
                    text: 'Debes seleccionar un motivo de caída',
                    icon: 'error'
                });
            });
        }
    } catch (error) {
        console.error(error);
    }
}
