async function ExtraerdatosEstimacion() {
    const dataValue = new URLSearchParams(new URL(window.location.href).search).get('data2');
    mostrarSwalopacidadDiv();
    try {
        const data = {
            id: dataValue
        };
        const response = await fetch("https://api-crm.roccacr.com/api/v2/estimacion/consult", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const result = await response.json();
        var ExTraerResultado = result['Detalle'];
        console.log(ExTraerResultado);
        if (ExTraerResultado.status === 200) {
            var dataEs = ExTraerResultado.data;
            var fields = dataEs.fields;


            $("#empleado_pre").val(" " + (fields.salesrep || "--"));
            $("#opo_caida").val(" " + (fields.opportunity || "--"));


            $("#train_id").text(" " + (ExTraerResultado.train_id || "--"));

            $("#data_Exp").text(" " + (ExTraerResultado.Exp || "--"));
            $("#data_Subsidaria").text(" " + (ExTraerResultado.Subsidaria || "--"));
            $("#data_estado").text(" " + (ExTraerResultado.Estado || "--"));
            $("#data_entityname").text(" " + (fields.entityname || "--"));
            $("#data_custbody38").text(" " + (fields.custbody38 || "--"));
            $("#data_entitystatus").text(" " + (fields.entitystatus || "--"));
            $("#data_opportunity").text(" " + (ExTraerResultado.opportunity_name || "--"));
            $("#data_expectedclosedate").text(" " + (fields.expectedclosedate || "--"));
            $("#data_custbody132").text(" " + (fields.custbody132 || "--"));
            $("#data_custbody13").text(" " + (fields.custbody13 || "--"));
            $("#data_custbody46").text(" " + (fields.custbody46 || "--"));
            $("#data_custbody47").text(" " + (fields.custbody47 || "--"));
            $("#data_custbodyix_salesorder_cashback").text(" " + (fields.custbodyix_salesorder_cashback || "--"));
            $("#data_custbody52").text(" " + (fields.custbody52 || "--"));
            $("#data_custbody16").text(" " + (fields.custbody16 || "--"));
            $("#data_custbody35").text(" " + (fields.custbody35 || "--"));
            $("#data_custbody_precio_vta_min").text(" " + (fields.custbody18 || "--"));
            $("#data_custbody185").text(" " + (fields.custbody185 || "--"));
            $("#data_custbody_ix_total_amount").text(" " + (fields.custbody_ix_total_amount || "--"));










            var precio_de_lista = Number(fields.custbody13) || 0;
            var descuento_directo = Number(fields.custbody132) || 0;
            var extrasPagadasPorelcliente = Number(fields.custbody46) || 0;
            var cashback = Number(fields.custbodyix_salesorder_cashback) || 0;
            var monto_de_cortecias = Number(fields.custbody16) || 0;

            var monto_total_precio_venta_neto = (precio_de_lista - Math.abs(descuento_directo) + extrasPagadasPorelcliente - cashback - monto_de_cortecias).toFixed(3);

            if (isNaN(monto_total_precio_venta_neto)) {
                monto_total_precio_venta_neto = "0";
            }


            if (fields.custbody113 == "T") {
                var div = document.getElementById("preReserva_button");
                div.style.display = ""; // Ocultar el div por defecto
                
                
                 //var div = document.getElementById("preReserva_button");
//                 div.style.display = "none"; // Ocultar el div por defecto

//                 var valor = document.getElementById("preReserva_button_Valor");
//                 valor.style.display = "none"; // Mostrar el div
//                 var preresrevachek = document.getElementById("preresrevachek");
//                 preresrevachek.style.display = ""; // Mostrar el div

//                 if (fields.custbody43 == "T") {
//                     var preReserva_button = document.getElementById("preReserva_button");
//                     preReserva_button.style.display = "none"; // Ocultar el div por defecto
//                     var preresrevachek = document.getElementById("preresrevachek");
//                     preresrevachek.style.display = "none"; // Mostrar el div
//                     var valor = document.getElementById("preReserva_button_Valor");
//                     valor.style.display = "none"; // Mostrar el div

//                     var div2 = document.getElementById("preReserva_button_Valor_caiada");
//                     div2.style.display = ""; // Mostrar el div
//                 }

               console.log(fields.custbody113)
            }




            if (fields.custbody43 == "T") {
                var div = document.getElementById("estimacion_caida_valor");
                div.style.display = ""; // Mostrar el div
                var div2 = document.getElementById("preReserva_button_Valor_caiada");
                div2.style.display = ""; // Mostrar el div
            } else {
                var div = document.getElementById("estimacion_caida");
                div.style.display = ""; // Mostrar el div

                if (fields.custbody113 == "F") {
                    if (fields.custbody189 !== "0") {
                        if (fields.custbody189) {
                            var div = document.getElementById("preReserva_button");
                            div.style.display = ""; // Mostrar el div
                        } else {
                            var div = document.getElementById("preReserva_button_Valor");
                            div.style.display = ""; // Mostrar el div
                        }
                    } else {
                        var div = document.getElementById("preReserva_button_Valor");
                        div.style.display = ""; // Mostrar el div
                    }
                }
            }


            $("#data_pvneto").text(" " + monto_total_precio_venta_neto);

            $("#data_custbody39").text(" " + (fields.custbody39 || "--"));
            $("#data_custbody60").text(" " + (fields.custbody60 || "--"));
            $("#data_custbody_ix_salesorder_monto_prima").text(" " + (fields.custbody_ix_salesorder_monto_prima || "--"));
            $("#data_custbody211").text(" " + (fields.custbody211 || "--"));

            var dataArray = ExTraerResultado.data;
            var sublists = dataArray.sublists;
            const tableBody = document.getElementById("table-body");
            let contador = 0;








            // //oportuindad
            // $("#opt").val((fields.entityname || "--"));
            //datos del modal estimacion
            $("#entityname_oport").val((fields.entityname || "--"));
            $("#entity").val(((fields.entity || "--")));

            $("#Nombre_de_Expediente_de_Unidad").val(((ExTraerResultado.Exp || "--")));
            $("#custbody38").val(((fields.custbody38 || "--")));

            $("#subsidiaria_oport").val(((ExTraerResultado.Subsidaria || "--")));

            $("#entitystatus").val(((ExTraerResultado.entitystatus_name || "--")));
            $("#entitystatus_id").val(((fields.entitystatus || "--")));


            $("#tranid_oport").val(((ExTraerResultado.opportunity_name || "--")));
            $("#opportunity").val(((fields.opportunity || "--")));

            $("#expectedclosedate").val(((fields.expectedclosedate || "--")));


            $("#custbody13").val(((fields.custbody13 || "0")));
            $("#custbody132").val(((fields.custbody132 || "0")));


            $("#custbody46").val(((fields.custbody46 || "0")));
            $("#custbody47").val(((fields.custbody47 || "--")));

            $("#custbodyix_salesorder_cashback").val((fields.custbodyix_salesorder_cashback || "0"));

            $("#custbody52").val((fields.custbody52 || "0"));
            $("#custbody16").val((fields.custbody16 || "0"));
            $("#custbody35").val((fields.custbody35 || "0"));
            $("#custbody18").val((fields.custbody18 || "0"));

            $("#pvneto").val(monto_total_precio_venta_neto);
            $("#custbody_ix_total_amount").val((fields.custbody_ix_total_amount || "0"));

            $("#custbody191").val((fields.custbody191 || "0"));
            $("#pre_3").val((fields.custbody189 || "0"));

            // Dividir la fecha en día, mes y año
           
            var div = document.getElementById("campos_prereserva");
            div.style.display = "none"; // Ocultar el div por defecto

            var custbody189Value = fields.custbody189 || "0";

            if (custbody189Value.trim() !== "" && parseFloat(custbody189Value) > 0) {

                var fechaPagoProyectado = fields.custbody206;
                // Convertir la fecha al formato aceptado por el campo de fecha
                var partesFecha = fechaPagoProyectado.split('/');
                var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');


                $("#pre_5").val((fechaFormateada));

                $("#pre_3").val(custbody189Value);
                var checkbox = document.getElementById("PRERESERVA");
                checkbox.checked = true;

                // Assuming "div" is defined somewhere in your code
                div.style.display = "";
            }



            
            $("#pre_8").val((fields.custbody190 || "0"));


            $("#custbody39").val((fields.custbody39 || "0"));


            $("#custbody60").val((fields.custbody60 || "0"));
            $("#custbody_ix_salesorder_monto_prima").val((fields.custbody_ix_salesorder_monto_prima || "0"));
            $("#custbody40").val((fields.custbody40 || "0"));
            $("#neta").val((fields.custbody211 || "0"));






            var pagos = fields.custbody188 || "0";
            const Pago = pagos;
            const selectElement = document.getElementById("custbody188");
            const optionValues = Array.from(selectElement.options).map(option => option.value);

            const index = optionValues.findIndex(optionValue => optionValue.includes(Pago));
            if (index !== -1) {
                selectElement.selectedIndex = index;
            }

            if (fields.custbody75) {
                var metodos = fields.custbody75 || "0";
                const metodo = metodos;
                const selectElement = document.getElementById("custbody75");
                const optionValues = Array.from(selectElement.options).map(option => option.value);

                const index = optionValues.findIndex(optionValue => optionValue.includes(metodo));
                if (index !== -1) {
                    selectElement.selectedIndex = index;
                }
            }


            /*EXTRAS SOBRE EL PRECIO DE LISTA */
            var extra_sobre_precio_lista = Number(fields.custbody46) || 0;
            var descuento_directo = Number(fields.custbody132) || 0;

            var total_espl = (extra_sobre_precio_lista + descuento_directo).toFixed(3);

            var diferenciaInput = document.getElementById("diferecia");
            diferenciaInput.value = isNaN(total_espl) ? "0" : total_espl;



         


            for (const key in sublists.item) {
                if (key.startsWith("line")) {
                    const line = sublists.item[key];
                    const { rate, custcol_porcentajediferenciadocrm, custcol_indentificadorprima, amount, custcolfecha_pago_proyectado, item_display, quantity, description, item } = line;
                    contador++;

                    if (custcol_indentificadorprima == 551) {
                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("fech_reserva");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;
                    }


                    if (custcol_indentificadorprima == 552) {
                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("pre_5");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;
                        var checkbox = document.getElementById("PRERESERVA");
                        checkbox.checked = true;
                        div.style.display = ""; // Mostrar el div si el checkbox está marcado


                        checkbox.addEventListener("change", function () {
                            if (checkbox.checked) {
                                div.style.display = ""; // Mostrar el div si el checkbox está marcado
                            } else {
                                div.style.display = "none"; // Ocultar el div si el checkbox no está marcado
                            }
                        });
                    }

                    var numeroConComas = custcol_indentificadorprima;
                    var numeroSinComas;

                    if (numeroConComas !== null) {
                        numeroSinComas = parseInt(numeroConComas.replace(/,/g, ''), 10);
                    } else {
                        // Valor predeterminado si custcol_indentificadorprima es null
                        numeroSinComas = 0;
                    }

                    if (numeroSinComas === 5551) {
                        var checkbox = document.getElementById("chec_fra");
                        checkbox.checked = true;

                        document.getElementById("f_0").style.display = "";
                        document.getElementById("fraccionado").style.display = "";
                        $("#custbody179").val(rate || "0");
                        $("#custbody180").val(quantity || "0");


                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody179_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody193").val(description || "0");
                    }

                    if (numeroSinComas === 5552) {
                        var checkbox = document.getElementById("chec_uica");
                        checkbox.checked = true;

                        document.getElementById("u_0").style.display = "";
                        document.getElementById("unico").style.display = "";
                        $("#custbody181").val(amount || "0");
                        $("#custbody182").val(quantity || "0");


                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody182_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody194").val(description || "0");
                    }

                    if (numeroSinComas === 5553) {
                        var checkbox = document.getElementById("chec_extra");
                        checkbox.checked = true;

                        document.getElementById("o_0").style.display = "";
                        document.getElementById("extra").style.display = "";
                        $("#custbody183").val(amount || "0");
                        $("#custbody184").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195").val(description || "0");
                    }


                    if (numeroSinComas === 5554) {
                        var checkbox = document.getElementById("chec_extra_uno");
                        checkbox.checked = true;

                        document.getElementById("o_Uno").style.display = "";
                        document.getElementById("ex_uno").style.display = "";
                        $("#o_2_uno_input").val(amount || "0");
                        $("#custbody184_uno").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_uno_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_uno").val(description || "0");
                    }

                    if (numeroSinComas === 5555) {
                        var checkbox = document.getElementById("chec_extra_dos");
                        checkbox.checked = true;

                        document.getElementById("o_dos").style.display = "";
                        document.getElementById("ex_dos").style.display = "";
                        $("#o_2_dos_input").val(amount || "0");
                        $("#custbody184").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_dos_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_dos").val(description || "0");
                    }
                    if (numeroSinComas === 5556) {
                        var checkbox = document.getElementById("chec_extra_tres");
                        checkbox.checked = true;

                        document.getElementById("o_tres").style.display = "";
                        document.getElementById("ex_tres").style.display = "";
                        $("#o_2_tres_input").val(amount || "0");
                        $("#custbody184_tres").val(quantity || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("custbody184_tres_date");
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;

                        $("#custbody195_tres").val(description || "0");
                    }

                    if (numeroSinComas === 5557) {
                        var div = document.getElementById("text_contra");
                        div.style.display = ""; // Ocultar el div por defecto

                        var contra_entrega = document.getElementById("contra_entrega");
                        contra_entrega.style.display = ""; // Ocultar el div por defecto
                        $("#contra_enterega1").val(amount || "0");

                        var fechaPagoProyectado = custcolfecha_pago_proyectado;
                        var fechaReservaInput = document.getElementById("contra_enterega11_date");
                        if (fechaPagoProyectado == null || fechaPagoProyectado === "") {
                            // Obtener la fecha actual
                            var fechaActual = new Date();
                            var dia = fechaActual.getDate().toString().padStart(2, '0');
                            var mes = (fechaActual.getMonth() + 1).toString().padStart(2, '0');
                            var anio = fechaActual.getFullYear();

                            fechaPagoProyectado = dia + '/' + mes + '/' + anio;
                        }
                        // Convertir la fecha al formato aceptado por el campo de fecha
                        var partesFecha = fechaPagoProyectado.split('/');
                        var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                        // Asignar el valor al campo de fecha
                        fechaReservaInput.value = fechaFormateada;
                        $("#mspt_contra_entrega").val(fields.custbody163 || "0");
                    }




                    if (numeroSinComas === 11115 || numeroSinComas === 11116 || numeroSinComas === 111167 || numeroSinComas === 111168 ||
                        numeroSinComas === 111169 || numeroSinComas === 111170) {


                        var div = document.getElementById("text_obra");
                        div.style.display = ""; // Ocultar el div por defecto

                        var avance_obra = document.getElementById("avance_obra");
                        avance_obra.style.display = ""; // Ocultar el div por defecto

                        if (numeroSinComas === 11115) {
                            $("#avnace_obra_hito1").val(amount || "0");
                        }
                        if (numeroSinComas === 11116) {
                            $("#avnace_obra_hito2").val(amount || "0");
                        }
                        if (numeroSinComas === 111167) {
                            $("#avnace_obra_hito3").val(amount || "0");
                        }
                        if (numeroSinComas === 111168) {
                            $("#avnace_obra_hito4").val(amount || "0");
                        }
                        if (numeroSinComas === 111169) {
                            $("#avnace_obra_hito5").val(amount || "0");
                        }
                        if (numeroSinComas === 111170) {
                            $("#avnace_obra_hito6").val(amount || "0");
                        }

                        $("#obra_enterega").val(fields.custbody163 || "0");
                    }


                    if (numeroSinComas === 55565 || numeroSinComas === 55566 || numeroSinComas === 55567 || numeroSinComas === 55568 ||
                        numeroSinComas === 55569 || numeroSinComas === 55570) {

                        var text_avance = document.getElementById("text_avance");
                        text_avance.style.display = ""; // Ocultar el div por defecto

                        var avance_diferenciado = document.getElementById("avance_diferenciado");
                        avance_diferenciado.style.display = ""; // Ocultar el div por defecto
                        var amountFormateado = parseFloat(amount).toLocaleString('es-ES');
                        if (numeroSinComas === 55565) {
                            $("#avance_diferenciado_hito11").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito1").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito1_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;
                            var checkbox = document.getElementById("hito_1");
                            checkbox.checked = true;
                            var input1 = document.getElementById("avance_diferenciado_hito11");
                            var input2 = document.getElementById("avance_diferenciado_hito1");
                            var input3 = document.getElementById("avance_diferenciado_hito1_date");

                            input1.disabled = false;
                            input2.disabled = false;
                            input3.disabled = false;
                        }
                        if (numeroSinComas === 55566) {
                            $("#avance_diferenciado_hito12").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito2").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito2_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_2");
                            checkbox.checked = true;


                            var input11 = document.getElementById("avance_diferenciado_hito12");
                            var input21 = document.getElementById("avance_diferenciado_hito2");
                            var input31 = document.getElementById("avance_diferenciado_hito2_date");

                            input11.disabled = false;
                            input21.disabled = false;
                            input31.disabled = false;



                        }
                        if (numeroSinComas === 55567) {
                            $("#avance_diferenciado_hito13").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito3").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito3_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;



                            var checkbox = document.getElementById("hito_3");
                            checkbox.checked = true;

                            var input12 = document.getElementById("avance_diferenciado_hito13");
                            var input22 = document.getElementById("avance_diferenciado_hito3");
                            var input32 = document.getElementById("avance_diferenciado_hito3_date");

                            input12.disabled = false;
                            input22.disabled = false;
                            input32.disabled = false;


                        }
                        if (numeroSinComas === 55568) {
                            $("#avance_diferenciado_hito14").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito4").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito4_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_4");
                            checkbox.checked = true;
                            var input13 = document.getElementById("avance_diferenciado_hito14");
                            var input23 = document.getElementById("avance_diferenciado_hito4");
                            var input33 = document.getElementById("avance_diferenciado_hito4_date");

                            input13.disabled = false;
                            input23.disabled = false;
                            input33.disabled = false;
                            ;
                        }
                        if (numeroSinComas === 55569) {
                            $("#avance_diferenciado_hito15").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito5").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito5_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;


                            var checkbox = document.getElementById("hito_5");
                            checkbox.checked = true;

                            var input14 = document.getElementById("avance_diferenciado_hito15");
                            var input24 = document.getElementById("avance_diferenciado_hito5");
                            var input34 = document.getElementById("avance_diferenciado_hito5_date");
                            input14.disabled = false;
                            input24.disabled = false;
                            input34.disabled = false;
                        }
                        if (numeroSinComas === 55570) {
                            $("#avance_diferenciado_hito16").val(custcol_porcentajediferenciadocrm || "0");
                            $("#avance_diferenciado_hito6").val(amountFormateado || "0");
                            var fechaPagoProyectado = custcolfecha_pago_proyectado;
                            var fechaReservaInput = document.getElementById("avance_diferenciado_hito6_date");
                            // Convertir la fecha al formato aceptado por el campo de fecha
                            var partesFecha = fechaPagoProyectado.split('/');
                            var fechaFormateada = partesFecha[2] + '-' + partesFecha[1].padStart(2, '0') + '-' + partesFecha[0].padStart(2, '0');
                            // Asignar el valor al campo de fecha
                            fechaReservaInput.value = fechaFormateada;

                            var checkbox = document.getElementById("hito_6");
                            checkbox.checked = true;
                            var input15 = document.getElementById("avance_diferenciado_hito16");
                            var input25 = document.getElementById("avance_diferenciado_hito6");
                            var input35 = document.getElementById("avance_diferenciado_hito6_date");


                            input15.disabled = false;
                            input25.disabled = false;
                            input35.disabled = false;
                        }

                        $("#avance_diferenciado_hito17").val(fields.custbody163 || "0");
                    }


                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td style="text-align: right;">${contador}</td>
                        <td style="text-align: right;">${item_display}</td>
                        <td style="text-align: right;">${amount}</td>
                        <td style="text-align: right;">${custcolfecha_pago_proyectado}</td>
                        <td style="text-align: right;">${quantity}</td>
                        <td style="text-align: right;">${description}</td>
                        `;

                    console.log("Amount:", amount);
                    tableBody.appendChild(row);
                }
            }



            $(document).ready(function () {
                var columnsDatatable = [];
                var columsDatatableExel = [];

                columnsDatatable = [1, 2, 3, 4];
                columsDatatableExel = [1, 2, 3, 4];



                var table = $("#datatable-buttons").DataTable({
                    initComplete: function () {
                        $("#preloader").hide();
                    },
                    searching: false, // Quita la funcionalidad de búsqueda
                    lengthChange: false, // Quita la opción de cambiar la cantidad de registros por página
                    processing: true,
                    stateSave: true,
                    order: [[1, "asc"]],
                    language: {
                        decimal: ",",
                        thousands: ".",
                        lengthMenu: "Mostrar _MENU_ registros",
                        zeroRecords: "No se encontraron resultados",
                        info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                        infoFiltered: "(filtrado de un total de _MAX_ registros)",
                        sSearch: "Buscar:",
                        oPaginate: {
                            sFirst: "Primero",
                            sLast: "Último",
                            sNext: "Siguiente",
                            sPrevious: "Anterior",
                        },
                        sProcessing: "Cargando...",
                    },
                    buttons: [
                        {
                            extend: "excel",
                            footer: true,
                            titleAttr: "Exportar a excel",
                            className: "btn btn-success",
                            exportOptions: {
                                columns: columsDatatableExel,
                            },
                        },
                    ],
                });

            });
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Datos de la estimacion',
                showConfirmButton: false,
                timer: 1500
            })
        } else {
            Swal.fire('Alo no esta bien.', 'Ese registro no existe.', 'question');
        }
    } catch (error) {
        alert('No se pudo procesar la consulta, inténtelo de nuevo o bien comunícate con soporte');
        console.error(error);
    }
}


/*ASIGNACIONES DE CAMPOS AL CARGAR LA VISTA*/
/*custbody60 -> PRIMA%*/
var custbody60 = document.getElementById('custbody60')
var custbody39 = document.getElementById('custbody39')
document.getElementById("custbody60").value = 0.15;




/* ***************************************************************************************************** */
/* Obtener el valor de los botones de opción. */
var discounted = document.getElementById('isDiscounted');
var no_discounted = document.getElementById('isNotDiscounted')

/**=====================================================================================================
  Si la casilla de verificación está marcada, deshabilite el primer campo y habilite el segundo campo.
  Si la casilla de verificación no está marcada, habilite el primer campo y deshabilite el segundo campo.
 =====================================================================================================*/
function updateStatus() {
    if (discounted.checked) {
        custbody39.disabled = true;
        custbody60.disabled = false;

        //reseteamos el campo si seleciona monto oporcentaje
        document.getElementById("custbody60").value = 0.15;


        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value)); /* variable global %*/
        var prima = Number(document.getElementById("custbody60").value);
        var res = (lista * prima).toFixed(5);
        Number(document.getElementById("custbody39").value = (res))
    } else {
        custbody39.disabled = false;
        custbody60.disabled = true;

        //reseteamos el campo si seleciona monto oporcentaje
        document.getElementById("custbody60").value = 0.15;

        var lista = Number(Number(document.getElementById("custbody_ix_total_amount").value));
        var prima = Number(document.getElementById("custbody60").value);
        var res = (lista * prima).toFixed(5);
        Number(document.getElementById("custbody39").value = (res))
    }
}
discounted.addEventListener('change', updateStatus)
no_discounted.addEventListener('change', updateStatus)
















/**=====================================================================================================
  FUNCION QUE SE ENCARGA DE CALCULAR TODOS LOS MONTOS Y REASIGNAR MONTOS PARA SU CALCULO
 =====================================================================================================*/
function calcular(input) {
    var valor = input.id;

    function calcularValor(func, field) {
        func(valor, field);
        PRIMA_NETA(valor, field);
        MONTO_PRIMA_ASIGNABLE_NETA(valor, field);
        HITOS_MONTO_SIN_PRIMA_TOTAL(valor, field);
    }

    /*MONTO DESCUENTO DIRECTO*/
    if (valor == "custbody132") {
        var valor = input.value.trim();
        // Validar si hay letras
        if (/[a-zA-Z]/.test(valor)) {
            alert("Por favor, asegúrate de que el campo sea un número negativo  y no contenga letras.");
            input.value = "";
            return;
        }
        // Validar si hay más de un guion
        if (valor.indexOf("-") !== valor.lastIndexOf("-")) {
            alert("Solo se permite un guion al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        // Validar si el guion no está al inicio
        if (valor.indexOf("-") !== 0 && valor.length > 0) {
            alert("El guion solo está permitido al inicio. Por favor, colóquelo correctamente.");
            input.value = valor.replace(/-/g, ""); // Remover todos los guiones
            return;
        }
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody132");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody132");
        MONTO_TOTAL(valor, "custbody132");
    }

    /*EXTRAS PAGADAS POR EL CLIENTE */
    if (valor == "custbody46") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody46");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "custbody46");
        MONTO_TOTAL(valor, "custbody46");
    }

    /*CASHBACK*/
    if (valor == "custbodyix_salesorder_cashback") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbodyix_salesorder_cashback");
    }

    /*MONTO RESERVA*/
    if (valor == "custbody52") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody52");
    }

    /*MONTO TOTAL DE CORTESÍAS*/
    if (valor == "custbody16") {
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody16");
    }

    /*EXTRAS SOBRE EL PRECIO DE LISTA*/
    if (valor == "diferecia") {
        calcularValor(PRECIO_DE_VENTA_NETO, "diferecia");
        EXTRAS_SOBRE_PRECIO_LISTA(valor, "diferecia");
        MONTO_TOTAL(valor, "diferecia");
    }

    /*PRIMA TOTAL*/
    if (valor == "custbody39") {
        PRIMA_TOTAL_VALUE(input.value, "custbody39");
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody39");
    }

    if (valor == "custbody60") {
        PORCENTAJE_VALUE(input.value, "custbody60");
        calcularValor(PRECIO_DE_VENTA_NETO, "custbody39");
    }

    if (valor == "custbody179" || valor == "custbody180" || valor == "custbody181" || valor == "custbody182" || valor == "custbody183" || valor == "custbody184" || valor == "o_2_uno_input" || valor == "custbody184_uno" || valor ==
        "o_2_dos_input" || valor == "custbody184_dos" || valor == "o_2_tres_input" || valor == "custbody184_tres") {
        MONTO_PRIMA_ASIGNABLE_NETA(input, valor);

        let valorCam = Number($("#avance_diferenciado_hito17").val().replace(',', '.'));

        let valorCam1 = Number($("#avance_diferenciado_hito11").val().replace(',', '.'));


        $("#avance_diferenciado_hito1").val((valorCam1 * valorCam).toFixed(0).replace('.', ','));
        let valorCam2 = Number($("#avance_diferenciado_hito12").val().replace(',', '.'));
        $("#avance_diferenciado_hito2").val((valorCam2 * valorCam).toFixed(0).replace('.', ','));

        let valorCam3 = Number($("#avance_diferenciado_hito13").val().replace(',', '.'));
        $("#avance_diferenciado_hito3").val((valorCam3 * valorCam).toFixed(0).replace('.', ','));

        let valorCam4 = Number($("#avance_diferenciado_hito14").val().replace(',', '.'));
        $("#avance_diferenciado_hito4").val((valorCam4 * valorCam).toFixed(0).replace('.', ','));

        let valorCam5 = Number($("#avance_diferenciado_hito15").val().replace(',', '.'));
        $("#avance_diferenciado_hito5").val((valorCam5 * valorCam).toFixed(0).replace('.', ','));

        let valorCam6 = Number($("#avance_diferenciado_hito16").val().replace(',', '.'));
        $("#avance_diferenciado_hito6").val((valorCam6 * valorCam).toFixed(0).replace('.', ','));

    }



    if (valor == "avance_diferenciado_hito11" || valor == "avance_diferenciado_hito12" || valor == "avance_diferenciado_hito13" || valor == "avance_diferenciado_hito14" || valor == "avance_diferenciado_hito15" || valor ==
        "avance_diferenciado_hito16") {


        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);


        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        // var suma_hitos = ( avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16 ).toFixed( 2 );
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);

        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());

            /* Multiplicando el valor de `mspt` por el valor de `avance_diferenciado_hito11` y luego
            redondeando el resultado a 2 decimales. */
            var monto_hito = (mspt * input.value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            /* Poniendo el valor de la entrada con id `avance_diferenciado_hito1` al valor de `monto_hito`. */

            if (valor == "avance_diferenciado_hito11") {
                Number(document.getElementById("avance_diferenciado_hito1").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito12") {
                Number(document.getElementById("avance_diferenciado_hito2").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito13") {
                Number(document.getElementById("avance_diferenciado_hito3").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito14") {
                Number(document.getElementById("avance_diferenciado_hito4").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito15") {
                Number(document.getElementById("avance_diferenciado_hito5").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
            if (valor == "avance_diferenciado_hito16") {
                Number(document.getElementById("avance_diferenciado_hito6").value = (monto_hito).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'El Porcentaje total del monto sin prima total, debe estar entre 0.00 y 0.100 , o bien verificar si la suma de los hitos no supera el 100%',
                footer: ''
            })

        }



    }

    if (valor == "avance_diferenciado_hito1" || valor == "avance_diferenciado_hito2" || valor == "avance_diferenciado_hito3" || valor == "avance_diferenciado_hito4" || valor == "avance_diferenciado_hito5" || valor ==
        "avance_diferenciado_hito6") {

        function removeCommasAndConvertToNumber(value) {
            return Number(value.replace(/[,]/g, ''));
        }

        var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
        var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
        var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
        var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
        var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
        var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

        // Sum all values
        var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

        var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

        // Ensure valorCam2 is a number
        valorCam2 = Number(valorCam2);

        // Subtract valorCam2 from the total sum
        var valortotal = (totalSum - valorCam2).toFixed(2);

        $("#valortotal").val(valortotal);


        let numero = parseFloat(input.value);
        let valorCam = Number($("#avance_diferenciado_hito17").val());
        if (valor == "avance_diferenciado_hito1") {
            $("#avance_diferenciado_hito11").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito2") {
            $("#avance_diferenciado_hito12").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito3") {
            $("#avance_diferenciado_hito13").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito4") {
            $("#avance_diferenciado_hito14").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito5") {
            $("#avance_diferenciado_hito15").val((numero * 1000 / valorCam).toFixed(5));
        }
        if (valor == "avance_diferenciado_hito6") {
            $("#avance_diferenciado_hito16").val((numero * 1000 / valorCam).toFixed(5));
        }

        var avance_diferenciado_hito11 = Number($("#avance_diferenciado_hito11").val());
        var avance_diferenciado_hito12 = Number($("#avance_diferenciado_hito12").val());
        var avance_diferenciado_hito13 = Number($("#avance_diferenciado_hito13").val());
        var avance_diferenciado_hito14 = Number($("#avance_diferenciado_hito14").val());
        var avance_diferenciado_hito15 = Number($("#avance_diferenciado_hito15").val());
        var avance_diferenciado_hito16 = Number($("#avance_diferenciado_hito16").val());
        var suma_hitos = (avance_diferenciado_hito11 + avance_diferenciado_hito12 + avance_diferenciado_hito13 + avance_diferenciado_hito14 + avance_diferenciado_hito15 + avance_diferenciado_hito16).toFixed(2);



        if (0 <= suma_hitos && suma_hitos <= 1) {
            Number($("#total_porcentaje").val(suma_hitos));
            var mspt = Number($("#avance_diferenciado_hito17").val());
            var monto_hito = (mspt * avance_diferenciado_hito11).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Porcentaje inválido',
                text: 'Asegúrate de que el valor del hito  esté correctamente formateado con comas para separar los miles. Además, verifica que la suma de todos los hitos no supere el 100% y que el valor del campo PORCENTAJE YA ASIGNADO sea menor a 1.00',
                footer: ''
            })

        }


    }
}



function PRIMA_TOTAL_VALUE(input, valor) {
    /*CALCULAMOS EL PORCENTAJE */
    var i = input;
    var a = Number(document.getElementById("custbody_ix_total_amount").value);
    rest = i / a;

    Number(document.getElementById("custbody60").value = (rest).toFixed(5));

}

function PORCENTAJE_VALUE(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_total_porcentaje = (monto_total * input).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_porcentaje));

}

/* CREAMOS LAS FUNCIONES QUE NOS VAN A CALCULAR LOS CAMPOS DEPENDIENDO DE CUAL EDITAMOS */
function PRECIO_DE_VENTA_NETO(input, valor) {


    /*CALCULAMOS EL PRECIO DE VENTA NETO */
    var precio_de_lista = Number(Number(document.getElementById("custbody13").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var extrasPagadasPorelcliente = Number(Number(document.getElementById("custbody46").value));
    var cashback = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var monto_de_cortecias = Number(Number(document.getElementById("custbody16").value));
    var monto_total_precio_venta_neto = (precio_de_lista - Math.abs(descuento_directo) + extrasPagadasPorelcliente - cashback - monto_de_cortecias).toFixed(3);
    Number(document.getElementById("pvneto").value = (monto_total_precio_venta_neto));

}


function EXTRAS_SOBRE_PRECIO_LISTA(input, valor) {


    /*EXTRAS SOBRE EL PRECIO DE LISTA */
    var extra_sobre_precio_lista = Number(Number(document.getElementById("custbody46").value));
    var descuento_directo = Number(Number(document.getElementById("custbody132").value));
    var total_espl = (extra_sobre_precio_lista + descuento_directo).toFixed(3);
    Number(document.getElementById("diferecia").value = (total_espl));

}


function PRIMA_TOTAL(input, valor) {
    /*PRIMA TOTAL */
    var monto_total = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var prima_porcentaje = Number(Number(document.getElementById("custbody60").value));
    var prima_total_ex = (monto_total * prima_porcentaje).toFixed(3);
    Number(document.getElementById("custbody39").value = (prima_total_ex));

}

function MONTO_TOTAL(input, valor) {
    /* CALCULAMOS EL MONTO TOAL */
    var precio_de_lista_mo = Number(Number(document.getElementById("custbody13").value));
    var extra_sobre_precio_lista_mo = Number(Number(document.getElementById("diferecia").value));
    var calcula_monto_total = (precio_de_lista_mo + extra_sobre_precio_lista_mo).toFixed(2);
    Number(document.getElementById("custbody_ix_total_amount").value = (calcula_monto_total));

}



function PRIMA_NETA(input, valor) {
    /* Monto Prima Asignable Neta */
    var prima_total = Number(Number(document.getElementById("custbody39").value));
    var cash = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva = Number(Number(document.getElementById("custbody52").value));
    var total = (prima_total - cash - reserva).toFixed(2);
    Number($("#custbody_ix_salesorder_monto_prima").val(total));

}

function MONTO_PRIMA_ASIGNABLE_NETA(input, valor) {
    /* PRIMA NETA */
    var prima_total_neta = Number(Number(document.getElementById("custbody39").value));
    var cashback_neta = Number(Number(document.getElementById("custbodyix_salesorder_cashback").value));
    var reserva_neta = Number(Number(document.getElementById("custbody52").value));


    //MONTOS PRIMAS UNICA
    var monto_prima_unico = Number(Number(document.getElementById("custbody181").value));
    var monto_tracto_unico = Number(Number(document.getElementById("custbody182").value));

    //MONTOS PRIMAS
    var monto_prima_fraccionado = Number(Number(document.getElementById("custbody179").value));
    var monto_tracto_fraccionado = Number(Number(document.getElementById("custbody180").value));


    //MONTOS PRIMAS
    var monto_prima_extra = Number(Number(document.getElementById("custbody183").value));
    var monto_tracto_extra = Number(Number(document.getElementById("custbody184").value));


    //MONTOS PRIMAS +1
    var monto_prima_extra_uno = Number(Number(document.getElementById("o_2_uno_input").value));
    var monto_tracto_extra_uno = Number(Number(document.getElementById("custbody184_uno").value));


    //MONTOS PRIMAS +2
    var monto_prima_extra_dos = Number(Number(document.getElementById("o_2_dos_input").value));
    var monto_tracto_extra_dos = Number(Number(document.getElementById("custbody184_dos").value));

    //MONTOS PRIMAS +3
    var monto_prima_extra_tres = Number(Number(document.getElementById("o_2_tres_input").value));
    var monto_tracto_extra_tres = Number(Number(document.getElementById("custbody184_tres").value));




    var total_neta = (prima_total_neta - cashback_neta - reserva_neta - (monto_prima_unico * monto_tracto_unico) - (monto_prima_fraccionado * monto_tracto_fraccionado) - (monto_prima_extra * monto_tracto_extra) - (monto_prima_extra_uno *
        monto_tracto_extra_uno) - (monto_prima_extra_dos * monto_tracto_extra_dos) - (monto_prima_extra_tres * monto_tracto_extra_tres)).toFixed(2);
    Number($("#neta").val(total_neta));









}






function HITOS_MONTO_SIN_PRIMA_TOTAL(input, valor) {
    var monto_total_sin_prima = Number(Number(document.getElementById("custbody_ix_total_amount").value));
    var monto_prima_total_sin_prima = Number(Number(document.getElementById("custbody39").value));
    var monto_sin_prima_total = (monto_total_sin_prima - monto_prima_total_sin_prima).toFixed(2);


    /* MONTO SIN PRIMA TOTAL   Avance De Obra*/
    Number(document.getElementById("obra_enterega").value = (monto_sin_prima_total));
    /*MONTO SIN PRIMA TOTAL Contra Entrega*/
    Number(document.getElementById("mspt_contra_entrega").value = (monto_sin_prima_total));
    /* MONTO SIN PRIMA TOTAL DIFERENCIADO */
    Number(document.getElementById("avance_diferenciado_hito17").value = (monto_sin_prima_total));

    var obra_enterega = Number(Number(document.getElementById("obra_enterega").value));
    // calculo de avance obra hitos del 1 al hito6
    var ao_hito6_rest = (obra_enterega * 0.05).toFixed(2);
    Number($("#avnace_obra_hito6").val(ao_hito6_rest));
    var ao_hito5_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito5").val(ao_hito5_rest));
    var ao_hito4_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito4").val(ao_hito4_rest));
    var ao_hito3_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito3").val(ao_hito3_rest));
    var ao_hito2_rest = (obra_enterega * 0.25).toFixed(2);
    Number($("#avnace_obra_hito2").val(ao_hito2_rest));
    var ao_hito1_rest = (obra_enterega * 0.15).toFixed(2);
    Number($("#avnace_obra_hito1").val(ao_hito1_rest));

    var mspt_contra_entrega = Number(Number(document.getElementById("mspt_contra_entrega").value));
    var contra = (mspt_contra_entrega * 1).toFixed(2);
    Number($("#contra_enterega1").val(contra));
}

function ocultarDiv(divId) {
    var div = document.getElementById(divId);
    div.style.display = "none"; // Ocultar el div
}
ocultarDiv("f_0");
ocultarDiv("fraccionado");
ocultarDiv("u_0");
ocultarDiv("unico");
ocultarDiv("o_0");
ocultarDiv("extra");
ocultarDiv("o_Uno");
ocultarDiv("ex_uno");
ocultarDiv("o_dos");
ocultarDiv("ex_dos");
ocultarDiv("o_tres");
ocultarDiv("ex_tres");




function toggleDivF(value, checkboxId, divId, iId) {
    var checkbox = document.getElementById(checkboxId);
    var div = document.getElementById(divId);
    var i = document.getElementById(iId);
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
        i.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
        i.style.display = "none"; // Mostrar el div
    }
}


/* OCULTAR Y MOSTAR CAMPOS DE PRE RESERVA*/
var div = document.getElementById("campos_prereserva");
div.style.display = "none"; // Ocultar el div
function toggleDiv() {
    var checkbox = document.getElementById("PRERESERVA");
    var div = document.getElementById("campos_prereserva");
    if (checkbox.checked) {
        div.style.display = ""; // Mostrar el div
    } else {
        div.style.display = "none"; // Ocultar el div
    }
}



/* OCULTANOS O MOSTRAMOS LOS DIV DE LOS DIFERENTES METODOS DE PAGO*/
function pagoOnChange(selectElement) {
    const selectedValue = selectElement.value;

    const elementos = [{
        id: "avance_diferenciado",
        textId: "text_avance",
        value: "7"
    }, {
        id: "avance_obra",
        textId: "text_obra",
        value: "2"
    }, {
        id: "contra_entrega",
        textId: "text_contra",
        value: "1"
    }];

    elementos.forEach(elemento => {
        const div = document.getElementById(elemento.id);
        const textDiv = document.getElementById(elemento.textId);

        if (selectedValue === elemento.value) {
            div.style.display = "";
            textDiv.style.display = "";
        } else {
            div.style.display = "none";
            textDiv.style.display = "none";
        }
    });
}


/* HABILITAR LOS CAMPOS DE LOS INPUTS DE AVANCE DIFERENCIADO*/
function habilitarCampos(checkbox, inputId1, inputId2, inputId3) {
    var input1 = document.getElementById(inputId1);
    var input2 = document.getElementById(inputId2);
    var input3 = document.getElementById(inputId3);

    if (checkbox.checked) {
        input1.disabled = false;
        input2.disabled = false;
        input3.disabled = false;
    } else {
        input1.disabled = true;
        input2.disabled = true;
        input3.disabled = true;
        input1.value = "0.00";
        input2.value = "0.00";


    }

    function removeCommasAndConvertToNumber(value) {
        return Number(value.replace(/[,]/g, ''));
    }

    var avance_diferenciado_hito1 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito1").value);
    var avance_diferenciado_hito2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito2").value);
    var avance_diferenciado_hito3 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito3").value);
    var avance_diferenciado_hito4 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito4").value);
    var avance_diferenciado_hito5 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito5").value);
    var avance_diferenciado_hito6 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito6").value);

    // Sum all values
    var totalSum = avance_diferenciado_hito1 + avance_diferenciado_hito2 + avance_diferenciado_hito3 + avance_diferenciado_hito4 + avance_diferenciado_hito5 + avance_diferenciado_hito6;

    var valorCam2 = removeCommasAndConvertToNumber(document.getElementById("avance_diferenciado_hito17").value);

    // Ensure valorCam2 is a number
    valorCam2 = Number(valorCam2);

    // Subtract valorCam2 from the total sum
    var valortotal = (totalSum - valorCam2).toFixed(2);

    $("#valortotal").val(valortotal);



}
