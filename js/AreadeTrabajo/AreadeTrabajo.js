




// Clase RequestManager
class RequestManager {
    // Constructor de la clase, establece la URL base de la API
    constructor() {
        this.apiUrl = "https://api-crm.roccacr.com/Api/V2/";
    }

    // Método asincrónico para enviar una solicitud a la API
    async sendRequest(resourceType, sqlQuery, methodUi) {
        // 1. Crea un objeto con la consulta SQL proporcionada
        const queryData = { sqlQuery };

        // 2. Construye la URL completa de la API combinando la URL base y el tipo de recurso
        const apiUrl = `${this.apiUrl}${resourceType}`;

        // 3. Configura las opciones de la solicitud, incluyendo el método (GET, POST, etc.), el encabezado y los datos del cuerpo
        const requestOptions = {
            method: methodUi,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(queryData)
        };

        let response;
        // 4. Realiza un bucle para enviar la solicitud y esperar una respuesta
        do {
            response = await fetch(apiUrl, requestOptions);

            // 5. Si la respuesta no es exitosa, espera 40 segundos antes de intentarlo de nuevo
            if (!response.ok) {
                await new Promise((resolve) => setTimeout(resolve, 40000));
            }
        } while (!response.ok);

        // 6. Si la respuesta aún no es exitosa después del bucle, lanza un error
        if (!response.ok) {
            throw new Error("Request failed");
        }

        // 7. Parsea la respuesta como JSON y la devuelve
        const responseData = await response.json();
        return responseData;
    }
}





// Clase UserManager
class UserManager {
    // Constructor de la clase, inicializa una instancia de RequestManager
    constructor() {
        this.requestManager = new RequestManager();
    }

    // Método asincrónico para seleccionar registros de una tabla
    async selectRecords(data, table, where = '', filterVendorId, aplication) {
        // 1. Obtiene datos de administrador almacenados en la sesión
        const storedAdminData = JSON.parse(sessionStorage.getItem('admin'));

        // 2. Verifica si el administrador tiene permiso para ciertos proveedores
        const isAllowedVendor = allowedVendorIds.includes(storedAdminData.NetsuiteId_acceso);

        // 3. Define una condición WHERE para la consulta SQL en función de si el administrador tiene permisos especiales
        const AttentionSaleorder = isAllowedVendor
            ? ` ${filterVendorId} NOT IN (13399,252464,1317185,252151,1321694,252665,38,111)`
            : ` ${filterVendorId} = ${storedAdminData.NetsuiteId_acceso}`;

        // 4. Construye la consulta SQL final con los parámetros proporcionados
        let sqlQuery = `SELECT ${data} FROM ${table} WHERE ${AttentionSaleorder} ${where}`;
        

        // 5. Si 'aplication' es igual a 2, reemplaza la consulta SQL para no incluir la condición WHERE
        if (aplication == 2) {
            sqlQuery = `SELECT ${data} FROM ${table} ${where}`;
        }

        // 6. Llama al método sendRequest de RequestManager para enviar la solicitud POST y devuelve los datos de respuesta
        return await this.requestManager.sendRequest('select', sqlQuery, 'POST');
    }

    // Método asincrónico para actualizar registros en una tabla
    async updateRecords(data, table, where = '') {
        // 7. Construye la consulta SQL para actualizar registros con los datos proporcionados
        const sqlQuery = `UPDATE ${table} SET ${data} ${where}`;

        // 8. Llama al método sendRequest de RequestManager para enviar la solicitud PUT y devuelve los datos de respuesta
        return await this.requestManager.sendRequest('update', sqlQuery, 'PUT');
    }

    // Método asincrónico para insertar registros en una tabla
    async insertRecords(data, table, where = '') {
        // 9. Construye la consulta SQL para insertar registros con los datos proporcionados
        const sqlQuery = `INSERT INTO ${table} (${data}) value (${where})`;

        // 10. Llama al método sendRequest de RequestManager para enviar la solicitud POST y devuelve los datos de respuesta
        return await this.requestManager.sendRequest('select', sqlQuery, 'POST');
    }

    // Método asincrónico para autenticar a un usuario
    async loginUser(data, table) {
        // 11. Construye un objeto con el correo y la contraseña del usuario
        const email_admin = data;
        const password = table;
        const sqlQuery = { email_admin, password };

        // 12. Llama al método sendRequest de RequestManager para enviar la solicitud POST y devuelve los datos de respuesta
        return await this.requestManager.sendRequest('authenticated', sqlQuery, 'POST');
    }
}


// Crear una instancia de UserManager
const userManager = new UserManager();

// Definir una función asincrónica para realizar diferentes operaciones
async function fetchRecords(data, table, where, validations, filterVendorId, aplication) {
    try {
        let response = "";

        // Realizar una operación basada en el valor de 'validations'
        if (validations === "login") {
            // 1. Si 'validations' es "login", llama al método 'loginUser' de 'userManager'
            response = await userManager.loginUser(data, table);
        }

        if (validations === "select") {
            // 2. Si 'validations' es "select", llama al método 'selectRecords' de 'userManager'
            response = await userManager.selectRecords(data, table, where, filterVendorId, aplication);
        }

        if (validations === "update") {
            // 3. Si 'validations' es "update", llama al método 'updateRecords' de 'userManager'
            response = await userManager.updateRecords(data, table, where);
        }

        if (validations === "insert") {
            // 4. Si 'validations' es "insert", llama al método 'insertRecords' de 'userManager'
            response = await userManager.insertRecords(data, table, where);
        }

        // 5. Devuelve la respuesta de la operación realizada
        return response;

    } catch (error) {
        // 6. Maneja cualquier error que ocurra durante la ejecución y lo registra en la consola
        console.error("Error al realizar la solicitud:", error);

        // 7. Lanza el error nuevamente para que pueda ser manejado por el código que llama a 'fetchRecords'
        throw error;
    }
}


/*
==================================================================
CREAMOS UN ARREGLO CON LOS ID DE LOS SUPERVISORES
==================================================================
*/
const allowedVendorIds = [13399, 653055, 252464, 1317185, 38, 111, 2146844, 13048 ];



/*
==================================================================
VALIDACION SI YA SE ENVIO EL REGISTRO
==================================================================
*/
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}

/*
==================================================================
VALIDACION SI ESTOY DESDE EL CELULAR
==================================================================
*/
function esDispositivoMovil() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}


/*
==================================================================
extraemos los parametros de la url esto nos dice si es el id del
lead, o otro id que pasemos por parametro
==================================================================
*/
// Clase URLParamExtractor
class URLParamExtractor {
    // Constructor que toma una URL como argumento
    constructor(url) {
        this.url = new URL(url);
    }

    // Método para obtener un parámetro específico de la URL
    getParameter(paramName) {
        // Crea una instancia de URLSearchParams a partir de la cadena de consulta de la URL
        return new URLSearchParams(this.url.search).get(paramName);
    }
}

// Uso de la clase
// Obtiene la URL actual del navegador
const url = window.location.href;

// Crea una instancia de URLParamExtractor utilizando la URL actual
const urlParamExtractor = new URLParamExtractor(url);

// Utiliza el método getParameter para obtener valores de parámetros específicos de la URL
const dataParam1 = urlParamExtractor.getParameter("data");
const dataParam2 = urlParamExtractor.getParameter("data2");
const dataParam3 = urlParamExtractor.getParameter("data3");
const dataParam4 = urlParamExtractor.getParameter("data1");



/*
==================================================================
CLASE QUE SE ENCARGA DE EXTRAER LA FECHA ACTUAL ADEMAS
DE LA FECHA ACTUAL LE RESTA 1 DIA Y LA FINAL LE SUAM 1 DIA
==================================================================
*/
// Clase DateFilter
class DateFilter {
    // Constructor que inicializa la fecha actual
    constructor() {
        this.currentDate = new Date();
    }

    // Método para calcular la fecha de inicio (ayer)
    calculateStart() {
        const startDate = new Date(this.currentDate);
        startDate.setDate(this.currentDate.getDate() - 1);
        return startDate;
    }

    // Método para calcular la fecha de finalización (mañana)
    calculateEnd() {
        const endDate = new Date(this.currentDate);
        endDate.setDate(this.currentDate.getDate() + 1);
        return endDate;
    }

    // Método para obtener la fecha actual
    today() {
        const today = new Date(this.currentDate);
        today.setDate(this.currentDate.getDate());
        return today;
    }

    // Método para formatear una fecha en el formato "YYYY-MM-DD"
    formatDate(date) {
        return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
    }

    // Método para obtener un filtro de fecha que incluye inicio, final y hoy
    getDateFilter() {
        const startDate = this.calculateStart();
        const endDate = this.calculateEnd();
        const today = this.today(); // Obtiene la fecha actual

        // Formatea las fechas en el formato deseado
        return {
            start: `${this.formatDate(startDate)}T00:00`,
            end: `${this.formatDate(endDate)}T23:59`,
            today: `${this.formatDate(today)}`,
        };
    }
}

// Crea una instancia de la clase DateFilter
const dateFilter = new DateFilter();

// Llama al método getDateFilter para obtener un objeto de filtro de fecha
const dateFilterResult = dateFilter.getDateFilter();





// activamos erl modal cuando sea necesario
function mostrarSwalopacidadDiv() {
    let timerInterval;
    Swal.fire({
        title: 'Cargando los datos...',
        html: 'Por favor, espere mientras se genera la vista. Tiempo estimado: <b></b> milisegundos.',
        timer: 48000,
        timerProgressBar: true,
        allowOutsideClick: false, // Evita cerrar al hacer clic fuera del modal
        didOpen: () => {
            Swal.showLoading();
            const b = Swal.getHtmlContainer().querySelector('b');
            timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft();
            }, 100);
        },
        willClose: () => {
            clearInterval(timerInterval);
        }
    }).then((result) => {

    });
}