async function report() {
    const currentDate = new Date();
    const firstDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    const lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);

    function formatDate(date) {
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    }

    const dataManager = new DataManager();
    await dataManager.fillSelect("ProyectReport", "", "selectProyectReport1");
    await dataManager.fillSelect("CampanaReport", "", "selectCampanaReport2");

    const reporteA1 = document.getElementById(`reporteA1`);
    const reporteA2 = document.getElementById(`reporteA2`);
    const reporteA3 = document.getElementById(`reporteA3`);
    const kpiA1 = document.getElementById(`kpiA1`);
    const kpiA2 = document.getElementById(`kpiA2`);
    const kpiA3 = document.getElementById(`kpiA3`);

    setTimeout(() => {
        reporteA1.style.display = "none";
        reporteA2.style.display = "block";
        reporteA3.style.display = "block";
        kpiA1.style.display = "none";
        kpiA2.style.display = "block";
        kpiA3.style.display = "block";
    }, 500);

    $("#fecha1").val(formatDate(firstDayOfMonth));
    $("#fecha2").val(formatDate(lastDayOfMonth));

    await amchartReport(1);
}



async function kpi() {


    // Obtiene la fecha actual
    const fechaActual = new Date();

    // Calcula el número de días que faltan para llegar al lunes
    const diasParaLunes = fechaActual.getDay() - 1; // 0: domingo, 1: lunes, ..., 6: sábado

    // Calcula la fecha del lunes actual restando los días necesarios
    const lunesActual = new Date(fechaActual);
    lunesActual.setDate(fechaActual.getDate() - diasParaLunes);

    // Calcula la fecha del sábado sumando 5 días al lunes actual
    const sabadoActual = new Date(lunesActual);
    sabadoActual.setDate(lunesActual.getDate() + 6);

    // Función para formatear la fecha en el formato "YYYY-M-D"
    const formato = (fecha) => {
        const year = fecha.getFullYear();
        const month = fecha.getMonth() + 1; // Los meses comienzan desde 0
        const day = fecha.getDate();
        // Agregar ceros a la izquierda si el día o el mes es menor que 10
        const monthStr = month < 10 ? `0${month}` : month;
        const dayStr = day < 10 ? `0${day}` : day;
        return `${year}-${monthStr}-${dayStr}`;
    };

    // Obtiene las fechas del lunes y el sábado formateadas
    const lunesFormateado = formato(lunesActual);
    // console.log("lunesFormateado: ", lunesFormateado);
    const sabadoFormateado = formato(sabadoActual);
    // console.log("sabadoFormateado: ", sabadoFormateado);

    // Establece las fechas formateadas en campos de entrada de un formulario
    $("#fecha3").val(lunesFormateado);
    $("#fecha4").val(sabadoFormateado);


    //esta funcion esta en kpi.js
    await amchartReportKpi(1);

}
