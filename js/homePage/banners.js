class BannerManager {
    constructor() {
        this.banners = {
            "NewLeads": "A",
            "AttentionLead": "B", // Corrected the action name
            "AttentionEvents": "C",
            "AttentionOpportunity": "D",
            "AttentionSaleorder": "E",
            "AttentionSigned": "F"
        };
    }

    async processBanners() {
        try {

            // Obtener la fecha de hoy
            var fechaHoy = new Date();

            // Calcular las fechas de un día antes y un día después
            var fechaUnDiaAntes = new Date(fechaHoy);


            var fechaUnDiaDespues = new Date(fechaHoy);
            fechaUnDiaDespues.setDate(fechaHoy.getDate() + 1);

            // Formatear las fechas en el formato deseado "YYYY-MM-DD"
            function formatearFecha(fecha) {
                var year = fecha.getFullYear();
                var month = (fecha.getMonth() + 1).toString().padStart(2, '0');
                var day = fecha.getDate().toString().padStart(2, '0');
                return year + '-' + month + '-' + day;
            }


            var start = formatearFecha(fechaUnDiaAntes);
            var end = formatearFecha(fechaUnDiaDespues);

            /*= Condiciones para el filtro del select sin esto filtraria todo=*/
            const newLeadsCondition = "accion_lead in (0,2) and estado_lead=1 and segimineto_lead in ('01-LEAD-INTERESADO')";
            const requireeCondition = "accion_lead = 3 and estado_lead=1 and seguimiento_calendar=0 and actualizadaaccion_lead <= DATE_SUB(NOW(), INTERVAL 4 DAY) and segimineto_lead NOT IN ('02-LEAD-OPORTUNIDAD', '03-LEAD-PRE-RESERVA', '04-LEAD-RESERVA', '05-LEAD-CONTRATO', '06-LEAD-ENTREGADO')";
            const AttentionEvents = `estado_calendar=1 and accion_calendar='Pendiente' and fechaIni_calendar >=  '${start}' and fechaIni_calendar < '${end}'`;
            const AttentionOpportunity = "chek_oport=1 and estatus_oport=1";
            const AttentionSaleorder = `caida_ov = 0 AND comision_cancelada_ov = 0 AND status_ov = 1 and contrado_frima_ov = 0`;
            const AttentionSigned = `caida_ov = 0 AND comision_cancelada_ov = 0 AND contrado_frima_ov = 1 AND cierre_firmado_ov = 1 AND aprobacion_forma_ov = 1 AND aprobacion__rdr_ov = 1 AND calculo_comision_asesor_ov = 1 and status_ov=1`;


            /*= EJECUTAR LA FUNCION=*/
            const newLeadsResult = await this.processQuery('leads', 'id_lead', newLeadsCondition, 'NewLeads', "id_empleado_lead");
            const requireeResult = await this.processQuery('leads', 'id_lead', requireeCondition, 'AttentionLead', "id_empleado_lead");
            const EventsResult = await this.processQuery('calendars', 'id_calendar', AttentionEvents, 'AttentionEvents', "id_admin");
            const OpportunityResult = await this.processQuery('oportunidades', 'id_oportunidad_oport', AttentionOpportunity, 'AttentionOpportunity', "employee_oport");
            const SaleorderyResult = await this.processQuery('ordenventa', 'id_ov_admin', AttentionSaleorder, 'AttentionSaleorder', "id_ov_admin");
            const SignedResult = await this.processQuery('ordenventa', 'id_ov_admin', AttentionSigned, 'AttentionSigned', "id_ov_admin");



            this.bannerLead(newLeadsResult, 'NewLeads');
            this.bannerLead(requireeResult, 'AttentionLead');
            this.bannerLead(EventsResult, 'AttentionEvents');
            this.bannerLead(OpportunityResult, 'AttentionOpportunity');
            this.bannerLead(SaleorderyResult, 'AttentionSaleorder');
            this.bannerLead(SignedResult, 'AttentionSigned');
        } catch (error) {
            console.error("Error general:", error);
        }
    }

    async processQuery(table, count, condition, resultName, vendorOd) {
        const data = `COUNT(${count}) as result`;
        const query = `and ${condition}`;
        return await fetchRecords(data, table, query, "select", vendorOd, 1);
    }

    bannerLead(result, action) {
        if (result.statusCode === 500) {
            console.log("El servidor no responde favor intentar de nuevo");
            return;
        }

        const bannerId = this.banners[action];
        if (bannerId) {
            const banner1 = document.getElementById(`banner${bannerId}1`);
            const banner2 = document.getElementById(`banner${bannerId}2`);
            const banner3 = document.getElementById(`banner${bannerId}3`);
            const banner4 = $(`#banner${bannerId}4`);

            const changeBannerStyles = () => {
                setTimeout(() => {
                    banner1.style.display = "none";
                    banner2.style.display = "block"; // Corrected the property name
                    banner3.style.display = "block"; // Corrected the property name
                }, 300);
            };

            result.data.forEach((datos) => {
                changeBannerStyles();
                banner4.text(datos.result || "0");
            });
        }
    }
}

// Usage
const bannerManager = new BannerManager();



// async function led() {
//     const data = `id_ov_netsuite`;
//     const table = "ordenventa"
//     const query = ``;
//     const result = await fetchRecords(data, table, query, "select", "", 2);



//     result.data.forEach(async (dataS) => {
//         const data = {
//             id: dataS.id_ov_netsuite
//         };
//         const response = await fetch("https://api-crm.roccacr.com/api/v2/orden/consult", {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify(data)
//         });


//         const result = await response.json();

//         var ids = result['Detalle'].data.fields.id;

//         var custbody208 = result['Detalle'].data.fields.custbody208 || "";


//         const ladat = `fechaClienteComprobante_ov="${custbody208}"`;
//         const table = "ordenventa"
//         const query = `WHERE id_ov_netsuite ="${ids}";`;

//         const result1 = await fetchRecords(ladat, table, query, "update", "", 2);
//         console.log("result1: ", result1);

     

//     })
// }

// led()

