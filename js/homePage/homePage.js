async function home() {
    await bannerManager.processBanners();
    await eventosPendientes.processEvents();
    await report();
    await kpi();
}
home();