async function generateChart() {

    let date1 = $('#fecha1').val();
    let date2 = $('#fecha2').val();
    const data = [];

    function generateDateSQL(fieldName, date1, date2) {
        return `and ${fieldName} >= '${date1}' AND ${fieldName} <= '${date2}'  `;
    }


    function generateDateSQ2L(fieldName, date1, date2) {

        return `and (${fieldName} LIKE '%/%' AND STR_TO_DATE(${fieldName}, '%d/%m/%Y') BETWEEN '${date1}' AND '${date2}') OR(${fieldName}  LIKE '%/%' AND ${fieldName} BETWEEN '${date1}' AND '${date2}')`;
    }
    
    const dateSqlLead = generateDateSQL('creado_lead', date1, date2);
    const dateSqlCalendar = generateDateSQL('fechaIni_calendar', date1, date2);
    const dateSqlOpportunity = generateDateSQL('fecha_creada_oport', date1, date2);

    const dateSqlEstimate = generateDateSQ2L('fechaClienteComprobante_est', date1, date2);
    const dateSqlOrder = generateDateSQ2L('fechaClienteComprobante_ov', date1, date2);


    const result1 = await processQueryReport('leads', 'id_lead', 'id_empleado_lead', dateSqlLead, '');
    await processArray(result1, "arrayLeadRepor", "LEADS");

    const result2 = await processQueryReport('calendars', 'id_calendar', 'id_admin', dateSqlCalendar, 'AND cita_lead = 1 AND estado_calendar = 0 AND accion_calendar = "Completado"');
    await processArray(result2, "arrarVisitasReport", "VISITAS");


    const result3 = await processQueryReport('oportunidades', 'id_oportunidad_oport', 'employee_oport', dateSqlOpportunity, 'and chek2_oport=1');
    await processArray(result3, "arrOportunidades", "OPORTUNIDAD");


    const result4 = await processQueryReport('estimaciones', 'idEstimacion_est', 'idAdmin_est', dateSqlEstimate, 'and pre_reserva=1');
    
    await processArray(result4, "arrarPreReservas", "PRE RESREVA");


    const result5 = await processQueryReport('estimaciones', 'idEstimacion_est', 'idAdmin_est', dateSqlEstimate, 'and pre_caida=1');
    await processArray(result5, "arraPrereservaCaida", "PRE RESREVA CAIDA");


    const result6 = await processQueryReport('ordenventa', 'id_ov_netsuite', 'id_ov_admin', dateSqlOrder, 'and reserva_ov=1');
    await processArray(result6, "arrayReservas", "RESREVA");



    const result7 = await processQueryReport('ordenventa', 'id_ov_netsuite', 'id_ov_admin', dateSqlOrder, 'and caida_ov=1');
    
    await processArray(result7, "arrastReservaCaida", "RESERVA CAIDA");


    async function processArray(result, id, campos) {
        for (const resultItem of result.data) {
            const dataObject = {
                "datas": campos,
            };
            dataObject[campos] = resultItem.result;
            data.push(dataObject);
            $(`#${id}`).text(resultItem.result || "0");
        }
    }

    return { array: data };
}
async function processQueryReport(table, count, vendorOd, date, condition) {

    // Definir la columna de datos y la consulta completa
    const data = `COUNT(${count}) as result`;
    const query = ` ${date} ${condition}`;


    // Ejecutar la consulta y devolver el resultado
    const result = await fetchRecords(data, table, query, "select", vendorOd, 1);
    return result;
}



let chartRoot;
let chart;

// Función asincrónica para generar un informe en un gráfico utilizando AmCharts
async function amchartReport(vendorId, array) {
    // Si ya existe una instancia de Root del gráfico, se elimina
    if (chartRoot) {
        chartRoot.dispose(); // Elimina la instancia de Root existente si la hay
    }

    // Crea una nueva instancia de Root para el gráfico
    chartRoot = am5.Root.new("chartdivReportes");
    chartRoot.setThemes([am5themes_Animated.new(chartRoot)]);

    // Crea una instancia de XYChart dentro de la instancia de Root
    chart = chartRoot.container.children.push(am5xy.XYChart.new(chartRoot, {
        panX: false,
        panY: false,
        wheelX: "panY",
        wheelY: "zoomY",
        layout: chartRoot.verticalLayout
    }));

    var dataResul = await generateChart(vendorId);
    var data = dataResul.array;







    // Crea un eje Y para el gráfico
    var yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(chartRoot, {
        categoryField: "datas",
        renderer: am5xy.AxisRendererY.new(chartRoot, {}),
        tooltip: am5.Tooltip.new(chartRoot, {})
    }));

    // Asigna los datos al eje Y
    yAxis.data.setAll(data);

    // Crea un eje X para el gráfico
    var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(chartRoot, {
        min: 0,
        renderer: am5xy.AxisRendererX.new(chartRoot, {})
    }));

    // Crea una leyenda para el gráfico
    var legend = chart.children.push(am5.Legend.new(chartRoot, {
        centerX: am5.p50,
        x: am5.p50
    }));

    // Función para crear y configurar series en el gráfico
    function makeSeries(name, fieldName) {
        var series = chart.series.push(am5xy.ColumnSeries.new(chartRoot, {
            name: name,
            stacked: true,
            xAxis: xAxis,
            yAxis: yAxis,
            baseAxis: yAxis,
            valueXField: fieldName,
            categoryYField: "datas"
        }));
        series.columns.template.setAll({
            tooltipText: "{name}, {categoryY}: {valueX}",
            tooltipY: am5.percent(90)
        });
        series.data.setAll(data);
        series.appear();
        series.bullets.push(function () {
            return am5.Bullet.new(chartRoot, {
                sprite: am5.Label.new(chartRoot, {
                    text: "{valueX}",
                    fill: chartRoot.interfaceColors.get("alternativeText"),
                    centerY: am5.p50,
                    centerX: am5.p50,
                    populateText: true,
                })
            });
        });
        legend.data.push(series);
    }

    /*
    ===============================================================
    ESTE BLOQUE DE CÓDIGO UTILIZA LA FUNCIÓN 'makeSeries' PARA
    ANALIZAR LOS RESULTADOS EN LA VARIABLE "DATA" Y TOMAR ACCIONES
    EN BASE A LOS RESULTADOS.
    ===============================================================
   */
    makeSeries("LEADS", "LEADS");
    makeSeries("VISITAS", "VISITAS");
    makeSeries("OPORTUNIDAD", "OPORTUNIDAD");
    makeSeries("PRERESREVA", "PRE RESREVA");
    makeSeries("PRERESREVA", "PRE RESREVA CAIDA");
    makeSeries("RESERVA", "RESERVA");
    makeSeries("RESERVA CAIDA", "RESERVA CAIDA");

    // Realiza la animación de aparición del gráfico
    chart.appear(1000, 100);
}