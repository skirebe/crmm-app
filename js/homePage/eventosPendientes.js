class EventosPendientes {
    async processEvents() {
        try {
            /*= Condiciones para el filtro del select sin esto filtraria todo=*/
            const PendiCondition = "accion_calendar='Pendiente' and STR_TO_DATE(fechaIni_calendar, '%Y-%m-%dT%H:%i') < NOW() ORDER BY estado_calendar DESC";
            const newLeadsResult = await this.processQuery('calendars INNER JOIN leads as l ON l.idinterno_lead = calendars.id_lead INNER JOIN admins as a ON a.idnetsuite_admin = calendars.id_admin', 'CASE  WHEN l.idinterno_lead = 0 THEN "Sin cliente" ELSE l.nombre_lead END AS nombre_cliente, id_calendar , a.name_admin, nombre_calendar, accion_calendar, decrip_calendar, fechaFin_calendar,calendars.id_lead', PendiCondition, 'AttentionEvents', "calendars.id_admin");
            this.bannerPenEeven(newLeadsResult, 'NewLeads');
        } catch (error) {
            console.error("Error general:", error);
        }
    }

    async processQuery(table, count, condition, resultName, vendorOd) {
        const data = `${count}`;
        const query = ` and ${condition} `;
        return await fetchRecords(data, table, query, "select", vendorOd, 1);
    }

    bannerPenEeven(result) {
        if (result.statusCode === 500) {
            console.log("El servidor no responde favor intentar de nuevo");
            return;
        }
        if (result.statusCode === 210) {
            console.log("no hay eventos pendientes");
            return;
        }
        const tableBody = document.querySelector("#table-body");
        function createRow(data) {
            const row = document.createElement("tr");

            var fechaIniCalendar = data.fechaFin_calendar.slice(0, 10); // Obtener la fecha de data.fechaIni_calendar en formato ISO
            row.innerHTML = `
            <td style="text-align: right; font-weight: bold;">${data.name_admin}</td>
            <td style="text-align: right; font-weight: bold;">${data.nombre_cliente}</td>
            <td style="text-align: right; font-weight: bold;">${data.nombre_calendar}</td>
            <td style="text-align: right; font-weight: bold;">${fechaIniCalendar}</td>
            <td style="text-align: right; font-weight: bold;"><span class="badge badge-pill badge-soft-danger font-size-11">${data.accion_calendar}</span> </td>
            <td style="text-align: right; font-weight: bold;"> <a href="/evento/edit?data=${data.id_lead}&data2=${data.id_calendar}"> VER EVENTO</a></td>
                    `;
            tableBody.appendChild(row);
        }
        async function procesarDatos() {
            return new Promise((resolve) => {
                result.data.forEach(async (datos, index) => {


                    createRow(datos)

                    // Simula una operación asincrónica, como una solicitud HTTP
                    await new Promise((innerResolve) => setTimeout(innerResolve, 1000));

                    // Si es el último elemento, resolvemos la promesa
                    if (index === result.data.length - 1) {
                        resolve();
                    }
                });
            });
        }
        procesarDatos().then(() => {
            const mostrarEventos = document.getElementById(`mostrarEventos`);
            mostrarEventos.style.display = "block";

            $(document).ready(function () {
                var columnsDatatable = [];
                var columsDatatableExel = [];
                columnsDatatable = [0, 1, 2];
                columsDatatableExel = [0, 1, 2, 3, 4];
                var table = $("#datatable-buttons").DataTable({
                    initComplete: function () {
                        $("#preloader").hide();
                    },
                    processing: true,
                    searchPanes: {
                        cascadePanes: true,
                        dtOpts: {
                            select: {
                                style: "multi",
                            },
                            count: {
                                show: false,
                            },
                        },
                        columns: columnsDatatable,
                    },
                    columnDefs: [
                        {
                            searchPanes: {
                                show: true,
                                initCollapsed: true,
                            },
                            targets: columnsDatatable,
                        },
                    ],
                    dom: "PBfrtip",
                    stateSave: true,
                    pageLength: 6,
                    order: [[3, "asc"]],


                    language: {
                        decimal: ",",
                        thousands: ".",
                        lengthMenu: "Mostrar _MENU_ registros",
                        zeroRecords: "No se encontraron resultados",
                        info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                        infoFiltered: "(filtrado de un total de _MAX_ registros)",
                        sSearch: "Buscar:",
                        oPaginate: {
                            sFirst: "Primero",
                            sLast: "Último",
                            sNext: "Siguiente",
                            sPrevious: "Anterior",
                        },
                        sProcessing: "Cargando...",
                    },
                    buttons: [
                        {
                            extend: "excel",
                            footer: true,
                            titleAttr: "Exportar a excel",
                            className: "btn btn-success",
                            exportOptions: {
                                columns: columsDatatableExel,
                            },
                        },
                    ],
                });
            });
        });

    }
}

// Usage
const eventosPendientes = new EventosPendientes();

