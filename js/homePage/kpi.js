


async function generateChartKpi(array) {
    const data = [];


    let proyecto = "";
    let campana = "";
    if (array == 100) {
        proyecto = obtenerConsultaSQL("selectProyectReport1", "proyecto_lead");
        campana = obtenerConsultaSQL("selectCampanaReport2", "campana_lead");

        // console.log("proyecto: ", proyecto);
        // console.log("campana: ", campana);

        function obtenerConsultaSQL(selectId, campo) {
            var selectElement = document.getElementById(selectId);
            var selectedOptions = Array.from(selectElement.selectedOptions);
            var selectedValues = selectedOptions.map(option => `"${option.value}"`);
            var selectedValuesString = selectedValues.join(",");
            return selectedOptions.length ? `AND ${campo} IN (${selectedValuesString})` : "";
        }
    }


    let date1 = $('#fecha3').val();
    let date2 = $('#fecha4').val();


    // Function to convert a 'YYYY-MM-DD' date string to a JavaScript Date object
    function convertToDate(dateStr) {
        return new Date(dateStr);
    }

    // Function to convert a JavaScript Date object to 'YYYY-MM-DD' format
    function formatDateToYYYYMMDD(date) {
        return date.toISOString().split('T')[0];
    }

    // Convert date1 and date2 to JavaScript Date objects
    let jsDate1 = convertToDate(date1);
    let jsDate2 = convertToDate(date2);

    // Subtract one day from date1
    jsDate1.setDate(jsDate1.getDate() - 1);
    // Add one day to date2
    jsDate2.setDate(jsDate2.getDate() + 1);

    // Convert the updated dates back to the 'YYYY-MM-DD' format
    let updatedDate1 = formatDateToYYYYMMDD(jsDate1);
    let updatedDate2 = formatDateToYYYYMMDD(jsDate2);

    let fech_sql = `AND   fechaIni_calendar BETWEEN "${updatedDate1}" AND "${updatedDate2}" `;
    const result1 = await KPIREPORT('calendars as c INNER JOIN leads as l ON c.id_lead = l.idinterno_lead', 'id_calendar', fech_sql, 'AND  cita_lead = 1 and estado_calendar=1 and accion_calendar="Pendiente"', proyecto, campana);
    await processArray(result1, "complete", proyecto);
    const result2 = await KPIREPORT('calendars as c INNER JOIN leads as l ON c.id_lead = l.idinterno_lead', 'id_calendar', fech_sql, 'AND  cita_lead = 1 and estado_calendar=0 and accion_calendar="Completado"', proyecto, campana);
    await processArray(result2, "pendiete", proyecto);
    const result3 = await KPIREPORT('calendars as c INNER JOIN leads as l ON c.id_lead = l.idinterno_lead', 'id_calendar', fech_sql, 'AND  cita_lead = 1 and estado_calendar=0 and accion_calendar="Cancelado"', proyecto, campana);
    await processArray(result3, "pendiete", proyecto);

    async function processArray(result, campos, proyecto) {

        return result.data.map(datos => {
            const elemto = {
                "datas": datos.result,
                "campo": campos,
                "valores": proyecto,
            };

            data.push(elemto);
        });

    }

    return { array: data };

}

async function KPIREPORT(table, count, date, condition, pro, cam) {
    const data = `COUNT(${count}) as result`;
    const query = ` ${condition} ${date} ${pro} ${cam} `;
    const result = await fetchRecords(data, table, query, "select", "id_admin", 1);
    console.log(`select ${data} ${table} ${query} `);
    if (result.statusCode === 500) {
        // console.log("Esperando mientras la consulta se vuelve a conectar : " + action);
        return "error";
    }
    if (result.statusCode === 210) {
        return 0;
    }

    if (result.statusCode === 200) {
        return result;

    }

}



let root;
async function amchartReportKpi(array) {

    if (root) {
        root.dispose(); // Elimina la instancia de Root existente si la hay
    }
    root = am5.Root.new("kpichar");
    root.setThemes([am5themes_Animated.new(root)]);

    chart = root.container.children.push(am5percent.PieChart.new(root, {
        startAngle: 180,
        endAngle: 360,
        layout: root.verticalLayout,
        innerRadius: am5.percent(50)
    }));

    chart.series.clear();
    // Create series
    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
    // start and end angle must be set both for chart and series
    var series = chart.series.push(am5percent.PieSeries.new(root, {
        startAngle: 180,
        endAngle: 360,
        valueField: "value",
        categoryField: "category",
        alignLabels: false
    }));

    series.states.create("hidden", {
        startAngle: 180,
        endAngle: 180
    });

    series.slices.template.setAll({
        cornerRadius: 5
    });

    series.ticks.template.setAll({
        forceHidden: true
    });

    const dataResul = await generateChartKpi(array);
    const kpiData = dataResul.array;

    $('#kpi_pediente').text(kpiData[0].datas);
    $('#kpi_completads').text(kpiData[1].datas);
    $('#kpi_canceladas').text(kpiData[2].datas);


    const total = kpiData[0].datas + kpiData[1].datas;
    $('#kpi_total').text(total);

    series.data.setAll([{
        value: kpiData[0].datas,
        category: "PEND"
    },
    {
        value: kpiData[1].datas,
        category: "COMP"
    },
    {
        value: kpiData[2].datas,
        category: "CANC"
    }
    ]);
    // Create legend
    // https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
    var legend = chart.children.push(am5.Legend.new(root, {
        centerX: am5.percent(50),
        x: am5.percent(50),
        marginTop: 15,
        marginBottom: 15,
    }));

    legend.data.setAll(series.dataItems);
    series.appear(1000, 100);
}

