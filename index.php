<?php
/*===========================================================================
Configuración de errores y registro
===========================================================================*/

// Habilitar la visualización de errores en pantalla
ini_set('display_errors', 1);

// Habilitar el registro de errores en un archivo de registro
ini_set("log_errors", 1);

// Establecer la ubicación del archivo de registro de errores
ini_set("error_log",  "C:/xampp/htdocs/Desarollo CRM/Sambox/SistemaSambox/php_error_log");


/*===========================================================================
CORS (Cross-Origin Resource Sharing)
===========================================================================*/

/* Permitir el uso del método POST. */
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST');


/*===========================================================================
Inclusión de archivos para el envío de correos
===========================================================================*/

// Incluir las clases necesarias para el envío de correos
require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';


/*===========================================================================
Creación de una nueva instancia de la clase TemplateController
===========================================================================*/

// Crear una instancia de la clase TemplateController
require_once "controllers/template.controller.php";
$index = new TemplateController();


/*===========================================================================
Llamada al método index de la clase TemplateController
===========================================================================*/

// Llamar al método index de la clase TemplateController
$index->index();
