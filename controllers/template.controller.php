<?php
/*===========================================================================
Uso de clases y espacios de nombres
===========================================================================*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
/*===========================================================================
Definición de la clase TemplateController
===========================================================================*/
class TemplateController {

  /*===========================================================================
  Traer la Vista Principal de la plantilla
  ===========================================================================*/

  public function index() {
    // Incluir la vista principal de la plantilla
    include "views/template.php";
  }
  /*===========================================================================
  Obteniendo el dominio principal y ruta de acceso
  ===========================================================================*/
  public static function path() {
    // Requiere el archivo de configuración del sistema
    require './app/systemConfig.php';
    // Retorna el valor del dominio principal
    return $dominio;
  }


    /*===========================================================================
    Función para enviar correos electrónicos
    ===========================================================================*/
    static public function sendEmail($name, $subject, $email, $message, $url, $info)
    {
        // Instancia y habilitación de excepciones para PHPMailer
        $mail = new PHPMailer(true);

        try {
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host       = 'smtp.office365.com';
            $mail->SMTPAuth   = true;
            $mail->Username = 'crmnoreply@roccacr.com';
            $mail->Password = 'Rocca.2022crm';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port       = 587;

            // Remitente
            $mail->From = 'crmnoreply@roccacr.com';
            $mail->FromName = 'ROCCA CRM';
            $mail->addAddress($email);

            // Contenido del correo
            $mail->isHTML(true);
            $mail->Subject = $subject;

            // Mensaje personalizado
            $name = $name;
            $message = '<table><tr><td>' . $message . '</td></tr></table>';
            $info = $info;
            $url = $url;

            $template = '
                <div id=":mv" class="a3s aiL">
                    <img width="200px" height="auto" src="https://i.ibb.co/58tptyD/descarga.png" class="CToWUd" data-bit="iit" jslog="138226; u014N:xr6bB; 53:W2ZhbHNlLDJd">
                    <br><br><br>Estimado(a) Cliente <strong>{name}</strong><br><br> Se ha generado este correo para usted.<br><br><br>
                    {message}<br>Este correo ha sido generado desde el sistema CRM de Rocca Development Group con fines informativos únicamente. No es necesario responder a este mensaje. Si este correo no es para usted, por favor informe a su supervisor correspondiente.<div class="yj6qo"></div>
                    {info}
                    <p style="margin:0;"><a href="{url}" style="background: #000; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#000"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:bold;">Ver esta acción?</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a></p>
                    <div class="adL"><br></div>
                </div>';

            $search = ['{name}', '{message}', '{info}', '{url}'];
            $replace = [$name, $message, $info, $url];
            $mensaje = str_replace($search, $replace, utf8_decode($template));
            $mail->Body    = $mensaje;

            // Envía el correo
            $mail->send();

            return "ok";
        } catch (Exception $e) {
            return "error";
        }
    }

}