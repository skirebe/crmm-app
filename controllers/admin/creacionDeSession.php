<?php
session_start(); // Inicia la sesión si aún no ha sido iniciada

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Obtiene los datos enviados a través de AJAX
    $adminData = json_decode($_POST['adminData'], true);

    // Crear una variable de sesión
    $_SESSION["admin"] = $adminData;

    if (isset($_SESSION["admin"])) {
        // La sesión se creó con éxito
        echo '1';
    } else {
        // La sesión no se creó
        echo '2';
    }
} else {
    // Manejar la solicitud GET u otras solicitudes de manera adecuada
    echo '0';
}
?>