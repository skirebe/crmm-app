<?php
session_start(); // Start the session

// Eliminar todas las variables de sesión (cerrar la sesión)
session_unset(); // Unset all session variables
session_destroy(); // Destroy the session

echo '<script>window.location = "/";</script>';
exit();
?>