<body>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body"
                            style="border-radius: 23px;box-shadow:  30px 30px 33px #bcbcbc,-30px -30px 33px #cccccc; ">
                            <div class="d-grid">
                                <a type="button" class="btn btn-primary waves-effect waves-light"
                                    href="/evento/crear"><i class="mdi mdi-plus-circle-outline"></i> Crear Nuevo
                                    evento</a>
                            </div>
                            <div id="external-events" class="mt-2">
                                <br>
                                <h4 class="card-title mb-4"><strong>¿Qué eventos deseas filtrar?</strong></h4>
                                <div class="custom-event">
                                    <div class="waves-effect" data-bs-toggle="tooltip" data-bs-placement="right"
                                        title="Esta vista te permite ver los lead, crear y consultar">
                                        Contenido personalizado
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="d-grid gap-2">
                                        <div>
                                            <div class="form-check form-check-primary mb-3" style="font-size: 22px;">
                                                <input class="form-check-input category-checkbox" type="checkbox"
                                                    id="formCheckcolor1" checked="" value="categoria1">
                                                <label class="form-check-label" for="formCheckcolor1">
                                                    Contactos
                                                </label>
                                            </div>
                                            <div class="form-check form-check-dark mb-3" style="font-size: 22px;">
                                                <input class="form-check-input category-checkbox" type="checkbox"
                                                    id="formCheckcolor2" checked="" value="categoria2">
                                                <label class="form-check-label" for="formCheckcolor2">
                                                    Tareas
                                                </label>
                                            </div>
                                            <div class="form-check form-check-success mb-3" style="font-size: 22px;">
                                                <input class="form-check-input category-checkbox" type="checkbox"
                                                    id="formCheckcolor3" checked="" value="categoria3">
                                                <label class="form-check-label" for="formCheckcolor3">
                                                    Reunion
                                                </label>
                                            </div>
                                            <div class="form-check form-check-danger mb-3" style="font-size: 22px;">
                                                <input class="form-check-input category-checkbox" type="checkbox"
                                                    id="formCheckcolor5" checked="" value="categoria4">
                                                <label class="form-check-label" for="formCheckcolor5">
                                                    Seguimientos
                                                </label>
                                            </div>
                                            <div class="form-check form-check-warning mb-3" style="font-size: 22px;">
                                                <input class="form-check-input category-checkbox" type="checkbox"
                                                    id="formCheckcolor6" checked="" value="categoria5">
                                                <label class="form-check-label" for="formCheckcolor6">
                                                    Primeras Citas
                                                </label>
                                            </div>
                                        </div>
                                        <a style="border-radius: 23px;" ata-bs-toggle="tooltip" href="calendario"
                                            data-bs-placement="top" title="Ver todos los eventos"
                                            class="btn btn-info btn-lg waves-effect waves-light">Ver todos los
                                            eventos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9" id="miDiv">
                    <div class="card"
                        style="border-radius: 23px;box-shadow:  30px 30px 33px #bcbcbc,-30px -30px 33px #cccccc; ">
                        <div class="card-body">
                            <div style="max-width:98%; height:auto; " id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div style='clear:both'></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>
    <script src='/assets/calendar/locales/es.js?rev=<?= time(); ?>'></script>
    <script src="/js/evento/validacionCampos.js?rev=<?= time(); ?>"></script>
    <script src="/js/calendario/calendario.js?rev=<?= time(); ?>"></script>