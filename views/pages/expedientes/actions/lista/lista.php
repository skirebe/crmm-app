<div class="card">
    <div class="card-body" style="box-shadow:  -8px -8px 16px #9a9a9a,8px 8px 16px #ffffff;">
        <div class="row">
            <div class="col-xl-12">
                <div class="mt-4">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Opciones de Filtro: Personaliza la visualización.
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <form class="needs-validation" novalidate="" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Seleccionar Proyecto:</label>
                                            <select class="select2 form-control"
                                                id="proyectos" name="proyectos" multiple="multiple" data-placeholder="Choose ..."
                                                style="width:100%">
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Seleccionar Modelo:</label>
                                            <select class="select2 form-control" 
                                                id="tipos" name="tipos" multiple="multiple" data-placeholder="Choose ..."
                                                style="width:100%">
                                            </select>
                                        </div>


                                        <div class="mb-3">
                                            <label class="form-label">Seleccionar Estado:</label>
                                            <select class="select2 form-control" 
                                                id="estados" name="estados" multiple="multiple" data-placeholder="Choose ..."
                                                style="width:100%">
                                            </select>
                                        </div>
                                          <div>
                                                <button type="submit" class="btn btn-dark">Filtrar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.holas {
    display: none;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <h4 class="card-title"><strong>Lista de Expedientes</strong></h4>
                <table id="datatable-buttons"
                    class="table table-striped table-light dt-responsive nowrap w-100 display">
                    <thead>
                        <tr>
                            <th hidden style="text-align: left;">ID</th>
                            <th style="text-align: left;">NOMBRE</th>
                            <th style="text-align: left;">PROYECTO</th>
                            <th style="text-align: left;">MODELO/ TIPO</th>
                            <th style="text-align: left;">PRECIO V-UNICO</th>
                            <th style="text-align: left;">ESTADO</th>
                            <th style="text-align: left;">FECHA ENTREGA</th>
                            <th style="text-align: left;">TOTAL DE M2</th>
                            <th style="text-align: left;">LOTE</th>
                            <th style="text-align: left;">AREA PARK M2</th>
                            <th style="text-align: left;">AREA BODE M2</th>
                            <th style="text-align: left;">AREA MEZZA M2</th>
                            <th style="text-align: left;" >ASL ASIGNADA</th>
                            <th style="text-align: left;" >PRRECIO V-MINIMO</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/expediente/expediente.js?rev=<?php echo time(); ?>"></script>