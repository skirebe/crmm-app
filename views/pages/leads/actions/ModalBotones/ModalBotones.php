<style>
.whatsapp-button {
    display: inline-block;
    background-color: #25D366;
    color: #fff;
    font-size: 14px;
    padding: 10px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

.whatsapp-button_aut {
    display: inline-block;
    background-color: #395660;
    color: #fff;
    font-size: 14px;
    padding: 10px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

.whatsapp-button:hover {
    background-color: #128C7E;
}

.whatsapp-button_aut:hover {
    background-color: #58D68D;
}


.correo-button {
    display: inline-block;
    background-color: blue;
    color: white;
    font-size: 14px;
    padding: 10px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

.correo-button:hover {
    background-color: blue;
}

.correo-button i {
    margin-right: 5px;
}

.modal-bodys {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 10px;
}

@media (max-width: 767px) {
    .modal-bodys {
        grid-template-columns: 1fr;
    }
}



.btns {
    width: 100%;
    max-width: 200px;
}
</style>
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-shrink-0 me-3">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZCFbSHtKJs8FQGGoVt0et48sdkplguoQ2-lhsm75HT64ppvONY0BH0eoF5HF3UVcBOVU&usqp=CAU"
                            alt="" class="avatar-sm rounded-circle img-thumbnail">
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <div class="text-muted">
                                    <h5 class="mb-1" id="DataName"></h5>
                                    <p class="mb-0" id="proyecto_lead"></p>
                                    <p class="mb-0" id="estado_cliente"></p>
                                    <p class="mb-0" id="admin_name"></p>
                                    <p class="mb-0" id="Dataid" hidden></p>
                                    <input id="dataidcLIENT" hidden>
                                </div>
                            </div>
                            <div class=" flex-shrink-0 dropdown ms-2">
                                <button onclick="ejecutar('DataName')" type="button" style="border-color: #556ee600;"
                                    class="btn btn-outline-primary mt-2 mb-2 waves-effect waves-light"><i
                                        class="mdi mdi-newspaper-variant-multiple"></i> Copiar Nombre</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p style="text-align: center;">¿Qué deseas hacer con este lead? <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close"></button></p>
            <div class="modal-body modal-bodys d-flex flex-wrap gap-2">
                <div class="d-flex flex-wrap gap-2">
                    <div class="d-flex flex-wrap gap-2">
                        <a id="whatsappLinkir1" type="button"
                            style="font-size: 16px;background-color: #128C7E;  color: #fff"
                            class="btn btn-success w-lg waves-effect waves-light" data-bs-toggle='tooltip'
                            data-bs-placement='top' title='Abrir conversación en WhatsApp de este cliente'
                            target="_blank"><i class="fab fa-whatsapp"></i> Ir a WhatsApp</a>
                        <button id="whatsappLink" type="button"
                            style="font-size: 14px;background-color: #58D68D;  color: #fff"
                            class="btn btn-success w-lg waves-effect waves-light" data-bs-toggle='tooltip'
                            data-bs-placement='top'
                            title='Enviar un mensaje de Whatsapp y generar un reporte automatico de contacto'
                            onclick="contact_whatsapp('Dataid')" target="_blank"><i class="fab fa-whatsapp"></i>
                            WhatsApp y
                            Contacto</button>
                        <a id="PrimerContacto" type="button"
                            style="font-size: 13px;background-color: #D35400;  color: #fff"
                            class="btn btn-danger w-lg waves-effect waves-light" data-bs-toggle='tooltip'
                            data-bs-placement='top' title='Crear una nota de primer contacto para este lead'><i
                                class="bx bxs-megaphone "></i>
                            Nota de Contacto</a>
                        <a id="CrearEvento" type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-dark w-lg waves-effect waves-dark" data-bs-toggle='tooltip'
                            data-bs-placement='top' title='Crear un nuevo evento para este lead'><i
                                class="mdi mdi-calendar-text-outline "></i> Crear evento,Crear Cita</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-danger w-lg waves-effect waves-light" id="ColocarPerdido"
                            data-bs-toggle='tooltip' data-bs-placement='top' title='Colocar como perdido'><i
                                class="bx bx-no-entry"></i> Dar como Perdido</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-warning w-lg waves-effect waves-light" id="ColocarSeguimiento"
                            data-bs-toggle='tooltip' data-bs-placement='top' title='Colocar en Seguimiento'><i
                                class="bx bxs-log-in-circle"></i>Colocar en seguimiento</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-success w-lg waves-effect waves-light" id="Oportuindad"
                            data-bs-toggle='tooltip' data-bs-placement='top'
                            title='Crear una oportuindad para este lead'><i class="bx bx-copy-alt"></i>Crear
                            Oportunidad</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-info w-lg waves-effect waves-light" id="Oportuindad_list"
                            data-bs-toggle='tooltip' data-bs-placement='top' title='Ver la lista de oportuindades'><i
                                class="mdi mdi-clipboard-list"></i> Listado
                            Oportunidades</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-primary w-lg waves-effect waves-light" id="perfil" data-bs-toggle='tooltip'
                            data-bs-placement='top' title='Ver perfil completo del lead'><i class="fa fa-eye"></i> Ver
                            Perfil CLiente &#160;&#160;</a>
                        <a type="button" style="font-size: 13px;  color: #fff"
                            class="btn btn-secondary w-lg waves-effect waves-light" id="llamar" data-bs-toggle='tooltip'
                            data-bs-placement='top' title='Llamar al cliente'><i class="bx bx-phone-call"></i> LLamar al
                            cliente &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</a>
                        <!-- <a type="button" style="font-size: 13px;  color: #fff" class="btn btn-success w-lg waves-effect waves-light correo-button" data-bs-toggle='tooltip' data-bs-placement='top' title='Enviar un correo' id="EmailLink"><i class="fas fa-envelope"></i> Enviar correo electrónico</a> -->
                    </div>
                    <div class="col-lg-12 mb-2" id="miDivCard">
                        <div id="ColorDiv" class="card border border-success">
                            <div class="card-header bg-transparent border-success">
                                <h5 class="my-0 text-success"><i class="mdi mdi-check-all me-3"></i>Últimas Acciones
                                    Realizadas a este lead</h5>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="mb-3">Resumen de acciones</h5>
                                    <div id="bitacoraA1">
                                        <h5 class="card-title placeholder-glow">
                                            <span class="placeholder col-6"></span>
                                        </h5>
                                        <p class="card-text placeholder-glow">
                                            <span class="placeholder col-7"></span>
                                            <span class="placeholder col-4"></span>
                                            <span class="placeholder col-4"></span>
                                            <span class="placeholder col-6"></span>
                                            <span class="placeholder col-8"></span>
                                        </p>
                                    </div>
                                    <ul class="verti-timeline list-unstyled" id="bitacoraA2" style="display: none;">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->