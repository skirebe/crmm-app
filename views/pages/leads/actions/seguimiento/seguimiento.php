<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h4 class="card-title"><strong>El motivo por el cual se pondrá este lead en seguimiento</strong>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> esto para poder brindarle
            una atención especializada y asegurarnos de que reciba toda la información necesaria para tomar una decisión
        </p>
        <div class="col-xl-6">
            <div class="mt-4 mt-lg-0">
                <blockquote class="blockquote  blockquote-reverse font-size-16 mb-0">
                    <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Volver a la pagina anteriror'>
                        <i class="bx bx-arrow-back"></i> Volver
                    </a>
                    <a onclick="ExtraerLeadModal('seguimiento')" class="btn  mb-2 btn-dark collapsed"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                        <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                    </a>
                </blockquote>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="mb-3">
                <label class="control-label">Motivo de Seguimiento</label>
                <input id="estado_lead" hidden disabled>
                <select class="form-control select2" name="motivo" id="motivo" required>
                    <option value="" required>Selecionar</option>
                </select>
            </div>
            <div class="mb-3">
                <label class="control-label">Fecha de Notificacion de seguimeinto</label>
                <input class="form-control" id="datepicker" width="100%" placeholder="yyyy-mm-dd" required
                    autocomplete="off" readonly />
            </div>
            <div class="mb-3">
                <label class="control-label">Seleccione el estado del lead</label>
                <select class="form-control select2" name="estado" id="estado" required>
                    <option selected value="1" required>Activo</option>
                    <option value="0" required>Inactivo</option>
                </select>
            </div>
            <div class="col-sm-12">
                <div class="mb-12">
                    <label for="metadescription">Reporte de seguimiento</label>
                    <textarea class="form-control" id="metadescription" name="reporte" rows="10"
                        placeholder="Describa el seguimiento" required></textarea>
                    <div class="valid-feedback">Ingresado.</div>
                    <div class="invalid-feedback">Ingrese un detalle breve para este reporte</div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="d-flex flex-wrap gap-2">
            <butto onclick="Button_Seguimiento()" class="btn btn-dark waves-effect waves-light">Generar Nota</button>
        </div>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/leads/seguimiento.js?rev=<?php echo time(); ?>"></script>
<script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
<script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
<script>
const dataManager = new DataManager();
dataManager.fillSelect("motivo", "", "motivo");
ExtraerLead("seguimiento");
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap5',
    format: 'yyyy-mm-dd'
});
</script>