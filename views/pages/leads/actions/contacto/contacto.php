<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h4 class="card-title"><strong>Genera una nota de contacto o describe cuál fue la forma de contacto</strong>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> Puedes generra una nota de
            contatco o bien poner el lead en contatco con solo darle clik al boton Primer Contacto </p>
        <div class="col-xl-6" style="margin-bottom: 20px;">
            <div class="mt-4 mt-lg-0">
                <blockquote class="blockquote  blockquote-reverse font-size-16 mb-0">
                    <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Volver a la pagina anteriror'>
                        <i class="bx bx-arrow-back"></i> Volver
                    </a>
                    <a onclick="ExtraerLeadModal('primerContacto')" class="btn  mb-2 btn-dark collapsed"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                        <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                    </a>
                </blockquote>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-sm-12">
                <div class="mb-12">
                    <label for="metadescription">Generar Nota de Contacto</label>
                    <input id="estado_lead" hidden disabled>
                    <textarea class="form-control" id="metadescription" name="reporte" rows="10" Value=""
                        placeholder="Describa la nota" required></textarea>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap gap-2">
            <butto onclick="Button_contactNota()" class="btn btn-dark waves-effect waves-light"> Generar
                Nota</button>
        </div>
    </div>
    <script src="/js/leads/PrimerContactos.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
    <script>
    ExtraerLead("primerContacto");
    </script>