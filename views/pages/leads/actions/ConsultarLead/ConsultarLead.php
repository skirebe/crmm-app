<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h5 class="modal-title" id="ConsultarLeadLabel">Consultar lead desde netsuite</h5>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> Brinda la mayor información
            para consultar el lead.
        </p>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i>Ingresa el id a buscar
        </p>
        <br>
        <br>
        <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)" data-bs-toggle='tooltip'
            data-bs-placement='top' title='Volver a la pagina anteriror'>
            <i class="bx bx-arrow-back"></i> Volver
        </a>
        <br>
        <br>
        <br>
        <div class="col-sm-12">
            <div class="mb-3">
                <input required type="number" name="valor" id="valor" class="form-control mb-2"
                    placeholder="Ingrese un dato" />
                <div class="" style="margin-top: 15px">
                    <button onclick="consultarLeadNetsuite()" class="btn btn-dark waves-effect waves-light">Consultar
                        Lead</button>
                </div>
            </div>
        </div>
        <div class="row" id="miDivConsulta" style="display: none;">
            <div class="row">
                <div class="col-sm-6">
                    <div class="mb-3">
                        <label for="productname">Nombre Completo</label>
                        <input type="text" name="firstnames" id="firstnames" class="form-control mb-2" readonly
                            disabled />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">ID Lead</label>
                        <input type="text" name="id" id="id" class="form-control mb-2" readonly disabled hidden />
                        <input type="text" name="entitynumber" id="entitynumber" class="form-control mb-2" readonly
                            disabled />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Correo</label>
                        <input type="text" name="emails" id="emails" class="form-control mb-2" readonly disabled />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Número de teléfono</label>
                        <input type="text" name="phones" id="phones" class="form-control mb-2" readonly disabled />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Comentarios</label>
                        <input type="text" name="comentario_clientes" id="comentario_clientes" class="form-control mb-2"
                            readonly disabled />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Campaña Marketing</label>
                        <input type="text" name="campana_clientes" id="campana_clientes" class="form-control mb-2"
                            readonly disabled />
                        <input type="text" name="id_campana_clientes" id="id_campana_clientes" class="form-control mb-2"
                            readonly disabled hidden />
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label" hidden>Moneda</label>
                        <input type="text" name="moneda_clientes" id="moneda_clientes" class="form-control mb-2"
                            readonly disabled hidden />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="mb-3">
                        <label class="required form-label">Proyecto</label>
                        <input type="text" name="proyecto_cliente" id="proyecto_cliente" class="form-control mb-2"
                            readonly disabled />
                        <input type="text" name="id_proyecto_cliente" id="id_proyecto_cliente" class="form-control mb-2"
                            readonly disabled hidden />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Subsidiaria</label>
                        <input type="text" name="subsidiaria_cliente" id="subsidiaria_cliente" class="form-control mb-2"
                            readonly disabled />
                        <input type="text" name="id_subsidaria_cliente" id="id_subsidaria_cliente"
                            class="form-control mb-2" readonly disabled hidden />
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Estado del LEAD Interesado</label>
                        <input type="text" name="id_estadointeresado_cliente" id="id_estadointeresado_cliente"
                            class="form-control mb-2" value="INTERESADO" readonly disabled />
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nombre de la empresa</label>
                        <input type="text" name="empresa_cliente" id="empresa_cliente" class="form-control mb-2"
                            value="rocca" readonly disabled />
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Donde esucho de nosotros?</label>
                        <input type="text" name="referencia_cliente" id="referencia_cliente" class="form-control mb-2"
                            readonly disabled />
                        <input type="text" name="id_referencia_cliente" id="id_referencia_cliente"
                            class="form-control mb-2" readonly disabled hidden />
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">Vendedor</h4>
                    <p class="card-title-desc">Información del vendedor asociado</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3" hidden>
                                <label class="required form-label">Nombre del vendedor</label>
                                <input type="text" name="employee_display" id="employee_display"
                                    class="form-control mb-2" readonly disabled />
                            </div>
                            <div class="mb-3">
                                <label class="required form-label">Id Relaciohado con el Lead</label>
                                <input type="text" name="customer" id="customer" class="form-control mb-2" readonly
                                    disabled />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label class="required form-label">Id Empleado</label>
                                <input type="text" name="employees" id="employees" class="form-control mb-2" readonly
                                    disabled />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label class="required form-label">Contribucion</label>
                                <input type="text" name="contribucion_cliente" id="contribucion_cliente"
                                    class="form-control mb-2" readonly disabled />
                            </div>
                        </div>
                        <input hidden type="text" name="id_admin" id="id_admin" class="form-control mb-2" readonly
                            disabled />
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="Guardar_Lead()" id="boton" class="btn btn-dark waves-effect waves-light">Guardar en
                        el sistema</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/js/leads/ConsultarLead.js?rev=<?php echo time(); ?>"></script>