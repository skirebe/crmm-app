<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h4 class="card-title"><strong>Crear un nuevo lead</strong>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> Brinda la mayor información
            para crear el lead.
        </p>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i>Ya se puede buscar por
            nombre la selección campos
        </p>
        <br>
        <br>
        <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)" data-bs-toggle='tooltip'
            data-bs-placement='top' title='Volver a la pagina anteriror'>
            <i class="bx bx-arrow-back"></i> Volver
        </a>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-6">
                <div class="mb-3">
                </div>
                <div class="mb-3">
                    <label for="productname">Nombre Completo del cliente</label><br>
                    <input type="text" name="firstname_new" id="firstname_new" class="form-control mb-2"
                        placeholder="Nombre del lead" required />
                </div>
                <div class="mb-3" hidden>
                    <label for="productname">Apellido lead</label>
                    <input type="text" name="lastname_new" id="lastname_new" value="-" class="form-control mb-2"
                        placeholder="Apellido lead" />
                </div>
                <div class="mb-3" hidden>
                    <label for="productname">segundo nombre</label>
                    <input type="text" name="middlename_new" id="middlename_new" value="-" class="form-control mb-2"
                        placeholder="segundo nombre" />
                </div>
                <div class="mb-3">
                    <label class="required form-label">Correo lead</label><br>
                    <input type="email" name="email_new" id="email_new" class="form-control mb-2"
                        placeholder="Correo electrónico" required />
                </div>
                <div class="mb-3">
                    <label class="required form-label">Número de teléfono</label><br>
                    <input type="text" name="phone_new" id="phone_new" class="form-control mb-2"
                        placeholder="Número de teléfono" required />
                </div>
                <div class="mb-3">
                    <label class="required form-label">Comentarios</label>
                    <input type="text" name="comentario_cliente_new" id="comentario_cliente_new"
                        class="form-control mb-2" placeholder="ingrese un comentario obligatorio" required />
                </div>
                <div class="mb-3">
                    <label class="required form-label">Campaña Marketing</label><br>
                    <select id="campana_new" name="campana_new" class="form-control select2" required>
                        <option value="">Seleccionar una campaña</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Corredor Cliente</label>
                    <select required name="corredor_lead" id="corredor_lead" class="form-control select2"
                        style="width:100%">
                        <option value="">Seleccionar</option>
                    </select>
                </div>
                <div class="mb-10 fv-row" hidden>
                    <label class="required form-label">Moneda</label>
                    <input type="text" name="currency_new" id="currency_new" value="1" class="form-control mb-2" />
                </div>
            </div>
            <div class="col-sm-6">
                <br>
                <div class="mb-3">
                    <label class="required form-label">Proyectos</label><br>
                    <select required name="proyecto_new" id="proyecto_new" class="form-control select2"
                        style="width:100%; height: 36px!important">
                        <option value="">Seleccionar una proyecto</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="required form-label">Subsidiaria</label><br>
                    <select required name="subsidiary_new" id="subsidiary_new" class="form-control select2"
                        style="width:100%">
                        <option value="">Seleccionar una Subsidiaria
                    </select>
                </div>
                <div class="mb-3" id="admins">
                    <label class="required form-label">Asignar Lead A un Vendedor</label><br>
                    <select required name="vendedor_new" id="vendedor_new" class="form-control"
                        style="width:100%; height: 36px!important">
                        <option value="">Seleccionar</option>
                    </select>
                </div>
            </div>
            <button type="button" onclick="CrearLead()" class="btn btn-dark">Crear Nueva Lead</button>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/crearLead.js?rev=<?php echo time(); ?>"></script>