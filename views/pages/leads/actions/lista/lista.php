<div class="col-lg-12">
    <div class="card">
        <div class="card-body cardStyleLeads">
            <h4 class="card-title"><strong>MODULO DE LEADS </strong></h4>
            <div class="row">
                <div class="col-xl-6">
                    <div>
                        <a class="btn btn-dark collapsed" data-bs-toggle="collapse" href="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample" data-bs-toggle='tooltip'
                            data-bs-placement='top'
                            title='Para organizar mejor tus leads, te recomendamos utilizar la función de filtrado por rango de fechas. De esta manera podrás visualizar solo aquellos leads que se encuentren dentro del rango seleccionado'>
                            <i class="fas fa-filter"></i> Rango de fechas
                        </a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="mt-4 mt-lg-0">
                        <div class="d-flex flex-wrap gap-2">
                            <a type="button" class="btn mb-2 btn-secondary waves-effect waves-light"
                                style="font-size: 16px; background-color: #641E16;" data-bs-toggle='tooltip'
                                data-bs-placement='top'
                                title='Contactar leads que estan como interesados y generar una nota automatica'
                                href="/leads/lista?data=9"> <i class="bx bx-customize"></i> Contacto Masivo</a>
                            <a type="button" class="btn mb-2 btn-warning waves-effect waves-light"
                                style="font-size: 16px;" href="/leads/lista?data=5" data-bs-toggle='tooltip'
                                data-bs-placement='top'
                                title='Consulte la lista completa de los leads Para el reporte final'> <i
                                    class="bx bx-line-chart"></i> Leads Totales</a>
                            <a type="button" class="btn mb-2 btn-info waves-effect waves-light"
                                style="font-size: 16px; background-color:#D35400;" href="/leads/lista?data=10"
                                data-bs-toggle='tooltip' data-bs-placement='top'
                                title='Consulte la lista completa de los leads Repetidos'> <i class="bx bx-sitemap"></i>
                                Leads Repetidos</a>
                            <!-- <a type="button" class="btn mb-2 btn-success waves-effect waves-light collapsed"
                                style="font-size: 16px;" href="/leads/crearLead?data=1" data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Crear un nuevo lead'> <i
                                    class="mdi mdi-face-profile"></i> Crear Lead &#160;&#160;&#160;&#160;</a> -->
                            <!-- <a type="button" class="btn mb-2 btn-secondary waves-effect waves-light collapsed"
                                 style="font-size: 16px;" href="/leads/ConsultarLead?data=1" data-bs-toggle='tooltip' data-bs-placement='top'
                                title='Consultar lead desde netsuite'> <i class="mdi mdi-face-profile"></i> Consultar
                                Lead&#160;&#160;</a> -->
                            <a type="button" class="btn mb-2 btn-danger waves-effect waves-light collapsed"
                                href="leads/lista?data=4" style="font-size: 16px;" id="lead_inactivos"
                                data-bs-toggle='tooltip' data-bs-placement='top'
                                title='Consulte la lista completa de los leads inactivos'><i class="bx bx-user-x"></i>
                                Leads Inactivos</a>
                            <a type="button" class="btn mb-2 btn-info waves-effect collapsed" href="/leads/lista?data=1"
                                id="lead_activos" style=" display:none;font-size: 16px; " data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Consulte la lista completa de los leads potenciales'> <i
                                    class="bx bx-user-check"></i> Leads potenciales</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Definir las fechas de inicio y fin por defecto o tomarlas de la entrada POST
            $start = isset($_POST['startFilter']) ? $_POST['startFilter'] : date('Y-m-01');
            $end = isset($_POST['endtFilter']) ? $_POST['endtFilter'] : date('Y-m-t');

            // Inicializar las variables de control de radio buttons
            $chek = "checked";
            $chek2 = "";

            // Comprobar si se ha enviado una solicitud POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // Obtener el valor del radio button seleccionado o establecer un valor predeterminado vacío
                $radio = $_POST['radio'] ?? '';

                // Verificar si se dejaron campos vacíos
                if (empty($start) || empty($end) || empty($radio)) {
                    echo '<div class="alert alert-danger">Si desea filtrar por rango de fecha debe seleccionar la fecha de inicio y la fecha final y el motivo</div>';
                }

                // Establecer las variables de control de radio buttons según la selección
                $chek = ($radio == 1) ? "checked" : "";
                $chek2 = ($radio == 2) ? "checked" : "";

                // Mostrar una sección adicional (puede requerir más código para esta sección)
                $show = "show";
            }
            ?>
            <div class="collapse show" id="collapseExample">
                <form class="needs-validation" novalidate="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Inicio</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="startFilter"
                                            id="startFilter" value="<?= $start; ?>" placeholder="FECHA DE INICIO"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Final</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="endtFilter"
                                            id="endtFilter" value="<?= $end; ?>" placeholder="FECHA FINAL"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="text-muted">MOTIVO</label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="radio" id="radio1" value="1"
                                        <?= $chek ?>>
                                    <label class="form-check-label" for="creacion">
                                        FECHA DE CREACIÓN
                                    </label><br>
                                    <input class="form-check-input" type="radio" name="radio" id="radio2" value="2"
                                        <?= $chek2 ?>>
                                    <label class="form-check-label" for="cierre">
                                        ÚLTIMA ACCIÓN
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-dark">Filtrar Busqueda</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.holas {
    display: none;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <button class="btn  mb-2 btns btn-secondary collapsed" style="display: none;"
                    id="extract-button">Cotacto Masivo</button>
                <h4 class="card-title"><strong>Lista de leads</strong></h4>
                 <div id="tableleads">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-12"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                    </p>
                </div>
                <table id="datatable-buttons"
                    class="table table-striped table-light dt-responsive nowrap w-100 display">
                    <thead>
                        <tr>
                            <th hidden style="text-align: left;">ASESOR</th>
                            <th style="text-align: left;">NOMBRE</th>
                            <th hidden style="text-align: left;"># NETSUITE</th>
                            <th style="text-align: left;">CORREO</th>
                            <th style="text-align: left;">TELÉFONO</th>
                            <th hidden style="text-align: left;">PROYECTO</th>
                            <th hidden style="text-align: left;">CAMPAÑA</th>
                            <th hidden style="text-align: left;">ESTADO</th>
                            <th hidden style="text-align: left;">CREADO</th>
                            <th style="text-align: left;">ULTIMA ACCION</th>
                            <th hidden style="text-align: left;">SUBSIDIARIAS</th>
                            <th style="text-align: left;" hidden id="Motivo">MOTIVO</th>
                            <th style="text-align: left;" hidden id="estado">ESTADO LEAD</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
               
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/leads/lista.js?rev=<?= time(); ?>"></script>
<script>
ExtraDataLead();
</script>