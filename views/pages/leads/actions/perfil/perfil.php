<link href="/assets/css/profile.css?rev=<?= time(); ?>" rel="stylesheet" type="text/css" />
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <h4 class="card-title mb-0">Aquí se monitorea de forma continua el progreso de este lead.</h4>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="mt-4">
                            <h5 class="font-size-14">Seguimiento</h5>
                            <p class="card-title-desc"> para expandir/contraer el contenido del seguimiento.</p>
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item ">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button fw-medium" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            Contenido
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body ">
                                            <div class="text-muted">
                                                <div class="hori-timeline" dir="ltr">
                                                    <div class="row " id="bitacorLi">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <center>
                                            <h4 class="card-title mb-5">AVANCE DE LEAD CAMBIAR ESTADO</h4>
                                        </center>
                                        <div class="hori-timeline" dir="ltr">
                                            <center>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-success">LEAD-INTERESADO</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-success">LEAD-SEGUIMIENTO</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-primary">LEAD-OPORTUNIDAD</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-warning">LEAD-PRE-RESERVA</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-warning">LEAD-RESERVA</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-info">LEAD-CONTRATO</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-dark">LEAD-ENTREGADO</a>
                                                <a style="border-radius: 27px;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;"
                                                    class=" mb-2 btn btn-outline-danger">LEAD-PERDIDO</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4">
        <div class="card overflow-hidden"
            style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
            <div class="bg-dark bg-soft">
                <div class="row">
                    <div class="col-7">
                        <div class="text-dark p-3">
                            <h5 class="text-dark">PERFIL DEL LEADS</h5>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="/assets/images/profile-img.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body cardStyleLeads">
                <h4 class="card-title mb-4">Información personal
                </h4>
                <a onclick="ExtraerLeadModal('Perfil')" class="btn  mb-2 btn-dark collapsed" data-bs-toggle='tooltip'
                    data-bs-placement='top' title='Ver perfil de este usuario'>
                    <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                </a>
                <div class="table-responsive">
                    <table class="table table-nowrap mb-0">
                        <tbody>
                            <tr>
                                <th scope="row">Estado , Editar:</th>
                                <td>
                                    <button onclick='estado_le("Activo", 1, )' id="active" style=" display:none"
                                        data-bs-toggle='tooltip' data-bs-placement='top' title='Activar leads'
                                        class="btn btn-outline-success"><i class="fas fa-calendar"></i> ACTIVAR</button>
                                    <button onclick='estado_le("Inactivo", 0)' id="inactive" style=" display:none"
                                        data-bs-toggle='tooltip' data-bs-placement='top' title='Inactivar leads'
                                        class="btn btn-outline-danger"><i class="fas fa-calendar"></i>
                                        INACTIVAR</button>
                                    <a id="editar_perfil" data-bs-toggle='tooltip' data-bs-placement='top'
                                        title='Editar perfil lead' class="btn btn-outline-dark"><i
                                            class="fas fa-calendar"></i> EDITAR</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-body cardStyleLeads" id="preload">
                    <h5 class="fw-semibold" id="perfil_name"></h5>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alertInformation">
                        <i class="mdi mdi-block-helper me-2"></i>
                        Este lead aún no tiene información adicional
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    <div class="table-responsive" id="preload">
                        <div class="loader">
                            <center><img
                                    src="https://www.seguroslagunaro.com/corporativa/uploads/html/memoria-2017/es/images/preload.gif">
                            </center>
                        </div>
                        <table class="table" id="table" style="display: none;">
                            <tbody>
                                <tr>
                                    <th scope="col">Estado Seguimiento Cliente:</th>
                                    <td scope="col" id="perfil_estado_segui"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Teléfono Móvil Cliente:</th>
                                    <td id="perfil_telefono"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Correo electrónico Cliente:</th>
                                    <td id="perfil_correo"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Proyecto Cliente:</th>
                                    <td id="perfil_proyecto"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Subsidiaria Cliente:</th>
                                    <td id="perfil_sub"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Campaña Cliente:</th>
                                    <td id="perfil_cam"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Corredor Cliente :</th>
                                    <td id="perfil_Corredor"></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <hr>
                                    </th>
                                    <td>
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Cédula cliente :</th>
                                    <td id="perfil_cedula"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Nacionalidad Cliente :</th>
                                    <td id="perfil_nacionalidad"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Estado Civil Cliente :</th>
                                    <td id="perfil_estado_civil"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Teléfono alternativo Cliente :</th>
                                    <td id="perfil_telefono_alternativo"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Edad Cliente</th>
                                    <td id="perfil_edad_cliente"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Profesión Cliente</th>
                                    <td id="perfil_profecion"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Hijos Cliente :</th>
                                    <td id="perfil_hijos"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Dirección Cliente :</th>
                                    <td id="perfil_direccion"></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <hr>
                                    </th>
                                    <td>
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">NOMBRE CLIENTE 2 :</th>
                                    <td id="perfil_nombreExtra"></td>
                                </tr>
                                <tr>
                                    <th scope="row">N° CEDULA 2 :</th>
                                    <td id="perfil_cedula2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">NACIONALIDAD 2 :</th>
                                    <td id="perfil_nacionalidad2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">E-MAIL 2 :</th>
                                    <td id="perfil_email2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">ESTADO CIVIL 2 :</th>
                                    <td id="perfil_estado2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">PROFESIÓN 2 :</th>
                                    <td id="perfil_progesion2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">TELEFONO 2 :</th>
                                    <td id="perfil_telefono2"></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="row">
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body"
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Oportunidades</p>
                                <?php
                                require_once "./app/conexion.php";
                                $data = $_GET['data'];
                                // Decodificar el valor en base64
                                $routesArray = $data;
                                $select_stmt = $db->prepare("SELECT COUNT(entity_oport) as result  FROM oportunidades  WHERE entity_oport=:id");
                                $select_stmt->execute([':id' => $routesArray]);
                                $row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
                                echo '<h5 class="mb-0">' . $row_lead["result"] . '</h5>';
                                ?>
                            </div>
                            <div class="flex-shrink-0 align-self-center">
                                <div class="mini-stat-icon avatar-sm rounded-circle bg-dark">
                                    <span class="avatar-title">
                                        <i class="bx bx-check-circle font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body"
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Estimaciones</p>
                                <?php
                                $select_stmt = $db->prepare("SELECT COUNT(idLead_est) as result  FROM estimaciones  WHERE idLead_est= :id");
                                $select_stmt->execute([':id' => $routesArray]);
                                $row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
                                echo '<h5 class="mb-0">' . $row_lead["result"] . '</h5>';
                                ?>
                            </div>
                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-dark">
                                    <span class="avatar-title">
                                        <i class="bx bx-hourglass font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body"
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Orden de venta</p>
                                <?php
                                $select_stmt = $db->prepare("SELECT COUNT(id_ov_lead) as result  FROM ordenventa  WHERE id_ov_lead= :id");
                                $select_stmt->execute([':id' => $routesArray]);
                                $row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
                                echo '<h5 class="mb-0">' . $row_lead["result"] . '</h5>';
                                ?>
                            </div>
                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-dark">
                                    <span class="avatar-title">
                                        <i class="bx bx-hourglass font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body"
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Eventos</p>
                                <?php
                                $select_stmt = $db->prepare("SELECT COUNT(id_lead) as result  FROM calendars  WHERE id_lead= :id");
                                $select_stmt->execute([':id' => $routesArray]);
                                $row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
                                echo '<h5 class="mb-0">' . $row_lead["result"] . '</h5>';
                                ?>
                            </div>
                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-dark">
                                    <span class="avatar-title">
                                        <i class="bx bx-hourglass font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body"
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Mensajes</p>
                                <h5 class="mb-0">0</h5>
                            </div>
                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-dark">
                                    <span class="avatar-title">
                                        <i class="bx bx-package font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body"
                style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                <h4 class="card-title mb-4">Eventos de este leads <a id="perfil_link_evento" style="float:right;"
                        class="">
                        <svg width="100px" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.0002 7.33398V16.6673M16.6668 12.0007L7.3335 12.0007" stroke="#475569"
                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <rect x="1" y="1" width="22" height="22" rx="13" stroke="#475569" stroke-width="2"></rect>
                        </svg>
                        <br>
                    </a></h4>
                <table id="datatable-buttons1"
                    class="display nowrap table table-striped table-light dt-responsive nowrap w-100 display ">
                    <thead>
                        <tr>
                            <th scope="col">Evento</th>
                            <th scope="col">Detalle</th>
                            <th scope="col">Accion</th>
                            <th scope="col">Tipo de evento</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM  calendars where id_lead=$routesArray ORDER BY fechaIni_calendar ASC";
                        $result = mysqli_query($con, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            foreach ($result as $mydata) {
                        ?>
                        <tr>
                            <th scope="row"><?= $mydata['nombre_calendar'] ?></th>
                            <td><?= $mydata['decrip_calendar'] ?></td>
                            <td><?= $mydata['accion_calendar'] ?></td>
                            <td><?= $mydata['tipo_calendar'] ?></td>
                            <td><?= $mydata['fechaIni_calendar'] ?></td>
                            <td><?= $mydata['horaInicio_calendar'] ?></td>
                            <td>
                                <a style="color:black;"
                                    href="/evento/edit?data=<?= $mydata['id_lead']; ?>&data2=<?= $mydata['id_calendar']; ?>"
                                    class="text-success p-1"><i class="bx bxs-edit-alt"></i></a>
                            </td>
                        </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card">
            <div class="card-body"
                style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                <h4 class="card-title mb-4">Oportuindades</h4>
                <table id="datatable-buttons4"
                    class="display nowrap table table-striped table-light dt-responsive nowrap w-100 display ">
                    <thead>
                        <tr>
                            <th scope="col">#ID OPORTUNIDAD</th>
                            <th scope="col">Motivo Condicion</th>
                            <th scope="col">Estado</th>
                            <th scope="col">CREADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM oportunidades  WHERE entity_oport=$routesArray";
                        $result = mysqli_query($con, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            foreach ($result as $mydata) {
                                $tranid_oport = $mydata['tranid_oport'];
                                $id_oportunidad_oport  = $mydata['id_oportunidad_oport'];
                                $entity_oport = $mydata['entity_oport'];
                                $Motico_Condicion = $mydata['Motico_Condicion'];
                                $chek_oport = ($mydata['chek_oport'] == 1) ? "+MAS PROBABLE" : "-MENOS PROBABLE";
                                $fecha_creada_oport = $mydata['fecha_creada_oport'];
                        ?>
                        <tr>
                            <td><a
                                    href="/oportunidad/oprt_view?data=<?= $entity_oport ?>&data2=<?= $id_oportunidad_oport  ?>"><?= $tranid_oport ?>
                                </a></td>
                            <td><?= $Motico_Condicion ?></td>
                            <td><?= $chek_oport ?></td>
                            <td><?= $fecha_creada_oport ?></td>
                        </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card">
            <div class="card-body"
                style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                <h4 class="card-title mb-4">Lista de Estimaciones </h4>
                <table id="datatable-buttons2"
                    class="display nowrap table table-striped table-light dt-responsive nowrap w-100 display ">
                    <thead>
                        <tr>
                            <th scope="col">#ESTIMACION</th>
                            <th scope="col">#OPORTUINDAD</th>
                            <th scope="col">CADUCA</th>
                            <th scope="col">PRE-RESERVA</th>
                            <th scope="col">PRE-RESERVA CAIDA</th>
                            <th scope="col">CREADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM estimaciones INNER JOIN oportunidades ON estimaciones.idOportunidad_est = oportunidades.id_oportunidad_oport where idLead_est=$routesArray ORDER BY creado_est ASC;";
                        $result = mysqli_query($con, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            foreach ($result as $mydata) {
                                $md5lead = $mydata['idLead_est'];
                                $PRE = ($mydata['pre_reserva'] == 1) ? "SI" : "NO";
                                $CAIDA = ($mydata['pre_caida'] == 1) ? "SI" : "NO";
                        ?>
                        <tr>
                            <td><a
                                    href="/estimaciones/view?data=<?= $md5lead ?>&data2=<?= $mydata['idEstimacion_est'] ?>"><?= $mydata['tranid_est'] ?>
                                </a></td>
                            <td><a href="/oportunidad/oprt_view?data=<?= $md5lead ?>&data2=<?= $idEstimacion_est ?>"><?= $mydata['tranid_oport'] ?>
                                </a></td>
                            <td><?= $mydata['caduca'] ?></td>
                            <td><?= $PRE ?></td>
                            <td><?= $CAIDA ?></td>
                            <td><?= $mydata['creado_est'] ?></td>
                        </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card">
            <div class="card-body"
                style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                <h4 class="card-title mb-4">Lista de Ordenes de venta </h4>
                <table id="datatable-buttons3"
                    class="display nowrap table table-striped table-light dt-responsive nowrap w-100 display ">
                    <thead>
                        <tr>
                            <th scope="col">#ORDEN DE VENTA</th>
                            <th scope="col">APROBACIONES</th>
                            <th scope="col">RESERVA</th>
                            <th scope="col">CAIDA</th>
                            <th scope="col">CREADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM ordenventa  where id_ov_lead=$routesArray ORDER BY creado_ov ASC;";
                        $result = mysqli_query($con, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            foreach ($result as $mydata) {
                                $md5lead = $mydata['id_ov_lead'];
                                $caida_ov = ($mydata['caida_ov'] == 1) ? "SI" : "NO";
                                $reserva_ov = ($mydata['reserva_ov'] == 1) ? "SI" : "NO";
                                if ($mydata['contrado_frima_ov'] == 1 && $mydata['cierre_firmado_ov'] == 1 &&  $mydata['aprobacion_forma_ov'] == 1 &&  $mydata['aprobacion__rdr_ov'] == 1 && $mydata['calculo_comision_asesor_ov'] = 1) {
                                    $ap = "SI";
                                } else {
                                    $ap = "NO";
                                }
                        ?>
                        <tr>
                            <td><a href="/orden/view?data=<?= $md5lead ?>&data2=<?= $mydata['id_ov_netsuite'] ?>"><?= $mydata['id_ov_tranid'] ?>
                                </a></td>
                            <td><?= $ap ?></td>
                            <td><?= $reserva_ov ?></td>
                            <td><?= $caida_ov ?></td>
                            <td><?= $mydata['creado_ov'] ?></td>
                        </tr>
                        <?php }
                        }  ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<input id="estado_lead" hidden disabled>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/js/leads/perfil/bitacoraPerfil.js?rev=<?= time(); ?>"></script>
<script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>">
</script>
<script src="/js/leads/perfil/estado.js?rev=<?php echo time(); ?>">
</script>
<script>
bitacoraPerfil();
ExtraerLead("Perfil");
$(document).ready(function() {
    new DataTable('#datatable-buttons1', {

        dom: 'Bfrtip',
        buttons: ['csv', 'excel'],
    });
    new DataTable('#datatable-buttons2', {
        dom: 'Bfrtip',
        buttons: ['csv', 'excel'],
    });
    new DataTable('#datatable-buttons3', {
        dom: 'Bfrtip',
        buttons: ['csv', 'excel'],
    });
    new DataTable('#datatable-buttons4', {
        dom: 'Bfrtip',
        buttons: ['csv', 'excel'],
    });
});
</script>
<div class="col-xl-12">
    <div class="card">
        <div class="card-body"
            style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
            <link rel="stylesheet" href="/assets/Ondrive/style.css">
            <div class="login">
                <div class="login-text">
                    <h3> Integración Netsuite - OneDrive</h3>
                    <label> Por favor inicie sesión para continuar.</label>
                </div>
                <div class="login-link">
                    <a id="signin" onclick="displayUI(); return false;" href="#">
                        <!-- Muestra imagen! -->
                        <img src="/assets/Ondrive/ms-image.png" alt="Sign in with Microsoft" />
                    </a>
                </div>
            </div>
            <main id="main-container" role="main" class="container" style="display: none; position: absolute;">
                <!-- Segmento de Contenedor Principal -->
                <div id="content">
                    <!-- Mensaje de bienvenida al nombre del usuario -->
                    <div class="user">
                        <!-- Animación de Carga!! -->
                        <div class="animation">
                            <div class="load">
                                <div id="uploadMessage" class="begin"> </div>
                            </div>
                        </div>
                        <div class="informacion">
                            <h3>Usuario: <span id="userName"></span></h3>
                        </div>
                    </div>
                    <hr />
                    <!-- Creación de Carpeta -->
                    <!-- Div.Folder tuvo modificaciones para Netsuite. -->
                    <div class="folder">
                        <h3> El registro consultado no posee una carpeta asociada.</h3>
                        <button class="folder-btn" onclick="createFolder()"><i class="icon fas fa-folder-plus"></i>
                            Crear Carpeta</button>
                    </div>
                    <!-- Trabajo de archivos
          *Muestra el área destinada para cargar archivos.
          *Muestra el área destinada para visualizar archivos.
        -->
                    <div class="files">
                        <!-- Carga de Archivos. -->
                        <div class="files-upload">
                            <div class="icon"><i class="fas fa-cloud-upload-alt"></i></div>
                            <header>Arrastre sus archivos aquí</header>
                            <span>o</span>
                            <button id="buscar">Seleccione su archivo.</button>
                            <input type="file" multiple hidden onchange="fileSelected(this)">
                        </div>
                        <!-- Muestra de Archivos -->
                        <div class="files-view">
                            <div class="files-titleDetails">
                                <label class="files-view-label"> Nombre.</label>
                                <label class="files-view-label"> Fecha. </label>
                                <label class="files-view-label"> Tamaño. </label>
                                <label class="files-view-label"> Descargar.</label>
                            </div>
                            <div class="files-details">
                                <ul id="downloadLinks"></ul>
                            </div>
                            <div class="emptyFiles">
                                <div class="icon"><i class="far fa-folder-open"></i></div>
                                <label class="emptyLabel">El registro consultado no posee archivos adjuntos.</label>
                            </div>
                        </div>
                    </div>
                    <div class="accessdenied">
                        <div class="icon"><i class="fas fa-exclamation-triangle"></i></div>
                        <label class="emptyLabel">El acceso a este recurso no se encuentra habilitado. Favor comunicarse
                            con el administrador del sistema.</label>
                        <button class="reload" onclick="reloadContent();"><i class="fas fa-sync"></i> "Inicio" </button>
                    </div>
                    <div class="recordUnregistered">
                        <div class="icon"><i class="fas fa-exclamation-triangle"></i></div>
                        <label class="emptyLabel">Este registro no está asociado a OneDrive. Favor comunicarse con
                            soporte.</label>
                        <button class="reload" onclick="reloadContent();"><i class="fas fa-arrow-alt-circle-left"></i>
                            "Volver" </button>
                    </div>
                </div>
            </main>
            <div class="oneDrivePreview" style="display: none;">
                <div class="previewActions">
                    <button class="close btnPreview" onclick="returnDisplay();"> <i
                            class="fas fa-arrow-alt-circle-left"></i> Volver</button>
                    <button class="expand btnPreview" onclick="expandPreview()"> <i
                            class="fas fa-expand-arrows-alt"></i> Expandir</button>
                </div>
                <iframe name="filePreviewframe" class="filePreviewframe" src="" height="100%" width="100%">
                </iframe>
            </div>
            <!-- Autenticación de Microsoft! -->
            <script src="https://alcdn.msauth.net/browser/2.1.0/js/msal-browser.min.js"
                integrity="sha384-EmYPwkfj+VVmL1brMS1h6jUztl4QMS8Qq8xlZNgIT/luzg7MAzDVrRa2JxbNmk/e"
                crossorigin="anonymous">
            </script>
            <!-- Cliente de Microsoft GRAPH -->
            <script src="https://cdn.jsdelivr.net/npm/@microsoft/microsoft-graph-client/lib/graph-js-sdk.js"></script>
            <script src="/assets/Ondrive/lead.js?version=3"></script>
        </div>
    </div>
</div>