    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Editar Lead</h4>
                    <!-- <a class="btn btn-dark waves-effect btn-label waves-light " href="javascript: history.go(-1)">Volver</a> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body "
                        style="border-radius: 27px;background: #ffffff;box-shadow:  -26px -26px 79px #bababa,26px 26px 79px #ffffff;">
                        <h4 class="card-title">Información recolectada de NetSuite (LEAD)</h4> <br>
                        <a onclick="ExtraerLeadModal('edit')" class="btn  mb-2 btn-dark collapsed"
                            data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                            <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                        </a>
                        <br>
                        <br>
                        <div class="row ticket">
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="productname">Nombre Completo</label>
                                    <input type="text" name="employee" id="employee" class="form-control mb-2" readonly
                                        disabled hidden />
                                    <input type="text" name="firstnames" id="firstnames" class="form-control mb-2" />
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">ID Lead</label>
                                    <input type="text" name="id" id="id" class="form-control mb-2" readonly disabled
                                        hidden />
                                    <input type="text" name="entitynumber" id="entitynumber" class="form-control mb-2"
                                        readonly disabled />
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Correo</label>
                                    <input type="text" name="emails" id="emails" class="form-control mb-2" />
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Número de teléfono</label>
                                    <input type="text" name="phones" id="phones" class="form-control mb-2" />
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Comentarios</label>
                                    <input type="text" name="comentario_clientes" id="comentario_clientes"
                                        class="form-control mb-2" />
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Campaña Marketing</label><br>
                                    <select id="campana_new_edit" name="campana_new_edit" class="form-control select2"
                                        required>
                                        <option value="">Seleccionar una campaña</option>
                                    </select>
                                </div>
                                <div class="mb-10 fv-row">
                                    <label class="required form-label" hidden>Moneda</label>
                                    <input type="text" name="moneda_clientes" id="moneda_clientes"
                                        class="form-control mb-2" readonly disabled hidden />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label class="required form-label">Proyectos</label><br>
                                    <select required name="proyecto_new_edit" id="proyecto_new_edit"
                                        class="form-control select2" style="width:100%; height: 36px!important">
                                        <option value="">Seleccionar una proyecto</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Subsidiaria</label><br>
                                    <select required name="subsidiary_new_edit" id="subsidiary_new_edit"
                                        class="form-control select2" style="width:100%">
                                        <option value="">Seleccionar una Subsidiaria
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="required form-label">Estado del LEAD Interesado</label>
                                    <input type="text" name="id_estadointeresado_cliente"
                                        id="id_estadointeresado_cliente" class="form-control mb-2" value="INTERESADO"
                                        readonly disabled />
                                </div>
                                <div class="mb-10 fv-row">
                                    <label class="required form-label">Nombre de la empresa</label>
                                    <input type="text" name="empresa_cliente" id="empresa_cliente"
                                        class="form-control mb-2" value="rocca" readonly disabled />
                                </div>
                                <div class="mb-10 fv-row">
                                    <label class="required form-label">Donde esucho de nosotros?</label>
                                    <input type="text" name="referencia_cliente" id="referencia_cliente"
                                        class="form-control mb-2" readonly disabled />
                                    <input type="text" name="id_referencia_cliente" id="id_referencia_cliente"
                                        class="form-control mb-2" readonly disabled hidden />
                                </div>
                            </div>
                        </div>
                        <input id="informacion_extra" class="form-control mb-2" value="0" readonly disabled hidden />
                        <input id="corredor_extra" class="form-control mb-2" value="0" readonly disabled hidden />
                        <input id="infromacion_extra_dos" class="form-control mb-2" value="0" readonly disabled
                            hidden />
                        <div class="row ticket" id="ticket">
                            <hr style="border: 2px solid #000;">
                            <h4 class="card-title">Información Extra </h4> <br>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label class="form-label">Cedula cliente</label>
                                    <input type="text" name="vatregnumber" id="vatregnumber" class="form-control mb-2"
                                        placeholder="Cedula Cliente" />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Nacionalidad cliente</label>
                                    <input autocomplete="of" type="text" name="custentity1" id="custentity1"
                                        class="form-control mb-2" placeholder="Nacionalidad del Cliente" required />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Estado Civil</label>
                                    <select required name="custentityestado_civil" id="custentityestado_civil"
                                        class="form-control" style="width:100%">
                                        <option value="">Seleccionar</option>
                                        <option value="1">Casado/a </option>
                                        <option value="2">Soltero/a </option>
                                        <option value="3">Unión Libre </option>
                                        <option value="4">Viudo </option>
                                        <option value="5">Divorciado(a) </option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Edad Cliente</label>
                                    <input autocomplete="of" type="text" name="custentity11" id="custentity11"
                                        class="form-control mb-2" placeholder="Edad del Cliente" required />
                                </div>
                                <div class="mb-10 fv-row">
                                    <label class="form-label">Hijos Cliente</label>
                                    <input type="text" name="custentityhijos_cliente" id="custentityhijos_cliente"
                                        value="0" class="form-control mb-2" placeholder="Hijos del cliente" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label class="form-label">Corredor Cliente</label>
                                    <select required name="corredor_lead_edit" id="corredor_lead_edit"
                                        class="form-control select2" style="width:100%">
                                        <option value="">Seleccionar</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="mb-1">Direccion cliente</label>
                                    <textarea id="defaultaddress" class="form-control" maxlength="225" rows="3"
                                        placeholder="Introduzaca una dirrecion  "></textarea>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Teléfono alternativo</label>
                                    <input autocomplete="of" type="text" name="altphone" id="altphone" value="00000000"
                                        class="form-control mb-2" placeholder="Teléfono alternativo" required />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Profesión Cliente</label>
                                    <input autocomplete="of" type="text" name="custentity_ix_customer_profession"
                                        id="custentity_ix_customer_profession" class="form-control mb-2"
                                        placeholder="Profesión del cliente" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <hr style="border: 2px solid #000;">
                                <h4 class="card-title">Información Extra Segundo Cliente </h4> <br>
                                <div class="mb-3">
                                    <label class="form-label">NOMBRE CLIENTE 2</label>
                                    <input type="text" name="custentity77" id="custentity77" class="form-control mb-2"
                                        placeholder="NOMBRE CLIENTE 2" />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">NACIONALIDAD 2</label>
                                    <input autocomplete="of" type="text" name="custentity81" id="custentity81"
                                        class="form-control mb-2" placeholder="NACIONALIDAD 2" required />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">ESTADO CIVIL 2</label>
                                    <select required name="custentityestado_civil_extra"
                                        id="custentityestado_civil_extra" class="form-control" style="width:100%">
                                        <option value="">Seleccionar</option>
                                        <option value="Casado/a ">Casado/a </option>
                                        <option value="Soltero/a ">Soltero/a </option>
                                        <option value="Unión Libre">Unión Libre </option>
                                        <option value="Viudo">Viudo </option>
                                        <option value="Divorciado(a) ">Divorciado(a) </option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">TELEFONO 2</label>
                                    <input autocomplete="of" type="text" name="custentity82" id="custentity82"
                                        value="00000000" class="form-control mb-2" placeholder="TELEFONO 2" required />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <hr style="border: 2px solid #000;">
                                <h4 class="card-title">Información Extra Segundo Cliente </h4> <br>
                                <div class="mb-3">
                                    <label class="form-label">N° CEDULA 2</label>
                                    <input autocomplete="of" type="text" name="custentity78" id="custentity78"
                                        class="form-control mb-2" placeholder="N° CEDULA 2" required />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-MAIL 2</label>
                                    <input autocomplete="of" type="text" name="custentity84" id="custentity84"
                                        class="form-control mb-2" placeholder="E-MAIL 2" required />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">PROFESIÓN 2</label>
                                    <input autocomplete="of" type="text" name="custentity79" id="custentity79"
                                        class="form-control mb-2" placeholder="PROFESIÓN 2" required />
                                </div>
                            </div>
                        </div>
                        <button type="button" onclick="Editar_lead_Net()"
                            class="btn btn-dark waves-effect waves-light">Editar en el
                            sistema</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="estado_lead" hidden disabled>
    <script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/perfil/editarPefil.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
    <script>
ExtraerLead("primerContacto");
    </script>