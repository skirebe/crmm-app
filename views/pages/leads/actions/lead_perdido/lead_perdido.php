<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h4 class="card-title"><strong>Reporte de Perdida de este Leads</strong>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> Describa este reporte
            brevemente</p>
        <div class="col-xl-6">
            <div class="mt-4 mt-lg-0">
                <blockquote class="blockquote  blockquote-reverse font-size-16 mb-0">
                    <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Volver a la pagina anteriror'>
                        <i class="bx bx-arrow-back"></i> Volver
                    </a>
                    <a onclick="ExtraerLeadModal('perdido')" class="btn  mb-2 btn-dark collapsed"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                        <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                    </a>
                </blockquote>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="mb-3 ajax-select mt-3 mt-lg-0" hidden disabled>
                <?php date_default_timezone_set("America/Costa_Rica");
                $fechaActual_in =  date('Y-m-d'); ?>
                <label>FECHA FINAL </label>
                <div class="docs-datepicker">
                    <div class="input-group">
                        <input autocomplete="off" type="date" class="form-control" name="start" id="start"
                            value="<?php echo $fechaActual_in; ?>">
                    </div>
                    <div class="docs-datepicker-container"></div>
                </div>
            </div>
            <div class="mb-3">
                <label class="control-label">Motivo de Perdida</label>
                <input id="estado_lead" hidden disabled>
                <select class="form-control select2" name="motivoss" id="motivoss" required>
                    <option value="">Seleccione un motivo...</option>
                </select>
                <div class="valid-feedback">Elemento seleccionado.</div>
                <div class="invalid-feedback">Debe seleccionar un elemento de esta lista</div>
            </div>
            <div class="col-sm-12">
                <div class="mb-12">
                    <label for="metadescription">Reporte</label>
                    <textarea class="form-control" id="metadescriptionMotivo" name="reporte" rows="10"
                        placeholder="Meta Description" required></textarea>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="d-flex flex-wrap gap-2">
            <butto onclick="Perdido()" class="btn btn-dark waves-effect waves-light">Generar
                Nota</button>
        </div>
    </div>
</div>
<script src="/js/leads/Perdido.js?rev=<?php echo time(); ?>"></script>
<script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>">
</script>
<script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>">
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js">
</script>
<script>
const dataManager = new DataManager();
dataManager.fillSelect("motivoss", "", "motivoss");
ExtraerLead("perdido");
</script>