<?php
if (isset($routesArray[2])) {
    switch ($routesArray[2]) {
        case "lista":
            include "actions/lista/" . $routesArray[2] . ".php";
            break;
        case "contacto":
            include "actions/contacto/" . $routesArray[2] . ".php";
            break;
        case "createEventLead":
            include "actions/createEventLead/" . $routesArray[2] . ".php";
            break;
        case "lead_perdido":
            include "actions/lead_perdido/" . $routesArray[2] . ".php";
            break;
        case "seguimiento":
            include "actions/seguimiento/" . $routesArray[2] . ".php";
            break;
        case "perfil":
            include "actions/perfil/" . $routesArray[2] . ".php";
            break;
        case "crearLead":
            include "actions/crearLead/" . $routesArray[2] . ".php";
            break;
        case "ConsultarLead":
            include "actions/ConsultarLead/" . $routesArray[2] . ".php";
            break;
        case "edit":
            include "actions/edit/" . $routesArray[2] . ".php";
            break;
    }
}