
<?php
/*=============================================
Requerir el archivo de conexión a la base de datos
=============================================*/
require_once './app/conexion.php';

/*=============================================
Función para actualizar leads
=============================================*/
function updateLeads($con, $arrayEventos, $whereClause, $valorSeguimiento)
{
    /*=============================================
    Consulta para seleccionar registros de la tabla 'calendars'
    =============================================*/
    $consulta = $con->query("SELECT * FROM `calendars` WHERE `id_lead` != 0 and `tipo_calendar` IN ('" . implode("','", $arrayEventos) . "') and $whereClause");

    /*=============================================
    Iterar a través de los resultados de la consulta
    =============================================*/
    while ($fila = mysqli_fetch_assoc($consulta)) {
        /*=============================================
        Obtener el ID del lead
        =============================================*/
        $id = $fila['id_lead'];

        /*=============================================
        Actualizar el lead en la tabla 'leads' con la acción y seguimiento especificados
        =============================================*/
        $actualizarLead = "UPDATE `leads` SET `seguimiento_calendar` = $valorSeguimiento, `accion_lead` = '6'  WHERE idinterno_lead = $id";
        mysqli_query($con, $actualizarLead);
      
    }
}

/*=============================================
Arreglo de eventos
=============================================*/
$arrayEventos = array('LLamada', 'Tarea', 'Reunion', 'Correo', 'Whatsapp', 'Seguimientos');

/*=============================================
Actualizar leads con acción 'Completado' o 'Cancelado'
si tiene un evento en estos estaos entonces
este lead requiere que le cambie el estado a seguimiento
cero significa seguimiento y 1 significa que esta en una accion
=============================================*/
updateLeads($con, $arrayEventos, "`accion_calendar` IN ('Completado','Cancelado')", 0);

/*=============================================
Actualizar leads con acción 'Pendiente'
si tiene un evento en estos estaos entonces
este lead requiere que le cambie el estado a seguimiento
0 significa seguimiento y 1 significa que esta en una accion
=============================================*/
updateLeads($con, $arrayEventos, "`accion_calendar` = 'Pendiente'", 1);

/*=============================================
Actualizar leads con acción '6' si han pasado más de 4 días desde la última actualización y no tienen seguimiento
=============================================*/
$actualizarLead = "UPDATE `leads` SET `accion_lead` = '3' WHERE `actualizadaaccion_lead` <= DATE_SUB(NOW(), INTERVAL 4 DAY) and `accion_lead` = '6' and `seguimiento_calendar` = 0";
mysqli_query($con, $actualizarLead);
?>
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Tablero</h4>
        </div>
    </div>
</div>
<?php include "actions/banners.php"; ?>
<?php include "actions/reportes.php"; ?>
<script src="/js/homePage/banners.js?rev=<?= time(); ?>"></script>
<script src="/js/homePage/eventosPendientes.js?rev=<?= time(); ?>"></script>
<script src="/js/homePage/grafico.js?rev=<?= time(); ?>"></script>
<script src="/js/homePage/kpi.js?rev=<?= time(); ?>"></script>
<script src="/js/homePage/reporteria.js?rev=<?= time(); ?>"></script>
<script src="/js/eventSelect/eventSelect.js?rev=<?=  time(); ?>"></script>
<script src="/js/homePage/homePage.js?rev=<?= time(); ?>"></script>