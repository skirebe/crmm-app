<style>
    #kpichar {
        width: 100%;
        height: 350px;
    }

</style>

<!-- HTML -->
<div id="kpichar"></div>

<div class="row text-center mb-3">
    <div class="col-4">
        <h5 class="mb-0" id="kpi_pediente"></h5>
        <p class="text-muted text-truncate">#: PENDIENTES</p>
    </div>
    <div class="col-4">
        <h5 class="mb-0"  id="kpi_completads"></h5>
        <p class="text-muted text-truncate">#: COMPLETADAS</p>
    </div>
    <div class="col-4">
        <h5 class="mb-0" id="kpi_canceladas"></h5>
        <p class="text-muted text-truncate">#: CANCELADAS</p>
    </div>
    <div class="col-4">
        <h5 class="mb-0" id="kpi_total"></h5>
        <p class="text-muted text-truncate" >#: TOTAL DE CITAS</p>
    </div>
