<!-- Styles -->

<style>
    #chartdivReportes {
        width: 100%;
        height: 400px;
    }
</style>
<!-- HTML -->
<div id="chartdivReportes"></div>
<br>
<div class="row text-center mb-3">
    <div class="col-3">
        <h5 class="mb-0" id="arrayLeadRepor"></h5>
        <p class="text-muted text-truncate">#: LEAD</p>
    </div>
    <div class="col-3">
        <h5 class="mb-0" id="arrarVisitasReport"></h5>
        <p class="text-muted text-truncate">#: VISITAS</p>
    </div>
    <div class="col-3">
        <h5 class="mb-0" id="arrOportunidades"></h5>
        <p class="text-muted text-truncate">#: OPORTUNIDADES</p>
    </div>
    <div class="col-3">
        <h5 class="mb-0" id="arrarPreReservas"></h5>
        <p class="text-muted text-truncate">#: PRE-RESERVA</p>
    </div>
</div>
<div class="row text-center mb-3">
    <div class="col-4">
        <h5 class="mb-0" id="arraPrereservaCaida"></h5>
        <p class="text-muted text-truncate">#: PRE RESREVA CAIDA</p>
    </div>
    <div class="col-4">
        <h5 class="mb-0" id="arrayReservas"></h5>
        <p class="text-muted text-truncate">#: RESERVA</p>
    </div>
    <div class="col-4">
        <h5 class="mb-0" id="arrastReservaCaida"></h5>
        <p class="text-muted text-truncate">#: RESERVA CAIDA</p>
    </div>
</div>