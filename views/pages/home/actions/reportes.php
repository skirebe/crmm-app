<div class="row" id="mostrarEventos" style="display: none">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">EVENTOS PENDIENTES DE ACCIÓN</h4>
                <div class="table-responsive">
                    <table id="datatable-buttons"
                        class="table table-striped table-light dt-responsive nowrap w-100 display ">
                        <thead class="table-light">
                            <tr>
                                <th style="text-align: right; font-weight: bold;">Vendedor</th>
                                <th style="text-align: right; font-weight: bold;">Cliente</th>
                                <th style="text-align: right; font-weight: bold;">Evento</th>
                                <th style="text-align: right; font-weight: bold;">Fecha de inicio</th>
                                <th style="text-align: right; font-weight: bold;">Estado</th>
                                <th style="text-align: right; font-weight: bold;">Ver evento</th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body"
                style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                <div id="reporteA1">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                </div>
                <h4 class="card-title mb-4" style=" display:none;" id="reporteA2">REPORTE MENSUAL</h4>
                <div class="tab-content mt-4" style="min-height: 340px; display:none;" id="reporteA3">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Abrir filtro
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse " aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="tab-pane active" id="buy-tab" role="tabpanel">
                                        <div hidden class="form-group mb-3">
                                            <label>Proyectos</label>
                                            <select hidden required name="selectProjectReport" placeholder="Choose ..."
                                                id="selectProyectReport" class="form-control select2" multiple
                                                style="width:100%; height: 36px!important">
                                                <option value="">Seleccionar una proyecto</option>
                                            </select>
                                        </div>
                                        <div hidden class="form-group mb-3">
                                            <label>Campaña</label>
                                            <select required name="selectCampanaReport"
                                                data-placeholder="Escoger Campañas..." id="selectCampanaReport"
                                                class="form-control select2" multiple
                                                style="width:100%; height: 36px!important">
                                                < </select>
                                        </div>
                                        <div>
                                            <div class="input-group mb-3">
                                                <input type="date" id="fecha1" autocomplete="off" class="form-control" placeholder="FECHA FINAL" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-container="#datepicker1" data-provide="datepicker" readonly>
                                            </div>
                                            <div class="input-group mb-3">
                                                <input type="date" id="fecha2" autocomplete="off" class="form-control" placeholder="FECHA FINAL" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-container="#datepicker1" data-provide="datepicker" readonly>
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button onclick="amchartReport(2)" class="btn btn-dark h-100 w-100"><i class="bx bx-search-alt align-middle"></i> Filtrar reporte</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <center>
                            <?php include "reportes/grafico.php"; ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card">
            <div class="card-body" style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                <div id="kpiA1">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                </div>
                <h4 class="card-title mb-4" style="display:none;" id="kpiA3">KPI REPORTE</h4>
                <div class="tab-content mt-4" style="min-height: 340px; display:none;" id="kpiA2">
                    <div class="accordion" id="accordionExample1">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                    Abrir filtro
                                </button>
                            </h2>
                            <div id="collapseOne1" class="accordion-collapse collapse " aria-labelledby="headingOne" data-bs-parent="#accordionExample1">
                                <div class="accordion-body">

                                    <div class="tab-pane active" id="buy-tab" role="tabpanel">
                                        <div class="form-group mb-3">
                                            <label>Proyectos</label>
                                            <select required name="selectProyectReport1" placeholder="Choose ..." id="selectProyectReport1" class="form-control select2" multiple style="width:100%; height: 36px!important">
                                                <option value="">Seleccionar una proyecto</option>
                                            </select>

                                        </div>
                                        <div class="form-group mb-3">
                                            <label>Campaña</label>
                                            <select required name="selectCampanaReport2" data-placeholder="Escoger Campañas..." id="selectCampanaReport2" class="form-control select2" multiple style="width:100%; height: 36px!important">
                                                < </select>
                                        </div>
                                        <div>
                                            <div class="input-group mb-3">
                                                <input type="date" id="fecha3" autocomplete="off" class="form-control" placeholder="FECHA FINAL" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-container="#datepicker1" data-provide="datepicker" readonly>
                                            </div>
                                            <div class="input-group mb-3">
                                                <input type="date" id="fecha4" autocomplete="off" class="form-control" placeholder="FECHA FINAL" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-container="#datepicker1" data-provide="datepicker" readonly>
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button onclick="amchartReportKpi(100)" id="tuBoon" class="btn btn-dark h-100 w-100"><i class="bx bx-search-alt align-middle"></i> Filtrar reporte</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <center>
                             <?php include "reportes/kpi.php"; ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
    $('#fecha1').datepicker({
        uiLibrary: 'bootstrap5',
        format: 'yyyy-mm-dd',

    });

    $('#fecha2').datepicker({
        uiLibrary: 'bootstrap5',
        format: 'yyyy-mm-dd',

    });

    $('#fecha3').datepicker({
        uiLibrary: 'bootstrap5',
        format: 'yyyy-mm-dd',
    });

    $('#fecha4').datepicker({
        uiLibrary: 'bootstrap5',
        format: 'yyyy-mm-dd',
    });
</script>