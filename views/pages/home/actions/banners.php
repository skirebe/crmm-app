<div class="row">
    <div class="col-lg-4">
        <a href="leads/lista?data=2">
            <div class="card mini-stats-wid">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div id="bannerA1">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                            <span class="placeholder col-7"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-6"></span>
                            <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="me-1" style="display: none;" id="bannerA2">
                            <p class="text-muted mb-2"><strong>LEADS NUEVOS</strong></p>
                            <h5 class="mb-0" id="bannerA4"> </h5>
                        </div>
                        <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                            id="bannerA3">
                            <span class="avatar-title rounded-circle bg-dark">
                                <i class="bx bx-user-x font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4">
        <a href="leads/lista?data=3" style="color: #000;">
            <div class="card mini-stats-wid">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div id="bannerB1">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                            <span class="placeholder col-7"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-6"></span>
                            <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="me-1" style="display: none;" id="bannerB2">
                            <p class="text-muted mb-2"><strong>LEADS REQUIEREN ATENCIÓN</strong></p>
                            <h5 class="mb-0" id="bannerB4"> </h5>
                        </div>
                        <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                            id="bannerB3">
                            <span class="avatar-title rounded-circle bg-dark">
                                <i class="bx bx-user-x font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4">
        <a href="evento/lista?data=1" style="color: #000;">
            <div class="card mini-stats-wid">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div id="bannerC1">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                            <span class="placeholder col-7"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-6"></span>
                            <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="me-1" style="display: none;" id="bannerC2">
                            <p class="text-muted mb-2"><strong>EVENTOS PARA HOY</strong></p>
                            <h5 class="mb-0" id="bannerC4"> </h5>
                        </div>
                        <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                            id="bannerC3">
                            <span class="avatar-title rounded-circle bg-dark">
                                <i class="bx bxs-calendar-plus font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <a href="oportunidad/list?data=1&data2=0" style="color: #000;">
            <div class="card mini-stats-wid">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div id="bannerD1">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                            <span class="placeholder col-7"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-6"></span>
                            <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="me-1" style="display: none;" id="bannerD2">
                            <p class="text-muted mb-2"><strong>OPORTUNIDADES</strong></p>
                            <h5 class="mb-0" id="bannerD4"> </h5>
                        </div>
                        <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                            id="bannerD3">
                            <span class="avatar-title rounded-circle bg-dark">
                                <i class="bx bx-bar-chart-alt-2 font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4">
        <a href="orden/lista?data=1" style="color: #000;">
            <div class="card mini-stats-wid">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div id="bannerE1">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                            <span class="placeholder col-7"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-4"></span>
                            <span class="placeholder col-6"></span>
                            <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="me-1" style="display: none;" id="bannerE2">
                            <p class="text-muted mb-2"><strong>ORDENES DE VENTA</strong></p>
                            <h5 class="mb-0" id="bannerE4"> </h5>
                        </div>
                        <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                            id="bannerE3">
                            <span class="avatar-title rounded-circle bg-dark">
                                <i class="bx bx-user-x font-size-24"></i>
                            </span>
                        </div>
                    </div>
                </div>
        </a>
    </div>
</div>
<div class="col-lg-4">
    <a href="/orden/lista?data=2" style="color: #000;">
        <div class="card mini-stats-wid">
            <div class="card-body"
                style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                <div id="bannerF1">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                </div>
                <div class="d-flex flex-wrap">
                    <div class="me-1" style="display: none;" id="bannerF2">
                        <p class="text-muted mb-2"><strong>CONTRATO FIRMADO</strong></p>
                        <h5 class="mb-0" id="bannerF4"> </h5>
                    </div>
                    <div class="ms-auto mini-stat-icon avatar-sm rounded-circle bg-dark" style="display: none;"
                        id="bannerF3">
                        <span class="avatar-title rounded-circle bg-dark">
                            <i class="bx bx-user-x font-size-24"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>