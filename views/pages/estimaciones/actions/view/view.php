<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Estimación</h4>
        </div>
    </div>
</div>
<a onclick="ExtraerLeadModal('estimacion')" class="btn  mb-2 btn-dark collapsed" data-bs-toggle='tooltip'
    data-bs-placement='top' title='Ver perfil de este usuario'>
    <i class="bx bxs-user-detail"></i> Realizar Nueva Accion
</a>
<br>
<br>
<div class="row">
    <?php
    include "app/conexion.php";
    $data = $_GET['data2'];
    $leadData = $_GET['data'];
    $routesArray = $data;
    $select_stmt = $db->prepare("SELECT COUNT(id_ov )  as result FROM ordenventa  WHERE status_ov=1 and  id_ov_est= :id_admin");
    $select_stmt->execute([':id_admin' => $routesArray]);
    $row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
    if ($row_lead['result'] > 0) { ?>
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">EDITAR</p>
                        <div class="alert alert-danger">Ya existe una orden de venta </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">Generar orden de venta</p>
                        <div class="alert alert-danger">Ya existe una orden de venta </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium"> <a data-bs-toggle="modal" data-bs-target=".bs-example-modal-xl"
                                class="btn btn-dark btn-sm"><i class="bx bxs-edit align-middle"></i></a> EDITAR</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium"><a
                                href="/orden/create?data=<?= $leadData ?>&data2=<?= $routesArray ?>"
                                class="btn btn-dark btn-sm"><i class="bx bx-add-to-queue align-middle"> </i></a> ORDEN
                            DE VENTA </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="col-lg-3" id="preReserva_button" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium"> <a onclick="prereserva()" class="btn btn-dark btn-sm"><i
                                    class="bx bx-mail-send align-middle"></i> </a> PRE RESERVA</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3" id="preresrevachek" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">PRE RESERVA</p>
                        <div class="alert alert-danger">Ya se envio la PRE-RESERVA</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3" id="preReserva_button_Valor" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">PRE- RESERVA</p>
                        <div class="alert alert-danger">
                            Para realizar una pre-reserva se debe llenar los campos correspondientes
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3" id="preReserva_button_Valor_caiada" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">PRE- RESERVA</p>
                        <div class="alert alert-danger">
                            YA SE ENVIO LA CONFIRMACION CAIDA
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3" id="estimacion_caida" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium"> <a onclick="estimacion_caida()" class="btn btn-dark btn-sm"><i
                                    class="mdi mdi-delete-forever align-middle"></i> </a> ESTIMACION CAIDA</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3" id="estimacion_caida_valor" style="display:none">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">ESTIMACION CAIDA</p>
                        <div class="alert alert-danger">
                            YA SE ENVIO LA CONFIRMACION CAIDA
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium"> <a class="btn btn-dark btn-sm" target="_blank"
                                href="https://4552704.app.netsuite.com/app/accounting/print/hotprint.nl?regular=T&sethotprinter=T&formnumber=342&trantype=estimate&&id=<?= $routesArray ?>&label=Estimaci%25u00F3n&printtype=transaction"><i
                                    class="bx bxs-file-pdf align-middle"></i> </a> PDF ESTIMACION</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
// echo "SELECT COUNT(id_ov) as result ,`id_ov_netsuite`,id_ov_tranid  FROM ordenventa WHERE id_ov_est=$routesArray";
$sql = mysqli_query($con, "SELECT COUNT(id_ov) as result ,`id_ov_netsuite`,id_ov_tranid  FROM ordenventa WHERE id_ov_est=$routesArray");
$num = mysqli_num_rows($sql);

if ($num > 0) {
    while ($row = mysqli_fetch_array($sql)) {
        if ($row['result'] > 0) {
            $id_ov_netsuite = $row['id_ov_netsuite'];
            $id_ov_tranid = $row['id_ov_tranid'];
?>
<div class="col-xl-12">
    <div class="card"
        style="border-radius: 24px; background: #ffffff; box-shadow:  22px 22px 44px #8c8c8c, -22px -22px 44px #ffffff;">
        <div>
            <div class="row">
                <div class="col-lg-9 col-sm-8">
                    <div class="p-4">
                        <div class="d-flex">
                            <div class="flex-grow-1 overflow-hidden">
                                <div class="alert alert-dark" role="alert">
                                    <h4 class="text-truncate font-size-15">ORDEN DE VENTA RELACIONADA</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row task-dates">
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> N.º DE
                                        PEDIDO</h5>
                                    <p class="text-muted mb-0"><?php echo $id_ov_tranid; ?></p>
                                </div>
                            </div>
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14">VER OV</h5>
                                    <p class="text-muted mb-0">
                                        <a href="/orden/view?data=<?= $leadData ?>&data2=<?= $id_ov_netsuite ?>"
                                            class="btn btn-dark btn-sm"><i class="fas fa-eye align-middle"></i> </a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14">VER PDF OV</h5>
                                    <p class="text-muted mb-0">
                                        <a class="btn btn-dark btn-sm"
                                            href="https://4552704.app.netsuite.com/app/accounting/print/hotprint.nl?regular=T&sethotprinter=T&formnumber=136&trantype=salesord&&id=<?= $id_ov_netsuite; ?>&label=Orden+de+venta&printtype=transaction"><i
                                                class="bx bxs-file-pdf align-middle"></i> </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="col-lg-3 col-sm-4 align-self-center">
                    <div>
                        <img src="assets/images/crypto/features-img/img-1.png" alt="" class="img-fluid d-block">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
        }
    }
}
?>
<div class="card "
    style="border-radius: 24px; background: #ffffff; box-shadow:  22px 22px 44px #8c8c8c,-22px -22px 44px #ffffff;">
    <div class="card-body">
        <div class="d-flex">
            <div class="flex-grow-1 overflow-hidden">
                <div class="alert alert-success" role="alert">
                    <h4 class="text-truncate font-size-15">ID ESTIMACION:<p class="text-muted mb-0" id="train_id"></p>
                    </h4>
                </div>
            </div>
        </div>
        <div class="row task-dates">
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> CLIENTE:</h5>
                    <p class="text-muted mb-0" id="data_entityname"></p>
                    <p class="text-muted mb-0" id="empleado"></p>
                    <input autocomplete="off" type="text" class="form-control" id="empleado_pre" name="empleado_pre"
                        disabled hidden>
                    <input autocomplete="off" type="text" class="form-control" id="opo_caida" name="opo_caida" disabled
                        hidden>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>UNIDAD EXPEDIENTE
                        LIGADO VTA</h5>
                    <p class="text-muted mb-0" id="data_Exp"></p>
                    <p hidden class="text-muted mb-0 " id="data_custbody38"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> SUBSIDIARIA</h5>
                    <p class="text-muted mb-0" id="data_Subsidaria"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> ESTADO</h5>
                    <p class="text-muted mb-0" id="data_estado"></p>
                    <p hidden class="text-muted mb-0" id="data_entitystatus"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>OPORTUNIDAD</h5>
                    <p class="text-muted mb-0" id="data_opportunity"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>CIERRE DE PREVISTO
                    </h5>
                    <p class="text-muted mb-0" id="data_expectedclosedate"></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row task-dates">
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> PRECIO DE LISTA:</h5>
                    <p class="text-muted mb-0" id="data_custbody13"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> MONTO DESCUENTO
                        DIRECTO</h5>
                    <p class="text-muted mb-0" id="data_custbody132"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> MONTO EXTRAS SOBRE
                        EL PRECIO DE LISTA</h5>
                    <p class="text-muted mb-0" id="data_custbody46"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> DESCRIPCIÓN EXTRAS
                    </h5>
                    <p class="text-muted mb-0" id="data_custbody47"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>CASHBACK</h5>
                    <p class="text-muted mb-0" id="data_custbodyix_salesorder_cashback"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>MONTO RESERVA</h5>
                    <p class="text-muted mb-0" id="data_custbody52"></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row task-dates">
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> MONTO TOTAL DE CORTESÍAS
                    </h5>
                    <p class="text-muted mb-0" id="data_custbody16"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>DESCRIPCIÓN DE LAS
                        CORTESIAS</h5>
                    <p class="text-muted mb-0" id="data_custbody35"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> PREC. DE VENTA
                        MÍNIMO:</h5>
                    <p class="text-muted mb-0" id="data_custbody_precio_vta_min"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i> PEC. DE VENTA NETO:
                    </h5>
                    <p class="text-muted mb-0" id="data_pvneto"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>EXTRAS SOBRE EL
                        PRECIO DE LISTA</h5>
                    <p class="text-muted mb-0" id="data_custbody185"></p>
                </div>
            </div>
            <div class="col-sm-2 col-6">
                <div class="mt-4">
                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>MONTO TOTAL</h5>
                    <p class="text-muted mb-0" id="data_custbody_ix_total_amount"></p>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="col-xl-12">
    <div class="card"
        style="border-radius: 24px; background: #ffffff; box-shadow:  22px 22px 44px #8c8c8c,-22px -22px 44px #ffffff;">
        <div>
            <div class="row">
                <div class="col-lg-9 col-sm-8">
                    <div class="p-4">
                        <div class="d-flex">
                            <div class="flex-grow-1 overflow-hidden">
                                <div class="alert alert-dark" role="alert">
                                    <h4 class="text-truncate font-size-15">CONDICIONES DE LA PRIMA</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row task-dates">
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> PRIMA
                                        TOTAL</h5>
                                    <p class="text-muted mb-0" id="data_custbody39"></p>
                                </div>
                            </div>
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14"><i
                                            class="bx bx-calendar-check me-1 text-primary"></i>PRIMA%</h5>
                                    <p class="text-muted mb-0" id="data_custbody60"></p>
                                </div>
                            </div>
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> MONTO
                                        PRIMA NETA</h5>
                                    <p class="text-muted mb-0" id="data_custbody_ix_salesorder_monto_prima"></p>
                                </div>
                            </div>
                            <div class="col-sm-2 col-6">
                                <div class="mt-4">
                                    <h5 class="font-size-14"><i class="bx bx-calendar-check me-1 text-primary"></i>
                                        MONTO ASIGNABLE PRIMA NETA:</h5>
                                    <p class="text-muted mb-0" id="data_custbody211"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card" style="border-radius: 24px; background: #ffffff; box-shadow:  22px 22px 44px #8c8c8c,
                         -22px -22px 44px #ffffff;">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="flex-grow-1 overflow-hidden">
                            <div class="alert alert-danger" role="alert">
                                <h4 class="text-truncate font-size-15"> ARTICULOS LINEAS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable-buttons"
                            class="table table-striped table-light dt-responsive nowrap w-100 display ">
                            <thead>
                                <tr class="text-center">
                                    <th colspan="7">LINEAS</th>
                                </tr>
                                <tr>
                                    <th style="text-align: right;" class="align-middle" scope="col">#</th>
                                    <th style="text-align: right;" class="align-middle" scope="col">ARTÍCULO</th>
                                    <th style="text-align: right;" class="align-middle" scope="col">MONTO</th>
                                    <th style="text-align: right;" class="align-middle" scope="col">FECHA DE PAGO
                                        PROYECTADO</th>
                                    <th style="text-align: right;" class="align-middle" scope="col">CANTIDAD</th>
                                    <th style="text-align: right;" class="align-middle" scope="col">DESCRIPCIÓN</th>
                                </tr>
                            </thead>
                            <tbody id="table-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div>
            <?php require_once "Modal_estimacion.php";  ?>
        </div>
    </div>
    <script src="/js/estimacion/extraerDatosEstimacion.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/estimacion/editar_estimacion.js?rev=<?= time(); ?>"></script>
    <script src="/js/estimacion/prereserva.js?rev=<?= time(); ?>"></script>
    <script src="/js/estimacion/estimacion_caida.js?rev=<?= time(); ?>"></script>
    <script>
    ExtraerdatosEstimacion()
    </script>