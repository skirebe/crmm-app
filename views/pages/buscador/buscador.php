<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css?versi=1">
<link rel="stylesheet" href="https://cdn.datatables.net/searchpanes/2.2.0/css/searchPanes.dataTables.min.css?versi=1">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.7.0/css/select.dataTables.min.css?versi=1">
<h1 class="mb-sm-0 font-size-18">Buscar el registro</h1>
<!-- Selector de opciones -->
<form id="buscar" class="needs-validation" novalidate="" method="POST">
    <label for="tabla">Selecciona una opción:</label>
    <select id="selectBuscador" name="selectBuscador" class="form-control">
        <option value="leads">Leads</option>
        <option value="oportunidades">Oportunidades</option>
        <option value="estimaciones">Estimaciones</option>
        <option value="ordenventa">Orden de Venta</option>
        <option value="expedientes">Expediente Unidad</option>

    </select>
    <br>
    <label for="searchTerm">Buscar referencia:</label>
    <input type="text" id="searchTerm" name="searchTerm" class="form-control" value=""
        placeholder="Ingrese el término de búsqueda" />
    <br>
    <br>
    <button onclick="buscadorGeneral()" class="btn mb-4 btns btn-dark" type="button">Buscar</button>
</form>
<table id="datatable-buttons" class="table table-striped table-light dt-responsive nowrap w-100 display">
    <thead>
        <tr>
            <th>ASESOR</th>
            <th>TIPO DE REGISTRO</th>
            <th>Resultado</th>
        </tr>
    </thead>
    <tbody id="table-body">
    </tbody>
    <div id="tableleads">
        <h5 class="card-title placeholder-glow">
            <span class="placeholder col-12"></span>
        </h5>
        <p class="card-text placeholder-glow">
            <span class="placeholder col-12"></span>
            <span class="placeholder col-12"></span>
            <span class="placeholder col-12"></span>
            <span class="placeholder col-12"></span>
            <span class="placeholder col-12"></span>
        </p>
    </div>
</table>
<script src="/js/buscador/buscador.js?rev=<?= time(); ?>"></script>