<?php
include "app/conexion.php";
$data = $_GET['data2'];
$select_stmt = $db->prepare("SELECT id_ov_est,id_ov_lead,id_ov_netsuite, COUNT(id_ov) AS result FROM ordenventa WHERE id_ov_est = :id");
$select_stmt->execute([':id' => $data]);
$row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);

$id_est = $row_lead['id_ov_est'];
$cant = $row_lead['result'];
$id_le = $row_lead['id_ov_lead'];

$id_ov_netsuite = $row_lead['id_ov_netsuite'];

if ($cant > 0) {
    echo "<script>window.location.href = 'https://crm.roccacr.com/orden/view?data=$id_le&data2=$id_ov_netsuite';</script>";
    exit;
} else {?>
<script src="/js/alertModal/alertModal.js?rev=<?=time();?>"></script>
<script src="assets/sweetalert2/sweetalert2.js?rev=<?=time();?>"></script>
<script src="/js/ordenVenta/generarOrdenVenta.js?rev=<?=time();?>"></script>
<script>
generarOrdenDeVenta()
</script>
<?php }?>