<div class="col-lg-12">
    <div class="card">
        <div class="card-body cardStyleLeads">
            <h4 class="card-title"><strong>MODULO DE Cotizaciones </strong></h4>
            <div class="row">
                <div class="col-xl-6">
                    <div>
                        <a class="btn btn-dark collapsed" data-bs-toggle="collapse" href="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample" data-bs-toggle='tooltip'
                            data-bs-placement='top'
                            title='Para organizar mejor tus Cotizaciones, te recomendamos utilizar la función de filtrado por rango de fechas. De esta manera podrás visualizar solo aquellos Cotizaciones que se encuentren dentro del rango seleccionado'>
                            <i class="fas fa-filter"></i> Rango de fechas
                        </a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="mt-4 mt-lg-0">
                        <div class="d-flex flex-wrap gap-2">
                            <!-- aqui van los botones -->
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Definir las fechas de inicio y fin por defecto o tomarlas de la entrada POST
            $start = isset($_POST['startFilter']) ? $_POST['startFilter'] : date('Y-m-01');
            $end = isset($_POST['endtFilter']) ? $_POST['endtFilter'] : date('Y-m-t');

            // Inicializar las variables de control de radio buttons
            $chek = "checked";
            $chek2 = "";

            // Comprobar si se ha enviado una solicitud POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // Obtener el valor del radio button seleccionado o establecer un valor predeterminado vacío
                $radio = $_POST['radio'] ?? '';

                // Verificar si se dejaron campos vacíos
                if (empty($start) || empty($end) || empty($radio)) {
                    echo '<div class="alert alert-danger">Si desea filtrar por rango de fecha debe seleccionar la fecha de inicio y la fecha final y el motivo</div>';
                }

 

                // Mostrar una sección adicional (puede requerir más código para esta sección)
                $show = "show";
            }
            ?>
            <div class="collapse show" id="collapseExample">
                <form class="needs-validation" novalidate="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Inicio</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="startFilter"
                                            id="startFilter" value="<?= $start; ?>" placeholder="FECHA DE INICIO"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Final</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="endtFilter"
                                            id="endtFilter" value="<?= $end; ?>" placeholder="FECHA FINAL"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-dark">Filtrar Busqueda</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.holas {
    display: none;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleCotizaciones">
                <button class="btn  mb-2 btns btn-secondary collapsed" style="display: none;"
                    id="extract-button">Cotacto Masivo</button>
                <h4 class="card-title"><strong>Lista de Cotizaciones</strong></h4>
                 <div id="tableCotizaciones">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-12"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                    </p>
                </div>
                <table id="datatable-buttons"
                    class="table table-striped table-light dt-responsive nowrap w-100 display">
                    <thead>
                        <tr>
                            <th style="text-align: left;">#COTIZACION</th>
                            <th style="text-align: left;">LEADS</th>
                            <th style="text-align: left;">#OPORTUINDAD</th>
                            <th style="text-align: left;">#ESTIMACION</th>
                            <th style="text-align: left;">#EXPEDIENTE</th>
                            <th style="text-align: left;">FECHA DE CREACION</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/ordenVenta/listaCotizaciones.js?rev=<?= time(); ?>"></script>
<script>
ExtraDataCotizaciones();
</script>