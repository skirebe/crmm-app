  <div class="row">
      <div class="col-12">
          <div class="page-title-box d-sm-flex align-items-center justify-content-between">
              <h4 class="mb-sm-0 font-size-18">Orden de venta</h4>
          </div>
      </div>
  </div>
  <a onclick="ExtraerLeadModal('estimacion')" class="btn  mb-2 btn-dark collapsed" data-bs-toggle='tooltip'
      data-bs-placement='top' title='Ver perfil de este usuario'>
      <i class="bx bxs-user-detail"></i> Realizar Nueva Accion
  </a>
  <br>
  <br>
  <br>
  <br>
  <div class="row">
      <?php
include "app/conexion.php";

$data = $_GET['data2'];
$leadData = $_GET['data'];
$routesArray = $data;
$select_stmt = $db->prepare("SELECT *  FROM ordenventa  WHERE  id_ov_netsuite= :id");
$select_stmt->execute([':id' => $routesArray]);
$row_lead = $select_stmt->fetch(PDO::FETCH_ASSOC);
$id_ov_netsuite = $row_lead['id_ov_netsuite'];
$id_ov_admin = $row_lead['id_ov_admin'];
$id_ov_tranid = $row_lead['id_ov_tranid'];
$id_ov_est = $row_lead['id_ov_est'];
$id_ov_opt = $row_lead['id_ov_opt'];
$caida_ov = $row_lead['caida_ov'];
$reserva_ov = $row_lead['reserva_ov'];
$validarcampo1 = $caida_ov == 1 ? "display:none;" : "display:;";
$validarcampo2 = $caida_ov == 0 ? "display:none;" : "display:;";


$campo_reserva = $reserva_ov == 1 ? "display:" : "display:none";
$valor_reserva = $reserva_ov == 1 ? "1" : "0";

?>
      <input autocomplete="off" type="text" class="form-control" id="empleado_pre" name="empleado_pre"
          value="<?=$id_ov_admin?>" disabled hidden>
      <div class="col-lg-3">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <a data-bs-toggle="modal" data-bs-target=".bs-example-modal-xl" class="btn btn-dark btn-sm"><i
                                  class="bx bx-edit align-middle"></i></a> EDITAR OV
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <a href="/estimaciones/view?data=<?=$leadData?>&data2=<?=$id_ov_est?>"
                              class="btn btn-dark btn-sm"><i class="mdi mdi-eye-check align-middle"> </i> </a> VER
                          ESTIMACIÓN
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <a href="/oportunidad/oprt_view?data=<?=$leadData?>&data2=<?=$id_ov_opt?>"
                              class="btn btn-dark btn-sm"><i class="mdi mdi-eye-check align-middle"> </i></a> VER
                          OPORTUNIDAD
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <a class="btn btn-dark btn-sm" onclick="abrirPDF()">
                              <i class="bx bxs-file-pdf align-middle"></i>
                          </a> PDF OV
                          <script>
                          function abrirPDF() {

                              const goURL =
                                  `https://4552704.app.netsuite.com/app/accounting/print/hotprint.nl?regular=T&sethotprinter=T&formnumber=136&trantype=salesord&&id=<?=$id_ov_netsuite?>&label=Orden+de+venta&printtype=transaction`;
                              // Abre una nueva ventana sin cerrar la existent
                              window.open(goURL, 'PopupWindow', 'width=900,height=800,scrollbars=yes');
                          }
                          </script>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" id="boton_reserav" style="display: none;">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"> <a onclick="reserva()" class="btn btn-dark btn-sm"><i
                                      class="bx bx-mail-send align-middle"></i> </a> ENVIAR RESERVA</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" id="boton_reserav_valido_campos" style="display: none;">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"> <i class="bx bx-mail-send align-middle"></i> </a> ENVIAR
                              RESERVA</p>
                          <div class="d-flex">
                              <div class="flex-grow-1">
                                  <div class="d-flex">
                                      <div class="flex-grow-1">
                                          <div class="alert alert-danger">
                                              LOS CAMPOS DE RESERVA ESTAN VACIOS.
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" style="<?=$campo_reserva?>">
          <input autocomplete="off" type="text" class="form-control" id="valor_reserva" name="valor_reserva"
              value="<?=$valor_reserva?>" hidden disabled>
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"> <i class="bx bx-mail-send align-middle"></i> </a> ENVIAR
                              RESERVA</p>
                          <div class="d-flex">
                              <div class="flex-grow-1">
                                  <div class="d-flex">
                                      <div class="flex-grow-1">
                                          <div class="alert alert-danger">
                                              YA SE ENVIO LA RESERVA
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" id="boton_cierre_firmado" style="display: none;">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"> <a onclick="cierre()" class="btn btn-dark btn-sm"><i
                                      class="bx bxs-folder-open align-middle"></i> </a> CIERRE FIRMADO</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" id="boton_cierre_firmado_enviado" style="display: none;">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"><i class="mdi mdi-file-cancel align-middle"></i> </a>CIERRE
                              FIRMADO</p>
                          <div class="d-flex">
                              <div class="flex-grow-1">
                                  <div class="alert alert-danger">
                                      YA SE ENVIO LA CONFIRMACION CIERRE FIRMADO
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" style="<?=$validarcampo1?>">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"> <a onclick="cierrecaida()" class="btn btn-dark btn-sm"><i
                                      class="mdi mdi-file-cancel align-middle"></i> </a> RESERVA CAÍDA</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3" style="<?=$validarcampo2?>">
          <div class="card mini-stats-wid">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-grow-1">
                          <p class="text-muted fw-medium"><i class="mdi mdi-file-cancel align-middle"></i> </a> RESERVA
                              CAÍDA</p>
                          <div class="d-flex">
                              <div class="flex-grow-1">
                                  <div class="alert alert-danger">
                                      YA SE ENVIO LA CONFIRMACION CAÍDA
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-lg-8">
          <div class="card">
              <div class="card-body">
                  <div class="d-flex">
                      <div class="flex-shrink-0 me-4">
                          <img src="https://icons.veryicon.com/png/o/business/crm-system-icon/sales-order-1.png" alt=""
                              class="avatar-sm">
                      </div>
                      <input autocomplete="off" type="text" class="form-control" name="exp_correo" id="exp_correo"
                          hidden disabled>
                      <input autocomplete="off" type="text" class="form-control" name="id_est" value="<?=$routesArray?>"
                          id="id_est" hidden disabled>
                      <input autocomplete="off" type="text" class="form-control" name="url_edit"
                          value="<?=$editar_orden?>" id="url_edit" hidden disabled>
                      <div class="flex-grow-1 overflow-hidden">
                          <h5 class="text-truncate font-size-15">Orden de venta</h5>
                          <span>
                              <h2 class="text-truncate font-size-15"><?=$id_ov_tranid?></h2>
                          </span>
                      </div>
                  </div>
                  <h5 class="font-size-15 mt-4">Cliente:</h5>
                  <p class="text-muted">
                  <div class="alert alert-primary" role="alert" id="div_entityname"></div>
                  </p>
                  <div class="row task-dates">
                      <div class="alert alert-secondary" role="alert"> INFORMACIÓN PRIMARIA</div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> N.º DE PEDIDO</h5>
                              <p class="text-muted mb-0" id=""><?=$id_ov_tranid?></p>
                          </div>
                      </div>
                      <!--
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> CLIENTE:</h5>
                              <p class="text-muted mb-0" id="div_entityname2"></p>

                          </div>
                      </div> -->
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> FECHA</h5>
                              <p class="text-muted mb-0" id="div_trandate"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> NOTA</h5>
                              <p class="text-muted mb-0" id="div_memo"></p>
                          </div>
                      </div>
                  </div>
                  <br>
                  <br>
                  <div class="row task-dates">
                      <div class="alert alert-secondary" role="alert"> INFORMACIÓN DE VENTA </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>UNIDAD EXPEDIENTE
                                  LIGADO</h5>
                              <p class="text-muted mb-0" id="div_expediente"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> PRECIO DE VENTA</h5>
                              <p class="text-muted mb-0" id="div_custbody_ix_total_amount"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> ENTREGA ESTIMADA
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody114"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MÉTODO DE PAGO</h5>
                              <p class="text-muted mb-0" id="div_custbody75"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>FONDOS DE COMPRA</h5>
                              <p class="text-muted mb-0" id="div_custbody37"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> FECHA DE VIGENCIA DE
                                  LA VENTA</h5>
                              <p class="text-muted mb-0" id="div_saleseffectivedate"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>CAMPAÑA DE MARKETING
                              </h5>
                              <p class="text-muted mb-0" id="div_leadsource"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>REPRESENTANTE DE
                                  VENTAS</h5>
                              <p class="text-muted mb-0" id="div_salesrep"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i> SOCIO</h5>
                              <p class="text-muted mb-0" id="div_partner"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MOTIVO DE CANCELACIÓN
                                  DE RESERVA O VENTA CAÍDA</h5>
                              <p class="text-muted mb-0" id="div_custbody115"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>COMENTARIOS
                                  CANCELACIÓN DE RESERVA</h5>
                              <p class="text-muted mb-0" id="div_custbody116"></p>
                          </div>
                      </div>
                  </div>
                  <br>
                  <br>
                  <div class="row task-dates">
                      <div class="alert alert-secondary" role="alert"> AUTORIZACION DE VENTA</div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>PRECIO DE LISTA</h5>
                              <p class="text-muted mb-0" id="div_custbody13"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO DESCUENTO
                                  DIRECTO</h5>
                              <p class="text-muted mb-0" id="div_custbody132"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO EXTRAS SOBRE EL
                                  PRECIO DE LISTA</h5>
                              <p class="text-muted mb-0" id="div_custbody46"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>DESCRIPCIÓN DE EXTRAS
                                  SOBRE EL PRECIO DE LISTA</h5>
                              <p class="text-muted mb-0" id="div_custbody47"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO TOTAL DE
                                  CORTESÍAS</h5>
                              <p class="text-muted mb-0" id="div_custbody16"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>DESCRIPCIÓN DE
                                  CORTESÍAS</h5>
                              <p class="text-muted mb-0" id="div_custbody35"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>PRIMA TOTAL</h5>
                              <p class="text-muted mb-0" id="div_custbody39"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO RESERVA</h5>
                              <p class="text-muted mb-0" id="div_custbody52"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>CASHBACK</h5>
                              <p class="text-muted mb-0" id="div_custbodyix"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>% COMISIÓN DEL
                                  CORREDOR</h5>
                              <p class="text-muted mb-0" id="div_custbody14"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>PRECIO DE VENTA NETO
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody17"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>PRECIO DE VENTA
                                  MÍNIMO</h5>
                              <p class="text-muted mb-0" id="div_custbody18"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>COMISIÓN DEL ASESOR %
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody20"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO DE COMISIÓN DEL
                                  ASESOR</h5>
                              <p class="text-muted mb-0" id="div_custbody21"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>PRECIO CÁLCULO
                                  COMISIÓN CORREDOR</h5>
                              <p class="text-muted mb-0" id="div_custbody22"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO DE COMISIÓN
                                  SEGUNDO ASESOR</h5>
                              <p class="text-muted mb-0" id="div_custbody71"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO DE COMISIÓN DE
                                  CORREDOR</h5>
                              <p class="text-muted mb-0" id="div_custbody15"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>DIFERENCIA ENTRE EL
                                  PVN Y EL PVM</h5>
                              <p class="text-muted mb-0" id="div_custbody19"></p>
                          </div>
                      </div>
                      <!-- <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>TIPO DE CAMBIO</h5>
                              <p class="text-muted mb-0" id="div_exchangerate"></p>
                          </div>
                      </div> -->
                  </div>
                  <div class="row task-dates">
                      <div class="alert alert-secondary" role="alert"> RESERVA</div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MEDIO DE PAGO</h5>
                              <p class="text-muted mb-0" id="div_custbody188"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>NÚMERO DE TRANSACCIÓN
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody189"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO RESERVA
                                  APLICADA</h5>
                              <p class="text-muted mb-0" id="div_custbody207"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>FECHA DE RESERVA
                                  APLICADA</h5>
                              <p class="text-muted mb-0" id="div_custbody208"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>MONTO PRE-RESERVA
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody191"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>FECHA DE PRE-RESERVA
                              </h5>
                              <p class="text-muted mb-0" id="div_custbody206"></p>
                          </div>
                      </div>
                      <div class="col-sm-4 col-6">
                          <div class="mt-4">
                              <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>OBSERVACIONES
                                  CONFIRMA RESERVA</h5>
                              <p class="text-muted mb-0" id="div_custbody190"></p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-4">
          <div class="card">
              <!-- *********************** TABLA DE CHEKCS **************************** -->
              <hr style="border: 0.5px solid black;">
              <div class="card-body">
                  <h4 class="card-title mb-4">ESTADOS ORDEN DE VENTA</h4>
                  <table class="table align-middle table-nowrap table-check" id="myTable">
                      <thead class="table-light">
                          <tr>
                              <th class="align-middle">VALIDACION</th>
                              <th class="align-middle">TIPO</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody90" disabled>
                                      <label class="form-check-label" for="custbody90"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">CONTRATO FIRMADO</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody51" disabled>
                                      <label class="form-check-label" for="custbody51"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold"> CIERRE FIRMADO</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody91" disabled>
                                      <label class="form-check-label" for="custbody91"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold"> UNIDAD ENTREGADA</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody105" disabled>
                                      <label class="form-check-label" for="custbody105"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">UNIDAD TRASPASADA</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody43" disabled>
                                      <label class="form-check-label" for="custbody43"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold"> VENTA CAÍDA</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody103" disabled>
                                      <label class="form-check-label" for="custbody103"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">COMISIÓN APROBADA</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody_ix_comision_pagada"
                                          disabled>
                                      <label class="form-check-label" for="custbody_ix_comision_pagada"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">COMISIÓN CANCELADA</a> </td>
                          </tr>
                  </table>
              </div>
              <!-- *********************** TABLA DE CHEKCS **************************** -->
              <hr style="border: 0.5px solid black;">
              <div class="card-body">
                  <h4 class="card-title mb-4">APROBACIONES ESTADOS</h4>
                  <table class="table align-middle table-nowrap table-check">
                      <thead class="table-light">
                          <tr>
                              <th class="align-middle">VALIDACION</th>
                              <th class="align-middle">TIPO</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbodyid_firma_rc" disabled>
                                      <label class="form-check-label" for="custbodyid_firma_rc"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">APROBACION FORMALIZACIÓN</a>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbodyid_firma_rocca"
                                          disabled>
                                      <label class="form-check-label" for="custbodyid_firma_rocca"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold"> APROBACION RDR</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody74" disabled>
                                      <label class="form-check-label" for="custbody74"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">CÁLCULO COMISIÓN
                                      ASESOR(AUTO)</a> </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody73" disabled>
                                      <label class="form-check-label" for="custbody73"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">CALCULO COMISIÓN
                                      CORREDOR(AUTO)</a> </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <!-- *********************** TABLA DE CHEKCS **************************** -->
              <hr style="border: 0.5px solid black;">
              <div class="card-body">
                  <h4 class="card-title mb-4">APROBACIONES PAGOS</h4>
                  <table class="table align-middle table-nowrap table-check">
                      <thead class="table-light">
                          <tr>
                              <th class="align-middle">VALIDACION</th>
                              <th class="align-middle">TIPO</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody141" disabled>
                                      <label class="form-check-label" for="custbody141"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">NO PAGA TRASPASO S.A</a>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="form-check font-size-16">
                                      <input class="form-check-input" type="checkbox" id="custbody157" disabled>
                                      <label class="form-check-label" for="custbody157"></label>
                                  </div>
                              </td>
                              <td><a href="javascript: void(0);" class="text-body fw-bold">NO PAGA CESIÓN DE
                                      ACCIONES</a> </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <!-- *********************** TABLA DE CHEKCS **************************** -->
              <hr style="border: 0.5px solid black;">
              <div class="card-body">
                  <h5 class="font-size-14"><i class="bx bx-poll me-1 text-primary"></i>COMENTARIO AUTORIZACIÓN DE VENTA
                  </h5>
                  <p id="custbody50"> </p>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xl-12">
              <div class="card" style="border-radius: 24px; background: #ffffff; box-shadow:  22px 22px 44px #8c8c8c,
                         -22px -22px 44px #ffffff;">
                  <div class="card-body">
                      <div class="d-flex">
                          <div class="flex-grow-1 overflow-hidden">
                              <div class="alert alert-danger" role="alert">
                                  <h4 class="text-truncate font-size-15"> ARTICULOS LINEAS</h4>
                              </div>
                          </div>
                      </div>
                      <div class="table-responsive">
                          <table id="datatable-buttons"
                              class="table table-striped table-light dt-responsive nowrap w-100 display ">
                              <thead>
                                  <tr class="text-center">
                                      <th colspan="7">LINEAS</th>
                                  </tr>
                                  <tr>
                                      <th style="text-align: right;" class="align-middle" scope="col">#</th>
                                      <th style="text-align: right;" class="align-middle" scope="col">ARTÍCULO</th>
                                      <th style="text-align: right;" class="align-middle" scope="col">MONTO</th>
                                      <th style="text-align: right;" class="align-middle" scope="col">FECHA DE PAGO
                                          PROYECTADO</th>
                                      <th style="text-align: right;" class="align-middle" scope="col">CANTIDAD</th>
                                      <th style="text-align: right;" class="align-middle" scope="col">DESCRIPCIÓN</th>
                                  </tr>
                              </thead>
                              <tbody id="table-body">
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="card-body">
      <div>
          <?php require_once "Modal_estimacion.php";?>
      </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
  <script src="assets/sweetalert2/sweetalert2.js?rev=<?=time();?>"></script>
  <script src="/js/eventSelect/eventSelect.js?rev=<?=time();?>"></script>
  <script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
  <script src="/js/ordenVenta/extraerDatosOrden.js?rev=<?=time();?>"></script>
  <script src="/js/alertModal/alertModal.js?rev=<?=time();?>"></script>
  <script src="/js/ordenVenta/editarOrden.js?rev=<?=time();?>"></script>
  <script src="/js/ordenVenta/reserva.js?rev=<?=time();?>"></script>
  <script src="/js/ordenVenta/cierre.js?rev=<?=time();?>"></script>
  <script src="/js/ordenVenta/reservaCaida.js?rev=<?=time();?>"></script>



  <!-- <script src="/js/ordenVenta/reservaCaida.js?rev=<?=time();?>"></script>
 
  <script src="/js/ordenVenta/cierre.js?rev=<?=time();?>"></script> -->
  <script>
verOrdenDeVenta()
  </script>
  <link rel="stylesheet" href="/assets/Ondrive/style.css">
  <div class="login">
      <div class="login-text">
          <h3> Integración Netsuite - OneDrive</h3>
          <label> Por favor inicie sesión para continuar.</label>
      </div>
      <div class="login-link">
          <a id="signin" onclick="displayUI(); return false;" href="#">
              <!-- Muestra imagen! -->
              <img src="/assets/Ondrive/ms-image.png" alt="Sign in with Microsoft" />
          </a>
      </div>
  </div>
  <main id="main-container" role="main" class="container" style="display: none; position: absolute;">
      <!-- Segmento de Contenedor Principal -->
      <div id="content">
          <!-- Mensaje de bienvenida al nombre del usuario -->
          <div class="user">
              <!-- Animación de Carga!! -->
              <div class="animation">
                  <div class="load">
                      <div id="uploadMessage" class="begin"> </div>
                  </div>
              </div>
              <div class="informacion">
                  <h3>Usuario: <span id="userName"></span></h3>
              </div>
          </div>
          <hr />
          <!-- Creación de Carpeta -->
          <!-- Div.Folder tuvo modificaciones para Netsuite. -->
          <div class="folder">
              <h3> El registro consultado no posee una carpeta asociada.</h3>
              <button class="folder-btn" onclick="createFolder()"><i class="icon fas fa-folder-plus"></i> Crear
                  Carpeta</button>
          </div>
          <!-- Trabajo de archivos
          *Muestra el área destinada para cargar archivos.
          *Muestra el área destinada para visualizar archivos.
        -->
          <div class="files">
              <!-- Carga de Archivos. -->
              <div class="files-upload">
                  <div class="icon"><i class="fas fa-cloud-upload-alt"></i></div>
                  <header>Arrastre sus archivos aquí</header>
                  <span>o</span>
                  <button id="buscar">Seleccione su archivo.</button>
                  <input type="file" multiple hidden onchange="fileSelected(this)">
              </div>
              <!-- Muestra de Archivos -->
              <div class="files-view">
                  <div class="files-titleDetails">
                      <label class="files-view-label"> Nombre.</label>
                      <label class="files-view-label"> Fecha. </label>
                      <label class="files-view-label"> Tamaño. </label>
                      <label class="files-view-label"> Descargar.</label>
                  </div>
                  <div class="files-details">
                      <ul id="downloadLinks"></ul>
                  </div>
                  <div class="emptyFiles">
                      <div class="icon"><i class="far fa-folder-open"></i></div>
                      <label class="emptyLabel">El registro consultado no posee archivos adjuntos.</label>
                  </div>
              </div>
          </div>
          <div class="accessdenied">
              <div class="icon"><i class="fas fa-exclamation-triangle"></i></div>
              <label class="emptyLabel">El acceso a este recurso no se encuentra habilitado. Favor comunicarse con el
                  administrador del sistema.</label>
              <button class="reload" onclick="reloadContent();"><i class="fas fa-sync"></i> "Inicio" </button>
          </div>
          <div class="recordUnregistered">
              <div class="icon"><i class="fas fa-exclamation-triangle"></i></div>
              <label class="emptyLabel">Este registro no está asociado a OneDrive. Favor comunicarse con
                  soporte.</label>
              <button class="reload" onclick="reloadContent();"><i class="fas fa-arrow-alt-circle-left"></i> "Volver"
              </button>
          </div>
      </div>
  </main>
  <div class="oneDrivePreview" style="display: none;">
      <div class="previewActions">
          <button class="close btnPreview" onclick="returnDisplay();"> <i class="fas fa-arrow-alt-circle-left"></i>
              Volver</button>
          <button class="expand btnPreview" onclick="expandPreview()"> <i class="fas fa-expand-arrows-alt"></i>
              Expandir</button>
      </div>
      <iframe name="filePreviewframe" class="filePreviewframe" src="" height="100%" width="100%">
      </iframe>
  </div>
  <!-- Autenticación de Microsoft! -->
  <script src="https://alcdn.msauth.net/browser/2.1.0/js/msal-browser.min.js"
      integrity="sha384-EmYPwkfj+VVmL1brMS1h6jUztl4QMS8Qq8xlZNgIT/luzg7MAzDVrRa2JxbNmk/e" crossorigin="anonymous">
  </script>
  <!-- Cliente de Microsoft GRAPH -->
  <script src="https://cdn.jsdelivr.net/npm/@microsoft/microsoft-graph-client/lib/graph-js-sdk.js"></script>
  <script src="/assets/Ondrive/script.js"></script>