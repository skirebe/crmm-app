<div class="col-lg-12">
    <div class="card">
        <div class="card-body cardStyleLeads">
            <h4 class="card-title"><strong>MODULO DE LEADS </strong></h4>
            <div class="row">
                <div class="col-xl-6">
                    <div>
                        <a class="btn btn-dark collapsed" data-bs-toggle="collapse" href="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample" data-bs-toggle='tooltip'
                            data-bs-placement='top'
                            title='Para organizar mejor tus leads, te recomendamos utilizar la función de filtrado por rango de fechas. De esta manera podrás visualizar solo aquellos leads que se encuentren dentro del rango seleccionado'>
                            <i class="fas fa-filter"></i> Rango de fechas
                        </a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="mt-4 mt-lg-0">
                        <div class="d-flex flex-wrap gap-2">
                            <a type="button" class="btn mb-2 btn-success waves-effect waves-light"
                                style="font-size: 16px;" data-bs-toggle='tooltip' data-bs-placement='top'
                                title='Ver todos los eventos Completados' href="evento/lista?data=2"> <i
                                    class="bx bx-customize"></i> Eventos Completados</a>
                            <a type="button" class="btn mb-2 btn-warning waves-effect waves-light"
                                style="font-size: 16px;" href="evento/lista?data=3" data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Ver todos los eventos de cita'> <i
                                    class="bx bx-line-chart"></i> Eventos de cita</a>
                            <a type="button" class="btn mb-2 btn-danger waves-effect waves-light"
                                style="font-size: 16px;" href="evento/lista?data=4" data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Ver todos los eventos Cancelados'> <i
                                    class="bx bx-sitemap"></i>
                                Eventos Cancelados </a>
                            <a type="button" class="btn mb-2 btn-info waves-effect waves-light collapsed"
                                style="font-size: 16px;" href="evento/lista?data=5" data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Ver todos los eventos'> <i
                                    class="mdi mdi-face-profile"></i> Todos los eventos &#160;&#160;&#160;&#160;</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Definir las fechas de inicio y fin por defecto o tomarlas de la entrada POST
            $start = isset($_POST['startFilter']) ? $_POST['startFilter'] : date('Y-m-01');
            $end = isset($_POST['endtFilter']) ? $_POST['endtFilter'] : date('Y-m-t');
            // Comprobar si se ha enviado una solicitud POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // Verificar si se dejaron campos vacíos
                if (empty($start) || empty($end)) {
                    echo '<div class="alert alert-danger">Si desea filtrar por rango de fecha debe seleccionar la fecha de inicio y la fecha final y el motivo</div>';
                }
                $show = "show";
            }
            ?>
            <div class="collapse show" id="collapseExample">
                <form class="needs-validation" novalidate="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Inicio</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="startFilter"
                                            id="startFilter" value="<?= $start; ?>" placeholder="FECHA DE INICIO"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Final</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="endtFilter"
                                            id="endtFilter" value="<?= $end; ?>" placeholder="FECHA FINAL"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-dark">Filtrar Busqueda</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.holas {
    display: none;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <h4 class="card-title"><strong>Tabla de eventos</strong></h4>
                <table id="datatable-buttons"
                    class="table table-striped table-light dt-responsive nowrap w-100 display">
                    <thead>
                        <tr>
                            <th style="text-align: right; font-weight: bold;">EVENTO</th>
                            <th style="text-align: right; font-weight: bold;">LEAD</th>
                            <th hidden style="text-align: right; font-weight: bold;">LEAD id</th>
                            <th style="text-align: right; font-weight: bold;">FECHA INICIO</th>
                            <th style="text-align: right; font-weight: bold;">HORA INICIAL</th>
                            <th style="text-align: right; font-weight: bold;">ESTADO</th>
                            <th style="text-align: right; font-weight: bold;">TIPO</th>
                            <th style="text-align: right; font-weight: bold;">CITA</th>
                            <th hidden style="text-align: right; font-weight: bold;">id_evento</th>
                            <th  style="text-align: right; font-weight: bold;">PROYECTO</th>
                            <th  style="text-align: right; font-weight: bold;">CAMPAÑA</th>

                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
                <div id="tableleads">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-12"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/evento/EnlistarEventos.js?rev=<?= time(); ?>"></script>