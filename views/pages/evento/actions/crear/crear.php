<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body" style="box-shadow:  -8px -8px 16px #9a9a9a,8px 8px 16px #ffffff;">
                <div style="text-align: center;">
                    <p class="fw-bolder">CREAR UN EVENTO NUEVO </p>
                </div>
                <br>
                <p class="fw-bolder"><span>ESTE EVENTO SE VERÁ REFLEJADO EN EL CALENDARIO</span></p>
                <a class="btn btn-success collapsed mb-2" href="/calendario">
                    <i class="fas fa-calendar"></i> Calendario
                </a>
                <br>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label class="form-label">NOMBRE DEL EVENTO</label>
                            <div class="input-group">
                                <input autocomplete="off" type="text" class="form-control" maxlength="600"
                                    name="nombre_evento" id="nombre_evento" placeholder="Nombre del evento" />
                                <span class="input-group-text"><i class="mdi mdi-rename-box"></i></span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">TIPO DE EVENTO</label>
                            <div class="input-group">
                                <select class="form-control" data-placeholder="Seleccione un evento" name="tipo_evento"
                                    id="tipo_evento">
                                    <option value="">Seleccione un evento...</option>
                                </select>
                                <span class="input-group-text"><i class="mdi mdi-rename-box"></i></span>
                            </div>
                        </div>
                        <div class="mt-3">
                            <label class="mb-1">DESCRIPCIÓN</label>
                            <p class="text-muted mb-2">
                                Describa este evento.
                            </p>
                            <textarea id="textarea" class="form-control" maxlength="10025" rows="3"
                                placeholder="Descripción"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3 ajax-select mt-3 mt-lg-0">
                            <label>FECHA INICIO </label>
                            <div class="docs-datepicker">
                                <div class="input-group">
                                    <input autocomplete="off" type="date" class="form-control" name="start" id="start">
                                </div>
                                <div class="docs-datepicker-container"></div>
                            </div>
                        </div>
                        <div class="templating-select">
                            <div class="mb-3 ajax-select mt-3 mt-lg-0">
                                <label>FECHA FINAL </label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input autocomplete="off" type="date" class="form-control" name="end" id="end">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <label class="form-label">HORA DE INICIO</label>
                            <div class="input-group" id="timepicker-input-group3">
                                <input autocomplete="off" id="timepicker3" type="text" class="form-control"
                                    data-provide="timepicker">
                                <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                            </div>
                        </div>
                        <br>
                        <div class="mb-3">
                            <label class="form-label">HORA FINAL</label>
                            <div class="input-group" id="timepicker-input-group1">
                                <input autocomplete="off" id="timepicker" type="text" class="form-control"
                                    data-provide="timepicker">
                                <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xl-12">
    <div class="card">
        <div class="card-body" style="box-shadow:  -8px -8px 16px #9a9a9a,8px 8px 16px #ffffff;">
            <h4 class="card-title">LEAD RELACIONADO</h4>
            <p class="card-title-desc">Para poder mostrar un lead debes seleccionar al menos uno de la lista</p>
            <div class="mb-3">
                <div class="mb-3">
                    <label class="ol-form-label col-lg-2">BUSCAR lEADS (Ayuda)</label><br>
                    <label class="form-label calss_exp" id="required_expediente" style="display:none; color: red">Campo
                        obligatorio *</label>
                    <div class="col-lg-10">
                        <select class="form-control select2" name="listaLeads" id="listaLeads"
                            onchange="buscar_datos();" required>
                            <option value="-0">Asignar lead...</option>
                        </select>
                        <div class="valid-feedback">Elemento seleccionado.</div>
                        <div class="invalid-feedback">Debe seleccionar un elemento de la de ayuda lista
                        </div>
                    </div>
                </div>
                <div id="leadA1">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                </div>
                <div class="row" id="contendorlead" style="display: none;">
                    <div class=" col-xl-12">
                        <div class="mt-4">
                            <h5 class="font-size-14 mb-3" id="validarCita"></h5>
                            <div class="d-flex">
                                <input type="number" id="citavalor" value="" disabled readonly hidden>
                                <div class="square-switch">
                                    <input type="checkbox" id="square-switch3" name="cita" switch="bool">
                                    <label for="square-switch3" data-on-label="Si" data-off-label="No"></label>
                                </div>
                                <div class="square-switch" id="cita2">
                                    <input type="checkbox" id="square-switch4" name="segundacita" switch="bool">
                                    <label for="square-switch4" data-on-label="Si" data-off-label="No"></label>
                                </div>
                            </div>
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button fw-medium" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            Abrir / cerrar pestaña
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="mt-4 mt-lg-0">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">NOMBRE DEL LEAD</label>
                                                            <div class="input-group">
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="200" id="nombre"
                                                                    readonly disabled />
                                                                <span class="input-group-text"><i
                                                                        class="mdi mdi-rename-box"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">CORREO DEL LEAD</label>
                                                            <div class="input-group">
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="200" id="correo"
                                                                    readonly disabled />
                                                                <span class="input-group-text"><i
                                                                        class="mdi mdi-rename-box"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">TELEFONO DEL LEAD</label>
                                                            <div class="input-group">
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="100" id="telefono"
                                                                    readonly disabled>
                                                                <span class="input-group-text"><i
                                                                        class="mdi mdi-rename-box"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">ESTADO DEL LEAD</label>
                                                            <div class="input-group">
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="100"
                                                                    id="estado_lead" readonly disabled />
                                                                <span class="input-group-text"><i
                                                                        class="mdi mdi-rename-box"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">ID #NETSUITE DEL LEAD</label>
                                                            <div class="input-group">
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="100" name="id_le"
                                                                    id="id_le" readonly disabled />
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="100" name="id_admin"
                                                                    id="id_admin" hidden readonly disabled />
                                                                <input autocomplete="off" type="text"
                                                                    class="form-control" maxlength="100" name="id_login"
                                                                    id="id_login"
                                                                    value="<?php echo $NetsuiteId_acceso?>" hidden
                                                                    readonly disabled />
                                                                <span class="input-group-text"><i
                                                                        class="mdi mdi-rename-box"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <button onclick="crearEvento()" class="btn btn-dark waves-effect waves-light">Generar
                    Evento</button>
            </div>
        </div>
    </div>
    <script src="/js/evento/validacionCampos.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="/js/evento/CrearNuevoEvento.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/evento/cargarData.js?rev=<?php echo time(); ?>"></script>
    <script src="/js/evento/extraerLeadEvento.js?rev=<?= time(); ?>"></script>