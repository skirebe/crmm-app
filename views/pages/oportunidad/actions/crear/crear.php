<div class="card">
    <div class="card-body"
        style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
        <h4 class="card-title"><strong>Generar una nueva oportunidad</strong>
        </h4>
        <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> Describa este
            reportebrevemente</p>
        <div class="col-xl-6">
            <div class="mt-4 mt-lg-0">
                <blockquote class="blockquote  blockquote-reverse font-size-16 mb-0">
                    <a class="btn btn-secondary  collapsed mb-2" href="javascript: history.go(-1)"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Volver a la pagina anteriror'>
                        <i class="bx bx-arrow-back"></i> Volver
                    </a>
                    <a onclick="ExtraerLeadModal('oportunidad')" class="btn  mb-2 btn-dark collapsed"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                        <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                    </a>
                </blockquote>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-xl-6">
                <h4 class="card-title"><span>Información principal</span></h4>
                <div class="mb-3">
                    <label data-bs-toggle="tooltip" data-bs-placement="right" title="as"
                        data-bs-original-title="Lead Relacionado" aria-describedby="tooltip692605">CLIENTE
                    </label>
                    <input hidden readonly disabled id="estado_lead">
                    <input type="text" class="form-control" maxlength="25" readonly disabled id="nombre">
                    <input type="text" class="form-control" maxlength="25" name="entity" id="entity" readonly hidden>
                </div>
                <div class="mb-3">
                    <label class="form-label">PROBABILIDAD </label>
                    <input type="text" class="form-control" maxlength="25" name="probability" value="80.0%"
                        id="probability" placeholder="" readonly disabled>
                    <input type="text" class="form-control" maxlength="25" name="nombre_valor" value="Firme"
                        id="nombre_valor" placeholder="" readonly disabled>
                </div>
                <div class="mb-3">
                    <label class="form-label">SUBSIDIARIA
                    </label>
                    <input type="text" class="form-control" maxlength="25" id="sub_name" readonly disabled>
                    <input type="text" class="form-control" maxlength="25" name="subsidiary" id="subsidiary" id="sub_id"
                        readonly hidden>
                </div>
                <div class="mb-3">
                    <label class="form-label" data-bs-toggle="tooltip" data-bs-placement="right" title=""
                        data-bs-original-title="Avance o contra entrega">ROYECTO </label>
                    <input type="text" class="form-control" maxlength="25" id="pro" readonly disabled>
                </div>
                <div class="mb-3">
                    <label class="mb-1">DETALLES</label>
                    <p class="text-muted mb-2">
                        Introduzca información adicional sobre la oportunidad.
                    </p>
                    <textarea id="memo" class="form-control" name="memo" maxlength="225" rows="8"
                        placeholder="Detalle"></textarea>
                </div>
            </div>
            <div class="col-xl-6">
                <h4 class="card-title"><span>Información obligatoria</span></h4>
                <div class="mb-3">
                    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
                    </script>
                    <label for="opciones">ESTADO</label>
                    <select class="form-control" name="opciones" id="opciones" onchange="cambioOpciones();">
                        <option value="11">Condicional</option>
                        <option value="22" selected>Firme</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">MOTIVO DE CONDICION</label>
                    <select class="form-control" name="miinput" id="miinput" disabled onchange="divisionChange()"
                        required>
                        <option value=""> Escoger ...</option>
                        <option value="Esperando un negocio"> Esperando un negocio</option>
                        <option value="Viendo opciones">Viendo opciones</option>
                        <option value="Depende la venta de la casa"> Depende la venta de la casa</option>
                        <option value="Definiendo Prima"> Definiendo Prima</option>
                        <option value="Análisis de banco">Análisis de banco</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">CIERRE PREVISTO SEGUN EL ESTADO</label><br>
                    <p style="font-size: 10px;">FECHA ACTUAL A COMPARAR: <span id="fechaActual"></span></p>
                    <div class="docs-datepicker">
                        <div class="input-group">
                            <input type="text" class="form-control docs-date" name="dateFirme" id="dateFirme" readonly
                                style="display:none;" disabled>
                            <input type="text" class="form-control docs-date" name="dateCon" id="dateCon" readonly
                                style="display:none;" disabled>
                            <input type="text" class="form-control docs-date" name="date" id="date" hidden disabled>
                            <button type="button" class="btn btn-secondary docs-datepicker-trigger" disabled>
                                <i class="mdi mdi-calendar" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="docs-datepicker-container"></div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" data-bs-toggle="tooltip" data-bs-placement="right" title=""
                        data-bs-original-title="Motivo de Compra">MOTIVO DE COMPRA </label>
                    <select class="form-control" name="custbody76" id="custbody76" required>
                        <option value="" required>Selecionar</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" data-bs-toggle="tooltip" data-bs-placement="right"
                        data-bs-original-title="Avance o contra entrega">METODO DE PAGO </label>
                    <select class="form-control" name="custbody75" id="custbody75" required>
                        <option value="">Seleccionar</option>
                    </select>
                </div>
                <div class="col">
                    <label class="col-form-label"> <span>UBICACION</span></label>
                    <select class="form-select select2" id="location" name="location">
                        <option selected value="">Seleccione una Ubicacion</option>
                    </select>
                </div>
                <div class="col">
                    <label class="col-form-label"> <span>CLASE</span></label>
                    <select class="form-select select2" id="class" name="class">
                        <option selected value="">Seleccione una Clase</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"
                    style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <h4 class="card-title">UNIDAD EXPEDIENTE LIGADO VTA</h4>
                    <p class="card-title-desc">Este es un campo personalizado creado para su cuenta. Comuníquese con el
                        administrador para obtener detalles.</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="ol-form-label col-lg-2">Buscar Expediente (Ayuda)</label>
                                <div class="col-lg-10">
                                    <select class="form-select select2" name="expediente" id="expediente"
                                        onchange="CargarExpediente();" required>
                                        <option value="0">Escoger Expediente</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label col-lg-2">ID INTERNO EXPEDIENTE</label>
                                <div class="col-lg-10">
                                    <input type="number" name="ID_interno_expediente" id="ID_interno_expediente"
                                        class="form-control" readonly required value="" disabled>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label col-lg-2">ESTADO EXPEDIENTE</label>
                                <div class="col-lg-10">
                                    <input type="text" name="estado_expediente" id="estado_expediente"
                                        class="form-control" readonly required value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 ajax-select mt-3 mt-lg-0">
                                <label class="form-label">NOMBRE DE EXPEDIENTE</label>
                                <input name="Nombre_expediente" id="Nombre_expediente" stype="text" class="form-control"
                                    readonly required disabled>
                            </div>
                            <div class="mb-3 ajax-select mt-3 mt-lg-0">
                                <div class="templating-select">
                                    <label class="form-label">PRECIO DE LISTA</label>
                                    <input name="Precio_de_Venta_Unico_expediente" id="Precio_de_Venta_Unico_expediente"
                                        stype="text" class="form-control" readonly required disabled>
                                </div>
                            </div>
                            <div class="mb-3 ajax-select mt-3 mt-lg-0">
                                <div class="templating-select">
                                    <label class="form-label">PRECIO DE VENTA MÍNIMO</label>
                                    <input name="Precio_de_Venta_Minimo_expediente"
                                        id="Precio_de_Venta_Minimo_expediente" stype="text" class="form-control"
                                        readonly required disabled>
                                </div>
                                <input name="salesrep" id="salesrep" stype="text" class="form-control" readonly required
                                    hidden>
                                <input name="currency" id="currency" value="1" stype="text" class="form-control"
                                    readonly required disabled hidden>
                            </div>
                        </div>
                    </div>
                    <button onclick="crear_oportunidad()" class="btn btn-dark waves-effect waves-light">Crear
                        Oportunidad</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript">
</script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
<script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/cargarData.js?rev=<?= time(); ?>"></script>
<script src="/js/oportuindades/crearOportuindad.js?rev=<?= time(); ?>"></script>