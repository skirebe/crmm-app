<style>
span {
    font-weight: bolder;
}
</style>
<?php $routesArray = $_GET['data']; ?>
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <div class="d-flex flex-wrap gap-2">
                    <button type="button" class="btn btn-info waves-effect mb-2 waves-light" data-bs-toggle="modal"
                        data-bs-target=".bs-example-modal-xl1">Editar Oportunidad</button>
                    <a onclick="ExtraerLeadModal('oportunidad')" class="btn  mb-2 btn-dark collapsed"
                        data-bs-toggle='tooltip' data-bs-placement='top' title='Realizar una accion a este cliente'>
                        <i class="bx bxs-user-detail"></i> Realizar Nueva Accion al cliente
                    </a>
                    <?php
                    include_once "./views/pages/oportunidad/actions/oprt_view/modalEditOpt.php" ?>
                </div>
                <br>
                <br>
                <div class="d-flex">
                    <div class="flex-grow-1 overflow-hidden">
                        <h5 class="font-size-12 mt-2">Colocar esta oportunidad + probable</h5>
                        <div class="square-switch">
                            <input type="checkbox" id="square-switch3" switch="info">
                            <label for="square-switch3" data-on-label="Si" data-off-label="No"></label>
                        </div>
                    </div>
                </div>
                <P id="fech_creacion" class="font-size-12 mt-2"></P>
                <h5 id="fech_modificaion" class="font-size-12 mt-1"></h5>
                <div class="row task-dates ">
                    <div class="col-sm-4 col-6 ">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="mdi mdi-format-list-numbered"></i> ID DE LA OPORTUNIDAD
                                NETSUITE:</h5>
                            <p id="id_oportunidad_div" class="text-muted mb-8 font-size-8" style="font-size:3;"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-users"></i> CLIENTE RELACIONADO</h5>
                            <p id="cliente_div" class=" text-muted mb-0"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-calendar-check"></i> ESTADO</h5>
                            <p id="estado_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row task-dates">
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-clock"></i> TIPO DE PRONÓSTICO :</h5>
                            <p id="tipo_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="bx bx-line-chart"></i> PROBABILIDAD </h5>
                            <p id="probabilidad_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="far fa-window-close"></i> CIERRE PREVISTO </h5>
                            <p id="cierre_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row task-dates">
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="bx bxs-business"></i> SUBSIDIARIA :</h5>
                            <p id="sub_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fab fa-buy-n-large"></i> MOTIVO DE COMPRA </h5>
                            <p id="motivo_div" class="text-muted mb-0"></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-clipboard-list"></i> METODO DE PAGO </h5>
                            <p id="metodo_div_pagp" class="text-muted mb-0"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row task-dates">
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="far fa-calendar"></i> DESDE :</h5>
                            <p id="desde_div" class="text-muted mb-0"><span></span></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="far fa-calendar-check"></i> HASTA </h5>
                            <p id="hasta_div" class="text-muted mb-0"><span></span></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-money-bill-wave"></i> TOTAL PREVISTO </h5>
                            <p id="total_previsto" class=" text-muted mb-0">
                                <span></span></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row task-dates">
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-braille"></i> DETALLE :</h5>
                            <p id="detalle_div" class="text-muted mb-0"><span></span></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="fas fa-braille"></i> MOTIVO DE CONDICION :</h5>
                            <p id="motivo_condicion" class="text-muted mb-0"><span></span></p>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card cardStyleLeads">
            <div class="card-body">
                <a data-bs-toggle='tooltip' data-bs-placement='top' title='Ver expediente de unidad' style="float:left;"
                    class="" id="link_port">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <i style="color:#000" class="bx bx-show fa-2x"></i>
                    </svg>
                    <br>
                </a>
                <br>
                <br>
                <h4 class="card-title mb-4"> EXPEDEINTE DE UNIDAD </h4>
                <div class="container">
                    <div>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>EXPEDIENTE ID:<span id="id_ex"></span></label>
                        </p>
                    </div>
                    <div>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>PLANOS DE LA UNIDAD: <a style="font-size:5"
                                    id="planos" target="_blank"></span>Ver Planos</a></label></p>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>PRECIO DE VENTA:<span
                                    id="precio_venta"></span></label></p>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>PRECIO DE VENTA UNICO:<span
                                    id="precio_unico"></span></label></p>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>PRECIO DE VENTA MINIMO:<span
                                    id="Precio_de_Venta_Minimo"></span></label></p>
                        <p class=" "><i class="mdi mdi-wallet"></i><label>FECHA DE ENTREGA:<span
                                    id="entrega"></span></label></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body cardStyleLeads">
                    <h4 class="card-title mb-4"> <a data-bs-placement='right'
                            title='Botón para crear una nueva estimación para esta oportunidad' class=""
                            data-bs-toggle="modal" data-bs-target=".bs-example-modal-xl">
                            <svg width="27" height="26" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.0002 7.33398V16.6673M16.6668 12.0007L7.3335 12.0007" stroke="#475569"
                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                <rect x="1" y="1" width="22" height="22" rx="13" stroke="#475569" stroke-width="2">
                                </rect>
                            </svg>
                        </a> &nbsp; ESTIMACIONES </h4>
                    <table id="datatable-buttons"
                        class="table table-striped table-light dt-responsive nowrap w-100 display ">
                        <thead>
                            <tr>
                                <th style="text-align: right;" class="align-middle">#Estimacion</th>
                                <th style="text-align: right;" class="align-middle">Caduca</th>
                                <th style="text-align: right;" class="align-middle">Creado</th>
                                <th style="text-align: right;" class="align-middle">Accion</th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div>
            <?php require_once "Modal_estimacion.php";  ?>
        </div>
    </div>
</div>
<script src="/js/eventSelect/eventSelect.js?rev=<?= time(); ?>"></script>
<script src="/js/leads/ExtraerLeadModal.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/extraerDatos.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/estadoOpt.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/funcionesEstimacion.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/crear_estimacion.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/tableOptEstimacion.js?rev=<?php echo time(); ?>"></script>
<script src="/js/oportuindades/editaOportunidad.js?rev=<?php echo time(); ?>"></script>