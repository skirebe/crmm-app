    <div class="modal fade bs-example-modal-xl1" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel1"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel1">Editar Oportuindad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="card">
                                <h4 class="card-title"><span>Información principal</span></h4>
                                <div class="mb-3">
                                    <label class="form-label">PROBABILIDAD </label>
                                    <input type="text" class="form-control" maxlength="25" name="probability"
                                        value="80.0%" id="probability" placeholder="" readonly disabled>
                                    <input type="text" class="form-control" maxlength="25" name="nombre_valor"
                                        value="Firme" id="nombre_valor" placeholder="" readonly disabled>
                                </div>
                                <div class="mb-3">
                                    <label class="mb-1">DETALLES</label>
                                    <p class="text-muted mb-2">
                                        Introduzca información adicional sobre la oportunidad.
                                    </p>
                                    <textarea id="memo" class="form-control" name="memo" maxlength="225" rows="3"
                                        placeholder="Detalle"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card">
                                <h4 class="card-title"><span>Información obligatoria</span></h4>
                                <div class="mb-3">
                                    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                                        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                                        crossorigin="anonymous"></script>
                                    <label>ESTADO</label>
                                    <select class=" form-control" name="opciones" id='opciones'
                                        onchange='cambioOpciones();' style="background: #2f3640; color: #f5f6fa;">
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">MOTIVO DE CONDICION</label>
                                    <select class="form-control" name="miinput" id="miinput" disabled
                                        onchange="divisionChange()" required>
                                        <option value=""> Escoger ...</option>
                                        <option value="Esperando un negocio">Esperando un negocio</option>
                                        <option value="Esperando prima">Esperando prima</option>
                                        <option value="Depende la venta de la casa"> Depende la venta de la casa
                                        </option>
                                        <option value="Definiendo Prima"> Definiendo Prima</option>
                                        <option value="Análisis de banco">Análisis de banco</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <?php
                                    date_default_timezone_set("America/Costa_Rica");
                                    $fechaActual = date('Y-m-d');
                                    ?>
                                    <label class="form-label">CIERRE PREVISTO SEGUN EL ESTADO </label><br>
                                    <div class="square-switch">
                                        <input type="checkbox" id="square-switch1" switch="none">
                                        <label for="square-switch1" data-on-label="Si" data-off-label="No"></label>
                                    </div>
                                    <p id="p_date" style="font-size:10px;"> FECHA ACTUAL A COMPARAR: </p>
                                    <div class="docs-datepicker">
                                        <label class="form-label">NUEVO VALOR A ASIGNAR </label><br>
                                        <div class="input-group">
                                            <input type="date" class="form-control" name="dateFirme" id="dateFirme"
                                                disabled>
                                            <input type="date" class="form-control " name="dateCon" id="dateCon"
                                                disabled>
                                            <input type="text" class="form-control" name="date" id="date" hidden
                                                disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" data-bs-toggle="tooltip" data-bs-placement="right"
                                        title="" data-bs-original-title="Motivo de Compra">MOTIVO DE COMPRA </label>
                                    <select class="form-control" name="custbody76_opt" id="custbody76_opt" required>
                                        <option value="" required>Selecionar</option>'
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" data-bs-toggle="tooltip" data-bs-placement="right"
                                        title="" data-bs-original-title="Avance o contra entrega">METODO DE PAGO
                                    </label>
                                    <select class="form-control" name="custbody75_opt" id="custbody75_opt" required>
                                        <option value="" required>Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-dark waves-effect waves-light" onclick="editaOportunidad()">Editar
                            Oportunidad</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
$(document).ready(function() {

    $('#opciones').change(function(e) {
        var selectedValue = $(this).val();

        // Mostrar u ocultar elementos basados en la opción seleccionada
        $('#dateCon, #lavel').toggle(selectedValue === "11");
        $('#miinput').prop("disabled", selectedValue !== "11");

        if (selectedValue === "11") {
            setTimeout(function() {
                $(".calss").fadeOut(10500);
            }, 3000);
        } else {
            $('#miinput').val("");
        }

        $('#dateFirme').toggle(selectedValue === "22");
    });

    // Mostrar el elemento 'dateFirme' al cargar la página si la opción seleccionada es "22"
    if ($('#opciones').val() === "22") {
        $('#dateFirme').show();
    }
});

$('#square-switch1').click(function() {
    var isChecked = $(this).is(':checked');
    $('#dateFirme, #dateCon').prop('disabled', !isChecked);
});
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>