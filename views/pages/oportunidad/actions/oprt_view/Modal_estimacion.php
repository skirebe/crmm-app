   <!--  Extra Large modal example -->
   <div class="modal fade bs-example-modal-xl cardStyleLeads" tabindex="-1" role="dialog"
       aria-labelledby="myExtraLargeModalLabel2" aria-hidden="true" id="ticket">
       <div class="modal-dialog modal-lg" style="max-width: 90%; max-height:80%">
           <div class="modal-content">
               <div class="modal-header">
                   <h5 class="modal-title" id="myExtraLargeModalLabel2"><em id="id_modal"></em></h5>
                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
               </div>
               <div style=" background-color: #F9F9F9;" class="modal-body">
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"> <span> ENTREGA FECHA: </span> </label>
                           <input autocomplete="off" type="text" class="form-control" readonly id="custbody114"
                               name="custbody114">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> CLIENTE: </span> </label>
                           <input autocomplete="off" type="text" class="form-control unputsEst" id="entityname_oport"
                               readonly disabled>
                           <input autocomplete="off" type="text" class="form-control" readonly id="entity" name="entity"
                               hidden>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> UNI. EXPED. LIGADO VTA:</span></label>
                           <input autocomplete="off" type="text" class="form-control unputsEst"
                               id="Nombre_de_Expediente_de_Unidad" readonly disabled>
                           <input autocomplete="off" type="text" class="form-control" readonly hidden id="custbody38"
                               name="custbody38" disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> PRYECTO:</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="proyecto_lead_est" readonly
                               disabled>
                       </div>
                       <div class="col" hidden>
                           <label class="col-form-label"> <span> SUBSIDIARIA:</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="subsidiaria_oport" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="subsidiary" name="subsidiary"
                               readonly hidden>
                       </div>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"> <span> ESTADO </span>:</label>
                           <input autocomplete="off" type="text" class="form-control" id="entitystatus" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="entitystatus_id" readonly
                               hidden>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> OPORTUNIDAD*</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="tranid_oport" readonly
                               disabled>
                           <input autocomplete="off" type="text" class="form-control" id="opportunity"
                               name="opportunity" hidden readonly disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> CIERRE DE PREVISTO</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="expectedclosedate"
                               name="expectedclosedate" readonly disabled>
                       </div>
                       <div class="col">
                           <br>
                           <br>
                           <div class="form-check">
                               <input autocomplete="off" class="form-check-input" type="checkbox" id="PRERESERVA"
                                   onchange="toggleDiv();">
                               <label class="form-check-label"><span> PRE - RESERVA</span> </label>
                           </div>
                       </div>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"><span> PRECIO DE LISTA:</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="custbody13" readonly disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>MONTO DESCUENTO DIRECTO</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="custbody132"
                               pattern="[-]" onKeyUp="calcular(this);">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>EXTRAS PAGADAS POR EL CLIENTE </span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="custbody46"
                               name="custbody46" onKeyUp="calcular(this);">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>DESCRIPCIÓN EXTRAS</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="custbody47"
                               value="" name="custbody47">
                       </div>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"> <span>CASHBACK</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control estiloCalidad"
                               id="custbodyix_salesorder_cashback" name="custbodyix_salesorder_cashback" value="0.00"
                               onKeyUp="calcular(this);">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>MONTO RESERVA </span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control estiloCalidad"
                               id="custbody52" name="custbody52" onKeyUp="calcular(this);">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>MONTO TOTAL DE CORTESÍAS</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="custbody16"
                               name="custbody16" onKeyUp="calcular(this);">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>DESCRIPCIÓN DE LAS CORTESIAS</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="custbody35"
                               value="" name="custbody35">
                       </div>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"> <span> PREC. DE VENTA MÍNIMO:</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="custbody18" name="custbody18"
                               readonly disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> FECHA RESERVA: </span></label>
                           <input autocomplete="off" type="date" class="form-control " id="fech_reserva"
                               name="fech_reserva">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> PEC. DE VENTA NETO: </span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control" id="pvneto"
                               name="pvneto" readonly disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> MONTO TOTAL</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control"
                               id="custbody_ix_total_amount" name="custbody_ix_total_amount" readonly disabled>
                       </div>
                       <div class="col" hidden>
                           <label class="col-form-label"> <span> EXTRAS SOBRE EL PRECIO DE LISTA</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" id="diferecia"
                               name="diferecia" value="0.00" onKeyUp="calcular(this);" readonly disabled>
                       </div>
                   </div>
                   <hr>
                   <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4" id="campos_prereserva">
                       <div class="col">
                           <label class="col-form-label" id="pre_1" aria-label="">MONTO PRERESERVA</span></label>
                           <input autocomplete="off" type="text" class="form-control estiloCalidad" value="500"
                               placeholder="0.00" id="custbody191" aria-label="" required>
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="pre_4"> <span>COMPROBANTE PRERESERVA</span></label>
                           <input autocomplete="off" type="text" value="0" class="form-control estiloCalidad" id="pre_3"
                               required>
                       </div>
                       <?php
                        date_default_timezone_set("America/Costa_Rica");
                        $fechaActual_in = date('Y-m-d');
                        ?>
                       <div class="col">
                           <label class="col-form-label" id="pre_6"> <span>FECHA DE PRERESERVA</span></label>
                           <input autocomplete="off" type="date" value="<?= $fechaActual_in ?>"
                               class="form-control estiloCalidad" id="pre_5" required>
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="pre_7"> <span>OBSERVACIONES PRERESERVA</span></label>
                           <input autocomplete="off" type="text" value="-" class="form-control estiloCalidad" id="pre_8"
                               required>
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="pre_10"> <span>METODO DE
                                   PAGO</span></label>
                           <select class="form-select" id="custbody188" name="custbody188">
                               <option value="">Selecione un Metodo de pago</option>
                               <option value="3">Cheque</option>
                               <option value="2">Efectivo</option>
                               <option value="4">Paypal</option>
                               <option value="5">Tarjetan de Credito</option>
                               <option value="6">Tarjeta de Débito</option>
                               <option selected value="1">Transferencia</option>
                           </select>
                       </div>
                   </div>
                   <br>
                   <br>
                   <div class="alert alert-secondary" role="alert">
                       <span> CONDICIONES DE LA PRIMA <br></span>
                       <p style="font-size: 0.875em"><em> <u> SELECCIONÉ COMO VA A CALCULAR LA PRIMA</u></em></p>
                       </label>
                       <div class="row row-cols-2 row-cols-lg-6">
                           <div class="col">
                               <div class="form-check">
                                   <input id="isDiscounted" class="form-check-input" name="isDiscounted" type="radio"
                                       value=0 th:field="*{discounted}" autocomplete="off">
                                   <label class="form-check-label"><span> PORCENTAJE </span></label>
                               </div>
                               <div class="form-check">
                                   <input id="isNotDiscounted" class="form-check-input" name="isDiscounted" type="radio"
                                       value=1 th:field="*{discounted}">
                                   <label class="form-check-label"><span> MONTO</span></label>
                               </div>
                           </div>
                       </div>
                   </div>
                   <style>
                   .estilo {
                       border-radius: 50px;
                       background: #d1d1d1;
                       box-shadow: 20px 20px 60px #b2b2b2, -20px -20px 60px #f0f0f0
                   }

                   .estiloCalidad {
                       border-radius: 26px;

                   }
                   </style>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <label class="col-form-label"> <span>PRIMA TOTAL</span></label>
                           <input type="text" class="form-control estiloCalidad" id="custbody39"
                               onKeyUp="calcular(this);" th:field="*{custbody39}" disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>PRIMA%</span></label>
                           <input type="text" class="form-control estiloCalidad" id="custbody60"
                               onKeyUp="calcular(this);" th:field="*{custbody60}" disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span>MONTO PRIMA NETA</span></label>
                           <input autocomplete="off" type="text" class="form-control "
                               id="custbody_ix_salesorder_monto_prima" disabled>
                       </div>
                       <div hidden class="col">
                           <label class="col-form-label"> <span>TRACTO</span></label>
                           <input autocomplete="off" type="text" class="form-control " id="custbody40" name="custbody40"
                               readonly>
                       </div>
                   </div>
                   <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
                       <div class="col">
                           <label class="col-form-label"> <span>MONTO ASIGNABLE PRIMA NETA:</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="neta"
                               name="neta" readonly disabled>
                       </div>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-4">
                       <div class="col">
                           <br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value="" name="chec_fra"
                               id="chec_fra" onchange="toggleDivF(1, 'chec_fra', 'fraccionado', 'f_0');">
                           <label class="form-check-label"> <span> PRIMA FRACCIONADA </span></label><br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value="" name="chec_uica"
                               id="chec_uica" onchange="toggleDivF(2, 'chec_uica', 'unico', 'u_0');">
                           <label class="form-check-label"> <span> PRIMA ÚNICA </span></label>
                           <br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value="" name="chec_extra"
                               id="chec_extra" onchange="toggleDivF(3, 'chec_extra', 'extra', 'o_0');">
                           <label class="form-check-label"> <span> PRIMA EXTRA-ORDINARIA </span></label>
                       </div>
                       <div class="col">
                           <br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value=""
                               name="chec_extra_uno" id="chec_extra_uno"
                               onchange="toggleDivF(3, 'chec_extra_uno', 'ex_uno', 'o_Uno');">
                           <label class="form-check-label"> <span> PRIMA EXTRA 1 </span></label><br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value=""
                               name="chec_extra_dos" id="chec_extra_dos"
                               onchange="toggleDivF(3, 'chec_extra_dos', 'ex_dos', 'o_dos');">
                           <label class="form-check-label"> <span> PRIMA EXTRA 2 </span></label>
                           <br>
                           <input autocomplete="off" class="form-check-input" type="checkbox" value=""
                               name="chec_extra_tres" id="chec_extra_tres"
                               onchange="toggleDivF(3, 'chec_extra_tres', 'ex_tres', 'o_tres');" <label
                               class="form-check-label"> <span> PRIMA EXTRA 3 </span></label>
                       </div>
                   </div>
                   <hr>
                   <div class="alert alert-info " role="alert" id="f_0" aria-label="">
                       <center> PRIMA FRACCIONADA </center>
                   </div>
                   <div class="row row-cols-2 row-cols-lg-12" id="fraccionado">
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>MONTO FRACCIONADO</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="custbody179"
                               name="custbody179" aria-label="" onKeyUp="calcular(this);" value="0.00">
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="f_4" aria-label=""> <span>TRACTOS
                                   FRACCIONADO</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody180"
                               name="custbody180" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody179_date"
                               name="custbody179_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="f_6" aria-label=""> <span>DESCRIPCIÓN
                                   FRACCIONADO:</span></label>
                           <textarea cols="50" rows="2" class="form-control " id="custbody193" name="custbody193"
                               aria-label="">PAGO DE PRIMA FRACCIONADO </textarea>
                       </div>
                   </div>
                   <hr>
                   <div class="alert alert-secondary " role="alert" id="u_0" aria-label="">
                       <center> PRIMA ÚNICA</center>
                   </div>
                   <!-- empieza la vista de los articulos -->
                   <div class="row row-cols-2 row-cols-lg-12" id="unico">
                       <div class="col">
                           <label class="col-form-label" id="u_2" aria-label=""> <span>MONTO ÚNICO</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="custbody181"
                               name="custbody181" onKeyUp="calcular(this);" value="0.00" aria-label="">
                       </div>
                       <div class="col">
                           <label f class="col-form-label" id="u_4" aria-label=""> <span>TRACTO ÚNICO</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody182"
                               name="custbody182" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody182_date"
                               name="custbody182_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="u_6" aria-label=""> <span>DESCRIPCIÓN ÚNICA</span></label>
                           <textarea cols="50" rows="2" class="form-control " id="custbody194" aria-label="">PAGO DE
                               ÚNICO </textarea>
                       </div>
                   </div>
                   <hr>
                   <div class="alert alert-warning " role="alert" id="o_0" aria-label="">
                       <center> PRIMA EXTRA-ORDINARIA</center>
                   </div>
                   <!-- empieza la vista de los articulos -->
                   <div class="row row-cols-2 row-cols-lg-12" id="extra">
                       <div class="col">
                           <label class="col-form-label" id="o_2" aria-label=""> <span>MONTO
                                   EXTRA-ORDINARIA</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="custbody183"
                               name="custbody183" aria-label="" onKeyUp="calcular(this);" value="0.00">
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="o_4" aria-label=""> <span>TRACTO
                                   EXTRA-ORDINARIA</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody184"
                               name="custbody184" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody184_date"
                               name="custbody184_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="o_6" aria-label=""> <span>DESCRIPCIÓN
                                   EXTRA-ORDINARIA</span></label>
                           <textarea cols="50" rows="2" class="form-control" id="custbody195" aria-label="">PAGO DE
                               EXTRA-ORDINARIA </textarea>
                       </div>
                   </div>
                   <br>
                   <div class="alert alert-warning " role="alert" id="o_Uno" aria-label="">
                       <center> PRIMA EXTRA +1</center>
                   </div>
                   <!-- empieza la vista de los articulos -->
                   <div class="row row-cols-2 row-cols-lg-12" id="ex_uno">
                       <div class="col">
                           <label class="col-form-label" id="o_2_uno" aria-label=""> <span>MONTO EXTRA +1</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="o_2_uno_input"
                               name="o_2_uno_input" aria-label="" onKeyUp="calcular(this);" value="0.00">
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="o_4_uno" aria-label=""> <span>TRACTO EXTRA
                                   +1</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody184_uno"
                               name="custbody184_uno" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody184_uno_date"
                               name="custbody184_uno_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="o_6_uno" aria-label=""> <span>DESCRIPCIÓN EXTRA
                                   +1</span></label>
                           <textarea cols="50" rows="2" class="form-control" id="custbody195_uno" aria-label="">PAGO DE
                               PRIMA EXTRA</textarea>
                       </div>
                   </div>
                   <br>
                   <div class="alert alert-warning " role="alert" id="o_dos" aria-label="">
                       <center> PRIMA EXTRA- +2</center>
                   </div>
                   <!-- empieza la vista de los articulos -->
                   <div class="row row-cols-2 row-cols-lg-12" id="ex_dos">
                       <div class="col">
                           <label class="col-form-label" id="o_2_dos" aria-label=""> <span>MONTO EXTRA
                                   +2</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="o_2_dos_input"
                               name="o_2_dos_input" aria-label="" onKeyUp="calcular(this);" value="0.00">
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="o_4_dos" aria-label=""> <span>TRACTO EXTRA
                                   +2</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody184_dos"
                               name="custbody184" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody184_dos_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="o_6_dos" aria-label=""> <span>DESCRIPCIÓN EXTRA
                                   +2</span></label>
                           <textarea cols="50" rows="2" class="form-control" id="custbody195_dos" aria-label="">PAGO DE
                               PRIMA EXTRA</textarea>
                       </div>
                   </div>
                   <br>
                   <div class="alert alert-warning " role="alert" id="o_tres" aria-label="">
                       <center> PRIMA EXTRA- +3</center>
                   </div>
                   <!-- empieza la vista de los articulos -->
                   <div class="row row-cols-2 row-cols-lg-12" id="ex_tres">
                       <div class="col">
                           <label class="col-form-label" id="o_2_tres" aria-label=""> <span>MONTO EXTRA
                                   +3</span></label>
                           <input autocomplete="off" type="text" pattern="[.]" class="form-control " id="o_2_tres_input"
                               name="o_2_tres_input" aria-label="" onKeyUp="calcular(this);" value="0.00">
                       </div>
                       <div class="col">
                           <label class="col-form-label" id="o_4_tres" aria-label=""> <span>TRACTO EXTRA
                                   +3</span></label>
                           <input autocomplete="off" type="text" class="form-control " value="1" id="custbody184_tres"
                               name="custbody184" onKeyUp="calcular(this);" aria-label="">
                       </div>
                       <div class="col">
                           <label class="col-form-label" aria-label=""> <span>FECHA :</span></label>
                           <input autocomplete="off" type="date" class="form-control " id="custbody184_tres_date">
                       </div>
                       <div class="col-md-12">
                           <label class="col-form-label" id="o_6_tres" aria-label=""> <span>DESCRIPCIÓN EXTRA
                                   +3</span></label>
                           <textarea cols="50" rows="2" class="form-control" id="custbody195_tres" aria-label="">PAGO DE
                               PRIMA EXTRA</textarea>
                       </div>
                   </div>
                   <div class="alert alert-secondary" role="alert">
                       <div class="col">
                           <span> METODO DE PAGO <br></span>
                           <center>
                               <select class="form-control" name="custbody75_estimacion" id="custbody75_estimacion"
                                   required style="background: #2f3640; color: #f5f6fa;" onclick="pagoOnChange(this)"
                                   onchange="pagoOnChange(this)">
                               </select>
                       </div>
                       </center>
                       <p style="font-size: 0.875em"><em> <u> SELECCIONÉ COMO VA A CALCULAR EL METODO DE PAGO</u></em>
                       </p>
                   </div>
                   <h5 style="display:none;" id="text_avance"><span><em> Avance Diferenciado</em></span></h5><br>
                   <div class="row row-cols-2 row-cols-lg-4" id="avance_diferenciado" style="display:none;">
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_1"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito11', 'avance_diferenciado_hito1', 'avance_diferenciado_hito1_date')">
                           <label class="col-form-label"> <span> HITO 1 </span> </label>
                           <input autocomplete="off" type="number" class="form-control" disabled
                               id="avance_diferenciado_hito11" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" disabled
                               id="avance_diferenciado_hito1" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control"
                               id="avance_diferenciado_hito1_date" disabled>
                       </div>
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_2"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito12', 'avance_diferenciado_hito2', 'avance_diferenciado_hito2_date')">
                           <label class="col-form-label"> <span> HITO 2</span></label>
                           <input autocomplete="off" type="number" class="form-control" disabled
                               id="avance_diferenciado_hito12" name="avance_diferenciado_hito12" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" id="avance_diferenciado_hito2"
                               disabled name="avance_diferenciado_hito2" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control "
                               id="avance_diferenciado_hito2_date" disabled>
                       </div>
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_3"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito13', 'avance_diferenciado_hito3', 'avance_diferenciado_hito3_date')">
                           <label class="col-form-label"> <span> HITO 3</span></label>
                           <input autocomplete="off" type="number" class="form-control" disabled
                               id="avance_diferenciado_hito13" name="avance_diferenciado_hito13" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" disabled
                               id="avance_diferenciado_hito3" name="avance_diferenciado_hito3" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control"
                               id="avance_diferenciado_hito3_date" disabled>
                       </div>
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_4"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito14', 'avance_diferenciado_hito4', 'avance_diferenciado_hito4_date')">
                           <label class="col-form-label"> <span> HITO 4</span></label>
                           <input autocomplete="off" type="number" class="form-control" id="avance_diferenciado_hito14"
                               disabled name="avance_diferenciado_hito14" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" disabled
                               id="avance_diferenciado_hito4" name="avance_diferenciado_hito4" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control "
                               id="avance_diferenciado_hito4_date" disabled>
                       </div>
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_5"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito15', 'avance_diferenciado_hito5', 'avance_diferenciado_hito5_date')">
                           <label class="col-form-label"> <span> HITO 5</span></label>
                           <input autocomplete="off" type="number" class="form-control" id="avance_diferenciado_hito15"
                               disabled name="avance_diferenciado_hito15" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" disabled
                               id="avance_diferenciado_hito5" name="avance_diferenciado_hito5" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control"
                               id="avance_diferenciado_hito5_date" disabled>
                       </div>
                       <div class="col">
                           <input style="width:15px; height:30px;" type="checkbox" id="hito_6"
                               onchange="habilitarCampos(this, 'avance_diferenciado_hito16', 'avance_diferenciado_hito6', 'avance_diferenciado_hito6_date')">
                           <label class="col-form-label"> <span> HITO 6</span></label>
                           <input autocomplete="off" type="number" class="form-control" id="avance_diferenciado_hito16"
                               disabled name="avance_diferenciado_hito16" value="0.00" onKeyUp="calcular(this);">
                           <input autocomplete="off" type="text" class="form-control" disabled
                               id="avance_diferenciado_hito6" name="avance_diferenciado_hito6" value="0.00"
                               onKeyUp="calcular(this);">
                           <input autocomplete="off" type="date" class="form-control "
                               id="avance_diferenciado_hito6_date" disabled>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> MONTO SIN PRIMA TOTAL</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avance_diferenciado_hito17"
                               name="avance_diferenciado_hito17" value="0.00" readonly>
                           <label class="col-form-label"> <span> PENDIENTE POR ASIGNAR</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="valortotal" name="valortotal"
                               value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> PORCENTAJE YA ASIGNADO</span></label>
                           <input autocomplete="off" type="number" class="form-control" id="total_porcentaje"
                               value="0.1" readonly>
                       </div>
                   </div>
                   <h5 style="display:none;" id="text_obra"><span><em> Avance De Obra</em></span></h5><br>
                   <div class="row row-cols-2 row-cols-lg-4" id="avance_obra" style="display:none;">
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 1 </span> </label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito11"
                               name="avnace_obra_hito11" value="0.15%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito1"
                               name="avnace_obra_hito1" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 2</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito12"
                               name="avnace_obra_hito12" value="0.25%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito2"
                               name="expectedclosedate" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 3</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito13"
                               name="avnace_obra_hito13" value="0.25%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito3"
                               name="avnace_obra_hito3" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 4</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito14"
                               name="avnace_obra_hito14" value="0.15%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito4"
                               name="avnace_obra_hito4" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 5</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito15"
                               name="avnace_obra_hito15" value="0.15%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito5"
                               name="avnace_obra_hito5" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 6</span></label>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito16"
                               name="avnace_obra_hito16" value="0.05%" readonly>
                           <input autocomplete="off" type="text" class="form-control" id="avnace_obra_hito6"
                               name="avnace_obra_hito6" value="0.00" readonly>
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> MONTO SIN PRIMA TOTAL</span></label>
                           <input type="text" class="form-control" id="obra_enterega" value="0.00" readonly>
                       </div>
                   </div>
                   <h5 style="display:none;" id="text_contra"><span><em> Contra Entrega</em></span></h5><br>
                   <div class="row row-cols-2 row-cols-lg-4" id="contra_entrega" style="display:none;">
                       <div class="col">
                           <label class="col-form-label"> <span> HITO 6 % </span> </label>
                           <input autocomplete="off" type="text" class="form-control" id="contra_enterega11"
                               name="contra_enterega11" value="100%" readonly>
                           <input type="text" class="form-control" id="contra_enterega1" name="contra_enterega1"
                               value="0.00" readonly>
                           <input autocomplete="off" type="date" class="form-control" id="contra_enterega11_date">
                       </div>
                       <div class="col">
                           <label class="col-form-label"> <span> MONTO SIN PRIMA TOTAL</span> </label>
                           <input type="text" class="form-control" id="mspt_contra_entrega" name="mspt_contra_entrega"
                               value="0.00" readonly>
                       </div>
                   </div>
               </div>
               <button class="btn btn-dark waves-effect waves-light" onclick="crear_estimacion()">Generar
                   Estimacion</button>
               <br>
               <br>
           </div>
       </div>
   </div>