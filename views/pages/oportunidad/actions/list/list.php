<div class="row">
    <div class="col-lg-4">
        <div class="card mini-stats-wid">
            <div class="card-body" style="box-shadow:  -8px -8px 16px #9a9a9a,8px 8px 16px #ffffff;">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">CANTIDAD DE OPORTUNIDADES</p>
                        <h4 class="mb-0" id="catidads"></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-center">
                        <div data-colors='["--bs-success", "--bs-transparent"]' dir="ltr"
                            id="eathereum_sparkline_charts"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card mini-stats-wid">
            <div class="card-body" style="   box-shadow:  -8px -8px 16px #9a9a9a,
                    8px 8px 16px #ffffff;">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">CANTIDAD FIRME</p>
                        <h4 class="mb-0" id="firme"></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-center">
                        <div data-colors='["--bs-success", "--bs-transparent"]' dir="ltr" id="new_application_charts">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card mini-stats-wid">
            <div class="card-body" style="  box-shadow:  -8px -8px 16px #9a9a9a,
                    8px 8px 16px #ffffff;">
                <div class="d-flex">
                    <div class="flex-grow-1">
                        <p class="text-muted fw-medium">CANTIDAD CONDICIONAL</p>
                        <h4 class="mb-0" id="condiconal"></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-center">
                        <div data-colors='["--bs-success", "--bs-transparent"]' dir="ltr" id="total_approved_charts">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="col-lg-12">
    <div class="card">
        <div class="card-body cardStyleLeads">
            <h4 class="card-title"><strong>MODULO DE LEADS </strong></h4>
            <div class="row">
                <div class="col-xl-6">
                    <div>
                        <a class="btn btn-dark collapsed" data-bs-toggle="collapse" href="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample" data-bs-toggle='tooltip'
                            data-bs-placement='top'
                            title='Para organizar mejor tus leads, te recomendamos utilizar la función de filtrado por rango de fechas. De esta manera podrás visualizar solo aquellos leads que se encuentren dentro del rango seleccionado'>
                            <i class="fas fa-filter"></i> Rango de fechas
                        </a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="mt-4 mt-lg-0">
                        <div class="d-flex flex-wrap gap-2">
                            <a id="oportInactivas" type="button" class="btn mb-2 btn-secondary waves-effect waves-light"
                                style="font-size: 16px; background-color: #641E16;" data-bs-toggle='tooltip'
                                data-bs-placement='top' title='Ver todas las oportuindades inactivas'> <i
                                    class="bx bx-customize"></i> Oportuindades
                                Inactivas</a>
                            <a id="oportTotales" type="button" class="btn mb-2 btn-warning waves-effect waves-light"
                                style="font-size: 16px;" data-bs-toggle='tooltip' data-bs-placement='top'
                                title='Ver el total de Oportuindades'>
                                <i class="bx bx-line-chart"></i> Total Oportuindades</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Definir las fechas de inicio y fin por defecto o tomarlas de la entrada POST
            $start = isset($_POST['startFilter']) ? $_POST['startFilter'] :'' ;
            $end = isset($_POST['endtFilter']) ? $_POST['endtFilter'] : '';

            // Inicializar las variables de control de radio buttons
            $chek = "checked";
            $chek2 = "";

            // Comprobar si se ha enviado una solicitud POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // Obtener el valor del radio button seleccionado o establecer un valor predeterminado vacío
                $radio = $_POST['radio'] ?? '';

                // Verificar si se dejaron campos vacíos
                if (empty($start) || empty($end) || empty($radio)) {
                    echo '<div class="alert alert-danger">Si desea filtrar por rango de fecha debe seleccionar la fecha de inicio y la fecha final y el motivo</div>';
                }

                // Establecer las variables de control de radio buttons según la selección
                $chek = ($radio == 1) ? "checked" : "";
                $chek2 = ($radio == 2) ? "checked" : "";

                // Mostrar una sección adicional (puede requerir más código para esta sección)
                $show = "show";
            }
            ?>
            <div class="collapse show" id="collapseExample">
                <form class="needs-validation" novalidate="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Inicio</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="startFilter"
                                            id="startFilter" value="<?= $start; ?>" placeholder="FECHA DE INICIO"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Fecha Final</label>
                                <div class="docs-datepicker">
                                    <div class="input-group">
                                        <input type="text" readonly class="form-control docs-date" name="endtFilter"
                                            id="endtFilter" value="<?= $end; ?>" placeholder="FECHA FINAL"
                                            autocomplete="off">
                                    </div>
                                    <div class="docs-datepicker-container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="text-muted">MOTIVO</label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="radio" id="radio1" value="1"
                                        <?= $chek ?>>
                                    <label class="form-check-label" for="creacion">
                                        FECHA DE CREACIÓN
                                    </label><br>
                                    <input class="form-check-input" type="radio" name="radio" id="radio2" value="2"
                                        <?= $chek2 ?>>
                                    <label class="form-check-label" for="cierre">
                                        FECHA DE CIERRE PREVISTO
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-dark">Filtrar Busqueda</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.holas {
    display: none;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body cardStyleLeads">
                <button class="btn  mb-2 btns btn-secondary collapsed" style="display: none;"
                    id="extract-button">Cotacto Masivo</button>
                <h4 class="card-title"><strong>Lista de leads</strong></h4>
                <table id="datatable-buttons"
                    class="table table-striped table-light dt-responsive nowrap w-100 display">
                    <thead>
                        <tr>
                            <th style="text-align: right;">LEADS</th>
                            <th style="text-align: right;">#OPORTUNIDAD</th>
                            <th hidden style="text-align: right;">#EXPEDIENTE</th>
                            <th hidden style="text-align: right;">MOTIVO CONDICION</th>
                            <th hidden style="text-align: right;">PROBABILIDAD</th>
                            <th style="text-align: right;">CIERRE PREV</th>
                            <th style="text-align: right;">EXPEDIENTE</th>
                            <th style="text-align: right;">MET PAGO</th>
                            <th style="text-align: right;">PREC DE LIS</th>
                            <th style="text-align: right;">PREC VENTA MÍN</th>
                            <th style="text-align: right;">ESTADO</th>
                            <th style="text-align: right;">CREADO</th>

                            
                            <th style="text-align: right;">CAPAÑAS</th>
                            <th style="text-align: right;">PROYECTO</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
                <div id="tableleads">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-12"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                        <span class="placeholder col-12"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="/js/oportuindades/EnlistarOportuindad.js?rev=<?= time(); ?>"></script>