
<?php
require_once "controllers/admin/admins_passRecover_controller.php";
require "./app/systemConfig.php";

/*=============================================
Capturar las rutas de la URL
=============================================*/
$routesArray = explode("/", $_SERVER['REQUEST_URI']);
$routesArray = array_filter($routesArray);
/*=============================================
Limpiar la Url de variables GET
=============================================*/
foreach ($routesArray as $key => $value) {
    $value = explode("?", $value)[0];
    $routesArray[$key] = $value;
}

?>



<div class="account-pages my-5 pt-sm-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden" style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div class="bg-dark bg-soft" style="background-color: rgb(0 0 0)!important;" >
                        <div class="row">
                            <div class="col-7">
                                <div class=" p-4" style=" color:#ffffff">
                                    <h5 class="">¡Recuperación de contraseña!</h5>
                                    <p> Ingresa tu correo para continuar</p>
                                </div>
                            </div>
                             <div class="col-5 align-self-end">
                                <img src="/assets/login.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                   <div class="card-body pt-0">
                        <div class="auth-logo">
                            <a href="" class="auth-logo-light">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-light">
                                        <img src="/assets/rocca2.jfif" alt="" class="rounded-circle" height="34">
                                    </span>
                                </div>
                            </a>
                            <a href="" class="auth-logo-dark">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-light">
                                        <img src="/assets/rocca2.jfif" alt="" class="rounded-circle" height="70">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="p-2">
                            <form method="POST" class="form-horizontal needs-validation" novalidate>
                                <div class="mb-3">
                                    <label for="username" class="form-label">Correo Corporativo</label>
                                    <input onchange="validateJS(event, 'email')" type="email" id="loginEmail"
                                        class="form-control" name="loginEmail" placeholder="Correo Corporativo"
                                        required>
                                    <div class="valid-feedback">Valido.</div>
                                    <div class="invalid-feedback">Por favor rellene este campo.</div>
                                </div>
                                <?php
                                    /*=============================================
                                    Metodo Login de administradores
                                    =============================================*/
                                    $login = new AdminsController();
                                    if (isset($_POST["loginEmail"])) {
                                        $loginEmail=$_POST["loginEmail"];
                                        $login->passRecover($loginEmail);
                                    }
                                    /*============================================*/
                                    ?>
                                <div class="row">
                                    <div class="mt-3 d-grid">
                                        <button class="btn btn-dark waves-effect waves-light" type="submit">Recuperar Acceso</button>
                                    </div>
                                    <div class="mt-4 text-center">
                                        <h5 hidden class="font-size-14 mb-3"><?php echo $company_name;?></h5>
                                    </div>
                                    <div class="mt-4 text-center">
                                        <a href="/" class="text-muted"><i class="mdi mdi-lock me-1"></i>¿Iniciar sesión?</a>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
</div>
<script src="webroot/formulario/formulario.js?ver=<?= time();?>"></script>
<script src="webroot/libs/jquery/jquery.min.js"></script>

<script>
/*=============================================
Función para recordar credenciales de ingreso
=============================================*/
function rememberMe(event) {
    if (event.target.checked) {
        localStorage.setItem("emailRemember", $('[name="loginEmail"]').val());
        localStorage.setItem("checkRemember", true);
    } else {
        localStorage.removeItem("emailRemember");
        localStorage.removeItem("checkRemember");
    }
}
/*=============================================
Capturar el email para login desde el LocalStorage
=============================================*/
$(document).ready(function() {
    if (localStorage.getItem("emailRemember") != null) {
        $('[name="loginEmail"]').val(localStorage.getItem("emailRemember"));
    }
    if (localStorage.getItem("checkRemember") != null && localStorage.getItem("checkRemember")) {
        $('#remember').attr("checked", true);
    }
})
</script>