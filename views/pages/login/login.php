<div class="account-pages my-5 pt-sm-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden" style="border-radius: 13px; background: #fcfcfc;box-shadow:  5px 5px 100px #656565,-5px -5px 100px #ffffff;">
                    <div class="bg-dark bg-soft" style="background-color: rgb(0 0 0)!important;">
                        <div class="row">
                            <div class="col-7">
                                <div class=" p-4" style=" color:#ffffff">
                                    <h5 class="" style=" color:#ffffff">¡Bienvenid@ de nuevo al sistema CRM de ventas new!
                                    </h5>
                                    <p>Inicia sesión para continuar</p>
                                </div>
                            </div>
                            <div class="col-5 align-self-end">
                                <img src="/assets/login.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="auth-logo">
                            <a href="" class="auth-logo-light">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-light">
                                        <img src="/assets/rocca2.jfif" alt="" class="rounded-circle" height="34">
                                    </span>
                                </div>
                            </a>
                            <a href="" class="auth-logo-dark">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-light">
                                        <img src="/assets/rocca2.jfif" alt="" class="rounded-circle" height="70">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="alert alert-danger" role="alert" id="exitLogin" style="display: none;"></div>
                        <div class="p-2">
                                <div class="mb-3">
                                    <label for="username" class="form-label">Correo Corporativo</label>
                                    <input onchange="validateJS(event, 'email')" type="email" id="loginEmail" class="form-control" name="loginEmail" placeholder="Correo Corporativo" required>
                                    <div class="valid-feedback">Valido.</div>
                                    <div class="invalid-feedback">Por favor rellene este campo.</div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Contraseña</label>
                                    <div class="input-group auth-pass-inputgroup">
                                        <input id="password" type="password" class="form-control" name="loginPassword" placeholder="Contraseña" required="required">
                                        <div class="valid-feedback">Valido.</div>
                                        <div class="invalid-feedback">Por favor rellene este campo.</div>
                                    </div>
                                </div>
                                 <div class="alert alert-danger" role="alert" id="errorLogin" style="display: none;"></div>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="remember" onchange="rememberMe(event)">
                                            <label for="remember">
                                                Recordar Acceso
                                            </label>
                                        </div>
                                    </div>
                                    <div class="mt-3 d-grid">
                                        <button class="btn btn-dark waves-effect waves-light" onclick="Login()" >Iniciar
                                            sesión</button>
                                    </div>
                                    <div class="mt-4 text-center">
                                        <a href="/auth-recover" class="text-muted"><i class="mdi mdi-lock me-1"></i>¿Olvidaste tu contraseña?</a>
                                    </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <p>© <script>
                                document.write(new Date().getFullYear())
                            </script> <i class="mdi mdi-alpha-c-circle text-dark"></i> Sistema
                            CRM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
 function addScriptWithTimestamp(src) {
        var script = document.createElement('script');
        script.src = src + '?ver=' + Date.now();
        document.body.appendChild(script);
    }


    addScriptWithTimestamp('assets/formulario/formulario.js');
    addScriptWithTimestamp('/js/login/login.js');
</script>
<script src="assets/libs/jquery/jquery.min.js"></script>
<script>
/*===========================================================
Function to remember login credentials
===========================================================*/
    function rememberMe(event) {
        if (event.target.checked) {
            // Store the email and check state in LocalStorage
            localStorage.setItem("emailRemember", $('[name="loginEmail"]').val());
            localStorage.setItem("checkRemember", true);
        } else {
            // Remove email and check state from LocalStorage
            localStorage.removeItem("emailRemember");
            localStorage.removeItem("checkRemember");
        }
    }

    /*===========================================================
    Retrieve the login email from LocalStorage
    ===========================================================*/
    $(document).ready(function() {
        if (localStorage.getItem("emailRemember") != null) {
            // Set the login email input field from LocalStorage
            $('[name="loginEmail"]').val(localStorage.getItem("emailRemember"));
        }

        if (localStorage.getItem("checkRemember") != null && localStorage.getItem("checkRemember")) {
            // Set the "Remember Me" checkbox to checked if needed
            $('#remember').attr("checked", true);
        }
    })
</script>