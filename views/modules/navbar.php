            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box" style="background:#000;">
                            <a href="/" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="assets/images/logo.svg" alt="" height="22">
                                </span>
                                <span class="logo-lg">
                                    <img src="assets/images/logo-dark.png" alt="" height="17">
                                </span>
                            </a>
                            <a href="/" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="assets/images/logo-light.svg" alt="" height="22">
                                </span>
                                <span class="logo-lg">
                                    <img src="/assets/rocca - copia.jpg " alt="" height="50">
                                </span>
                            </a>
                        </div>
                        <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect"
                            id="vertical-menu-btn">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>
                    </div>
                    <div class="d-flex">
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user"
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjE0s9y3MGRatfrJMeR3Vqb90BvW9bA0uj5Z2DD3K7i7ngyWkl-MC7PhLNE_FtFBAzXu0&usqp=CAUg"
                                    alt="Header Avatar">
                                <span class="d-none d-xl-inline-block ms-1" key="t-henry" id="nombreVendedor"></span>
                                <script src="/js/NavvarSidebar/NavvarSidebar.js?rev=<?= time(); ?>"></script>
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item text-dark" href="/leads/crearLead?data=1"><i
                                        class="bx bx-news font-size-16 align-middle me-1 text-dark"></i> <span
                                        key="t-logout">Crear Lead</span></a>
                                     <a class="dropdown-item text-dark" href="/leads/ConsultarLead?data=1"><i
                                        class="bx bx-news font-size-16 align-middle me-1 text-dark"></i> <span
                                        key="t-logout">Consultar Lead</span></a>
                                <a class="dropdown-item text-danger" href="/logout"><i
                                        class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i> <span
                                        key="t-logout">Salir</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>