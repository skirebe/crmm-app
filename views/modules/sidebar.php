<div class="vertical-menu" style="background: #000000;">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>
                <li>
                    <a href="/" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-inicio">Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="/buscador" class="waves-effect" data-bs-toggle='tooltip' data-bs-placement='right'
                        title='Buscar cualquier tipo de registro'>
                        <i class="bx bx-directions"></i>
                        <span key="t-leads">Buscador General</span>
                    </a>
                </li>
                <li class="menu-title" key="t-apps">Apps</li>
                <li>
                    <a href="leads/lista?data=1" class="waves-effect" data-bs-toggle='tooltip' data-bs-placement='right'
                        title='Esta vista te permite ver los lead, crear y consultar'>
                        <i class="bx bxs-contact"></i>
                        <span key="t-leads">Leads</span>
                    </a>
                </li>
                <li>
                    <a href="/calendario" class="waves-effect" data-bs-toggle='tooltip' data-bs-placement='right'
                        title='Esta vista te permite ver los lead, crear y consultar'>
                        <i class="bx bx-calendar"></i>
                        <span key="t-leads">Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="evento/lista?data=1" class="waves-effect" data-bs-toggle='tooltip'
                        data-bs-placement='right' title='Mi Lista de eventos'>
                        <i class="bx bx-list-ol"></i>
                        <span key="t-evento">Eventos para hoy</span>
                    </a>
                </li>
                <li>
                    <a href="oportunidad/list?data=1&data2=0" class="waves-effect">
                        <i class="bx bx-user-voice"></i>
                        <span key="t-oportuindad">Oportunidades</span>
                    </a>
                </li>
                <li>
                    <a href="expedientes/lista" class="waves-effect">
                        <i class="bx bx-file-find"></i>
                        <span key="t-oportuindad">Expediente</span>
                    </a>
                </li>
                <li>
                    <a href="orden/lista?data=1" class="waves-effect">
                        <i class="bx bx-file-find"></i>
                        <span key="t-coti">Cotizaciones</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>