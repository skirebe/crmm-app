<?php
/*===========================================================================
Código PHP Funcionalidades
===========================================================================*/

// Incluir el archivo de configuración del sistema
include "app/systemConfig.php";

// Iniciar la sesión
session_start();

/*===========================================================================
Capturar las rutas de la URL
===========================================================================*/

// Dividir la URL en segmentos y eliminar segmentos vacíos
$routesArray = explode("/", $_SERVER['REQUEST_URI']);
$routesArray = array_filter($routesArray);

/*===========================================================================
Limpiar la URL de variables GET
===========================================================================*/

foreach ($routesArray as $key => $value) {
    // Eliminar cualquier parte de la URL que contenga variables GET
    $value = explode("?", $value)[0];
    $routesArray[$key] = $value;
}

/*===========================================================================
Fin del Código PHP Funcionalidades
===========================================================================*/
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title><?= $titulo ?></title>
    <base href="<?= TemplateController::path() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="CRM ROCCA" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="/assets/rocca2.jfif">
    <link href="assets/libs/select2/css/select2.min.css?rev=<?= time(); ?>" rel="stylesheet" type="text/css" />
    <link href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css?rev=<?= time(); ?>" rel="stylesheet"
        type="text/css">
    <link href="assets/libs/spectrum-colorpicker2/spectrum.min.css?rev=<?= time(); ?>" rel="stylesheet" type="text/css">
    <link href="assets/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css?rev=<?= time(); ?>" rel="stylesheet"
        type="text/css">
    <link href="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css?rev=<?= time(); ?>" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" href="assets/libs/@chenfengyuan/datepicker/datepicker.min.css?rev=<?= time(); ?>">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css?rev=<?= time(); ?>" id="bootstrap-style" rel="stylesheet"
        type="text/css" />
    <!-- Icons Css -->
    <link href="assets/css/icons.min.css?rev=<?= time(); ?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="assets/css/app.min.css?rev=<?= time(); ?>" id="app-style" rel="stylesheet" type="text/css" />
    <link href="assets/css/card.css?rev=<?= time(); ?>" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css?rev=<?= time(); ?>" rel="stylesheet"
        type="text/css" />
    <link href="assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css?rev=<?= time(); ?>"
        rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet"
        href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.dataTables.min.css?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css?rev=<?= time(); ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/searchpanes/2.1.0/js/dataTables.searchPanes.min.js?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/searchpanes/2.1.0/js/searchPanes.bootstrap5.min.js?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.bootstrap5.min.css?rev=<?= time(); ?>">
    <link rel="stylesheet"
        href="https://cdn.datatables.net/select/1.5.0/css/select.bootstrap4.min.css?rev=<?= time(); ?>">
    <script src="/js/modalAcciones/modalAcciones.js?rev=<?= time(); ?>"></script>
    <script src="/assets/sweetalert2/sweetalert2.js?rev=<?= time(); ?>"></script>
    <script src="/js/AreadeTrabajo/AreadeTrabajo.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/CrearBitacoraCliente.js?rev=<?= time(); ?>"></script>
    <script src="/js/leads/ActulizarCliente.js?rev=<?= time(); ?>"></script>
</head>

<body data-sidebar="dark" data-layout-mode="light">
    <div id="layout-wrapper">
        <?php
        if (!isset($_SESSION["admin"])) {
            $currentPage = isset($routesArray[1]) ? $routesArray[1] : "login";
            if ($currentPage != "login" && $currentPage != "auth-recover") {
                $currentPage = "login";
            }
            include "views/pages/login/$currentPage.php";
            echo '</head></body>';
            return;
        }

        include "views/modules/sidebar.php";
        include "views/modules/navbar.php";
        ?>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="/js/validarSession/validarSession.js?rev=<?= time(); ?>"></script>
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <?php
                    if (isset($_SESSION["admin"])) :
                        if (!empty($routesArray[1])) {
                            if (
                                $routesArray[1] == "home" ||
                                $routesArray[1] == "logout" ||
                                $routesArray[1] == "leads"||
                                $routesArray[1] == "evento"||
                                $routesArray[1] == "calendario"||
                                $routesArray[1] == "oportunidad"||
                                $routesArray[1] == "estimaciones"||
                                $routesArray[1] == "buscador" ||
                                $routesArray[1] == "orden" ||
                                $routesArray[1] == "expedientes"
                            ) {

                                include "views/pages/" . $routesArray[1] . "/" . $routesArray[1] . ".php";
                                 include "./views/pages/leads/actions/ModalBotones/ModalBotones.php";
                            } else {
                                include "views/pages/404/404.php";
                            }
                        } else {
                            include "views/pages/home/home.php";
                        }
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/libs/jquery/jquery.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/bootstrap/js/bootstrap.bundle.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/metismenu/metisMenu.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/simplebar/simplebar.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/node-waves/waves.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/select2/js/select2.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/spectrum-colorpicker2/spectrum.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/@chenfengyuan/datepicker/datepicker.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/js/pages/form-advanced.init.js?rev=<?= time(); ?>"></script>
    <script src="assets/js/pages/sweet-alerts.init.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js?rev=<?= time(); ?>"></script>
    <script src="https://cdn.datatables.net/searchpanes/2.2.0/js/dataTables.searchPanes.min.js?rev=<?= time(); ?>">
    </script>
    <script src="https://cdn.datatables.net/select/1.7.0/js/dataTables.select.min.js?rev=<?= time(); ?>"></script>
    <script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js?rev=<?= time(); ?>"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js?rev=<?= time(); ?>"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js?rev=<?= time(); ?>">
    </script>
    <script src="assets/libs/datatables.net-buttons/js/buttons.html5.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/datatables.net-buttons/js/buttons.print.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/datatables.net-buttons/js/buttons.colVis.min.js?rev=<?= time(); ?>"></script>
    <script src="assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js?rev=<?= time(); ?>">
    </script>
    <script src="assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js?rev=<?= time(); ?>">
    </script>
    <script src="assets/js/app.js?rev=<?= time(); ?>"></script>
</body>

</html>